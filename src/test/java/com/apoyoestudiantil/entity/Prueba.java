/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.dao.AuxiliarMatriculaDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.excepciones.AppException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author Leonardo Pérez
 */
public class Prueba {
    
    public Prueba() {
    }
    
    
    
     @Test
     public void hello() {
         Documento doc = new Documento();
         doc.setNumero(new Long(111111));
         doc.setPaisEmisor("URUGUAY");
         doc.setTipo(Documento.TipoDocumento.CEDULA);
         
         EntityManager mng = ContextoDB.getInstancia().getManager();
         AlumnoDAO daoAlumn =new AlumnoDAO(mng);
         AuxiliarMatriculaDAO auxDao = new AuxiliarMatriculaDAO(mng);
         
         Alumno a;
        try {
            a = daoAlumn.findAlumnoByNumeroDoc(new Long(111111));
            AuxiliarMatricula aux = auxDao.findByAlumno(a);
         
         assertTrue(aux.getAlumno().equals(a));
        } catch (AppException ex) {
            Logger.getLogger(Prueba.class.getName()).log(Level.SEVERE, null, ex);
        }
         
     }
}
