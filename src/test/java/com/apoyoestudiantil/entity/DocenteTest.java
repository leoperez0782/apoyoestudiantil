/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

import com.apoyoestudiantil.dao.DocenteDAO;
import com.apoyoestudiantil.dao.InstitucionDAO;
import java.util.Calendar;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author leo
 */
public class DocenteTest {

    public DocenteTest() {
    }

    private static int numeroGenerado = (int) (Math.random() * 10000) + 1;

    ;

    /**
     * Test of getGrado method, of class Docente.
     */
//    @Test
//    public void testGetGrado() {
//        System.out.println("getGrado");
//        Docente instance = new Docente();
//        int expResult = 0;
//        int result = instance.getGrado();
//        assertEquals(expResult, result);
//        fail("The test case is a prototype.");
//    }
//
//    /**
//     * Test of setGrado method, of class Docente.
//     */
//    @Test
//    public void testSetGrado() {
//        System.out.println("setGrado");
//        int grado = 0;
//        Docente instance = new Docente();
//        instance.setGrado(grado);
//        fail("The test case is a prototype.");
//    }

//    @Test
//    public void guardarDocente() {
//        //Esto guarda un docente sin usuario, y sin institucion asignada
//        DocenteDAO dao = new DocenteDAO();
//        Docente d = new Docente();
//        d.setApellidos("apellidosTest docente");
//        d.setCedula(String.valueOf(numeroGenerado));
//        d.setFechaNacimiento(Calendar.getInstance());
//        d.setGrado(1);
//        d.setNombres("nombre docenteTest" + numeroGenerado);
//        d.setNumeroEmpleado(numeroGenerado);
//        d.setUsuario(null);
//        boolean guardo = dao.save(d);
//        assertTrue(guardo);
//    }
//
//    @Test
//    public void recuperarDocentePorCedula() {
//        DocenteDAO dao = new DocenteDAO();
//        Docente d = dao.findByCedula(String.valueOf(numeroGenerado));
//        String expected = "nombre docenteTest" + numeroGenerado;
//        String actual = d.getNombres();
//        assertEquals(expected, actual);
//    }
//
//    @Test
//    public void recuperarTodosLosDocentes() {
//        //fail("prototipo");
//        DocenteDAO dao = new DocenteDAO();
//        List<Docente> lista = dao.getAll();
//        int expected = dao.getCount();
//
//        assertTrue(expected == lista.size());
//
//    }

//    @Test
//    public void borrarDocente() {//no pasa, hay que borrarlo desde el colegio
//        DocenteDAO dao = new DocenteDAO();
//        Docente d = dao.findByCedula(String.valueOf("4855"));
//
//        boolean elimino = dao.delete(d);
//        assertTrue(elimino);
//    }
//    @Test
//    public void actualizarDocente() {
//        DocenteDAO dao = new DocenteDAO();
//        //Docente d = dao.findByCedula(String.valueOf(numeroGenerado));
//        Docente d = dao.findByCedula("3257");//Docente guardado con institucion
//        d.setNombres("Cambiado desde updateTest");
//        boolean actualizo = dao.update(d);
//        assertTrue(actualizo);
//    }
    
//    @Test
//    public void actualizarDocenteConUsuarioYRoldeInstitucion() {
//        DocenteDAO dao = new DocenteDAO();
//        InstitucionDAO daoCole = new InstitucionDAO();
//        //cargo la institucion
//        Institucion cole = daoCole.findByRut("3932");
//        //Docente d = dao.findByCedula(String.valueOf(numeroGenerado));
//        
//        //cargo el docente
//        Docente d = dao.findByCedula("9611");//Docente guardado sin institucion y sin usuario
//        Usuario usu = new Usuario();//creo usuario nuevo
//        usu.setClave("123");
//        usu.setNombre(d.getCedula());
//        usu.addRol(cole.getRol(0));//le asigno un rol del colegio
//        d.setUsuario(usu);//seteo el usuario
//        cole.addEmpleado(d);//agrego el docente a la lista de empleados
//        cole.addUsuario(usu);//agrego el usuario a la lista de usuarios
//        
//        //boolean actualizo = dao.update(d);
//        boolean actualizo = daoCole.update(cole);
//        assertTrue(actualizo);
//    }
//    

}
