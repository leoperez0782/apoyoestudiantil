/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.LibretaDAO;
import com.apoyoestudiantil.excepciones.AppException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author Leonardo Pérez
 */
public class LibretasPorAlumno {
    
    public LibretasPorAlumno() {
    }
    
   
     @Test
     public void hello() {
         EntityManager mng = ContextoDB.getInstancia().getManager();
         LibretaDAO libDAo = new LibretaDAO(mng);
         AlumnoDAO aluDao = new AlumnoDAO(mng);
         
         Alumno a;
        try {
            a = aluDao.findAlumnoByNumeroDoc(new Long(90121212));
            List<Libreta> lista = libDAo.getAllByAlumno(a);
         
            assertTrue(lista.size() == 2);
        } catch (AppException ex) {
            Logger.getLogger(LibretasPorAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
         
         
     }
}
