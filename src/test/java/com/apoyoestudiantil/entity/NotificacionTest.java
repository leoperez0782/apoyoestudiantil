/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

import com.apoyoestudiantil.dao.AdultoResponsableDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.NotificacionDAO;
import java.util.List;
import javax.persistence.EntityManager;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Leonardo Pérez
 */
public class NotificacionTest {
    
   
     @Test
     public void hello() {
         EntityManager mng = ContextoDB.getInstancia().getManager();
         NotificacionDAO daoNotif = new NotificacionDAO(mng);
         AdultoResponsableDAO daoAdult = new AdultoResponsableDAO(mng);
         Documento doc = new Documento();
         doc.setPaisEmisor("Uruguay");
         doc.setTipo(Documento.TipoDocumento.CEDULA);
         doc.setNumero(new Long(22265588));
         
         AdultoResponsable adu = daoAdult.findByDocumento(doc);
         
         List<Notificacion> lista = daoNotif.getAllByDestinatrio(adu);
         
         Assert.assertTrue(lista.size() == 8);
     }
}
