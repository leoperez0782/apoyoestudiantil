/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import com.apoyoestudiantil.dao.DomicilioDAO;
import com.apoyoestudiantil.dao.RolDAO;
import com.apoyoestudiantil.dao.UsuarioDAO;
import com.apoyoestudiantil.entity.Administrativo;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Rol;
import java.util.Calendar;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author leo
 */
public class UsuarioDAOTest {

    public UsuarioDAOTest() {
    }

//    @Test
//    public void guardarUnUSuario(){
//        Usuario usu = new Usuario();
//        usu.setNombre("prueba");
//        usu.setClave("claveprueba");
//        Rol rol = new Rol();
//        rol.setRol("Admin");
//        
//        usu.addRol(rol);
//        
//        UsuarioDAO dao = new UsuarioDAO();
//        
//        assertTrue(dao.save(usu));
//    }
    
    @Test
    public void agregarRol(){
        RolDAO daoRol = new RolDAO();
        Rol r = new Rol();
        r.setRol("ADMINISTRADOR");
        
        boolean agrego = daoRol.save(r);
        
        assertTrue(agrego);
    }
//    @Test
//    public void agregarAdministrativo(){
//        //para garegar administrativo primero hay que agregar un rol y luego buscarlo para asignarlo
//        //los dos daos deben compartir en entityMAnager, si no las entidades estan en contextos distintos y no se guardan todas.
//        Administrativo a = new Administrativo();
//        UsuarioDAO dao = new UsuarioDAO();
//        RolDAO daoRol = new RolDAO();
//        DomicilioDAO daoDomi = new DomicilioDAO(dao.getManager());
//        daoRol.setManager(dao.getManager());
//        Documento d = new Documento();
//        d.setNumero(new Long(12345789));
//        d.setPaisEmisor("Uruguay");
//        d.setTipo(Documento.TipoDocumento.CEDULA);
//        
//        a.setDocumento(d);
//        a.setFechaNacimiento(Calendar.getInstance());
//        a.setPrimerApellido("Apellido1");
//        a.setPrimerNombre("nombre1");
//        a.setSegundoApellido("2apellido");
//        a.setSegundoNombre("2nombre");
//        a.setEmail("mailPrueba1234567");
//        Rol r = daoRol.findById(new Long(1));//el rol ya estaba cargado
//        r.setRol("ADMINISTRADOR");
//        a.addRol(r);
//        
//        //agregar direccion
//        Domicilio domi = new Domicilio();
//        domi.setApartamento("1");
//        domi.setBarrio("Las vegas");
//        domi.setBlock("0");
//        domi.setCalle("calle 1");
//        domi.setDepartamento(Domicilio.Departamento.CANELONES);
//        domi.setEsquina("calle 5");
//        domi.setIsEdificio(false);
//        domi.setLocalidad("Las vegas");
//        domi.setManzanaSolar("M9 S25");
//        domi.setNumero("0");
//        domi.setTorre("no");
//        
//        domi.addUsuario(a);//esto ya le asigna el domicilio al usuario.
//        //boolean agrego = dao.save(a);
//       // boolean agrego = daoRol.save(r);
//       boolean agrego = daoDomi.save(domi);//si lo guardo con direccion hay que guardarlo desde la direccion y ya guarda el usuario tambien.
//        assertTrue(agrego);
//    }
    
//    @Test
//    public void getUsuarioByDocumento(){
//         Administrativo a ;
//        UsuarioDAO dao = new UsuarioDAO();
//        
//        Documento d = new Documento();
//        d.setNumero(new Long(102357890));
//        d.setPaisEmisor("Uruguay");
//        d.setTipo(Documento.TipoDocumento.CEDULA);
//        
//        a =(Administrativo) dao.findByDocumento(d);
//        
//        assertTrue(a.getEmail().equals("mailPrueba12345"));
//    }
//    @Test
//    public void deleteUsuario() {
//        Administrativo a;
//        UsuarioDAO dao = new UsuarioDAO();
//
//        Documento d = new Documento();
//        d.setNumero(new Long(12345789));
//        d.setPaisEmisor("Uruguay");
//        d.setTipo(Documento.TipoDocumento.CEDULA);
//
//        a = (Administrativo) dao.findByDocumento(d);//si quiero borrar el domicilio, tendria que borrar desde domicilio.
//       // a.removeRoles();
//        boolean elimino = dao.delete(a);
//
//        assertTrue(elimino);
//    }
    
//    @Test
//    public void agregaradministrativoConRolYaCargado(){
//         Administrativo a = new Administrativo();
//        UsuarioDAO dao = new UsuarioDAO();
//       RolDAO daoRol = new RolDAO();
//       
//       daoRol.setManager(dao.getManager());//tienen que compartir entitymanager o una de las entidades queda como detached
//        Documento d = new Documento();
//        d.setNumero(new Long(1110235789));
//        d.setPaisEmisor("Uruguay");
//        d.setTipo(Documento.TipoDocumento.CEDULA);
//        
//        a.setDocumento(d);
//        a.setFechaNacimiento(Calendar.getInstance());
//        a.setPrimerApellido("Apellido1");
//        a.setPrimerNombre("nombre1");
//        a.setSegundoApellido("2apellido");
//        a.setSegundoNombre("2nombre");
//        a.setEmail("mailPrueba1");
//        Rol r = daoRol.findById(new Long(1));
//        
//        a.addRol(r);
//        
//        boolean agrego = dao.save(a);
//        
//        assertTrue(agrego);
////    }
}
