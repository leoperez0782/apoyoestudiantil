/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import com.apoyoestudiantil.dao.DomicilioDAO;
import com.apoyoestudiantil.entity.Domicilio;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author leo
 */
public class DomicilioDAOTest {
    
    public DomicilioDAOTest() {
    }
    
    @Test
    public void guardarDomicilio(){
        Domicilio domi = new Domicilio();
        domi.setApartamento("0");
        domi.setBarrio("Barrio");
        domi.setBlock("block");
        domi.setCalle("calle 13");
        domi.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi.setEsquina("calle 1");
        domi.setEdificio("");
        domi.setLocalidad("Durazno");
        domi.setManzanaSolar("N/A");
        domi.setNumero("1524");
        domi.setTorre("no");
        
        DomicilioDAO dao = new DomicilioDAO();
        
        boolean agrego = dao.save(domi);
        
        assertTrue(agrego);
    }
}
