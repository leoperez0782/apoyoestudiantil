/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package daos;

import com.apoyoestudiantil.dao.DocenteDAO;
import com.apoyoestudiantil.dao.InstitucionDAO;
import com.apoyoestudiantil.dao.UsuarioDAO;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.entity.Rol;
import com.apoyoestudiantil.entity.Usuario;
import java.util.Calendar;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author leo
 */
public class InstitucionDAOTest {

    private static long numeroGenerado = (long) (Math.random() * 10000) + 1;

    ;

    public InstitucionDAOTest() {
    }

//    @Test
//    public void cargarInstitucion() {
//
//        Institucion colegio = new Institucion();
//        colegio.setNombre("ColegioTest" + numeroGenerado);
//        colegio.setRut(String.valueOf(numeroGenerado));
//
//        InstitucionDAO dao = new InstitucionDAO();
//
//        assertTrue(dao.save(colegio));
//    }
//
//    @Test
//    public void traerInstitucionPorRut() {
//        String rut = "3932";//String.valueOf(numeroGenerado);
//
//        InstitucionDAO dao = new InstitucionDAO();
//        Institucion colegio = dao.findByRut(rut);
//
//        String exp = "ColegioTest3932";//+ numeroGenerado;//"Otra Prueba mas";//numeroGenerado;
//        String act = colegio.getNombre();
//
//        assertEquals(exp, act);
//    }
//
//    @Test
//    public void guardarAlumnoEnInstitucion() {
//        //Rol rol = new Rol();
//        Alumno a = new Alumno();
//        a.setApellidos("testApellidos2");
//        a.setCedula(String.valueOf(numeroGenerado));
//        a.setFechaNacimiento(Calendar.getInstance());
//        a.setNombres("testNombreunMillon");
//        a.setFaltas(1);
//        a.setUsuario(null);
//
//        String rut = "3932";//String.valueOf(numeroGenerado);
//
//        InstitucionDAO dao = new InstitucionDAO();
//        Institucion colegio = dao.findByRut(rut);
//
//        colegio.addAlumno(a);
//        boolean guardo = dao.update(colegio);
//        assertTrue(guardo);
//
//    }
//
////    @Test
////    public void realizarUpdateColegio(){
////        String rut = String.valueOf(numeroGenerado);
////
////        InstitucionDAO dao = new InstitucionDAO();
////        Institucion colegio = dao.findByRut(rut);
////        
////        colegio.setNombre("Otra Prueba mas");
////        boolean modifico = dao.update(colegio);
////        assertTrue(modifico);
////    }
////    
////    @Test
////    public void guardarDocenteEnColegio() {
////        String rut = String.valueOf(numeroGenerado);
////
////        InstitucionDAO dao = new InstitucionDAO();
////        Institucion colegio = dao.findByRut(rut);
////
////        Docente d = new Docente();
////        d.setApellidos("testInst");
////        d.setCedula(String.valueOf((long) (Math.random() * 10000) + 1));
////        d.setFechaNacimiento(Calendar.getInstance());
////        d.setGrado(5);
////        d.setNombres("Tito");
////        d.setNumeroEmpleado((int) numeroGenerado);
////
////        colegio.addEmpleado(d);
////        boolean modifico = dao.update(colegio);
////        assertTrue(modifico);
////
////    }
//
//    @Test
//    public void guardarDocenteConUsuario() {
//        String rut ="3932"; //String.valueOf(numeroGenerado);
//
//        InstitucionDAO dao = new InstitucionDAO();
//        Institucion colegio = dao.findByRut(rut);
//
//        Docente d = new Docente();
//        d.setApellidos("testInst");
//        d.setCedula(String.valueOf((long) (Math.random() * 10000) + 1));
//        d.setFechaNacimiento(Calendar.getInstance());
//        d.setGrado(5);
//        d.setNombres("Tito");
//        d.setNumeroEmpleado((int) numeroGenerado);
//        Usuario usu = new Usuario();
//        Rol r = new Rol();//esto seria en caso de que no hubiera roles asociados a la institucion en bd.
//        r.setRol("Docente");
//        usu.setNombre(d.getCedula());
//        usu.setClave("123");
//        usu.addRol(r);
//        d.setUsuario(usu);
//        colegio.addRol(r);//si no lo agrego no lo guarda en el colegio
//        colegio.addEmpleado(d);
//        colegio.addUsuario(usu);
//        boolean modifico = dao.update(colegio);
//        assertTrue(modifico);
//    }
//    
////    @Test
////    public void borrarDocenteDesdeLaInstitucion(){
////        InstitucionDAO dao = new InstitucionDAO();
////        Institucion colegio = dao.findByRut("7600");
////        
////        DocenteDAO daoDocente = new DocenteDAO();
////        Docente d = daoDocente.findByCedula(String.valueOf("4855"));
////        
////        colegio.removeEmpleado(d);
////        
////        boolean elimino = dao.update(colegio);
////        assertTrue(elimino);
////    }
}
