/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

import com.apoyoestudiantil.dao.AdministrativoDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.DomicilioDAO;
import com.apoyoestudiantil.dao.RolDAO;
import com.apoyoestudiantil.entity.Administrativo;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Rol;
import java.util.Set;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import static org.junit.Assert.assertEquals;
import org.junit.Test;

/**
 *
 * @author Leonardo Pérez
 */
public class PruebaValidator {

    public PruebaValidator() {
    }

    @Test
    public void validarAdmin() {
        EntityManager manager = ContextoDB.getInstancia().getManager();
        EntityTransaction tx = manager.getTransaction();
        AdministrativoDAO daoAdministrativo = new AdministrativoDAO(manager);
        RolDAO roldao = new RolDAO(manager);
        DomicilioDAO domiDao = new DomicilioDAO(manager);
        //buscamos el rol.
        Rol r = roldao.findByRol("ADMINISTRATIVO");
        //Buscamos  el nivel.
        //creamos primer alumno.
        Administrativo a1 = new Administrativo();
        a1.addRol(r);
        a1.setActivo(true);
        Documento d1 = new Documento();
        d1.setNumero(new Long(1234567));
        d1.setPaisEmisor("URUGUAY");
        d1.setTipo(Documento.TipoDocumento.CEDULA);

        a1.setDocumento(d1);
        a1.setClave("123");
        Domicilio domi = new Domicilio();
        domi.setApartamento("0");
        domi.setBarrio("Barrio");
        domi.setBlock("block");
        domi.setCalle("calle 13");
        domi.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi.setEsquina("calle 1");
        domi.setEdificio("a");
        domi.setLocalidad("Durazno");
        domi.setManzanaSolar("N/A");
        domi.setNumero("1524");
        domi.setTorre("no");
        //iniciamos la transaccion.
        //tx.begin();
        //domi.setId(domiDao.persistWithTransaction(tx, manager, domi));
        //a1.setDomicilio(domi);
        a1.addDomicilio(domi);
        a1.setEmail("admin");
        //a1.setFaltas(0);
        //a1.setFechaNacimiento(Calendar.getInstance());
        a1.setFechaNacimiento("11/02/1980");
        a1.setPrimerApellido("lopez");
        a1.setPrimerNombre("memo");
        a1.setSegundoApellido("segunbdo");
        a1.setSegundoNombre("segunbdo");

        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        Validator validator = factory.getValidator();
        Set<ConstraintViolation<Administrativo>> violations = validator.validate(a1);
        assertEquals(1, violations.size());
//        //agregamos el primer alumno a la transaccion y recuperamos su id.
//        a1.setId(daoAdministrativo.persistWithTransaction(tx, manager, a1));

    }
}
