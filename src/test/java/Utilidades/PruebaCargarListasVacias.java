/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

import com.apoyoestudiantil.entity.Horario;
import com.apoyoestudiantil.entity.Lista;
import com.apoyoestudiantil.excepciones.AppException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author Leonardo Pérez
 */
public class PruebaCargarListasVacias {
    
    public PruebaCargarListasVacias() {
    }
    
    
     @Test
     public void hello() {
        try {
            Horario horario = new Horario();
            horario.setDia(Horario.Dias.LUNES);
            
            Horario horario2 = new Horario();
            horario2.setDia(Horario.Dias.MIERCOLES);
            
            List<Horario> horarios = new ArrayList<>();
            horarios.add(horario);
            horarios.add(horario2);
            
            UtilidadCargaListas util = new UtilidadCargaListas();
            
//            util.setFechaInicio("01-07-2019");
//            util.setFechaFin("14-07-2019");
            
            List<Lista> prueba = util.cargarListasVacias(horarios);
            
            assertTrue(prueba.size() == 4);
        } catch (AppException ex) {
            Logger.getLogger(PruebaCargarListasVacias.class.getName()).log(Level.SEVERE, null, ex);
        }
     }
}
