/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Documento;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author leo
 */
public class TraerUsuarioYCompararClave {
    
    public TraerUsuarioYCompararClave() {
    }
    
    @Test
    public void compararClave(){
        AlumnoDAO dao = new AlumnoDAO();
        
        Documento d1 = new Documento();
        d1.setNumero(new Long(12345));
        d1.setPaisEmisor("URUGUAY");
        d1.setTipo(Documento.TipoDocumento.CEDULA);
        
        Alumno a =(Alumno) dao.findByDocumento(d1);
        UtilSeguridad util = new UtilSeguridad();
                
        assertTrue(util.compararClave("123", a.getClave()));
    }
}
