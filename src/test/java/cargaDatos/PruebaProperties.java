/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author Leonardo Pérez
 */
public class PruebaProperties {

    public PruebaProperties() {
    }

    @Test
    public void pruebaProp() {
        FileInputStream file = null;
        try {
            Properties props = new Properties();
            file = new FileInputStream("src//main//resources//aniolectivo.properties");
            String propiedad = null;
            props.load(file);
            propiedad = props.getProperty("fechaFin");
            assertTrue(propiedad.equals("09/12/2019"));
            
        } catch (FileNotFoundException ex) {
            Logger.getLogger(PruebaProperties.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(PruebaProperties.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                file.close();
            } catch (IOException ex) {
                Logger.getLogger(PruebaProperties.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
}
