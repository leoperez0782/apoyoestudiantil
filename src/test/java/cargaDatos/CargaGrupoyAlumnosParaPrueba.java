/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.dao.AsignaturaDAO;
import com.apoyoestudiantil.dao.AuxiliarMatriculaDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.DocenteDAO;
import com.apoyoestudiantil.dao.GrupoDAO;
import com.apoyoestudiantil.dao.LibretaDAO;
import com.apoyoestudiantil.dao.NivelDAO;
import com.apoyoestudiantil.dao.RolDAO;
import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Asignatura;
import com.apoyoestudiantil.entity.AuxiliarMatricula;
import com.apoyoestudiantil.entity.Clase;
import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Horario;
import com.apoyoestudiantil.entity.ItemAuxiliarMatricula;
import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.entity.ProgramaPrimeroSegundoCBReformulacion2006;
import com.apoyoestudiantil.entity.Rol;
import com.apoyoestudiantil.entity.Telefono;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 * Prueba para cargar la bd con datos para pasaje de lista.
 *
 * @author leo
 */
public class CargaGrupoyAlumnosParaPrueba {

    public CargaGrupoyAlumnosParaPrueba() {
    }

    @Test
    public void poblarDB() {
        EntityManager mng = ContextoDB.getInstancia().getManager();
        EntityTransaction trx = mng.getTransaction();
        DocenteDAO docDao = new DocenteDAO(mng);
        NivelDAO nivDao = new NivelDAO(mng);
        AlumnoDAO alumDao = new AlumnoDAO(mng);
        GrupoDAO daoGrup = new GrupoDAO(mng);
        AsignaturaDAO asigDao = new AsignaturaDAO(mng);
        LibretaDAO libDao = new LibretaDAO(mng);
        RolDAO rolDao = new RolDAO(mng);
        AuxiliarMatriculaDAO auxDao = new AuxiliarMatriculaDAO(mng);
        Clase clase = new Clase();
        clase.setCodigoClase('A');
        clase.setNivel(1);
        clase.setTipoDeClase(Clase.TipoClase.COMUN);

        Nivel niv = nivDao.findByClase(clase);

        Grupo grupo = daoGrup.findByNivelYAnio(niv, 2019);
        //busco rol
        Rol r = rolDao.findByRol("ESTUDIANTE");
        Rol rolTutor = rolDao.findByRol("ADULTO_RESPONSABLE");

        Documento docDocente = new Documento();
        docDocente.setNumero(new Long(Integer.valueOf("3330003")));
        docDocente.setPaisEmisor("URUGUAY");
        docDocente.setTipo(Documento.TipoDocumento.CEDULA);
        Docente docente = docDao.findByDocumento(docDocente);

        trx.begin();
        //creacion alumnos
        Alumno a1 = new Alumno();
        a1.addRol(r);
        Documento d1 = new Documento();
        d1.setNumero(new Long(90121212));
        d1.setPaisEmisor("URUGUAY");
        d1.setTipo(Documento.TipoDocumento.CEDULA);

        a1.setDocumento(d1);
        a1.setActivo(true);
        Domicilio domi = new Domicilio();
        domi.setApartamento("0");
        domi.setBarrio("Barrio");
        domi.setBlock("block");
        domi.setCalle("calle 13");
        domi.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi.setEsquina("calle 1");
        domi.setEdificio("");
        domi.setLocalidad("Durazno");
        domi.setManzanaSolar("N/A");
        domi.setNumero("1524");
        domi.setTorre("no");

        // a1.setDomicilio(domi);
        a1.addDomicilio(domi);
        a1.setEmail("alumTest1@mail");
        //a1.setFaltas(0);
        //a1.setFechaNacimiento(Calendar.getInstance());
        a1.setFechaNacimiento("24/08/2006");
        a1.setMatricula(90121212);
        a1.setPrimerApellido("lopez");
        a1.setPrimerNombre("gonzalo");
        a1.setSegundoApellido("segunbdo");
        a1.setSegundoNombre("segunbdo");
        a1.setClave("123");
        //tutor1
        AdultoResponsable ad1 = new AdultoResponsable();
        ad1.setActivo(true);
        ad1.addDomicilio(domi);
        ad1.addRol(rolTutor);
        ad1.setClave("123");
        Documento docAdult = new Documento();
        docAdult.setNumero(new Long(6565658));
        docAdult.setPaisEmisor("Uruguay");
        docAdult.setTipo(Documento.TipoDocumento.CEDULA);

        ad1.setDocumento(docAdult);

        ad1.setEmail("tutor1email@hotmail.com");
        ad1.setFechaNacimiento("12/03/1978");
        ad1.setPrimerApellido("Lopez");
        ad1.setPrimerNombre("Rodrigo");
        ad1.setSegundoApellido("Fernandez");
        ad1.setSegundoNombre("");

        Telefono telAdul1 = new Telefono();
        telAdul1.setNumero("099123456");
        telAdul1.setObservaciones("siempre ocupado");
        telAdul1.setTipo(Telefono.TipoTelefono.MOVIL);

        ad1.addTelefono(telAdul1);
        a1.addPadre(ad1);

        a1.setId(alumDao.persistWithTransaction(trx, mng, a1));
        assertTrue(a1.getId() != 0);
        //alumno 2

        Alumno a2 = new Alumno();
        a2.addRol(r);
        Documento d2 = new Documento();
        d2.setNumero(new Long(80121212));
        d2.setPaisEmisor("URUGUAY");
        d2.setTipo(Documento.TipoDocumento.CEDULA);

        a2.setDocumento(d2);
        a2.setActivo(true);
        Domicilio domi2 = new Domicilio();
        domi2.setApartamento("");
        domi2.setBarrio("Barrio 2");
        domi2.setBlock("block 2");
        domi2.setCalle("calle 113");
        domi2.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi2.setEsquina("calle 11");
        domi2.setEdificio("");
        domi2.setLocalidad("Durazno");
        domi2.setManzanaSolar("N/A");
        domi2.setNumero("1530");
        domi2.setTorre("no");

        a2.addDomicilio(domi2);
        a2.setEmail("alumTest2@mail");

        a2.setFechaNacimiento("24/07/2006");
        a2.setMatricula(80121212);
        a2.setPrimerApellido("Estevez");
        a2.setPrimerNombre("Carolina");
        a2.setSegundoApellido("Menendez");
        a2.setSegundoNombre("Jimena");
        a2.setClave("123");
        //tutor2
        AdultoResponsable ad2 = new AdultoResponsable();
        ad2.setActivo(true);
        ad2.addDomicilio(domi2);
        ad2.addRol(rolTutor);
        ad2.setClave("123");
        Documento docAdult2 = new Documento();
        docAdult2.setNumero(new Long(57665658));
        docAdult2.setPaisEmisor("Uruguay");
        docAdult2.setTipo(Documento.TipoDocumento.CEDULA);

        ad2.setDocumento(docAdult2);

        ad2.setEmail("tutor2email@hotmail.com");
        ad2.setFechaNacimiento("12/03/1980");
        ad2.setPrimerApellido("Menendez");
        ad2.setPrimerNombre("Natalia");
        ad2.setSegundoApellido("Fernandez");
        ad2.setSegundoNombre("Alejandra");
        ad2.setClave("123");
        Telefono telAdul2 = new Telefono();
        telAdul2.setNumero("098123456");
        telAdul2.setObservaciones("siempre ocupado");
        telAdul2.setTipo(Telefono.TipoTelefono.MOVIL);

        ad2.addTelefono(telAdul2);
        a2.addPadre(ad2);

        a2.setId(alumDao.persistWithTransaction(trx, mng, a2));
        assertTrue(a2.getId() != 0);
        //Alumno 3

        Alumno a3 = new Alumno();
        a3.addRol(r);
        Documento d3 = new Documento();
        d3.setNumero(new Long(70121212));
        d3.setPaisEmisor("URUGUAY");
        d3.setTipo(Documento.TipoDocumento.CEDULA);

        a3.setDocumento(d3);
        a3.setActivo(true);
        Domicilio domi3 = new Domicilio();
        domi3.setApartamento("");
        domi3.setBarrio("Barrio 3");
        domi3.setBlock("block 3");
        domi3.setCalle("calle 11");
        domi3.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi3.setEsquina("calle 13");
        domi3.setEdificio("");
        domi3.setLocalidad("Durazno");
        domi3.setManzanaSolar("N/A");
        domi3.setNumero("1520");
        domi3.setTorre("no");

        a3.addDomicilio(domi3);
        a3.setEmail("alumTest3@mail");

        a3.setFechaNacimiento("03/07/2006");
        a3.setMatricula(70121212);
        a3.setPrimerApellido("Birmino");
        a3.setPrimerNombre("Diana");
        a3.setSegundoApellido("Gutierrez");
        a3.setSegundoNombre("");
        a3.setClave("123");
        //tutor 3
        AdultoResponsable ad3 = new AdultoResponsable();
        ad3.setActivo(true);
        ad3.addDomicilio(domi3);
        ad3.addRol(rolTutor);
        ad3.setClave("123");
        Documento docAdult3 = new Documento();
        docAdult3.setNumero(new Long(33665588));
        docAdult3.setPaisEmisor("Uruguay");
        docAdult3.setTipo(Documento.TipoDocumento.CEDULA);

        ad3.setDocumento(docAdult3);

        ad3.setEmail("tutor3email@hotmail.com");
        ad3.setFechaNacimiento("12/03/1980");
        ad3.setPrimerApellido("Gutierrez");
        ad3.setPrimerNombre("Maria");
        ad3.setSegundoApellido("Fernandez");
        ad3.setSegundoNombre("Ines");
        ad3.setClave("123");
        Telefono telAdul3 = new Telefono();
        telAdul3.setNumero("092123456");
        telAdul3.setObservaciones("siempre ocupado");
        telAdul3.setTipo(Telefono.TipoTelefono.MOVIL);

        ad3.addTelefono(telAdul3);
        a3.addPadre(ad3);

        a3.setId(alumDao.persistWithTransaction(trx, mng, a3));
        assertTrue(a3.getId() != 0);
        //Alumno 4

        Alumno a4 = new Alumno();
        a4.addRol(r);
        Documento d4 = new Documento();
        d4.setNumero(new Long(50121212));
        d4.setPaisEmisor("URUGUAY");
        d4.setTipo(Documento.TipoDocumento.CEDULA);

        a4.setDocumento(d4);
        a4.setActivo(true);
        Domicilio domi4 = new Domicilio();
        domi4.setApartamento("");
        domi4.setBarrio("Barrio 4");
        domi4.setBlock("block 4");
        domi4.setCalle("calle 41");
        domi4.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi4.setEsquina("calle 43");
        domi4.setEdificio("");
        domi4.setLocalidad("Durazno");
        domi4.setManzanaSolar("N/A");
        domi4.setNumero("1524");
        domi4.setTorre("no");

        a4.addDomicilio(domi4);
        a4.setEmail("alumTest4@mail");

        a4.setFechaNacimiento("03/07/2006");
        a4.setMatricula(50121212);
        a4.setPrimerApellido("Jimenzez");
        a4.setPrimerNombre("Joaquin");
        a4.setSegundoApellido("Guti");
        a4.setSegundoNombre("Hernan");
        a4.setClave("123");
        //tutor 4
        AdultoResponsable ad4 = new AdultoResponsable();
        ad4.setActivo(true);
        ad4.addDomicilio(domi4);
        ad4.addRol(rolTutor);
        ad4.setClave("123");
        Documento docAdult4 = new Documento();
        docAdult4.setNumero(new Long(22265588));
        docAdult4.setPaisEmisor("Uruguay");
        docAdult4.setTipo(Documento.TipoDocumento.CEDULA);

        ad4.setDocumento(docAdult4);

        ad4.setEmail("tutor4email@hotmail.com");
        ad4.setFechaNacimiento("12/03/1980");
        ad4.setPrimerApellido("Guti");
        ad4.setPrimerNombre("Maria");
        ad4.setSegundoApellido("Ascona");
        ad4.setSegundoNombre("Jose");
        ad4.setClave("123");
        Telefono telAdul4 = new Telefono();
        telAdul4.setNumero("091123456");
        telAdul4.setObservaciones("siempre ocupado");
        telAdul4.setTipo(Telefono.TipoTelefono.MOVIL);

        ad4.addTelefono(telAdul4);
        a4.addPadre(ad4);

        a4.setId(alumDao.persistWithTransaction(trx, mng, a4));
        assertTrue(a4.getId() != 0);

        //cargo la asignatura
        Asignatura asignatura = new Asignatura();
        asignatura.setNombre(ProgramaPrimeroSegundoCBReformulacion2006.HISTORIA.getNombre());
        asignatura.setHorasSemanales(ProgramaPrimeroSegundoCBReformulacion2006.HISTORIA.getHoras());
        asignatura.setProgramaAnio(ProgramaPrimeroSegundoCBReformulacion2006.HISTORIA.getProgramaAnio());

        asignatura.setId(asigDao.persistWithTransaction(trx, mng, asignatura));

        grupo.addAlumno(a4);
        grupo.addAlumno(a3);
        grupo.addAlumno(a2);
        grupo.addAlumno(a1);

        daoGrup.updateWithTransaction(trx, mng, grupo);

        Libreta lib = new Libreta();
        lib.setDocente(docente);
        lib.setGrupo(grupo);
        lib.setMateria(asignatura);
        lib.setPractico(false);
        lib.setSalon("Salon 1");
        lib.setTeorico(true);
        lib.setTurno(grupo.getTurno());

        Horario horario1 = new Horario();
        horario1.setDia(Horario.Dias.LUNES);
        horario1.setHoraFin("10:00");
        horario1.setHoraInicio("08:00");
        Horario horario2 = new Horario();
        horario2.setDia(Horario.Dias.MIERCOLES);
        horario2.setHoraFin("10:00");
        horario2.setHoraInicio("08:00");

        lib.addHorario(horario1);
        lib.addHorario(horario2);
        lib.setId(libDao.persistWithTransaction(trx, mng, lib));

        //agrego los auxiliares de matricula.
        AuxiliarMatricula aux1 = new AuxiliarMatricula();
        aux1.setAlumno(a1);
        ItemAuxiliarMatricula item1 = new ItemAuxiliarMatricula();

        item1.setNivel(niv);
        aux1.addItem(item1);
        aux1.setId(auxDao.persistWithTransaction(trx, mng, aux1));
        AuxiliarMatricula aux2 = new AuxiliarMatricula();
        aux2.setAlumno(a2);
        ItemAuxiliarMatricula item2 = new ItemAuxiliarMatricula();

        item2.setNivel(niv);
        aux2.addItem(item2);
        aux2.setId(auxDao.persistWithTransaction(trx, mng, aux2));
        AuxiliarMatricula aux3 = new AuxiliarMatricula();
        aux3.setAlumno(a3);
        ItemAuxiliarMatricula item3 = new ItemAuxiliarMatricula();

        item3.setNivel(niv);
        aux3.addItem(item3);
        aux3.setId(auxDao.persistWithTransaction(trx, mng, aux3));
        AuxiliarMatricula aux4 = new AuxiliarMatricula();
        aux4.setAlumno(a4);
        ItemAuxiliarMatricula item4 = new ItemAuxiliarMatricula();

        item4.setNivel(niv);
        aux4.addItem(item4);
        aux4.setId(auxDao.persistWithTransaction(trx, mng, aux4));

        trx.commit();
        assertTrue(lib.getId() != 0);

    }

}
