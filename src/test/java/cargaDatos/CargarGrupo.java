/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.GrupoDAO;
import com.apoyoestudiantil.dao.NivelDAO;
import com.apoyoestudiantil.dao.RolDAO;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Clase;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.entity.Rol;
import java.util.Calendar;
import javax.persistence.EntityManager;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author leo
 */
public class CargarGrupo {

    public CargarGrupo() {
    }

    @Test
    public void cargarGrupo(){
        NivelDAO daoNiv = new NivelDAO();
        GrupoDAO daoGrup = new GrupoDAO(daoNiv.getManager());
        
        Clase clase = new Clase();
        clase.setCodigoClase('A');
        clase.setNivel(1);
        clase.setTipoDeClase(Clase.TipoClase.COMUN);
        
        Nivel niv = daoNiv.findByClase(clase);
        
        
        Grupo grupo = new Grupo();
       
        grupo.setNivel(niv);
        grupo.setAnio(2019);
        grupo.setTurno(Libreta.Turno.MATUTINO);
        boolean agrego = daoGrup.save(grupo);
        
        Clase clase2 = new Clase();
        clase2.setCodigoClase('B');
        clase2.setNivel(4);
        clase2.setTipoDeClase(Clase.TipoClase.COMUN);
        
        Nivel niv2 = daoNiv.findByClase(clase2);
        
        Grupo grupo2 = new Grupo();
       
        grupo2.setNivel(niv2);
        grupo2.setAnio(2019);
        grupo2.setTurno(Libreta.Turno.MATUTINO);
        boolean agrego2 = daoGrup.save(grupo2);
        assertTrue(agrego && agrego2);
        
    }
//    @Test
//    public void cargarAlumnoDesdeGrupo() {
//        EntityManager mng = ContextoDB.getInstancia().getManager();
//        NivelDAO daoNiv = new NivelDAO(mng);
//        GrupoDAO daoGrup = new GrupoDAO(mng);
//        AlumnoDAO daoAlumn = new AlumnoDAO(mng);
//        Clase clase = new Clase();
//        clase.setCodigoClase('A');
//        clase.setNivel(1);
//        clase.setTipoDeClase(Clase.TipoClase.COMUN);
//
//        Nivel niv = daoNiv.findByClase(clase);
//        
//        Grupo grupo = daoGrup.findByNivelYAnio(niv, 2019);
//        Documento doc = new Documento();
//        doc.setNumero(new Long(1234560));
//        doc.setPaisEmisor("URUGUAY");
//        doc.setTipo(Documento.TipoDocumento.CEDULA);
//        Alumno a1 =(Alumno) daoAlumn.findByDocumento(doc);
//        
//        grupo.addAlumno(a1);
//        
//        boolean agrego = daoGrup.update(grupo);
//        assertTrue(agrego);
//    }
//    @Test
//    public void pruebaCargarAlumnoDesdeGrupo() {
//        EntityManager manager = ContextoDB.getInstancia().getManager();
//        RolDAO daoRol = new RolDAO(manager);
//        NivelDAO daoNiv = new NivelDAO(manager);
//        GrupoDAO daoGrup = new GrupoDAO(manager);
//        Clase clase = new Clase();
//        clase.setCodigoClase('A');
//        clase.setNivel(1);
//        clase.setTipoDeClase(Clase.TipoClase.COMUN);
//
//        Nivel niv = daoNiv.findByClase(clase);
//
//        Grupo grupo = daoGrup.findByNivelYAnio(niv, 2019);
//        Domicilio domi = new Domicilio();
//        domi.setApartamento("1");
//        domi.setBarrio("Barrio");
//        domi.setBlock("block");
//        domi.setCalle("calle 13");
//        domi.setDepartamento(Domicilio.Departamento.CANELONES);
//        domi.setEsquina("calle 21");
//        domi.setIsEdificio(false);
//        domi.setLocalidad("Las vegas");
//        domi.setManzanaSolar("N/A");
//        domi.setNumero("15234");
//        domi.setTorre("no");
//        Rol r = daoRol.findByRol("ESTUDIANTE");
//
//        Alumno a1 = new Alumno();
//        a1.addRol(r);
//        Documento d1 = new Documento();
//        d1.setNumero(new Long(111111));
//        d1.setPaisEmisor("URUGUAY");
//        d1.setTipo(Documento.TipoDocumento.CEDULA);
//        a1.setDocumento(d1);
//        a1.setDomicilio(domi);
//        a1.setEmail("emaila1111@mail");
//        a1.setFaltas(0);
//        a1.setFechaNacimiento(Calendar.getInstance());
//        a1.setMatricula(111111);
//        a1.setPrimerApellido("lopez");
//        a1.setPrimerNombre("lola");
//        a1.setSegundoApellido("segunbdo");
//        a1.setSegundoNombre("segunbdo");
//        a1.setDomicilio(domi);
//        grupo.addAlumno(a1);
//        boolean agrego = daoGrup.update(grupo);
//        assertTrue(agrego);

 //   }
}
