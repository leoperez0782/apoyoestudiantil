/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.AsignaturaDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.DocenteDAO;
import com.apoyoestudiantil.dao.GrupoDAO;
import com.apoyoestudiantil.dao.LibretaDAO;
import com.apoyoestudiantil.dao.NivelDAO;
import com.apoyoestudiantil.dao.RolDAO;
import com.apoyoestudiantil.entity.Asignatura;
import com.apoyoestudiantil.entity.Clase;
import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Horario;
import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.entity.ProgramaPrimeroSegundoCBReformulacion2006;
import com.apoyoestudiantil.entity.Rol;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Leonardo Pérez
 */
public class CargarNuevoDocenteAlGrupoPrimero {

    public CargarNuevoDocenteAlGrupoPrimero() {
    }

    @Test
    public void cargarNuevoDocente() {
        EntityManager mng = ContextoDB.getInstancia().getManager();
        EntityTransaction trx = mng.getTransaction();
        DocenteDAO docDao = new DocenteDAO(mng);
        NivelDAO nivDAo = new NivelDAO(mng);

        GrupoDAO daoGrup = new GrupoDAO(mng);
        AsignaturaDAO asigDao = new AsignaturaDAO(mng);
        LibretaDAO libDao = new LibretaDAO(mng);
        RolDAO rolDao = new RolDAO(mng);

        Clase clase = new Clase();
        clase.setCodigoClase('A');
        clase.setNivel(1);
        clase.setTipoDeClase(Clase.TipoClase.COMUN);

        Nivel niv = nivDAo.findByClase(clase);

        Grupo grupo = daoGrup.findByNivelYAnio(niv, 2019);
        
        Documento docDocente = new Documento();
        docDocente.setNumero(new Long(Integer.valueOf("123321")));
        docDocente.setPaisEmisor("URUGUAY");
        docDocente.setTipo(Documento.TipoDocumento.CEDULA);
        Docente docente = docDao.findByDocumento(docDocente);
        trx.begin();
        //cargo la asignatura
        Asignatura asignatura = new Asignatura();
        asignatura.setNombre(ProgramaPrimeroSegundoCBReformulacion2006.IDIOMA_ESPANOL.getNombre());
        asignatura.setHorasSemanales(ProgramaPrimeroSegundoCBReformulacion2006.IDIOMA_ESPANOL.getHoras());
        asignatura.setProgramaAnio(ProgramaPrimeroSegundoCBReformulacion2006.IDIOMA_ESPANOL.getProgramaAnio());

        asignatura.setId(asigDao.persistWithTransaction(trx, mng, asignatura));
        
        Libreta lib = new Libreta();
        lib.setDocente(docente);
        lib.setGrupo(grupo);
        lib.setMateria(asignatura);
        lib.setPractico(false);
        lib.setSalon("Salon 1");
        lib.setTeorico(true);
        lib.setTurno(Libreta.Turno.MATUTINO);

        Horario horario1 = new Horario();
        horario1.setDia(Horario.Dias.MARTES);
        horario1.setHoraFin("10:00");
        horario1.setHoraInicio("08:00");
        Horario horario2 = new Horario();
        horario2.setDia(Horario.Dias.VIERNES);
        horario2.setHoraFin("10:00");
        horario2.setHoraInicio("08:00");

        lib.addHorario(horario1);
        lib.addHorario(horario2);
        lib.setId(libDao.persistWithTransaction(trx, mng, lib));
        
        trx.commit();
        Assert.assertTrue(lib.getId() != 0);
        
    }
}
