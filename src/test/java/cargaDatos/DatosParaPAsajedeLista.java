/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.GrupoDAO;
import com.apoyoestudiantil.dao.NivelDAO;
import com.apoyoestudiantil.dao.RolDAO;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Clase;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.entity.Rol;
import java.util.Calendar;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.junit.Test;

/**
 *
 * @author leo
 */
public class DatosParaPAsajedeLista {

    public DatosParaPAsajedeLista() {
    }

    @Test
    public void cargarDatosPrueba() {
        EntityManager mng = ContextoDB.getInstancia().getManager();
        EntityTransaction tx = mng.getTransaction();
        NivelDAO daoNiv = new NivelDAO(mng);
        GrupoDAO daoGrup = new GrupoDAO(mng);
        AlumnoDAO daoAlumno = new AlumnoDAO(mng);
        Clase c = new Clase();
        c.setCodigoClase('A');
        c.setNivel(1);
        c.setTipoDeClase(Clase.TipoClase.COMUN);
        Nivel nivel = daoNiv.findByClase(c);

        Grupo g = new Grupo();
        g.setAnio(2019);
        g.setNivel(nivel);

        RolDAO roldao = new RolDAO(mng);
        Rol r = roldao.findByRol("ESTUDIANTE");

        Alumno a1 = new Alumno();
        a1.addRol(r);
        Documento d1 = new Documento();
        d1.setNumero(new Long(1234));
        d1.setPaisEmisor("URUGUAY");
        d1.setTipo(Documento.TipoDocumento.CEDULA);

        a1.setDocumento(d1);
        Domicilio domi = new Domicilio();
        domi.setApartamento("0");
        domi.setBarrio("Barrio");
        domi.setBlock("block");
        domi.setCalle("calle 13");
        domi.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi.setEsquina("calle 1");
        domi.setEdificio("no");
        domi.setLocalidad("Durazno");
        domi.setManzanaSolar("N/A");
        domi.setNumero("1524");
        domi.setTorre("no");

        a1.addDomicilio(domi);
        a1.setEmail("emaila1@mail");

        a1.setFechaNacimiento("11/08/2003");
        a1.setMatricula(1234);
        a1.setPrimerApellido("lopez");
        a1.setPrimerNombre("memo");
        a1.setSegundoApellido("segunbdo");
        a1.setSegundoNombre("segunbdo");
        tx.begin();
        a1.setId(daoAlumno.persistWithTransaction(tx, mng, a1));
        tx.commit();
        nivel.addAlumno(a1);

        Alumno a2 = new Alumno();
        Documento d2 = new Documento();
        d2.setNumero(new Long(12345));
        d2.setPaisEmisor("URUGUAY");
        d2.setTipo(Documento.TipoDocumento.CEDULA);

        a2.setDocumento(d2);
        Domicilio domi2 = new Domicilio();
        domi2.setApartamento("1");
        domi2.setBarrio("Barrio");
        domi2.setBlock("block");
        domi2.setCalle("calle 13");
        domi2.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi2.setEsquina("calle 1");
        domi2.setEdificio("N/C");
        domi2.setLocalidad("Durazno");
        domi2.setManzanaSolar("N/A");
        domi2.setNumero("1524");
        domi2.setTorre("no");

        a2.addDomicilio(domi2);
        a2.setEmail("emaila2@mail");

        a2.setFechaNacimiento("25/05/2003");
        a2.setMatricula(12345);
        a2.setPrimerApellido("Calcuta");
        a2.setPrimerNombre("TEresa");
        a2.setSegundoApellido("segunbdo");
        a2.setSegundoNombre("segunbdo");

        nivel.addAlumno(a2);

    }
}
