/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.GrupoDAO;
import com.apoyoestudiantil.dao.NivelDAO;
import com.apoyoestudiantil.dao.RolDAO;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Clase;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.entity.Rol;
import java.util.Calendar;
import javax.persistence.EntityManager;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author leo
 */
public class CargarAlumnosDesdeNivel {
    
    public CargarAlumnosDesdeNivel() {
    }
    @Test
    public void cargarAlumnosDesdeNivel(){
        EntityManager manager = ContextoDB.getInstancia().getManager();
        RolDAO daoRol = new RolDAO(manager);
        NivelDAO daoNiv = new NivelDAO(manager);
        GrupoDAO daoGrup = new GrupoDAO(manager);
        Clase clase = new Clase();
        clase.setCodigoClase('A');
        clase.setNivel(1);
        clase.setTipoDeClase(Clase.TipoClase.COMUN);

        Nivel niv = daoNiv.findByClase(clase);

        Grupo grupo = daoGrup.findByNivelYAnio(niv, 2019);
        
         Domicilio domi = new Domicilio();
        domi.setApartamento("0");
        domi.setBarrio("Barrio");
        domi.setBlock("block");
        domi.setCalle("calle 13");
        domi.setDepartamento(Domicilio.Departamento.CANELONES);
        domi.setEsquina("calle 1");
        domi.setEdificio("");
        domi.setLocalidad("Las vegas");
        domi.setManzanaSolar("N/A");
        domi.setNumero("1524");
        domi.setTorre("no");
        Rol r = daoRol.findByRol("ESTUDIANTE");

        Alumno a1 = new Alumno();
        a1.addRol(r);
        Documento d1 = new Documento();
        d1.setNumero(new Long(1234560));
        d1.setPaisEmisor("URUGUAY");
        d1.setTipo(Documento.TipoDocumento.CEDULA);
        a1.setDocumento(d1);
        //a1.setDomicilio(domi);
         a1.addDomicilio(domi);
        a1.setEmail("emaila1250@mail");
        //a1.setFaltas(0);
        //a1.setFechaNacimiento(Calendar.getInstance());
        a1.setFechaNacimiento("25/06/2003");
        a1.setMatricula(1234560);
        a1.setPrimerApellido("lopez");
        a1.setPrimerNombre("pancho");
        a1.setSegundoApellido("segunbdo");
        a1.setSegundoNombre("segunbdo");
       
        
        niv.addAlumno(a1);
        boolean agrego = daoNiv.update(niv);
        
        assertTrue(agrego);
    }
}
