/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.AsignaturaDAO;
import com.apoyoestudiantil.entity.Asignatura;
import com.apoyoestudiantil.entity.ProgramaCuartoRef2006;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author leo
 */
public class GuardarUnaAsignatura {
    
    public GuardarUnaAsignatura() {
    }
    
    @Test
    public void guardarUnaAsignatura(){
        Asignatura a = new Asignatura();
        a.setNombre(ProgramaCuartoRef2006.ASTRONOMIA.getNombre());
        a.setHorasSemanales(ProgramaCuartoRef2006.ASTRONOMIA.getHoras());
        a.setProgramaAnio(ProgramaCuartoRef2006.ASTRONOMIA.getProgramaAnio());
        
        AsignaturaDAO dao = new AsignaturaDAO();
        
        boolean agrego = dao.save(a);
        
        assertTrue(agrego);
    }
}
