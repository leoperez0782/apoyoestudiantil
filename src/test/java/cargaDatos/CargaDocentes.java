/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.CargoDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.DocenteDAO;
import com.apoyoestudiantil.dao.RolDAO;
import com.apoyoestudiantil.entity.Cargo;
import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Rol;
import com.apoyoestudiantil.entity.Telefono;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Leonardo Pérez
 */
public class CargaDocentes {

    public CargaDocentes() {
    }

    @Test
    public void hello() {
        EntityManager mng = ContextoDB.getInstancia().getManager();
        EntityTransaction tx = mng.getTransaction();
        DocenteDAO daoDocente = new DocenteDAO(mng);
        RolDAO daoRol = new RolDAO(mng);
        CargoDAO daoCargo = new CargoDAO(mng);
        Rol r = daoRol.findByRol("DOCENTE");
        Cargo c = daoCargo.findByTipo("Docente tiempo completo");

        Documento d1 = new Documento();
        d1.setNumero(new Long(3330003));
        d1.setPaisEmisor("URUGUAY");
        d1.setTipo(Documento.TipoDocumento.CEDULA);

        Docente doc1 = new Docente();
        doc1.setActivo(true);
        doc1.setCargo(c);
        doc1.setClave("3330003");//la cedula
        doc1.setDocumento(d1);
        doc1.setEmail("elprofe@hotmail.com");
        doc1.setFechaNacimiento("15/08/1980");
        doc1.setNumeroEmpleado(10);
        doc1.setPrimerApellido("Bengoechea");
        doc1.setPrimerNombre("Pablo");
        doc1.setSegundoApellido("Curbelo");
        doc1.setSegundoNombre("Javier");
        Telefono t = new Telefono();
        t.setNumero("099198777");
        t.setObservaciones("No contesta nunca");
        t.setTipo(Telefono.TipoTelefono.MOVIL);
        doc1.addTelefono(t);
        doc1.addRol(r);

        //segundo docente
        Documento d2 = new Documento();
        d2.setNumero(new Long(123321));
        d2.setPaisEmisor("URUGUAY");
        d2.setTipo(Documento.TipoDocumento.CEDULA);

        Docente doc2 = new Docente();
        doc2.setActivo(true);
        doc2.setCargo(c);
        doc2.setClave("123321");//la cedula
        doc2.setDocumento(d2);
        doc2.setEmail("pao@hotmail.com");
        doc2.setFechaNacimiento("15/08/1980");
        doc2.setNumeroEmpleado(11);
        doc2.setPrimerApellido("Fernandez");
        doc2.setPrimerNombre("Paola");
        doc2.setSegundoApellido("Rodriguez");
        doc2.setSegundoNombre("");
        Telefono t2 = new Telefono();
        t2.setNumero("091198777");
        t2.setObservaciones("No contesta nunca");
        t2.setTipo(Telefono.TipoTelefono.MOVIL);
        doc2.addTelefono(t2);
        doc2.addRol(r);
        tx.begin();
//        assertTrue(daoDocente.save(doc2));
//        assertTrue(daoDocente.save(doc1));
        doc1.setId(daoDocente.persistWithTransaction(tx, mng, doc1));
        doc2.setId(daoDocente.persistWithTransaction(tx, mng, doc2));
        tx.commit();
        assertTrue(doc1.getId() != 0 && doc2.getId() != 0);
    }
}
