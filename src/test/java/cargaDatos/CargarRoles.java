/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.RolDAO;
import com.apoyoestudiantil.entity.Rol;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author leo
 */
public class CargarRoles {
    
    public CargarRoles() {
    }
    
    @Test
    public void cargarRoles(){
        RolDAO dao = new RolDAO();
        
        Rol rol1 = new Rol();
        Rol rol2 = new Rol();
        Rol rol3 = new Rol();
        Rol rol4 = new Rol();
        Rol rol5 = new Rol();
        rol1.setRol("ADMINISTRADOR");
        rol2.setRol("ESTUDIANTE");
        rol3.setRol("DOCENTE");
        rol4.setRol("ADMINISTRATIVO");
        rol5.setRol("ADULTO_RESPONSABLE");
        //boolean agrego = dao.save(rol1);
        assertTrue(dao.save(rol1));
        assertTrue(dao.save(rol2));
        assertTrue(dao.save(rol3));
        assertTrue(dao.save(rol4));
        assertTrue(dao.save(rol5));
    }
    
//    @Test
//    public void getRolByRol(){
//        RolDAO dao = new RolDAO();
//        Rol r = dao.findByRol("ESTUDIANTE");
//        
//        assertTrue(r.getRol().equals("ESTUDIANTE"));
//    }
}
