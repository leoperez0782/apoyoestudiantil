/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.NivelDAO;
import com.apoyoestudiantil.entity.Clase;
import com.apoyoestudiantil.entity.Nivel;
import org.junit.Test;

/**
 *
 * @author leo
 */
public class pRUEBAcREARBASEVACIA {

    @Test(expected = Exception.class)
    public void crearBase() {
        NivelDAO dao = new NivelDAO();
        Clase clase = new Clase();
        clase.setCodigoClase('A');
        clase.setNivel(1);
        clase.setTipoDeClase(Clase.TipoClase.COMUN);
        
        Nivel N = dao.findByClase(clase);
    }
}
