/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.AdministradorDAO;
import com.apoyoestudiantil.dao.AdministrativoDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.DomicilioDAO;
import com.apoyoestudiantil.dao.RolDAO;
import com.apoyoestudiantil.entity.Administrador;
import com.apoyoestudiantil.entity.Administrativo;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Rol;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author leo
 */
public class AdminYNivel {

    public AdminYNivel() {
    }

    @Test
    public void cargarAdminYAsignarlosANivel() {
        EntityManager mng = ContextoDB.getInstancia().getManager();
        EntityTransaction tx = mng.getTransaction();
        AdministrativoDAO daoAdministrativo = new AdministrativoDAO(mng);
        AdministradorDAO daoAdministrador = new AdministradorDAO(mng);
        RolDAO roldao = new RolDAO(mng);
        DomicilioDAO domiDao = new DomicilioDAO(mng);
        //buscamos el rol.
        Rol r = roldao.findByRol("ADMINISTRATIVO");
        //Buscamos  el nivel.
        //creamos primer alumno.
        Administrativo a1 = new Administrativo();
        a1.addRol(r);
        a1.setActivo(true);
        Documento d1 = new Documento();
        d1.setNumero(new Long(1234567));
        d1.setPaisEmisor("URUGUAY");
        d1.setTipo(Documento.TipoDocumento.CEDULA);

        a1.setDocumento(d1);
        a1.setClave("123");
        Domicilio domi = new Domicilio();
        domi.setApartamento("0");
        domi.setBarrio("Barrio");
        domi.setBlock("block");
        domi.setCalle("calle 13");
        domi.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi.setEsquina("calle 1");
        domi.setEdificio("a");
        domi.setLocalidad("Durazno");
        domi.setManzanaSolar("N/A");
        domi.setNumero("1524");
        domi.setTorre("no");
        //iniciamos la transaccion.
        tx.begin();
        domi.setId(domiDao.persistWithTransaction(tx, mng, domi));
        //a1.setDomicilio(domi);
        a1.addDomicilio(domi);
        a1.setEmail("admin@admin");
        //a1.setFaltas(0);
        //a1.setFechaNacimiento(Calendar.getInstance());
        a1.setFechaNacimiento("11/02/1980");
        a1.setPrimerApellido("lopez");
        a1.setPrimerNombre("memo");
        a1.setSegundoApellido("segunbdo");
        a1.setSegundoNombre("segunbdo");

        //agregamos el primer alumno a la transaccion y recuperamos su id.
        a1.setId(daoAdministrativo.persistWithTransaction(tx, mng, a1));

        //creamos el administrador.
        Administrador a2 = new Administrador();
        Documento d2 = new Documento();
        d2.setNumero(new Long(12345678));
        d2.setPaisEmisor("URUGUAY");
        d2.setTipo(Documento.TipoDocumento.CEDULA);
        Rol rolAdor = roldao.findByRol("ADMINISTRADOR");
        a2.addRol(rolAdor);
        a2.setDocumento(d2);
        a2.setClave("123");
        a2.setActivo(true);
        //le agregamos el mismo rol
        a2.addRol(r);
        Domicilio domi2 = new Domicilio();
        domi2.setApartamento("0");
        domi2.setBarrio("Barrio");
        domi2.setBlock("block");
        domi2.setCalle("calle 14");
        domi2.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi2.setEsquina("calle 12");
        domi2.setEdificio("b");
        domi2.setLocalidad("Durazno");
        domi2.setManzanaSolar("N/A");
        domi2.setNumero("15224");
        domi2.setTorre("no");
        domi2.setId(domiDao.persistWithTransaction(tx, mng, domi2));
        // a2.setDomicilio(domi2);
        a2.addDomicilio(domi2);
        a2.setEmail("admin2@admin");
        //a2.setFaltas(0);
        //a2.setFechaNacimiento(Calendar.getInstance());
        a2.setFechaNacimiento("25/03/1978");
        a2.setPrimerApellido("Calcuta");
        a2.setPrimerNombre("TEresa");
        a2.setSegundoApellido("segunbdo");
        a2.setSegundoNombre("segunbdo");
        //guardamos el segundo alumno con la misma transaccion y el mismo manager y guardamos el id
        a2.setId(daoAdministrador.persistWithTransaction(tx, mng, a2));
        tx.commit();//guardamos los alumnos.
        assertTrue(a1.getId()!=0 && a2.getId() !=0);
    }
}
