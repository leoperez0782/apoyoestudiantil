/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.AdultoResponsableDAO;
import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.RolDAO;
import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Rol;
import java.util.Calendar;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.junit.Assert;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author leo
 */
public class GuardarAlumnoConAdultoResponsable {
    
    public GuardarAlumnoConAdultoResponsable() {
    }
    
    @Test
    public void guardarAlumnoConPadre(){
        ContextoDB bd = ContextoDB.getInstancia();
        EntityManager mng = bd.getManager();
        EntityTransaction tx = mng.getTransaction();
        AlumnoDAO daoAlumno = new AlumnoDAO(mng);
        RolDAO daoRol = new RolDAO(mng);
        AdultoResponsableDAO daoAdulto = new AdultoResponsableDAO(mng);
        AdultoResponsable adu = new AdultoResponsable();
        Alumno a = new Alumno();
 
        Domicilio domi = new Domicilio();
        domi.setApartamento("0");
        domi.setBarrio("Barrio");
        domi.setBlock("block");
        domi.setCalle("calle 10");
        domi.setDepartamento(Domicilio.Departamento.CANELONES);
        domi.setEsquina("calle 12");
        domi.setEdificio("");
        domi.setLocalidad("Las vegas");
        domi.setManzanaSolar("Manz 10 Sol. 25");
        domi.setNumero("12");
        domi.setTorre("no");
        Rol r = daoRol.findByRol("ESTUDIANTE");

        Alumno a1 = new Alumno();
        a1.addRol(r);
        Documento d1 = new Documento();
        d1.setNumero(new Long(1100044));
        d1.setPaisEmisor("URUGUAY");
        d1.setTipo(Documento.TipoDocumento.CEDULA);
        a1.setDocumento(d1);
        
         a1.addDomicilio(domi);
        a1.setEmail("emaillolo1140@mail");
        
        //a1.setFechaNacimiento(Calendar.getInstance());
        a1.setFechaNacimiento("21/09/2003");
        a1.setMatricula(1100044);
        a1.setPrimerApellido("lopez");
        a1.setPrimerNombre("lolo");
        a1.setSegundoApellido("segunbdo");
        a1.setSegundoNombre("segunbdo");
        
        Rol rolAdulto = daoRol.findByRol("ADULTO_RESPONSABLE");
        adu.addRol(rolAdulto);
        adu.addDomicilio(domi);
        adu.setClave("123");
        adu.setEmail("maildeladulto@gmail.com");
        //adu.setFechaNacimiento(Calendar.getInstance());
        adu.setFechaNacimiento("24/03/1980");
        adu.setPrimerApellido("Lopez");
        adu.setPrimerNombre("Memo");
        adu.setSegundoApellido("Gonzalez");
        adu.setSegundoNombre("pepe");
        Documento doc =new Documento();
        doc.setNumero(new Long(3321123));
        doc.setPaisEmisor("Uruguay");
        doc.setTipo(Documento.TipoDocumento.CEDULA);
        adu.setDocumento(doc);
        tx.begin();
        adu.setId(daoAdulto.persistWithTransaction(tx, mng, adu));
        a1.addPadre(adu);
        a1.setId(daoAlumno.persistWithTransaction(tx, mng, a1));
        tx.commit();
        assertTrue(a1.getId()!= 0);
        
    }
}
