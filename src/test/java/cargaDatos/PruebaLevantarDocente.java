/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.DocenteDAO;
import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.entity.Documento;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author leo
 */
public class PruebaLevantarDocente {
    
    public PruebaLevantarDocente() {
    }
    
  @Test
  public void pruebaCargarDocente(){
      DocenteDAO docDao = new DocenteDAO();
      
      Documento docDocente = new Documento();
        docDocente.setNumero(new Long(Integer.valueOf("3330003")));
        docDocente.setPaisEmisor("uruguay");
        docDocente.setTipo(Documento.TipoDocumento.CEDULA);
        Docente docente = docDao.findByDocumento(docDocente);
        
        assertTrue(docente.getId() == 3);
  }
}
