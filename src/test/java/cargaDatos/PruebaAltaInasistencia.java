/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.dao.AsignaturaDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.DocenteDAO;
import com.apoyoestudiantil.dao.GrupoDAO;
import com.apoyoestudiantil.dao.LibretaDAO;
import com.apoyoestudiantil.dao.NivelDAO;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Inasistencia;
import com.apoyoestudiantil.entity.Libreta;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import static junit.framework.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author Leonardo Pérez
 */
public class PruebaAltaInasistencia {

    public PruebaAltaInasistencia() {
    }

    @Test
    public void pasarLista() {
        EntityManager mng = ContextoDB.getInstancia().getManager();
        EntityTransaction trx = mng.getTransaction();
        DocenteDAO docDao = new DocenteDAO(mng);
        NivelDAO nivDAo = new NivelDAO(mng);
        AlumnoDAO alumDao = new AlumnoDAO(mng);
        GrupoDAO daoGrup = new GrupoDAO(mng);
        AsignaturaDAO asigDao = new AsignaturaDAO(mng);
        LibretaDAO libDao = new LibretaDAO(mng);
        
        Documento docDocente = new Documento();
        docDocente.setNumero(new Long(Integer.valueOf("3330003")));
        docDocente.setPaisEmisor("uruguay");
        docDocente.setTipo(Documento.TipoDocumento.CEDULA);
        Docente docente = docDao.findByDocumento(docDocente);
        
        List<Libreta> libretas = libDao.getAllByDocente(docente);
        
        Libreta lib = libretas.get(0);
        
        Alumno elQuefalta = lib.getGrupo().getAlumno(0);
        
        Inasistencia in = new Inasistencia();
        //in.setAlumno(elQuefalta);
       // elQuefalta.addInasistencia(in);
        in.setEstado(Inasistencia.EstadoInasistencia.EXONERADA);
        in.setFecha(LocalDate.now());
        in.setHoraInicio("05:00");
        in.setJustificacion("JUSTIFICADA");
        in.setTipo(Inasistencia.TipoInasistencia.TARDE);
        in.setAlumno(elQuefalta);
        //lib.addInasistencia(in);
        
        boolean actualizo = libDao.update(lib);
        assertTrue(actualizo);
    }
}
