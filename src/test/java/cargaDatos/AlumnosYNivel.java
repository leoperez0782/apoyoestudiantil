/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.DomicilioDAO;
import com.apoyoestudiantil.dao.NivelDAO;
import com.apoyoestudiantil.dao.RolDAO;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Clase;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.entity.Rol;
import java.util.Calendar;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author leo
 */
public class AlumnosYNivel {

    public AlumnosYNivel() {
    }

//    @Test
//    public void cargarAlumnosANivelYaExistente() {
//
//        EntityManager manager = ContextoDB.getInstancia().getManager();
//        EntityTransaction tx = manager.getTransaction();
//        Nivel nivel;
//        Clase clase = new Clase();
//        clase.setCodigoClase('A');
//        clase.setNivel(1);
//        clase.setTipoDeClase(Clase.TipoClase.COMUN);
//        AlumnoDAO daoAlumno = new AlumnoDAO(manager);
//        NivelDAO dao = new NivelDAO(manager);
//        RolDAO roldao = new RolDAO(manager);
//        Rol r = roldao.findByRol("ESTUDIANTE");
//        nivel = dao.findByClase(clase);//Busco el nivel para agregarlos desde aca?
//
//        Alumno a1 = new Alumno();
//        a1.addRol(r);
//        Documento d1 = new Documento();
//        d1.setNumero(new Long(1234));
//        d1.setPaisEmisor("URUGUAY");
//        d1.setTipo(Documento.TipoDocumento.CEDULA);
//
//        a1.setDocumento(d1);
//        Domicilio domi = new Domicilio();
//        domi.setApartamento("0");
//        domi.setBarrio("Barrio");
//        domi.setBlock("block");
//        domi.setCalle("calle 13");
//        domi.setDepartamento(Domicilio.Departamento.DURAZNO);
//        domi.setEsquina("calle 1");
//        domi.setIsEdificio(false);
//        domi.setLocalidad("Durazno");
//        domi.setManzanaSolar("N/A");
//        domi.setNumero("1524");
//        domi.setTorre("no");
//
//        a1.setDomicilio(domi);
//        a1.setEmail("emaila1@mail");
//        a1.setFaltas(0);
//        a1.setFechaNacimiento(Calendar.getInstance());
//        a1.setMatricula(1234);
//        a1.setPrimerApellido("lopez");
//        a1.setPrimerNombre("memo");
//        a1.setSegundoApellido("segunbdo");
//        a1.setSegundoNombre("segunbdo");
//        tx.begin();
//        a1.setId(daoAlumno.persistWithTransaction(tx, manager, a1));
//        tx.commit();
//        nivel.addAlumno(a1);
//
////        Alumno a2 = new Alumno();
////        Documento d2 = new Documento();
////        d2.setNumero(new Long(12345));
////        d2.setPaisEmisor("URUGUAY");
////        d2.setTipo(Documento.TipoDocumento.CEDULA);
////        
////        a2.setDocumento(d2);
////        Domicilio domi2 = new Domicilio();
////        domi2.setApartamento("1");
////        domi2.setBarrio("Barrio");
////        domi2.setBlock("block");
////        domi2.setCalle("calle 13");
////        domi2.setDepartamento(Domicilio.Departamento.DURAZNO);
////        domi2.setEsquina("calle 1");
////        domi2.setIsEdificio(false);
////        domi2.setLocalidad("Durazno");
////        domi2.setManzanaSolar("N/A");
////        domi2.setNumero("1524");
////        domi2.setTorre("no");
////        
////        a2.setDomicilio(domi2);
////        a2.setEmail("emaila2@mail");
////        a2.setFaltas(0);
////        a2.setFechaNacimiento(Calendar.getInstance());
////        a2.setMatricula(12345);
////        a2.setPrimerApellido("Calcuta");
////        a2.setPrimerNombre("TEresa");
////        a2.setSegundoApellido("segunbdo");
////        a2.setSegundoNombre("segunbdo");
////        
////        nivel.addAlumno(a2);
////        
//        boolean actualizo = dao.update(nivel);
//
//        assertTrue(actualizo);
//
//    }

    @Test
    public void cargarVariosAlumnosEnTransaccionYAsignarlosANivel() {
        EntityManager manager = ContextoDB.getInstancia().getManager();
        //usamos la transaccion para guardar todos los alumnos.
        EntityTransaction tx = manager.getTransaction();
        Nivel nivel;
        Clase clase = new Clase();
        clase.setCodigoClase('A');
        clase.setNivel(1);
        clase.setTipoDeClase(Clase.TipoClase.COMUN);
        //Todos los daos que vamos a usar deben usar el mismo manager.
        AlumnoDAO daoAlumno = new AlumnoDAO(manager);
        NivelDAO dao = new NivelDAO(manager);
        RolDAO roldao = new RolDAO(manager);
        DomicilioDAO domiDao = new DomicilioDAO(manager);
        //buscamos el rol.
        Rol r = roldao.findByRol("ESTUDIANTE");
        //Buscamos  el nivel.
        nivel = dao.findByClase(clase);
        //creamos primer alumno.
        Alumno a1 = new Alumno();
        a1.addRol(r);
        Documento d1 = new Documento();
        d1.setNumero(new Long(12345));
        d1.setPaisEmisor("URUGUAY");
        d1.setTipo(Documento.TipoDocumento.CEDULA);

        a1.setDocumento(d1);
        a1.setClave("123");
        Domicilio domi = new Domicilio();
        domi.setApartamento("0");
        domi.setBarrio("Barrio");
        domi.setBlock("block");
        domi.setCalle("calle 13");
        domi.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi.setEsquina("calle 1");
        domi.setEdificio("h");
        domi.setLocalidad("Durazno");
        domi.setManzanaSolar("N/A");
        domi.setNumero("1524");
        domi.setTorre("no");
        //iniciamos la transaccion.
        tx.begin();
        domi.setId(domiDao.persistWithTransaction(tx, manager, domi));
        //a1.setDomicilio(domi);
        a1.addDomicilio(domi);
        a1.setEmail("email");//esto tendria que explotar
        //a1.setFaltas(0);
        //a1.setFechaNacimiento(Calendar.getInstance());
        a1.setFechaNacimiento("15/10/2003");
        a1.setMatricula(12345);
        a1.setPrimerApellido("lopez");
        a1.setPrimerNombre("memo");
        a1.setSegundoApellido("segunbdo");
        a1.setSegundoNombre("segunbdo");
        
        //agregamos el primer alumno a la transaccion y recuperamos su id.
        a1.setId(daoAlumno.persistWithTransaction(tx, manager, a1));

        //creamos el segundo alumno.
        Alumno a2 = new Alumno();
        Documento d2 = new Documento();
        d2.setNumero(new Long(123456));
        d2.setPaisEmisor("URUGUAY");
        d2.setTipo(Documento.TipoDocumento.CEDULA);
        
        a2.setDocumento(d2);
        a2.setClave("123");
        //le agregamos el mismo rol
        a2.addRol(r);
        Domicilio domi2 = new Domicilio();
        domi2.setApartamento("0");
        domi2.setBarrio("Barrio");
        domi2.setBlock("block");
        domi2.setCalle("calle 14");
        domi2.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi2.setEsquina("calle 12");
        domi2.setEdificio("oooo");
        domi2.setLocalidad("Durazno");
        domi2.setManzanaSolar("N/A");
        domi2.setNumero("15224");
        domi2.setTorre("no");
        domi2.setId(domiDao.persistWithTransaction(tx, manager, domi2));
       // a2.setDomicilio(domi2);
        a2.addDomicilio(domi2);
        a2.setEmail("emaila123456@mail");
        //a2.setFaltas(0);
        //a2.setFechaNacimiento(Calendar.getInstance());
        a2.setFechaNacimiento("12/11/2003");
        a2.setMatricula(123456);
        a2.setPrimerApellido("Calcuta");
        a2.setPrimerNombre("TEresa");
        a2.setSegundoApellido("segunbdo");
        a2.setSegundoNombre("segunbdo");
        //guardamos el segundo alumno con la misma transaccion y el mismo manager y guardamos el id
        a2.setId(daoAlumno.persistWithTransaction(tx, manager, a2));
        tx.commit();//guardamos los alumnos.
        //los agregamos al nivel
        nivel.addAlumno(a2);
        nivel.addAlumno(a1);
        //actualizamos el nivel
        boolean agrego = dao.update(nivel);
        assertTrue(agrego);
    }
}
