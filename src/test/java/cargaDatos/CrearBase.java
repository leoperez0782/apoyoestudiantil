/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.AdministradorDAO;
import com.apoyoestudiantil.dao.AdministrativoDAO;
import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.dao.AsignaturaDAO;
import com.apoyoestudiantil.dao.AuxiliarMatriculaDAO;
import com.apoyoestudiantil.dao.CargoDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.DocenteDAO;
import com.apoyoestudiantil.dao.DomicilioDAO;
import com.apoyoestudiantil.dao.GrupoDAO;
import com.apoyoestudiantil.dao.LibretaDAO;
import com.apoyoestudiantil.dao.NivelDAO;
import com.apoyoestudiantil.dao.RolDAO;
import com.apoyoestudiantil.entity.Administrador;
import com.apoyoestudiantil.entity.Administrativo;
import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Asignatura;
import com.apoyoestudiantil.entity.AuxiliarMatricula;
import com.apoyoestudiantil.entity.Cargo;
import com.apoyoestudiantil.entity.Clase;
import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Horario;
import com.apoyoestudiantil.entity.ItemAuxiliarMatricula;
import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.entity.ProgramaCuartoRef2006;
import com.apoyoestudiantil.entity.ProgramaPrimeroSegundoCBReformulacion2006;
import com.apoyoestudiantil.entity.Rol;
import com.apoyoestudiantil.entity.Telefono;
import java.util.Calendar;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.junit.Assert;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author Leonardo Pérez
 */
public class CrearBase {

    EntityManager mng = ContextoDB.getInstancia().getManager();
    NivelDAO nivDao = new NivelDAO(mng);
    RolDAO daoRol = new RolDAO(mng);
    GrupoDAO daoGrup = new GrupoDAO(mng);
    CargoDAO daoCargo = new CargoDAO(mng);
    AdministrativoDAO daoAdministrativo = new AdministrativoDAO(mng);
    AdministradorDAO daoAdministrador = new AdministradorDAO(mng);
    DomicilioDAO domiDao = new DomicilioDAO(mng);
    DocenteDAO daoDocente = new DocenteDAO(mng);
    AsignaturaDAO asigDao = new AsignaturaDAO(mng);
    LibretaDAO libDao = new LibretaDAO(mng);
    AuxiliarMatriculaDAO auxDao = new AuxiliarMatriculaDAO(mng);
    AlumnoDAO alumDao = new AlumnoDAO(mng);

    public CrearBase() {
    }

    @Test
    public void crear() {
        cargarNiveles();
        cargarRoles();
        cargarGrupos();
        cargarCargos();
        cargaDeAdmins();
        cargaDocentes();
        cargarGrupoYAlumnosParaPrueba();
        cargarNuevoDocenteAlgrupoPrimero();
        cargaNuevoGrupoAlDocenteDistintoNivel();
    }

    private void cargarNiveles() {
        Nivel niv = new Nivel();
        Clase clase = new Clase();
        clase.setCodigoClase('A');
        clase.setNivel(1);
        clase.setTipoDeClase(Clase.TipoClase.COMUN);

        niv.setClase(clase);
        niv.setFechaCreacion(Calendar.getInstance());
        niv.setEstado(Nivel.Estado.ACTIVO);

        boolean agrego = nivDao.save(niv);

        Nivel niv2 = new Nivel();
        Clase clase2 = new Clase();
        clase2.setCodigoClase('B');
        clase2.setNivel(4);
        clase2.setTipoDeClase(Clase.TipoClase.COMUN);

        niv2.setClase(clase2);
        niv2.setFechaCreacion(Calendar.getInstance());
        niv2.setEstado(Nivel.Estado.ACTIVO);
        boolean agrego2 = nivDao.save(niv2);
        assertTrue(agrego && agrego2);
    }

    private void cargarRoles() {
        Rol rol1 = new Rol();
        Rol rol2 = new Rol();
        Rol rol3 = new Rol();
        Rol rol4 = new Rol();
        Rol rol5 = new Rol();
        rol1.setRol("ADMINISTRADOR");
        rol2.setRol("ESTUDIANTE");
        rol3.setRol("DOCENTE");
        rol4.setRol("ADMINISTRATIVO");
        rol5.setRol("ADULTO_RESPONSABLE");
        //boolean agrego = nivDao.save(rol1);
        assertTrue(daoRol.save(rol1));
        assertTrue(daoRol.save(rol2));
        assertTrue(daoRol.save(rol3));
        assertTrue(daoRol.save(rol4));
        assertTrue(daoRol.save(rol5));
    }

    private void cargarGrupos() {
        Clase clase = new Clase();
        clase.setCodigoClase('A');
        clase.setNivel(1);
        clase.setTipoDeClase(Clase.TipoClase.COMUN);

        Nivel niv = nivDao.findByClase(clase);

        Grupo grupo = new Grupo();

        grupo.setNivel(niv);
        grupo.setAnio(2019);
        boolean agrego = daoGrup.save(grupo);

        Clase clase2 = new Clase();
        clase2.setCodigoClase('B');
        clase2.setNivel(4);
        clase2.setTipoDeClase(Clase.TipoClase.COMUN);

        Nivel niv2 = nivDao.findByClase(clase2);

        Grupo grupo2 = new Grupo();

        grupo2.setNivel(niv2);
        grupo2.setAnio(2019);
        boolean agrego2 = daoGrup.save(grupo2);
        assertTrue(agrego && agrego2);
    }

    private void cargarCargos() {
        Cargo c = new Cargo();
        //c.setActivo(true);
        c.setDescripcion("Tareas administrativas");
        c.setTipoCargo("Administrativo");

        boolean agrego = daoCargo.save(c);
        assertTrue(agrego);

        Cargo c1 = new Cargo();
        //c.setActivo(true);
        c1.setDescripcion("8 horas");
        c1.setTipoCargo("Docente tiempo completo");

        boolean agrego1 = daoCargo.save(c1);
        assertTrue(agrego1);

        Cargo c2 = new Cargo();
        //c.setActivo(true);
        c2.setDescripcion("4 horas");
        c2.setTipoCargo("Docente medio tiempo");

        boolean agrego2 = daoCargo.save(c2);
        assertTrue(agrego2);

        Cargo c3 = new Cargo();
        //c.setActivo(true);
        c3.setDescripcion("Direccion");
        c3.setTipoCargo("Directivo");

        boolean agrego3 = daoCargo.save(c3);
        assertTrue(agrego3);
    }

    private void cargaDeAdmins() {
        EntityTransaction tx = mng.getTransaction();
        //buscamos el rol.
        Rol r = daoRol.findByRol("ADMINISTRATIVO");
        //Buscamos  el nivel.
        //creamos primer alumno.
        Administrativo a1 = new Administrativo();
        a1.addRol(r);
        a1.setActivo(true);
        Documento d1 = new Documento();
        d1.setNumero(new Long(1234567));
        d1.setPaisEmisor("URUGUAY");
        d1.setTipo(Documento.TipoDocumento.CEDULA);

        a1.setDocumento(d1);
        a1.setClave("123");
        Domicilio domi = new Domicilio();
        domi.setApartamento("0");
        domi.setBarrio("Barrio");
        domi.setBlock("block");
        domi.setCalle("calle 13");
        domi.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi.setEsquina("calle 1");
        domi.setEdificio("a");
        domi.setLocalidad("Durazno");
        domi.setManzanaSolar("N/A");
        domi.setNumero("1524");
        domi.setTorre("no");
        //iniciamos la transaccion.
        tx.begin();
        domi.setId(domiDao.persistWithTransaction(tx, mng, domi));
        //a1.setDomicilio(domi);
        a1.addDomicilio(domi);
        a1.setEmail("admin@admin");
        //a1.setFaltas(0);
        //a1.setFechaNacimiento(Calendar.getInstance());
        a1.setFechaNacimiento("11/02/1980");
        a1.setPrimerApellido("lopez");
        a1.setPrimerNombre("memo");
        a1.setSegundoApellido("segunbdo");
        a1.setSegundoNombre("segunbdo");

        //agregamos el primer alumno a la transaccion y recuperamos su id.
        a1.setId(daoAdministrativo.persistWithTransaction(tx, mng, a1));

        //creamos el administrador.
        Administrador a2 = new Administrador();
        Documento d2 = new Documento();
        d2.setNumero(new Long(12345678));
        d2.setPaisEmisor("URUGUAY");
        d2.setTipo(Documento.TipoDocumento.CEDULA);
        Rol rolAdor = daoRol.findByRol("ADMINISTRADOR");
        a2.addRol(rolAdor);
        a2.setDocumento(d2);
        a2.setClave("123");
        a2.setActivo(true);
        //le agregamos el mismo rol
        a2.addRol(r);
        Domicilio domi2 = new Domicilio();
        domi2.setApartamento("0");
        domi2.setBarrio("Barrio");
        domi2.setBlock("block");
        domi2.setCalle("calle 14");
        domi2.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi2.setEsquina("calle 12");
        domi2.setEdificio("b");
        domi2.setLocalidad("Durazno");
        domi2.setManzanaSolar("N/A");
        domi2.setNumero("15224");
        domi2.setTorre("no");
        domi2.setId(domiDao.persistWithTransaction(tx, mng, domi2));
        // a2.setDomicilio(domi2);
        a2.addDomicilio(domi2);
        a2.setEmail("admin2@admin");
        //a2.setFaltas(0);
        //a2.setFechaNacimiento(Calendar.getInstance());
        a2.setFechaNacimiento("25/03/1978");
        a2.setPrimerApellido("Calcuta");
        a2.setPrimerNombre("TEresa");
        a2.setSegundoApellido("segunbdo");
        a2.setSegundoNombre("segunbdo");
        //guardamos el segundo alumno con la misma transaccion y el mismo manager y guardamos el id
        a2.setId(daoAdministrador.persistWithTransaction(tx, mng, a2));
        tx.commit();//guardamos los alumnos.
    }

    private void cargaDocentes() {
        EntityTransaction tx = mng.getTransaction();
        Rol r = daoRol.findByRol("DOCENTE");
        Cargo c = daoCargo.findByTipo("Docente tiempo completo");
        
        Documento d1 = new Documento();
        d1.setNumero(new Long(3330003));
        d1.setPaisEmisor("URUGUAY");
        d1.setTipo(Documento.TipoDocumento.CEDULA);

        Docente doc1 = new Docente();
        doc1.setActivo(true);
        doc1.setCargo(c);
        doc1.setClave("3330003");//la cedula
        doc1.setDocumento(d1);
        doc1.setEmail("elprofe@hotmail.com");
        doc1.setFechaNacimiento("15/08/1980");
        doc1.setNumeroEmpleado(10);
        doc1.setPrimerApellido("Bengoechea");
        doc1.setPrimerNombre("Pablo");
        doc1.setSegundoApellido("Curbelo");
        doc1.setSegundoNombre("Javier");
        Telefono t = new Telefono();
        t.setNumero("099198777");
        t.setObservaciones("No contesta nunca");
        t.setTipo(Telefono.TipoTelefono.MOVIL);
        doc1.addTelefono(t);
        doc1.addRol(r);

        //segundo docente
        Documento d2 = new Documento();
        d2.setNumero(new Long(123321));
        d2.setPaisEmisor("URUGUAY");
        d2.setTipo(Documento.TipoDocumento.CEDULA);

        Docente doc2 = new Docente();
        doc2.setActivo(true);
        doc2.setCargo(c);
        doc2.setClave("123321");//la cedula
        doc2.setDocumento(d2);
        doc2.setEmail("pao@hotmail.com");
        doc2.setFechaNacimiento("15/08/1980");
        doc2.setNumeroEmpleado(11);
        doc2.setPrimerApellido("Fernandez");
        doc2.setPrimerNombre("Paola");
        doc2.setSegundoApellido("Rodriguez");
        doc2.setSegundoNombre("");
        Telefono t2 = new Telefono();
        t2.setNumero("091198777");
        t2.setObservaciones("No contesta nunca");
        t2.setTipo(Telefono.TipoTelefono.MOVIL);
        doc2.addTelefono(t2);
        doc2.addRol(r);
        tx.begin();
//        assertTrue(daoDocente.save(doc2));
//        assertTrue(daoDocente.save(doc1));
        doc1.setId(daoDocente.persistWithTransaction(tx, mng, doc1));
        doc2.setId(daoDocente.persistWithTransaction(tx, mng, doc2));
        tx.commit();
        assertTrue(doc1.getId()!=0 && doc2.getId() != 0);

    }

    private void cargarGrupoYAlumnosParaPrueba() {
        EntityTransaction trx = mng.getTransaction();

        Clase clase = new Clase();
        clase.setCodigoClase('A');
        clase.setNivel(1);
        clase.setTipoDeClase(Clase.TipoClase.COMUN);

        Nivel niv = nivDao.findByClase(clase);

        Grupo grupo = daoGrup.findByNivelYAnio(niv, 2019);
        //busco rol
        Rol r = daoRol.findByRol("ESTUDIANTE");
        Rol rolTutor = daoRol.findByRol("ADULTO_RESPONSABLE");

        Documento docDocente = new Documento();
        docDocente.setNumero(new Long(Integer.valueOf("3330003")));
        docDocente.setPaisEmisor("uruguay");
        docDocente.setTipo(Documento.TipoDocumento.CEDULA);
        Docente docente = daoDocente.findByDocumento(docDocente);

        trx.begin();
        //creacion alumnos
        Alumno a1 = new Alumno();
        a1.addRol(r);
        Documento d1 = new Documento();
        d1.setNumero(new Long(90121212));
        d1.setPaisEmisor("URUGUAY");
        d1.setTipo(Documento.TipoDocumento.CEDULA);

        a1.setDocumento(d1);
        a1.setActivo(true);
        Domicilio domi = new Domicilio();
        domi.setApartamento("0");
        domi.setBarrio("Barrio");
        domi.setBlock("block");
        domi.setCalle("calle 13");
        domi.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi.setEsquina("calle 1");
        domi.setEdificio("");
        domi.setLocalidad("Durazno");
        domi.setManzanaSolar("N/A");
        domi.setNumero("1524");
        domi.setTorre("no");

        // a1.setDomicilio(domi);
        a1.addDomicilio(domi);
        a1.setEmail("alumTest1@mail");
        //a1.setFaltas(0);
        //a1.setFechaNacimiento(Calendar.getInstance());
        a1.setFechaNacimiento("24/08/2006");
        a1.setMatricula(90121212);
        a1.setPrimerApellido("lopez");
        a1.setPrimerNombre("gonzalo");
        a1.setSegundoApellido("segunbdo");
        a1.setSegundoNombre("segunbdo");
        a1.setClave("123");
        //tutor1
        AdultoResponsable ad1 = new AdultoResponsable();
        ad1.setActivo(true);
        ad1.addDomicilio(domi);
        ad1.addRol(rolTutor);
        ad1.setClave("123");
        Documento docAdult = new Documento();
        docAdult.setNumero(new Long(6565658));
        docAdult.setPaisEmisor("Uruguay");
        docAdult.setTipo(Documento.TipoDocumento.CEDULA);

        ad1.setDocumento(docAdult);

        ad1.setEmail("tutor1email@hotmail.com");
        ad1.setFechaNacimiento("12/03/1978");
        ad1.setPrimerApellido("Lopez");
        ad1.setPrimerNombre("Rodrigo");
        ad1.setSegundoApellido("Fernandez");
        ad1.setSegundoNombre("");

        Telefono telAdul1 = new Telefono();
        telAdul1.setNumero("099123456");
        telAdul1.setObservaciones("siempre ocupado");
        telAdul1.setTipo(Telefono.TipoTelefono.MOVIL);

        ad1.addTelefono(telAdul1);
        a1.addPadre(ad1);

        a1.setId(alumDao.persistWithTransaction(trx, mng, a1));
        assertTrue(a1.getId() != 0);
        //alumno 2

        Alumno a2 = new Alumno();
        a2.addRol(r);
        Documento d2 = new Documento();
        d2.setNumero(new Long(80121212));
        d2.setPaisEmisor("URUGUAY");
        d2.setTipo(Documento.TipoDocumento.CEDULA);

        a2.setDocumento(d2);
        a2.setActivo(true);
        Domicilio domi2 = new Domicilio();
        domi2.setApartamento("");
        domi2.setBarrio("Barrio 2");
        domi2.setBlock("block 2");
        domi2.setCalle("calle 113");
        domi2.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi2.setEsquina("calle 11");
        domi2.setEdificio("");
        domi2.setLocalidad("Durazno");
        domi2.setManzanaSolar("N/A");
        domi2.setNumero("1530");
        domi2.setTorre("no");

        a2.addDomicilio(domi2);
        a2.setEmail("alumTest2@mail");

        a2.setFechaNacimiento("24/07/2006");
        a2.setMatricula(80121212);
        a2.setPrimerApellido("Estevez");
        a2.setPrimerNombre("Carolina");
        a2.setSegundoApellido("Menendez");
        a2.setSegundoNombre("Jimena");
        a2.setClave("123");
        //tutor2
        AdultoResponsable ad2 = new AdultoResponsable();
        ad2.setActivo(true);
        ad2.addDomicilio(domi2);
        ad2.addRol(rolTutor);
        ad2.setClave("123");
        Documento docAdult2 = new Documento();
        docAdult2.setNumero(new Long(57665658));
        docAdult2.setPaisEmisor("Uruguay");
        docAdult2.setTipo(Documento.TipoDocumento.CEDULA);

        ad2.setDocumento(docAdult2);

        ad2.setEmail("tutor2email@hotmail.com");
        ad2.setFechaNacimiento("12/03/1980");
        ad2.setPrimerApellido("Menendez");
        ad2.setPrimerNombre("Natalia");
        ad2.setSegundoApellido("Fernandez");
        ad2.setSegundoNombre("Alejandra");
        ad2.setClave("123");
        Telefono telAdul2 = new Telefono();
        telAdul2.setNumero("098123456");
        telAdul2.setObservaciones("siempre ocupado");
        telAdul2.setTipo(Telefono.TipoTelefono.MOVIL);

        ad2.addTelefono(telAdul2);
        a2.addPadre(ad2);

        a2.setId(alumDao.persistWithTransaction(trx, mng, a2));
        assertTrue(a2.getId() != 0);
        //Alumno 3

        Alumno a3 = new Alumno();
        a3.addRol(r);
        Documento d3 = new Documento();
        d3.setNumero(new Long(70121212));
        d3.setPaisEmisor("URUGUAY");
        d3.setTipo(Documento.TipoDocumento.CEDULA);

        a3.setDocumento(d3);
        a3.setActivo(true);
        Domicilio domi3 = new Domicilio();
        domi3.setApartamento("");
        domi3.setBarrio("Barrio 3");
        domi3.setBlock("block 3");
        domi3.setCalle("calle 11");
        domi3.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi3.setEsquina("calle 13");
        domi3.setEdificio("");
        domi3.setLocalidad("Durazno");
        domi3.setManzanaSolar("N/A");
        domi3.setNumero("1520");
        domi3.setTorre("no");

        a3.addDomicilio(domi3);
        a3.setEmail("alumTest3@mail");

        a3.setFechaNacimiento("03/07/2006");
        a3.setMatricula(70121212);
        a3.setPrimerApellido("Birmino");
        a3.setPrimerNombre("Diana");
        a3.setSegundoApellido("Gutierrez");
        a3.setSegundoNombre("");
        a3.setClave("123");
        //tutor 3
        AdultoResponsable ad3 = new AdultoResponsable();
        ad3.setActivo(true);
        ad3.addDomicilio(domi3);
        ad3.addRol(rolTutor);
        ad3.setClave("123");
        Documento docAdult3 = new Documento();
        docAdult3.setNumero(new Long(33665588));
        docAdult3.setPaisEmisor("Uruguay");
        docAdult3.setTipo(Documento.TipoDocumento.CEDULA);

        ad3.setDocumento(docAdult3);

        ad3.setEmail("tutor3email@hotmail.com");
        ad3.setFechaNacimiento("12/03/1980");
        ad3.setPrimerApellido("Gutierrez");
        ad3.setPrimerNombre("Maria");
        ad3.setSegundoApellido("Fernandez");
        ad3.setSegundoNombre("Ines");
        ad3.setClave("123");
        Telefono telAdul3 = new Telefono();
        telAdul3.setNumero("092123456");
        telAdul3.setObservaciones("siempre ocupado");
        telAdul3.setTipo(Telefono.TipoTelefono.MOVIL);

        ad3.addTelefono(telAdul3);
        a3.addPadre(ad3);

        a3.setId(alumDao.persistWithTransaction(trx, mng, a3));
        assertTrue(a3.getId() != 0);
        //Alumno 4

        Alumno a4 = new Alumno();
        a4.addRol(r);
        Documento d4 = new Documento();
        d4.setNumero(new Long(50121212));
        d4.setPaisEmisor("URUGUAY");
        d4.setTipo(Documento.TipoDocumento.CEDULA);

        a4.setDocumento(d4);
        a4.setActivo(true);
        Domicilio domi4 = new Domicilio();
        domi4.setApartamento("");
        domi4.setBarrio("Barrio 4");
        domi4.setBlock("block 4");
        domi4.setCalle("calle 41");
        domi4.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi4.setEsquina("calle 43");
        domi4.setEdificio("");
        domi4.setLocalidad("Durazno");
        domi4.setManzanaSolar("N/A");
        domi4.setNumero("1524");
        domi4.setTorre("no");

        a4.addDomicilio(domi4);
        a4.setEmail("alumTest4@mail");

        a4.setFechaNacimiento("03/07/2006");
        a4.setMatricula(50121212);
        a4.setPrimerApellido("Jimenzez");
        a4.setPrimerNombre("Joaquin");
        a4.setSegundoApellido("Guti");
        a4.setSegundoNombre("Hernan");
        a4.setClave("123");
        //tutor 4
        AdultoResponsable ad4 = new AdultoResponsable();
        ad4.setActivo(true);
        ad4.addDomicilio(domi4);
        ad4.addRol(rolTutor);
        ad4.setClave("123");
        Documento docAdult4 = new Documento();
        docAdult4.setNumero(new Long(22265588));
        docAdult4.setPaisEmisor("Uruguay");
        docAdult4.setTipo(Documento.TipoDocumento.CEDULA);

        ad4.setDocumento(docAdult4);

        ad4.setEmail("tutor4email@hotmail.com");
        ad4.setFechaNacimiento("12/03/1980");
        ad4.setPrimerApellido("Guti");
        ad4.setPrimerNombre("Maria");
        ad4.setSegundoApellido("Ascona");
        ad4.setSegundoNombre("Jose");
        ad4.setClave("123");
        Telefono telAdul4 = new Telefono();
        telAdul4.setNumero("091123456");
        telAdul4.setObservaciones("siempre ocupado");
        telAdul4.setTipo(Telefono.TipoTelefono.MOVIL);

        ad4.addTelefono(telAdul4);
        a4.addPadre(ad4);

        a4.setId(alumDao.persistWithTransaction(trx, mng, a4));
        assertTrue(a4.getId() != 0);

        //cargo la asignatura
        Asignatura asignatura = new Asignatura();
        asignatura.setNombre(ProgramaPrimeroSegundoCBReformulacion2006.HISTORIA.getNombre());
        asignatura.setHorasSemanales(ProgramaPrimeroSegundoCBReformulacion2006.HISTORIA.getHoras());
        asignatura.setProgramaAnio(ProgramaPrimeroSegundoCBReformulacion2006.HISTORIA.getProgramaAnio());

        asignatura.setId(asigDao.persistWithTransaction(trx, mng, asignatura));

        grupo.addAlumno(a4);
        grupo.addAlumno(a3);
        grupo.addAlumno(a2);
        grupo.addAlumno(a1);

        daoGrup.updateWithTransaction(trx, mng, grupo);

        Libreta lib = new Libreta();
        lib.setDocente(docente);
        lib.setGrupo(grupo);
        lib.setMateria(asignatura);
        lib.setPractico(false);
        lib.setSalon("Salon 1");
        lib.setTeorico(true);
        lib.setTurno(Libreta.Turno.MATUTINO);

        Horario horario1 = new Horario();
        horario1.setDia(Horario.Dias.LUNES);
        horario1.setHoraFin("10:00");
        horario1.setHoraInicio("08:00");
        Horario horario2 = new Horario();
        horario2.setDia(Horario.Dias.MIERCOLES);
        horario2.setHoraFin("10:00");
        horario2.setHoraInicio("08:00");

        lib.addHorario(horario1);
        lib.addHorario(horario2);
        lib.setId(libDao.persistWithTransaction(trx, mng, lib));

        //agrego los auxiliares de matricula.
        AuxiliarMatricula aux1 = new AuxiliarMatricula();
        aux1.setAlumno(a1);
        ItemAuxiliarMatricula item1 = new ItemAuxiliarMatricula();

        item1.setNivel(niv);
        aux1.addItem(item1);
        aux1.setId(auxDao.persistWithTransaction(trx, mng, aux1));
        AuxiliarMatricula aux2 = new AuxiliarMatricula();
        aux2.setAlumno(a2);
        ItemAuxiliarMatricula item2 = new ItemAuxiliarMatricula();

        item2.setNivel(niv);
        aux2.addItem(item2);
        aux2.setId(auxDao.persistWithTransaction(trx, mng, aux2));
        AuxiliarMatricula aux3 = new AuxiliarMatricula();
        aux3.setAlumno(a3);
        ItemAuxiliarMatricula item3 = new ItemAuxiliarMatricula();

        item3.setNivel(niv);
        aux3.addItem(item3);
        aux3.setId(auxDao.persistWithTransaction(trx, mng, aux3));
        AuxiliarMatricula aux4 = new AuxiliarMatricula();
        aux4.setAlumno(a4);
        ItemAuxiliarMatricula item4 = new ItemAuxiliarMatricula();

        item4.setNivel(niv);
        aux4.addItem(item4);
        aux4.setId(auxDao.persistWithTransaction(trx, mng, aux4));

        trx.commit();
        assertTrue(lib.getId() != 0);
    }

    private void cargarNuevoDocenteAlgrupoPrimero() {
        EntityTransaction trx = mng.getTransaction();

        Clase clase = new Clase();
        clase.setCodigoClase('A');
        clase.setNivel(1);
        clase.setTipoDeClase(Clase.TipoClase.COMUN);

        Nivel niv = nivDao.findByClase(clase);

        Grupo grupo = daoGrup.findByNivelYAnio(niv, 2019);

        Documento docDocente = new Documento();
        docDocente.setNumero(new Long(Integer.valueOf("123321")));
        docDocente.setPaisEmisor("uruguay");
        docDocente.setTipo(Documento.TipoDocumento.CEDULA);
        Docente docente = daoDocente.findByDocumento(docDocente);
        trx.begin();
        //cargo la asignatura
        Asignatura asignatura = new Asignatura();
        asignatura.setNombre(ProgramaPrimeroSegundoCBReformulacion2006.IDIOMA_ESPANOL.getNombre());
        asignatura.setHorasSemanales(ProgramaPrimeroSegundoCBReformulacion2006.IDIOMA_ESPANOL.getHoras());
        asignatura.setProgramaAnio(ProgramaPrimeroSegundoCBReformulacion2006.IDIOMA_ESPANOL.getProgramaAnio());

        asignatura.setId(asigDao.persistWithTransaction(trx, mng, asignatura));

        Libreta lib = new Libreta();
        lib.setDocente(docente);
        lib.setGrupo(grupo);
        lib.setMateria(asignatura);
        lib.setPractico(false);
        lib.setSalon("Salon 1");
        lib.setTeorico(true);
        lib.setTurno(Libreta.Turno.MATUTINO);

        Horario horario1 = new Horario();
        horario1.setDia(Horario.Dias.MARTES);
        horario1.setHoraFin("10:00");
        horario1.setHoraInicio("08:00");
        Horario horario2 = new Horario();
        horario2.setDia(Horario.Dias.VIERNES);
        horario2.setHoraFin("10:00");
        horario2.setHoraInicio("08:00");

        lib.addHorario(horario1);
        lib.addHorario(horario2);
        lib.setId(libDao.persistWithTransaction(trx, mng, lib));

        trx.commit();
        Assert.assertTrue(lib.getId() != 0);
    }

    private void cargaNuevoGrupoAlDocenteDistintoNivel() {
        EntityTransaction trx = mng.getTransaction();

        Clase clase = new Clase();
        clase.setCodigoClase('B');
        clase.setNivel(4);
        clase.setTipoDeClase(Clase.TipoClase.COMUN);

        Nivel niv = nivDao.findByClase(clase);

        Grupo grupo = daoGrup.findByNivelYAnio(niv, 2019);
        //busco rol
        Rol r = daoRol.findByRol("ESTUDIANTE");
        Rol rolTutor = daoRol.findByRol("ADULTO_RESPONSABLE");

        Documento docDocente = new Documento();
        docDocente.setNumero(new Long(Integer.valueOf("3330003")));
        docDocente.setPaisEmisor("uruguay");
        docDocente.setTipo(Documento.TipoDocumento.CEDULA);
        Docente docente = daoDocente.findByDocumento(docDocente);

        trx.begin();
        //creacion alumnos

        //creacion alumnos
        Alumno a1 = new Alumno();
        a1.addRol(r);
        Documento d1 = new Documento();
        d1.setNumero(new Long(910000));
        d1.setPaisEmisor("URUGUAY");
        d1.setTipo(Documento.TipoDocumento.CEDULA);

        a1.setDocumento(d1);
        a1.setActivo(true);
        Domicilio domi = new Domicilio();
        domi.setApartamento("0");
        domi.setBarrio("Barrio");
        domi.setBlock("block");
        domi.setCalle("calle 13");
        domi.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi.setEsquina("calle 1");
        domi.setEdificio("");
        domi.setLocalidad("Durazno");
        domi.setManzanaSolar("N/A");
        domi.setNumero("1524");
        domi.setTorre("no");

        // a1.setDomicilio(domi);
        a1.addDomicilio(domi);
        a1.setEmail("alumCuarto1@mail");
        //a1.setFaltas(0);
        //a1.setFechaNacimiento(Calendar.getInstance());
        a1.setFechaNacimiento("24/08/2006");
        a1.setMatricula(91000);
        a1.setPrimerApellido("lopez");
        a1.setPrimerNombre("Jimena");
        a1.setSegundoApellido("segunbdo");
        a1.setSegundoNombre("segunbdo");
        a1.setClave("123");
        //tutor1
        AdultoResponsable ad1 = new AdultoResponsable();
        ad1.setActivo(true);
        ad1.addDomicilio(domi);
        ad1.addRol(rolTutor);
        ad1.setClave("123");
        Documento docAdult = new Documento();
        docAdult.setNumero(new Long(912000));
        docAdult.setPaisEmisor("Uruguay");
        docAdult.setTipo(Documento.TipoDocumento.CEDULA);

        ad1.setDocumento(docAdult);

        ad1.setEmail("tutorAlumCuarto@hotmail.com");
        ad1.setFechaNacimiento("12/03/1978");
        ad1.setPrimerApellido("Sosa");
        ad1.setPrimerNombre("Ruben");
        ad1.setSegundoApellido("Fernandez");
        ad1.setSegundoNombre("");

        Telefono telAdul1 = new Telefono();
        telAdul1.setNumero("0991234566");
        telAdul1.setObservaciones("siempre ocupado");
        telAdul1.setTipo(Telefono.TipoTelefono.MOVIL);

        ad1.addTelefono(telAdul1);
        a1.addPadre(ad1);

        a1.setId(alumDao.persistWithTransaction(trx, mng, a1));
        assertTrue(a1.getId() != 0);
        //alumno 2

        Alumno a2 = new Alumno();
        a2.addRol(r);
        Documento d2 = new Documento();
        d2.setNumero(new Long(92000));
        d2.setPaisEmisor("URUGUAY");
        d2.setTipo(Documento.TipoDocumento.CEDULA);

        a2.setDocumento(d2);
        a2.setActivo(true);
        Domicilio domi2 = new Domicilio();
        domi2.setApartamento("");
        domi2.setBarrio("Barrio 2");
        domi2.setBlock("block 2");
        domi2.setCalle("calle 113");
        domi2.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi2.setEsquina("calle 11");
        domi2.setEdificio("");
        domi2.setLocalidad("Durazno");
        domi2.setManzanaSolar("N/A");
        domi2.setNumero("1530");
        domi2.setTorre("no");

        a2.addDomicilio(domi2);
        a2.setEmail("alumCuartoTest2@mail");

        a2.setFechaNacimiento("24/07/2006");
        a2.setMatricula(92000);
        a2.setPrimerApellido("Rivero");
        a2.setPrimerNombre("Soledad");
        a2.setSegundoApellido("Menendez");
        a2.setSegundoNombre("Jimena");
        a2.setClave("123");
        //tutor2
        AdultoResponsable ad2 = new AdultoResponsable();
        ad2.setActivo(true);
        ad2.addDomicilio(domi2);
        ad2.addRol(rolTutor);
        ad2.setClave("123");
        Documento docAdult2 = new Documento();
        docAdult2.setNumero(new Long(922000));
        docAdult2.setPaisEmisor("Uruguay");
        docAdult2.setTipo(Documento.TipoDocumento.CEDULA);

        ad2.setDocumento(docAdult2);

        ad2.setEmail("tutor2AlumCuarto@hotmail.com");
        ad2.setFechaNacimiento("12/03/1980");
        ad2.setPrimerApellido("Menendez");
        ad2.setPrimerNombre("Maria");
        ad2.setSegundoApellido("Fernandez");
        ad2.setSegundoNombre("Alejandra");
        ad2.setClave("123");
        Telefono telAdul2 = new Telefono();
        telAdul2.setNumero("0981234566");
        telAdul2.setObservaciones("siempre ocupado");
        telAdul2.setTipo(Telefono.TipoTelefono.MOVIL);

        ad2.addTelefono(telAdul2);
        a2.addPadre(ad2);

        a2.setId(alumDao.persistWithTransaction(trx, mng, a2));
        assertTrue(a2.getId() != 0);
        //Alumno 3

        Alumno a3 = new Alumno();
        a3.addRol(r);
        Documento d3 = new Documento();
        d3.setNumero(new Long(93000));
        d3.setPaisEmisor("URUGUAY");
        d3.setTipo(Documento.TipoDocumento.CEDULA);

        a3.setDocumento(d3);
        a3.setActivo(true);
        Domicilio domi3 = new Domicilio();
        domi3.setApartamento("");
        domi3.setBarrio("Barrio 3");
        domi3.setBlock("block 3");
        domi3.setCalle("calle 11");
        domi3.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi3.setEsquina("calle 13");
        domi3.setEdificio("");
        domi3.setLocalidad("Durazno");
        domi3.setManzanaSolar("N/A");
        domi3.setNumero("1520");
        domi3.setTorre("no");

        a3.addDomicilio(domi3);
        a3.setEmail("alumCuarto3@mail");

        a3.setFechaNacimiento("03/07/2006");
        a3.setMatricula(93000);
        a3.setPrimerApellido("Sanchez");
        a3.setPrimerNombre("Eduardo");
        a3.setSegundoApellido("Gutierrez");
        a3.setSegundoNombre("");
        a3.setClave("123");
        //tutor 3
        AdultoResponsable ad3 = new AdultoResponsable();
        ad3.setActivo(true);
        ad3.addDomicilio(domi3);
        ad3.addRol(rolTutor);
        ad3.setClave("123");
        Documento docAdult3 = new Documento();
        docAdult3.setNumero(new Long(931000));
        docAdult3.setPaisEmisor("Uruguay");
        docAdult3.setTipo(Documento.TipoDocumento.CEDULA);

        ad3.setDocumento(docAdult3);

        ad3.setEmail("tutor3alumCuarto@hotmail.com");
        ad3.setFechaNacimiento("12/03/1980");
        ad3.setPrimerApellido("Gutierrez");
        ad3.setPrimerNombre("Maria");
        ad3.setSegundoApellido("Fernandez");
        ad3.setSegundoNombre("Ines");
        ad3.setClave("123");
        Telefono telAdul3 = new Telefono();
        telAdul3.setNumero("0921223456");
        telAdul3.setObservaciones("siempre ocupado");
        telAdul3.setTipo(Telefono.TipoTelefono.MOVIL);

        ad3.addTelefono(telAdul3);
        a3.addPadre(ad3);

        a3.setId(alumDao.persistWithTransaction(trx, mng, a3));
        assertTrue(a3.getId() != 0);
        //Alumno 4

        Alumno a4 = new Alumno();
        a4.addRol(r);
        Documento d4 = new Documento();
        d4.setNumero(new Long(94000));
        d4.setPaisEmisor("URUGUAY");
        d4.setTipo(Documento.TipoDocumento.CEDULA);

        a4.setDocumento(d4);
        a4.setActivo(true);
        Domicilio domi4 = new Domicilio();
        domi4.setApartamento("");
        domi4.setBarrio("Barrio 4");
        domi4.setBlock("block 4");
        domi4.setCalle("calle 41");
        domi4.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi4.setEsquina("calle 43");
        domi4.setEdificio("");
        domi4.setLocalidad("Durazno");
        domi4.setManzanaSolar("N/A");
        domi4.setNumero("1524");
        domi4.setTorre("no");

        a4.addDomicilio(domi4);
        a4.setEmail("alumCuartoTest4@mail");

        a4.setFechaNacimiento("03/07/2006");
        a4.setMatricula(94000);
        a4.setPrimerApellido("Estavillo");
        a4.setPrimerNombre("Francisco");
        a4.setSegundoApellido("Rodriguez");
        a4.setSegundoNombre("Hernan");
        a4.setClave("123");
        //tutor 4
        AdultoResponsable ad4 = new AdultoResponsable();
        ad4.setActivo(true);
        ad4.addDomicilio(domi4);
        ad4.addRol(rolTutor);
        ad4.setClave("123");
        Documento docAdult4 = new Documento();
        docAdult4.setNumero(new Long(941000));
        docAdult4.setPaisEmisor("Uruguay");
        docAdult4.setTipo(Documento.TipoDocumento.CEDULA);

        ad4.setDocumento(docAdult4);

        ad4.setEmail("tutor4AlumCuarto@hotmail.com");
        ad4.setFechaNacimiento("12/03/1980");
        ad4.setPrimerApellido("Estavillo");
        ad4.setPrimerNombre("Pablo");
        ad4.setSegundoApellido("Ascona");
        ad4.setSegundoNombre("Andres");
        ad4.setClave("123");
        Telefono telAdul4 = new Telefono();
        telAdul4.setNumero("0911123456");
        telAdul4.setObservaciones("siempre ocupado");
        telAdul4.setTipo(Telefono.TipoTelefono.MOVIL);

        ad4.addTelefono(telAdul4);
        a4.addPadre(ad4);

        a4.setId(alumDao.persistWithTransaction(trx, mng, a4));
        assertTrue(a4.getId() != 0);

        //cargo la asignatura
        Asignatura asignatura = new Asignatura();
        asignatura.setNombre(ProgramaCuartoRef2006.LITERATURA.getNombre());
        asignatura.setHorasSemanales(ProgramaCuartoRef2006.LITERATURA.getHoras());
        asignatura.setProgramaAnio(ProgramaCuartoRef2006.LITERATURA.getProgramaAnio());

        asignatura.setId(asigDao.persistWithTransaction(trx, mng, asignatura));

        grupo.addAlumno(a4);
        grupo.addAlumno(a3);
        grupo.addAlumno(a2);
        grupo.addAlumno(a1);

        daoGrup.updateWithTransaction(trx, mng, grupo);

        Libreta lib = new Libreta();
        lib.setDocente(docente);
        lib.setGrupo(grupo);
        lib.setMateria(asignatura);
        lib.setPractico(false);
        lib.setSalon("Salon 1");
        lib.setTeorico(true);
        lib.setTurno(Libreta.Turno.MATUTINO);

        Horario horario1 = new Horario();
        horario1.setDia(Horario.Dias.MARTES);
        horario1.setHoraFin("12:00");
        horario1.setHoraInicio("10:00");
        Horario horario2 = new Horario();
        horario2.setDia(Horario.Dias.JUEVES);
        horario2.setHoraFin("10:00");
        horario2.setHoraInicio("08:00");

        lib.addHorario(horario1);
        lib.addHorario(horario2);
        lib.setId(libDao.persistWithTransaction(trx, mng, lib));

        //agrego los auxiliares de matricula.
        AuxiliarMatricula aux1 = new AuxiliarMatricula();
        aux1.setAlumno(a1);
        ItemAuxiliarMatricula item1 = new ItemAuxiliarMatricula();

        item1.setNivel(niv);
        aux1.addItem(item1);
        aux1.setId(auxDao.persistWithTransaction(trx, mng, aux1));
        AuxiliarMatricula aux2 = new AuxiliarMatricula();
        aux2.setAlumno(a2);
        ItemAuxiliarMatricula item2 = new ItemAuxiliarMatricula();

        item2.setNivel(niv);
        aux2.addItem(item2);
        aux2.setId(auxDao.persistWithTransaction(trx, mng, aux2));
        AuxiliarMatricula aux3 = new AuxiliarMatricula();
        aux3.setAlumno(a3);
        ItemAuxiliarMatricula item3 = new ItemAuxiliarMatricula();

        item3.setNivel(niv);
        aux3.addItem(item3);
        aux3.setId(auxDao.persistWithTransaction(trx, mng, aux3));
        AuxiliarMatricula aux4 = new AuxiliarMatricula();
        aux4.setAlumno(a4);
        ItemAuxiliarMatricula item4 = new ItemAuxiliarMatricula();

        item4.setNivel(niv);
        aux4.addItem(item4);
        aux4.setId(auxDao.persistWithTransaction(trx, mng, aux4));
        trx.commit();
        assertTrue(lib.getId() != 0);
    }
}
