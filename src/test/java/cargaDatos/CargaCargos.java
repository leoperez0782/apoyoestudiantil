/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.CargoDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.entity.Cargo;
import javax.persistence.EntityManager;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author leo
 */
public class CargaCargos {
    
    public CargaCargos() {
    }
    @Test
    public void cargarCargos(){
        EntityManager mng = ContextoDB.getInstancia().getManager();
        
        CargoDAO dao = new CargoDAO(mng);
        
        Cargo c = new Cargo();
        //c.setActivo(true);
        c.setDescripcion("Tareas administrativas");
        c.setTipoCargo("Administrativo");
        
        boolean agrego = dao.save(c);
        assertTrue(agrego);
        
         Cargo c1 = new Cargo();
        //c.setActivo(true);
        c1.setDescripcion("8 horas");
        c1.setTipoCargo("Docente tiempo completo");
        
        boolean agrego1 = dao.save(c1);
        assertTrue(agrego1);
        
         Cargo c2 = new Cargo();
        //c.setActivo(true);
        c2.setDescripcion("4 horas");
        c2.setTipoCargo("Docente medio tiempo");
        
        boolean agrego2 = dao.save(c2);
        assertTrue(agrego2);
        
         Cargo c3 = new Cargo();
        //c.setActivo(true);
        c3.setDescripcion("Direccion");
        c3.setTipoCargo("Directivo");
        
        boolean agrego3 = dao.save(c3);
        assertTrue(agrego3);
    }
}
