/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.NivelDAO;
import com.apoyoestudiantil.entity.Clase;
import com.apoyoestudiantil.entity.Nivel;
import java.util.Calendar;
import java.util.Locale;
import org.junit.Assert;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author leo
 */
public class CargaNivel {
    
    public CargaNivel() {
    }
    @Test
    public void cargarNivel(){
        NivelDAO dao = new NivelDAO();
        Nivel niv = new Nivel();
        Clase clase = new Clase();
        clase.setCodigoClase('A');
        clase.setNivel(1);
        clase.setTipoDeClase(Clase.TipoClase.COMUN);
        
        niv.setClase(clase);
        niv.setFechaCreacion(Calendar.getInstance());
        niv.setEstado(Nivel.Estado.ACTIVO);
        
        boolean agrego = dao.save(niv);
        
        Nivel niv2 = new Nivel();
        Clase clase2 = new Clase();
        clase2.setCodigoClase('B');
        clase2.setNivel(4);
        clase2.setTipoDeClase(Clase.TipoClase.COMUN);
        
        niv2.setClase(clase2);
        niv2.setFechaCreacion(Calendar.getInstance());
        niv2.setEstado(Nivel.Estado.ACTIVO);
        boolean agrego2 = dao.save(niv2);
        assertTrue(agrego && agrego2);
    }
    
    
}
