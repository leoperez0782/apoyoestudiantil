/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.AdultoResponsableDAO;
import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.excepciones.AppException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Leonardo Pérez
 */
public class PruebaCargaHijosDeTutor {
    
    public PruebaCargaHijosDeTutor() {
    }
    
    
     @Test
     public void encontrarHijos() {
        try {
            EntityManager mng = ContextoDB.getInstancia().getManager();
            AdultoResponsableDAO daoAdulto = new AdultoResponsableDAO(mng);
            AlumnoDAO daoAlumno = new AlumnoDAO(mng);
            
            AdultoResponsable tutor = daoAdulto.findByNumeroDoc(new Long(22265588)); //tiene un solo hijo con num doc 50121212.
            
            List<Alumno> hijos = daoAlumno.getAlumnosByTutor(tutor);
            Documento doc = new Documento();
            doc.setNumero(new Long (50121212));
            doc.setPaisEmisor("URUGUAY");
            doc.setTipo(Documento.TipoDocumento.CEDULA);
            Assert.assertTrue(hijos.get(0).getDocumento().equals(doc));
        } catch (AppException ex) {
            Logger.getLogger(PruebaCargaHijosDeTutor.class.getName()).log(Level.SEVERE, null, ex);
        }
         
     }
}
