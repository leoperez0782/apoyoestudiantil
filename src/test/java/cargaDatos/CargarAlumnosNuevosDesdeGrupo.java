/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.GrupoDAO;
import com.apoyoestudiantil.dao.NivelDAO;
import com.apoyoestudiantil.dao.RolDAO;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Clase;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.entity.Rol;
import java.util.Calendar;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.junit.Test;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author leo
 */
public class CargarAlumnosNuevosDesdeGrupo {
    
    public CargarAlumnosNuevosDesdeGrupo() {
    }
    @Test
    public void cargarNuevoAlumnoDesdeGrupo(){
        
        EntityManager manager = ContextoDB.getInstancia().getManager();
        EntityTransaction tx = manager.getTransaction();
        NivelDAO daoNiv = new NivelDAO(manager);
        GrupoDAO daoGrup = new GrupoDAO(manager);
        AlumnoDAO daoAlumn = new AlumnoDAO(manager);
        Clase clase = new Clase();
        clase.setCodigoClase('A');
        clase.setNivel(1);
        clase.setTipoDeClase(Clase.TipoClase.COMUN);
        //buscamos el nivel
        Nivel niv = daoNiv.findByClase(clase);
        //buscamos el grupo
        Grupo grupo = daoGrup.findByNivelYAnio(niv, 2019);
        
        RolDAO roldao = new RolDAO(manager);
        //buscamos el rol.
        Rol r = roldao.findByRol("ESTUDIANTE");
        
        Alumno a1 = new Alumno();
        a1.addRol(r);
        Documento d1 = new Documento();
        d1.setNumero(new Long(1234567));
        d1.setPaisEmisor("URUGUAY");
        d1.setTipo(Documento.TipoDocumento.CEDULA);

        a1.setDocumento(d1);
        Domicilio domi = new Domicilio();
        domi.setApartamento("0");
        domi.setBarrio("Barrio");
        domi.setBlock("block");
        domi.setCalle("calle 13");
        domi.setDepartamento(Domicilio.Departamento.DURAZNO);
        domi.setEsquina("calle 1");
        domi.setEdificio("");
        domi.setLocalidad("Durazno");
        domi.setManzanaSolar("N/A");
        domi.setNumero("1524");
        domi.setTorre("no");

       // a1.setDomicilio(domi);
         a1.addDomicilio(domi);
        a1.setEmail("emaila1234567@mail");
        //a1.setFaltas(0);
        //a1.setFechaNacimiento(Calendar.getInstance());
        a1.setFechaNacimiento("24/08/2006");
        a1.setMatricula(1234567);
        a1.setPrimerApellido("lopez");
        a1.setPrimerNombre("memo");
        a1.setSegundoApellido("segunbdo");
        a1.setSegundoNombre("segunbdo");
        tx.begin();
        a1.setId(daoAlumn.persistWithTransaction(tx, manager, a1));
        grupo.addAlumno(a1);
        tx.commit();
        boolean agrego;
        agrego = daoGrup.update(grupo);//esto deberia ser en la transaccion tambien
        
        assertTrue(agrego);
    }
}
