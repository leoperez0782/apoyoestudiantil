/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.DomicilioDAO;
import com.apoyoestudiantil.dao.RolDAO;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Rol;
import java.util.Calendar;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import static org.junit.Assert.assertTrue;
import org.junit.Test;

/**
 *
 * @author leo
 */
public class CargarAlumnoYDomicilio {

    public CargarAlumnoYDomicilio() {
    }

//    @Test
//    public void cargarAlumnoYDomicilio() {
//        EntityManager manager = ContextoDB.getInstancia().getManager();
//        EntityTransaction tx = manager.getTransaction();
//        tx.begin();
//        //todos comparten el mismo manager.
//        RolDAO daoRol = new RolDAO(manager);
//        AlumnoDAO daoAlumno = new AlumnoDAO(manager);
//        DomicilioDAO daoDomi = new DomicilioDAO(manager);
//
//        Domicilio domi = new Domicilio();
//        domi.setApartamento("0");
//        domi.setBarrio("Barrio");
//        domi.setBlock("block");
//        domi.setCalle("calle 13");
//        domi.setDepartamento(Domicilio.Departamento.CANELONES);
//        domi.setEsquina("calle 1");
//        domi.setIsEdificio(false);
//        domi.setLocalidad("Las vegas");
//        domi.setManzanaSolar("N/A");
//        domi.setNumero("1524");
//        domi.setTorre("no");
//        //Primero agregamos el domimcilio si no marca error a guardar el alimno por que no existe la pk del domicilio.
//        //assertTrue(daoDomi.save(domi));
//        domi.setId(daoDomi.persistWithTransaction(tx, manager, domi));
//        //Buscamos el rol que esta guardado en la bd.
//        Rol r = daoRol.findByRol("ESTUDIANTE");
//
//        Alumno a1 = new Alumno();
//        a1.addRol(r);
//        Documento d1 = new Documento();
//        d1.setNumero(new Long(1234560));
//        d1.setPaisEmisor("URUGUAY");
//        d1.setTipo(Documento.TipoDocumento.CEDULA);
//        a1.setDocumento(d1);
//        a1.setDomicilio(domi);
//        a1.setEmail("emaila1250@mail");
//        a1.setFaltas(0);
//        a1.setFechaNacimiento(Calendar.getInstance());
//        a1.setMatricula(1234560);
//        a1.setPrimerApellido("lopez");
//        a1.setPrimerNombre("pancho");
//        a1.setSegundoApellido("segunbdo");
//        a1.setSegundoNombre("segunbdo");
//
//        //boolean agrego = daoAlumno.save(a1);
//        assertTrue(daoAlumno.persistWithTransaction(tx, manager, a1) != null);
//
//        //agregamos otro
//        Domicilio domi2 = new Domicilio();
//        domi2.setApartamento("0");
//        domi2.setBarrio("Barrio");
//        domi2.setBlock("block");
//        domi2.setCalle("calle 15");
//        domi2.setDepartamento(Domicilio.Departamento.CANELONES);
//        domi2.setEsquina("calle 10");
//        domi2.setIsEdificio(false);
//        domi2.setLocalidad("Las vegas");
//        domi2.setManzanaSolar("N/A");
//        domi2.setNumero("154");
//        domi2.setTorre("no");
//        //Primero agregamos el domimcilio si no marca error a guardar el alimno por que no existe la pk del domicilio.
//        //assertTrue(daoDomi.save(domi2));
//        domi2.setId(daoDomi.persistWithTransaction(tx, manager, domi2));
//        Alumno a2 = new Alumno();
//        a2.addRol(r);//el rol es el mismo
//        Documento d2 = new Documento();
//        d2.setNumero(new Long(12345678));
//        d2.setPaisEmisor("URUGUAY");
//        d2.setTipo(Documento.TipoDocumento.CEDULA);
//        a2.setDocumento(d2);
//        a2.setDomicilio(domi2);
//        a2.setEmail("emaila12578@mail");
//        a2.setFaltas(0);
//        a2.setFechaNacimiento(Calendar.getInstance());
//        a2.setMatricula(12345678);
//        a2.setPrimerApellido("torres");
//        a2.setPrimerNombre("tito");
//        a2.setSegundoApellido("segunbdo");
//        a2.setSegundoNombre("segunbdo");
//
//        //agrego = daoAlumno.save(a2);
//        //assertTrue(agrego);
//        assertTrue(daoAlumno.persistWithTransaction(tx, manager, a1) != null);
//        tx.commit();
//    }

    @Test
    public void pruebaCArgarConDomicilioOneToOne() {
        EntityManager manager = ContextoDB.getInstancia().getManager();
        RolDAO daoRol = new RolDAO(manager);
        AlumnoDAO daoAlumno = new AlumnoDAO(manager);
        Domicilio domi = new Domicilio();
        domi.setApartamento("0");
        domi.setBarrio("Barrio");
        domi.setBlock("block");
        domi.setCalle("calle 13");
        domi.setDepartamento(Domicilio.Departamento.CANELONES);
        domi.setEsquina("calle 1");
        domi.setEdificio("");
        domi.setLocalidad("Las vegas");
        domi.setManzanaSolar("N/A");
        domi.setNumero("1524");
        domi.setTorre("no");
        Rol r = daoRol.findByRol("ESTUDIANTE");

        Alumno a1 = new Alumno();
        a1.addRol(r);
        Documento d1 = new Documento();
        d1.setNumero(new Long(1234560));
        d1.setPaisEmisor("URUGUAY");
        d1.setTipo(Documento.TipoDocumento.CEDULA);
        a1.setDocumento(d1);
        //a1.setDomicilio(domi);
         a1.addDomicilio(domi);
        a1.setEmail("emaila1250@mail");
        //a1.setFaltas(0);
        //a1.setFechaNacimiento(Calendar.getInstance());
        a1.setFechaNacimiento("15/05/2005");
        a1.setMatricula(1234560);
        a1.setPrimerApellido("lopez");
        a1.setPrimerNombre("pancho");
        a1.setSegundoApellido("segunbdo");
        a1.setSegundoNombre("segunbdo");
        //a1.setDomicilio(domi);
        
        boolean agrego = daoAlumno.save(a1);
        assertTrue(agrego);
    }
}
