/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.GrupoDAO;
import com.apoyoestudiantil.dao.NivelDAO;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Clase;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Nivel;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author leo
 */
public class TraerAlumnoGuardadosYAsignarlosAGrupo {

    public TraerAlumnoGuardadosYAsignarlosAGrupo() {
    }

    @Test
    public void cargarGrupoConAlumnos() {
        EntityManager manager = ContextoDB.getInstancia().getManager();

        NivelDAO daoNiv = new NivelDAO(manager);
        GrupoDAO daoGrup = new GrupoDAO(manager);

        Clase clase = new Clase();
        clase.setCodigoClase('A');
        clase.setNivel(1);
        clase.setTipoDeClase(Clase.TipoClase.COMUN);
        //buscamos el nivel
        Nivel niv = daoNiv.findByClase(clase);
        //buscamos el grupo
        Grupo grupo = daoGrup.findByNivelYAnio(niv, 2019);
        //los alumnos se cargaron con el nivel
        List<Alumno> alumnos = niv.getAlumnos();
        //agregamos cada alumno al grupo
        alumnos.forEach((a) -> {
            grupo.addAlumno(a);
        });
        //actualizamos el grupo.
        boolean agrego = daoGrup.update(grupo);

        Assert.assertTrue(agrego);
    }
}
