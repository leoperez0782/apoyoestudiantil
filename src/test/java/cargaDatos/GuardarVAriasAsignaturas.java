/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cargaDatos;

import com.apoyoestudiantil.dao.AsignaturaDAO;
import com.apoyoestudiantil.entity.Asignatura;
import com.apoyoestudiantil.entity.ProgramaCuartoRef2006;
import java.util.ArrayList;
import java.util.List;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author leo
 */
public class GuardarVAriasAsignaturas {

    public GuardarVAriasAsignaturas() {
    }

    @Test
    public void guardarVariasAsignaturas() {

        List<Asignatura> lista = new ArrayList<>();
        AsignaturaDAO dao = new AsignaturaDAO();
        int cantidadAntesDeInsert = dao.getCount();
        Asignatura a = new Asignatura();
        a.setNombre(ProgramaCuartoRef2006.EDUCACION_FISICA_Y_RECREACION.getNombre());
        a.setHorasSemanales(ProgramaCuartoRef2006.EDUCACION_FISICA_Y_RECREACION.getHoras());
        a.setProgramaAnio(ProgramaCuartoRef2006.EDUCACION_FISICA_Y_RECREACION.getProgramaAnio());
        lista.add(a);
        a = new Asignatura();
        a.setNombre(ProgramaCuartoRef2006.BIOLOGIA.getNombre());
        a.setHorasSemanales(ProgramaCuartoRef2006.BIOLOGIA.getHoras());
        a.setProgramaAnio(ProgramaCuartoRef2006.BIOLOGIA.getProgramaAnio());
        lista.add(a);

        a = new Asignatura();
        a.setNombre(ProgramaCuartoRef2006.DIBUJO.getNombre());
        a.setHorasSemanales(ProgramaCuartoRef2006.DIBUJO.getHoras());
        a.setProgramaAnio(ProgramaCuartoRef2006.DIBUJO.getProgramaAnio());
        lista.add(a);
       // dao.saveAll(lista);
        
        int actual = dao.getCount();
        int exp = cantidadAntesDeInsert + lista.size();
        
        assertEquals(exp, actual);
    }
}
