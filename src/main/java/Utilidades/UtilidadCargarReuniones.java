/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Utilidades;

import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.entity.PeriodoCes;
import com.apoyoestudiantil.entity.Reunion;
import java.util.ArrayList;
import java.util.List;

/**
 * @date 10-ago-2019
 * @time 3:59:10
 * @author Leonardo Pérez
 */
public class UtilidadCargarReuniones {

    public List<Reunion> crearReuniones(Libreta lib){
        List<Reunion> retorno = new ArrayList<>();
        List<PeriodoCes> listaPeriodos = lib.getPeriodos();
        
        for(PeriodoCes pe: listaPeriodos){
            Reunion r = new Reunion();
            r.asignarNombreSegunNombrePeriodo(lib.getMateria().getProgramaAnio(), pe.getNombre());
            r.setLibreta(lib);
            r.setPeriodoEnLibreta(pe.getNombre());
            retorno.add(r);
        }
        return retorno;
    }
}
