/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

import com.apoyoestudiantil.excepciones.AppException;
import com.lambdaworks.crypto.SCryptUtil;

/**
 * @date 31-may-2019
 * @time 19:14:09
 * @author Leonardo Pérez
 * Clase de utilidad que provee metodo para generar un
 * hash de un string. Al hash se le agrega salt utilizando scrypt.
 * @see <a href="https://howtodoinjava.com/security/how-to-generate-secure-password-hash-md5-sha-pbkdf2-bcrypt-examples/">
 * Java scrypt with salt example</a>
 */
public class UtilSeguridad {

    private final static int ITERACIONES = 16;

    public String crearClaveConSalt(String clave) throws AppException {

        String hash = SCryptUtil.scrypt(clave, ITERACIONES, 16,16 );
        return hash;
        
    }

    
    public boolean compararClave(String ingresada, String actual){
        return SCryptUtil.check(ingresada, actual);
    }
}
