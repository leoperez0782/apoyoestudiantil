/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Utilidades;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @date 24-jun-2019
 * @time 19:42:18
 * @author Leonardo Pérez
 */
public class ConvertidorFecha {

    public static Calendar convertirFecha(String fecha) throws ParseException{
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        Date date = sdf.parse(fecha);
        cal.setTime(date);
        return cal;
    }
    /**
     * cambia el formato recibido por el request de yyyy-mm-dd
     * al formato dd/mm/yyyy.
     * Sirve para que cambiar fecha no tire error.
     * @param fecha
     * @return 
     */
    public static String convertirString(String fecha){
        String[] partes = fecha.split("-");
        String ret ="";
        int largo =partes.length -1;
        for(int i = largo; i>=0 ; i--){
           if(i != 0){
                ret += partes[i] + "/";
           }else{
               ret+=partes[i];
           }
        }
        return ret;
    }
}
