/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Utilidades;

import com.apoyoestudiantil.entity.Horario;
import com.apoyoestudiantil.entity.Lista;
import com.apoyoestudiantil.excepciones.AppException;
import java.io.FileInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @date 12-jul-2019
 * @time 17:09:44
 * @author Leonardo Pérez
 */
public class UtilidadCargaListas {

    private String fechaFin;
    private String fechaInicio;

    public UtilidadCargaListas() throws AppException {
        cargarProperties();
    }

    private void cargarProperties() throws AppException {
        FileInputStream file = null;
        //try {
        //Properties props = new Properties();
//            URL url = this.getClass().getResource("aniolectivo.properties");
//            file = new FileInputStream("src/main/resources/aniolectivo.properties");
//            file = new FileInputStream(url.getFile());

        // props.load(file);
//            String propiedadFin = props.getProperty("fechaFin");
//            String propiedadComienzo = props.getProperty("fechaComienzo");
        this.fechaFin = "2019-12-09";//propiedadFin;
        this.fechaInicio = "2019-03-01";//propiedadComienzo;

//        } catch (FileNotFoundException ex) {
//            Logger.getLogger(UtilidadCargaListas.class.getName()).log(Level.SEVERE, null, ex);
//            throw new AppException("Error al cargar propiedades de fechas de inicio/fin");
//        } catch (IOException ex) {
//            Logger.getLogger(UtilidadCargaListas.class.getName()).log(Level.SEVERE, null, ex);
//            throw new AppException("Error al cargar propiedades de fechas de inicio/fin");
//        } finally {
//            try {
//                file.close();
//            } catch (IOException ex) {
//                Logger.getLogger(UtilidadCargaListas.class.getName()).log(Level.SEVERE, null, ex);
//                throw new AppException("Error al cargar propiedades de fechas de inicio/fin");
//            }
//        }
    }

    /**
     * Crea una lista ordenada por fecha, de Objetos Lista vacios, con las
     * fechas correspondientes a los dia de clase que se encuentran como
     * parametro en cada horario.
     *
     * @param horarios
     * @return
     * @throws AppException
     */
    public List<Lista> cargarListasVacias(List<Horario> horarios) throws AppException {
        try {
            List<Lista> listado = new ArrayList<>();
            SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            Date fechaIni = df.parse(this.fechaInicio);
            Date fechaFinal = df.parse(this.fechaFin);

            GregorianCalendar fechaCalendario = new GregorianCalendar();

            GregorianCalendar fechaTope = new GregorianCalendar();
            fechaTope.setTime(fechaFinal);
            int diaSemana;
            for (Horario horario : horarios) {
                fechaCalendario.setTime(fechaIni);
                while (!fechaCalendario.after(fechaTope)) {
                    diaSemana = fechaCalendario.get(Calendar.DAY_OF_WEEK);

                    if (horario.isDiaClase(diaSemana - 1)) {//le resto uno por que DAY_OF_WEEK comienza en uno y isDiaClase compara el indice del enum dias
                        Lista l = new Lista();
                        Calendar fecha = Calendar.getInstance();
                        fecha.setTime(fechaCalendario.getTime());
                        //l.setFecha(df.format(fechaCalendario.getTime()));
                        //l.setFecha(fechaCalendario);
                        l.setFecha(df.format(fecha.getTime()));
                        l.setHorario(horario);
                        l.setLibreta(horario.getLibreta());

                        listado.add(l);
                    }
                    fechaCalendario.add(Calendar.DATE, 1);
                }
            }
            Collections.sort(listado);
            return listado;
        } catch (ParseException ex) {
            Logger.getLogger(UtilidadCargaListas.class.getName()).log(Level.SEVERE, null, ex);
            throw new AppException("Error al cargar listas vacias");
        }
    }

}
