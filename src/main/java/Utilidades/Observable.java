/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Utilidades;

import Utilidades.Observador;
import java.util.ArrayList;

/**
 * @date 08-ago-2019
 * @time 5:01:42
 * @author Leonardo Pérez
 */
public class Observable {
    private ArrayList<Observador> lista = new ArrayList();
    
    public void agregar(Observador es){
        if(!lista.contains(es)){
            lista.add(es);
        }
    }
    public void quitar(Observador es){
        lista.remove(es);
    }
    public void avisar(Object evento) {
        ArrayList<Observador> tmp = new ArrayList(lista);
        for(Observador obs:tmp){
            obs.actualizar(evento, this);
        }
    }
}
