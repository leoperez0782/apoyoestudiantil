/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.servlets.listeners;

import com.mchange.v2.c3p0.C3P0Registry;
import com.mchange.v2.c3p0.PooledDataSource;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

/**
 * @date 5 sept. 2019
 * @time 19:22:09
 * @author Leonardo Pérez
 */
@WebListener
public class AppListener implements ServletContextListener {

    private Logger logger;

    @Override
    public void contextInitialized(ServletContextEvent sce) {

    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {

       Enumeration<Driver> drivers = DriverManager.getDrivers();
       Driver driver;
       String msj;
        System.out.println("Suprimiendo drivers");
       while(drivers.hasMoreElements()){
           try {
               driver =drivers.nextElement();
               DriverManager.deregisterDriver(driver);
               msj = String.format("suprimiendo driver %s", driver.toString());
               Logger.getLogger(AppListener.class.getName()).log(Level.SEVERE,msj );
           } catch (SQLException ex) {
               Logger.getLogger(AppListener.class.getName()).log(Level.SEVERE, null, ex);
           }
       }
       //cerrarConexiones(); //tira errores
    }

    private void cerrarConexiones() {
        System.out.println("Cerraando conexiones");
        PooledDataSource ds;
        Iterator<Set> it = C3P0Registry.getPooledDataSources().iterator();
        while (it.hasNext()) {
            try {
                ds = (PooledDataSource) it.next();
                String msj = String.format("cerrando conexion %s", ds.getDataSourceName());
                Logger.getLogger(AppListener.class.getName()).log(Level.SEVERE, msj);
                ds.close();
            } catch (SQLException ex) {
                Logger.getLogger(AppListener.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
