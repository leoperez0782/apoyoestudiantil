/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.servlets.filters;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * @date 11-ago-2019
 * @time 9:56:55
 * @author Leonardo Pérez Filtro para que la aplicación acepte codificación en
 * formato UTF-8
 * @see https://www.adictosaltrabajo.com/2007/02/16/tomcat-utf8/
 */
public class UTF8Filter implements Filter {

    private String encoding;

    /**
     * Recogemos el tipo de codificación definido en el web.xml Si no se hubiera
     * especificado ninguno se toma "UTF-8" por defecto
     */
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        encoding = filterConfig.getInitParameter("requestEncoding");
        if (encoding == null) {
            encoding = "UTF-8";
        }
    }

    /**
     * Metemos en la request el formato de codificacion UTF-8
     */
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {

            if (request != null && response != null && chain != null) {
                request.setCharacterEncoding(encoding);
                response.setCharacterEncoding(encoding);
                chain.doFilter(request, response);
            }
        } catch (Exception e) {
            Logger.getLogger(UTF8Filter.class.getName()).log(Level.SEVERE, null, e);
        }

    }

    @Override
    public void destroy() {

    }

}
