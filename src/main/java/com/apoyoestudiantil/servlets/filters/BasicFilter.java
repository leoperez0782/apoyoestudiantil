/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.servlets.filters;

import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.entity.Usuario_Rol;
import java.io.IOException;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @date 9 sept. 2019
 * @time 18:17:26
 * @author Leonardo Pérez
 */
public abstract class BasicFilter {

    /**
     * Revisa si el rol del usuario logueado, se corresponde con el indicado en
     * @param tipoUsuario. El valor de tipoUsuario debe venir en mayusculas;
     *
     * @param req
     * @param tipoUsuario
     * @return
     */
    public boolean isPermitido(HttpServletRequest req, String tipoUsuario) {
        boolean permitido = false;
        Usuario usu = (Usuario) req.getSession().getAttribute("usuario");
        for (Usuario_Rol ur : usu.getRoles()) {
            if (ur.getRol().getRol().equals(tipoUsuario)) {
                permitido = true;
                return permitido;
            }
        }
        return permitido;
    }

    public boolean isUsuarioLogueado(HttpServletRequest req) {
        return req.getSession().getAttribute("usuario") != null;
    }

    public void enviarError403(HttpServletResponse res, String mensaje) throws IOException {
        res.sendError(HttpServletResponse.SC_FORBIDDEN, mensaje);
    }

    public void filtrarUsuarioYRol(ServletRequest request, ServletResponse response, FilterChain chain, String tipoUsuario, String mensajeError) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;
        if (!isUsuarioLogueado(req)) {
            //req.getServletContext().getRequestDispatcher("index.jsp");
            res.sendRedirect(req.getContextPath() + "/index.jsp");
        } else {

            if (!isPermitido(req, tipoUsuario)) {
                enviarError403(res, mensajeError);
            }
        }
        chain.doFilter(request, response);
    }
}
