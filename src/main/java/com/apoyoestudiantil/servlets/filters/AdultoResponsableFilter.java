/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.servlets.filters;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

/**
 * @date 15 sept. 2019
 * @time 21:58:26
 * @author Leonardo Pérez
 */
public class AdultoResponsableFilter extends BasicFilter implements Filter{

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try{
            filtrarUsuarioYRol(request, response, chain, "ADULTO_RESPONSABLE", "No tiene acceso a la zona de Adultos responsables");
        }catch(Exception e){
            Logger.getLogger(DocenteFilter.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public void destroy() {
       
    }

}
