/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.servlets.filters;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @date 9 sept. 2019
 * @time 19:33:52
 * @author Leonardo Pérez
 */
public class AdministrativoFilter extends BasicFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        try {
            HttpServletRequest req = (HttpServletRequest) request;
            HttpServletResponse res = (HttpServletResponse) response;
            if (!isUsuarioLogueado(req)) {
                //req.getServletContext().getRequestDispatcher("index.jsp");
                res.sendRedirect(req.getContextPath() + "/index.jsp");
            }else{
                if(!isPermitido(req, "ADMINISTRATIVO") && !isPermitido(req, "ADMINISTRADOR")){
                    enviarError403(res, "Accesso no permitido");
                }
            }
            chain.doFilter(request, response);
        } catch (Exception e) {
            Logger.getLogger(AdministrativoFilter.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    @Override
    public void destroy() {

    }

}
