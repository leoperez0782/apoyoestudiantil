/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.servlets.filters;

import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.entity.Usuario_Rol;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @date 2 sept. 2019
 * @time 22:45:43
 * @author Leonardo Pérez
 */
public class DocenteFilter extends BasicFilter implements Filter{
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        try{
            filtrarUsuarioYRol(request, response, chain, "DOCENTE", "No tiene acceso a la zona de docentes");
        }catch(Exception e){
            Logger.getLogger(DocenteFilter.class.getName()).log(Level.SEVERE, null, e);
        }
        
    }

    @Override
    public void destroy() {

    }
}
