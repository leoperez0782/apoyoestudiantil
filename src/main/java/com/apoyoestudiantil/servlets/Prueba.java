/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.servlets;

import com.apoyoestudiantil.dao.IDao;
import com.apoyoestudiantil.dao.UsuarioDAO;
import com.apoyoestudiantil.entity.Alumno;

import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Usuario;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Optional;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author leo
 */
@WebServlet(name = "Prueba", urlPatterns = {"/prueba"})
public class Prueba extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet Prueba</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet Prueba at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
//            Usuario usu = new Usuario();
//            usu.setNombre("lola");
//            usu.setClave("lolo");
//            usu.setRol(Usuario.Rol.ADMINISTRADOR);
////            IDao<Usuario> dao = new UsuarioDAO();
////            dao.save(usu);
//            Docente d = new Docente();
//            d.setApellidos("apellido");
//            d.setCedula("1234");
//            Calendar fecha = Calendar.getInstance();
//            fecha.set(1982,10,5);
//            d.setFechaNacimiento(fecha);
//            d.setGrado(1);
//            d.setNombres("nombres");
//            d.setNumeroEmpleado(3);
//            d.setUsuario(usu);
//            IDao<Persona> dao = new UsuarioDAO();
//            dao.save(d);
           // cargarAlumno();
           imprimirDatosAlumno();
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void cargarAlumno() {
//        Alumno a = new Alumno();
//        
//        a.setNombres("pancho");
//        a.setApellidos("Villa");
//        a.setCedula("11111");
//        a.setFechaNacimiento(Calendar.getInstance());
//        a.setUsuario(null);
//        
//        IDao<Persona> dao = new UsuarioDAO();
//        dao.save(a);
        //creo alumno
//         Alumno al = new Alumno();
//        
//        al.setNombres("cacho");
//        al.setApellidos("Villa");
//        al.setCedula("12");
//        System.out.println(al.getCedula() + " deberia ser 3");
//        al.setFechaNacimiento(Calendar.getInstance());
//        al.setUsuario(null);
//        //creo al padre
//        AdultoResponsable p = new AdultoResponsable();
//        
//        p.setNombres("tato");
//        p.setApellidos("Villa");
//        p.setCedula("13");
//        p.setFechaNacimiento(Calendar.getInstance());
//        p.setUsuario(null);
//        p.addAlumno(al);
//        IDao<Persona> dao2 = new UsuarioDAO();
//        dao2.save(al);
    }

    private void imprimirDatosAlumno() {
        UsuarioDAO dao = new UsuarioDAO();
        //Optional<Alumno> opt= dao.findByCedula("12");
    }

}
