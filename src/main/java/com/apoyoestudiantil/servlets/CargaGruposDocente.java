/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.servlets;

import com.apoyoestudiantil.vistas.VistaCargaGruposDocente;
import com.apoyoestudiantil.vistas.VistaUploadDocente;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Cominza la carga de los grupos del docente para pasar la lista.
 *
 * @author leo
 */
@WebServlet(name = "CargaGruposDocente", urlPatterns = {"/cargargruposdocente"})
public class CargaGruposDocente extends HttpServlet {

    private VistaCargaGruposDocente vista;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try (PrintWriter out = response.getWriter()) {
            String evento = request.getParameter("evento");
            if (evento == null) {
                this.vista = new VistaCargaGruposDocente(response, out, request);
                this.vista.procesar();
                //getServletContext().getRequestDispatcher("/grupos-docente.jsp").forward(request, response);
                getServletContext().getRequestDispatcher("/index-docente.jsp").forward(request, response);
            }else{
                //int indice = Integer.parseInt(evento);
                //this.vista.cargarLista(indice);
                String mensaje = request.getParameter("mensaje");
                getServletContext().getRequestDispatcher("/index-docente.jsp?evento=" + evento + "&mensaje=" + mensaje).forward(request, response);
            }

        } catch (Exception e) {
            Logger.getLogger(CargaAlumnosPorLote.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
