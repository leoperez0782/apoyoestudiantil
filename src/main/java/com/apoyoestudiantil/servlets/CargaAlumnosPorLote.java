/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.servlets;

import com.apoyoestudiantil.vistas.VistaUploadAlumno;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
//import static org.apache.poi.openxml4j.opc.PackagingURIHelper.getFilename;

/**
 *
 * @author leo
 */
@MultipartConfig
@WebServlet(name = "CargaAlumnosPorLote", urlPatterns = {"/uploadalumnos"})
public class CargaAlumnosPorLote extends HttpServlet {

    private VistaUploadAlumno vista;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
//        response.setContentType("text/event-stream");
//        response.setCharacterEncoding("UTF-8");
        try (PrintWriter out = response.getWriter()) {
            this.vista = new VistaUploadAlumno(response, out, request);
            //this.vista.conectarSSE(request);
            //response.sendRedirect("/carga-alumnos-por-lote.jsp");
            getServletContext().getRequestDispatcher("/carga-alumnos-por-lote.jsp").forward(request, response);
        } catch (Exception e) {
            Logger.getLogger(CargaAlumnosPorLote.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
       
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        cargarArchivo(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void cargarArchivo(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            Part file = request.getPart("file");
            InputStream filecontent = file.getInputStream();
//            File archivo = File.createTempFile("temp", ".xlsx");
//            return new FileInputStream(file);
////            filecontent.available();
            String tipoCarga = request.getParameter("optradio");
            //VistaUploadAlumno vista = new VistaUploadAlumno(response,out, request);
            vista.inicializarProcesoCarga(filecontent, tipoCarga);
            //vista.enviar("Funciona", "Llega");
        } catch (IOException ex) {
            Logger.getLogger(CargaAlumnosPorLote.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(CargaAlumnosPorLote.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
