/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.servlets;

import com.apoyoestudiantil.vistas.VistaEvaluarGrupo;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Leonardo Pérez
 */
@MultipartConfig
@WebServlet(name = "EvaluarGrupo", urlPatterns = {"/evaluargrupo"})
public class EvaluarGrupo extends HttpServlet {

    private VistaEvaluarGrupo vista;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try (PrintWriter out = response.getWriter()) {
            if (this.vista == null) {
                this.vista = new VistaEvaluarGrupo(response, out, request);

            }
            String index = request.getParameter("index");
            if (index != null) {
                int indice = Integer.parseInt(index);
                vista.cargarLista(indice, request);
                getServletContext().getRequestDispatcher("/evaluacion-grupo.jsp").forward(request, response);
            }

        } catch (Exception e) {
            Logger.getLogger(ListadoGrupo.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter out = response.getWriter();
        Part file = request.getPart("fulFile");
        InputStream filecontent = file.getInputStream();
        this.vista.guardarEvaluacion(request, response, filecontent);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
