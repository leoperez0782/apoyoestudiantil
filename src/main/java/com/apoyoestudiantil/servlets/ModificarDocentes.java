/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.servlets;

import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.vistas.VistaModificarDocente;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Polachek
 */
@WebServlet(name = "Modificardocentes", urlPatterns = {"/modificardocentes"})
public class ModificarDocentes extends HttpServlet {
    VistaModificarDocente vista = new VistaModificarDocente();

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");
        
        if(request.getParameter("index")!=null){
            int index =Integer.parseInt(request.getParameter("index"));
            List<Docente> lista = (List)request.getSession(false).getAttribute("listado");
            Docente d = lista.get(index);
            request.getSession().removeAttribute("usermod");
            request.getSession(false).setAttribute("usermod", d);
            response.sendRedirect("modificar-docentes.jsp");
            return;
        }
        
        
        if(accion == null || accion.isEmpty() || accion.equals("")){
            vista.conectarSSE(request);
            vista.inicializar();
        }else{
            try {
                vista.procesar(request,accion);
            } catch (AppException ex) {
//                Logger.getLogger(servlet.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
