/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.servlets;

import com.apoyoestudiantil.vistas.VistaCierreReunionesDireccion;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Leonardo Pérez
 */
@WebServlet(name = "CierreReunionesDireccion", urlPatterns = {"/cierrereunionesdireccion"})
public class CierreReunionesDireccion extends HttpServlet {

   private VistaCierreReunionesDireccion vista;
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         String accion = request.getParameter("accion");
          if(vista == null){
            vista = new VistaCierreReunionesDireccion();
            vista.conectarSSE(request);
            vista.inicializar();
            
        }else if(accion == null || accion.isEmpty() || accion == ""){
            vista.conectarSSE(request);
            vista.reconectar();
        }else{
            try {
                vista.procesar(request,accion);
            } catch (Exception ex) {
                Logger.getLogger(CierreReunionesDireccion.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
       try{
           this.vista.procesarPost(request, response);
           //getServletContext().getRequestDispatcher("/cierre-reuniones-direccion.jsp").forward(request, response);
       }catch(Exception e){
           Logger.getLogger(CierreReunionesDireccion.class.getName()).log(Level.SEVERE, null, e);
       }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
