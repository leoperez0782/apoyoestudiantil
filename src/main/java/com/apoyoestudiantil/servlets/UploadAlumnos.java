/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.servlets;

import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.vistas.VistaCargaAlumnosExcel;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Leonardo Pérez
 */
@MultipartConfig
@WebServlet(name = "UploadAlumnos", urlPatterns = {"/uploadalumnosexcel"})
public class UploadAlumnos extends HttpServlet {

   private VistaCargaAlumnosExcel vista = new VistaCargaAlumnosExcel();
   private boolean conectada;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String accion = request.getParameter("accion");
        
        
        if(!conectada){
            vista.conectarSSE(request);
            vista.inicializar();
            conectada = true;
        }else if(accion == null || accion.isEmpty() || accion == ""){
            vista.conectarSSE(request);
            vista.reconectar();
        }else{
            try {
                vista.procesar(request,accion);
            } catch (Exception ex) {
                Logger.getLogger(UploadAlumnos.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        cargarArchivo(request, response);
        //getServletContext().getRequestDispatcher("/carga-alumnos-excel.jsp").forward(request, response);
    }

    private void cargarArchivo(HttpServletRequest request, HttpServletResponse response) {
        try {
            PrintWriter out = response.getWriter();
            Part file = request.getPart("file");
            InputStream filecontent = file.getInputStream();
//            File archivo = File.createTempFile("temp", ".xlsx");
//            return new FileInputStream(file);
////            filecontent.available();
            String tipoCarga = request.getParameter("optradio");
            //VistaUploadAlumno vista = new VistaUploadAlumno(response,out, request);
            vista.inicializarProcesoCarga(filecontent, tipoCarga, request, response);
            //vista.enviar("Funciona", "Llega");
        } catch (IOException ex) {
            Logger.getLogger(CargaAlumnosPorLote.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(CargaAlumnosPorLote.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
