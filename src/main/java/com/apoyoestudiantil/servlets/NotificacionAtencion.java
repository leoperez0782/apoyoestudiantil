/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.servlets;

import com.apoyoestudiantil.vistas.VistaNotificacionAtencion;
import com.apoyoestudiantil.vistas.VistaPasajeLista;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Leonardo Pérez
 */
@WebServlet(name = "NotificacionAtencion", urlPatterns = {"/notificacionatencion"})
public class NotificacionAtencion extends HttpServlet {

   private VistaNotificacionAtencion vista;

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         try (PrintWriter out = response.getWriter()) {
              if(this.vista == null){
                   this.vista = new VistaNotificacionAtencion(response, out, request);
              }
               

            String index = request.getParameter("index");
            if (index != null) {
                int indice = Integer.parseInt(index);
                vista.cargarLista(indice, request);
                //muestro los datos de los alumnos del grupo.
                getServletContext().getRequestDispatcher("/notificacion-atencion.jsp").forward(request, response);
            } 

        } catch (Exception e) {
            Logger.getLogger(ListadoGrupo.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        this.vista.procesarMensajes(request);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
