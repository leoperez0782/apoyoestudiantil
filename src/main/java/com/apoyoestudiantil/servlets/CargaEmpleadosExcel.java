/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.servlets;

import com.apoyoestudiantil.vistas.VistaCargaEmpleadosExcel;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

/**
 *
 * @author Leonardo Pérez
 */
@MultipartConfig
@WebServlet(name = "CargaEmpleadosExcel", urlPatterns = {"/uploadempleados"})
public class CargaEmpleadosExcel extends HttpServlet {

    private VistaCargaEmpleadosExcel vista;
    
    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try (PrintWriter out = response.getWriter()) {
            this.vista = new VistaCargaEmpleadosExcel(response,out, request);
           
           //response.sendRedirect("carga-docentes-por-lote.jsp");
           getServletContext().getRequestDispatcher("/carga-empleados-desde-excel.jsp").forward(request, response);
        }catch(Exception e){
            Logger.getLogger(CargaAlumnosPorLote.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
         try {
            PrintWriter out = response.getWriter();
            Part file = request.getPart("file");
            InputStream filecontent = file.getInputStream();
            String tipoCarga = request.getParameter("optradio");
            String idCargo = request.getParameter("slcCargo");
            String rol = request.getParameter("slcRol");
            
            vista.inicializarProcesoCarga(filecontent, tipoCarga, idCargo, rol);
            
        } catch (IOException ex) {
            Logger.getLogger(CargaDocentesPorLote.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ServletException ex) {
            Logger.getLogger(CargaDocentesPorLote.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
