/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorAdmin;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.AsyncContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Polachek
 */
public class VistaAdmin {
    private HttpServletRequest request;
    private HttpServletResponse response;
    private RequestDispatcher dispatcher;
    private ControladorAdmin controlador;
    private PrintWriter out;
    private boolean conected = false;

    public void mostrarError(String mensaje) {
        try {
            response.sendRedirect("administrador.jsp?mensaje=" + mensaje);
        } catch (IOException ex) {
        }
    }
    
    public void inicializar() {
        Usuario usu = (Usuario)request.getSession(false).getAttribute("usuario");
        controlador = new ControladorAdmin(this, usu);
    }
    
    public void conectarSSE(HttpServletRequest request) throws IOException {        
        request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
        AsyncContext contexto = request.startAsync();
        this.request = (HttpServletRequest)contexto.getRequest();
        contexto.getResponse().setContentType("text/event-stream");
        contexto.getResponse().setCharacterEncoding("UTF-8");
        contexto.setTimeout(0);//SIN TIMEOUT
        out = contexto.getResponse().getWriter();
        this.conected = true;
    }

    public void procesar(HttpServletRequest request, String accion) throws AppException {
        switch(accion){
            case "mensaje" : enviar("MensajeExito",accion);break;
        }
    }
    
    public void enviar(String evento, String dato) {
        out.write("event: " + evento + "\n");
        dato = dato.replace("\n", "");
        out.write("data: " + dato + "\n\n");
        if (out.checkError()) {//checkError llama a flush, si da false evio bien
            System.out.println("Falló Envío");            
        } else {
            //TODO OK!
        }
    }

    public boolean isConected() {
        return conected;
    }   
    
}
