/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorCargaAlumnosExcel;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Nivel;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @date 22-ago-2019
 * @time 11:01:39
 * @author Leonardo Pérez
 */
public class VistaCargaAlumnosExcel extends VistaAbstracta {

    private ControladorCargaAlumnosExcel controlador;
    private HttpServletResponse response;
    public void inicializar() {
       this.controlador = new ControladorCargaAlumnosExcel(this);
    }

    public void reconectar() {
        controlador.reconectar();
    }

    public void procesar(HttpServletRequest request, String accion) {
        switch(accion){
            case "nivels" : 
                nivelSeleccionado(request);
                break;
            case "grupo":
                grupoSeleccionado(request);
        }
    }

    public void mostrarNiveles(ArrayList<Nivel> niveles) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
       if(niveles == null || niveles.isEmpty()){
            enviar("mostrarAlert","No existen niveles cragados");
        }else{
            String msg =""; 
            for (Nivel nv : niveles) {
                String idNv="<span class='nivel-id' id='nivel-"+nv.getId()+"'> "+"</span>";
                String fechaNv ="<span>Fecha de creacion: "+dateFormat.format(nv.getFechaCreacion().getTime())+"</span>";
                String estadoNv ="<span>Estado: "+nv.getEstado().toString()+"</span>";
                
                String claseNv ="<span>Clase : "+nv.getClase().getNivel()+ " - " + nv.getClase().getCodigoClase() +"</span>";
                msg += "<li data-nivel-id='"+ nv.getId() +"'><a href='#' onclick='nivelSeleccionado(event,this)'>"+idNv+fechaNv+estadoNv+claseNv+"</a></li>";
            }
            enviar("mostrarNiveles",msg);
        }
    }

    private void nivelSeleccionado(HttpServletRequest request) {
        try {
            long nvSel = Long.parseLong(request.getParameter("nivelseleccionado"));
            this.controlador.nivelSeleccionado(nvSel);
        } catch (Exception e) {
            /*UtilidadesVista u = new UtilidadesVista();
            u.mostrarError(this, e.getMessage());*/
            enviar("mostrarAlert",e.getMessage());
        }
    }

    public void mostrarGruposNivel(List<Grupo> grupos) {
       if(grupos == null || grupos.isEmpty()){
            enviar("mostrarAlert","No existen grupos cragados");
        }else{
            String msg ="";  
            for (Grupo g : grupos) {
                String idG="<span class='grupo-id' id='grupo-"+g.getId()+"'>Turno : "+g.getTurno()+"</span>";
                String fechaG ="<span>Año: "+g.getAnio()+"</span>";
                String cantAluG ="<span>Cantidad de Alumnos: "+g.getAlumnos().size()+"</span>";
                msg += "<li data-grupo-id='"+ g.getId() +"'><a href='#' onclick='grupoSeleccionado(event,this)'>"+idG+" - "+fechaG+" - "+cantAluG+"</a></li>";
            }
            enviar("mostrarGrupos",msg);
        }
    }

    private void grupoSeleccionado(HttpServletRequest request) {
        long grupoSel = Long.parseLong(request.getParameter("gruposeleccionado"));
        this.controlador.grupoSeleccionado(grupoSel);
    }


    public void devolverPost(String evento, String mensaje) {
        try {

            this.request.getServletContext().getRequestDispatcher("/carga-alumnos-excel.jsp?evento=" + evento + "&mensaje=" + mensaje).forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(VistaCargaAlumnosExcel.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void inicializarProcesoCarga(InputStream filecontent, String tipoCarga, HttpServletRequest request, HttpServletResponse response) {
        this.request = request;
        this.response = response;
        controlador.importar(filecontent, tipoCarga);
    }
}
