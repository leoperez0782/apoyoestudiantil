/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorCerrarPromedios;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @date 10-ago-2019
 * @time 8:37:12
 * @author Leonardo Pérez
 */
public class VistaCerrarPromedios extends VistaAbstracta {

    private ControladorCerrarPromedios controlador;
    private HttpServletResponse response;

    public VistaCerrarPromedios(HttpServletResponse response, PrintWriter out, HttpServletRequest request) {
        super(out, request);
        this.response = response;
        this.controlador = new ControladorCerrarPromedios(this);
    }

    public void cargarLista(int indice, HttpServletRequest request) {
        this.controlador.cargarLista(indice, request);
    }

    @Override
    public void enviar(String evento, String mensaje) {
        try {
            //String url = this.request.getServletContext().getServletContextName();//getRealPath("evaluacion-grupo.jsp");
            //System.out.println(url);
            this.request.getServletContext().getRequestDispatcher("/cerrar-promedios.jsp?evento=" + evento + "&mensaje=" + mensaje).forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(VistaUploadAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void procesar(HttpServletRequest request, HttpServletResponse res) {
        this.setRequest(request);
        this.response = res;
        this.controlador.procesar(request);
    }

    public void enviar(String exito, String mensaje, HttpServletRequest request) {

    }

}
