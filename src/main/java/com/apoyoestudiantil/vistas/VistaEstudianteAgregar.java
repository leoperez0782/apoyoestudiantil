/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorEstudianteAgregar;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.AsyncContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Polachek
 */
public class VistaEstudianteAgregar {
    private HttpServletRequest request;
    private HttpServletResponse response;
    private RequestDispatcher dispatcher;
    private PrintWriter out;
    private ControladorEstudianteAgregar controlador;
    private boolean conected = false;  
    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); 
    
    public void mostrarError(String mensaje) {
        enviar("Error",mensaje);
    }
    
    public void mostrarExito(String mensaje) {
        enviar("Exito",mensaje);
    }
    
    public void inicializar() {
        Usuario usu = (Usuario)request.getSession(false).getAttribute("usuario");
        controlador = new ControladorEstudianteAgregar(this, usu);
    }
    
    public void reconectar(){
        controlador.reconectar();
    }
    
    public void conectarSSE(HttpServletRequest request) throws IOException {        
        request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
        AsyncContext contexto = request.startAsync();
        this.request = (HttpServletRequest)contexto.getRequest();
        contexto.getResponse().setContentType("text/event-stream");
        contexto.getResponse().setCharacterEncoding("UTF-8");
        contexto.setTimeout(0);//SIN TIMEOUT
        out = contexto.getResponse().getWriter();
        this.conected = true;
    }

    public void procesar(HttpServletRequest request, String accion) throws AppException, ParseException {
        switch(accion){
            case "nivels" : nivelSeleccionado(request);break;
            case "alta_alumno" : altaEstudiante(request);break;
            case "grupo" : grupoSeleccionado(request);break;
            case "telefono" : telefonoEstudiante(request);break;
            case "direccion" : direccion(request);break;
        }
    }
    
    public void enviar(String evento, String dato) {
        out.write("event: " + evento + "\n");
        dato = dato.replace("\n", "");
        out.write("data: " + dato + "\n\n");
        if (out.checkError()) {//checkError llama a flush, si da false evio bien
            System.out.println("Falló Envío");            
        } else {
            //TODO OK!
        }
    }
    
    public boolean isConected() {
        return conected;
    } 

    public void mostrarNiveles(ArrayList<Nivel> niveles) {
        if(niveles == null || niveles.isEmpty()){
            enviar("mostrarError","No existen niveles cragados");
        }else{
            String msg =""; 
            for (Nivel nv : niveles) {
                String idNv="<span class='nivel-id' id='nivel-"+nv.getId()+"'>Clase : "+nv.getClase().getNivel()+ " - " + nv.getClase().getCodigoClase() +"</span>";
                String fechaNv ="<span>Fecha de creacion: "+dateFormat.format(nv.getFechaCreacion().getTime())+"</span>";
                String estadoNv ="<span>Estado: "+nv.getEstado().toString()+"</span>";
                String alumnNv ="<span>Cantidad de Alumnos: "+nv.getAlumnos().size()+"</span>";
                msg += "<li data-nivel-id='"+ nv.getId() +"'><a href='#' onclick='nivelSeleccionado(event,this)'>"+idNv+fechaNv+estadoNv+alumnNv+"</a></li>";
            }
            enviar("mostrarNiveles",msg);
        }
    }
    
    private void nivelSeleccionado(HttpServletRequest request) {
        try {
            long nvSel = Long.parseLong(request.getParameter("nivelseleccionado"));
            this.controlador.nivelSeleccionado(nvSel);
        } catch (Exception e) {
            /*UtilidadesVista u = new UtilidadesVista();
            u.mostrarError(this, e.getMessage());*/
            enviar("mostrarAlert",e.getMessage());
        }
    }

    public void mostrarGruposNivel(List<Grupo> grupos) {
        if(grupos == null || grupos.isEmpty()){
            enviar("mostrarAlert","No existen grupos cragados");
        }else{
            String msg ="";  
            for (Grupo g : grupos) {
                String idG="<span class='grupo-id' id='grupo-"+g.getId()+"'>Año: "+g.getAnio()+"</span>";
                String cantAluG ="<span>Cantidad de Alumnos: "+g.getAlumnos().size()+"</span>";
                msg += "<li data-grupo-id='"+ g.getId() +"'><a href='#' onclick='grupoSeleccionado(event,this)'>"+idG+" - "+cantAluG+"</a></li>";
            }
            enviar("mostrarGrupos",msg);
        }
    }

    private void altaEstudiante(HttpServletRequest request) throws ParseException {
        controlador.altaEstudiante(request);
    }

    private void grupoSeleccionado(HttpServletRequest request) {
        long grupoSel = Long.parseLong(request.getParameter("gruposeleccionado"));
        this.controlador.grupoSeleccionado(grupoSel);
    }

    private void telefonoEstudiante(HttpServletRequest request) {
        String tipoTel = request.getParameter("tipotel");
        String numTel = request.getParameter("numtel");
        String obsTel = request.getParameter("obstel");
        controlador.agregarTelefono(tipoTel,numTel,obsTel);  
    }
    
    private void direccion(HttpServletRequest request) {
        controlador.agregarDireccion(request);
    }
}
