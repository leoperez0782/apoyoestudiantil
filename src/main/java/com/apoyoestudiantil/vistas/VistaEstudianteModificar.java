/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorEstudianteModificar;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.AsyncContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Polachek
 */
public class VistaEstudianteModificar {
    private HttpServletRequest request;
    private HttpServletResponse response;
    private RequestDispatcher dispatcher;
    private PrintWriter out;
    private ControladorEstudianteModificar controlador;
    private boolean conected = false;
    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy"); 
    
    
    public void mostrarError(String mensaje) {
        enviar("mostrarAlert",mensaje);
    }
    
    public void inicializar() {
        Usuario usu = (Usuario)request.getSession(false).getAttribute("usuario");
        controlador = new ControladorEstudianteModificar(this, usu);
    }
    
    public void conectarSSE(HttpServletRequest request) throws IOException {        
        request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
        AsyncContext contexto = request.startAsync();
        this.request = (HttpServletRequest)contexto.getRequest();
        contexto.getResponse().setContentType("text/event-stream");
        contexto.getResponse().setCharacterEncoding("UTF-8");
        contexto.setTimeout(0);//SIN TIMEOUT
        out = contexto.getResponse().getWriter();
        this.conected = true;
    }

    public void procesar(HttpServletRequest request, String accion) throws AppException, ParseException {
        switch(accion){
            case "nivels" : nivelSeleccionado(request);break;
            case "grupo" : grupoSeleccionado(request);break;
            case "alumnos" : alumnoSeleccionado(request);break;
            case "limpiarsession" : limpiarsession();break;
        }
    }
    
    private void limpiarsession() {
        if (request.getSession() != null) {
            request.getSession().invalidate();
        }
        enviar("redireccion","index.jsp");
    } 
    
    public void enviar(String evento, String dato) {
        out.write("event: " + evento + "\n");
        dato = dato.replace("\n", "");
        out.write("data: " + dato + "\n\n");
        if (out.checkError()) {//checkError llama a flush, si da false envio bien
            System.out.println("Falló Envío");            
        } else {
            //TODO OK!
        }
    }
    
    public boolean isConected() {
        return conected;
    } 

    public void mostrarNiveles(ArrayList<Nivel> niveles) {
        if(niveles == null || niveles.isEmpty()){
            enviar("mostrarAlert","No existen niveles cragados");
        }else{
            String msg ="<tr><th>Nivel</th><th>Fecha de creación</th>\n" +
                     "<th>Estado</th><th>Cantidad de Alumnos</th><th>Clase</th>\n" +
                     "<th></th></tr>"; 
            for (Nivel nv : niveles) {
                String idNv="<td>"+nv.getId()+"</td>";
                String fechaNv ="<td>"+dateFormat.format(nv.getFechaCreacion().getTime())+"</td>";
                String estadoNv ="<td>"+nv.getEstado().toString()+"</td>";
                String alumnNv ="<td>"+nv.getAlumnos().size()+"</td>";
                String claseNv ="<td>"+nv.getClase().getNivel()+ " - " + nv.getClase().getCodigoClase() +"</td>";
                String seleccion ="<td><a href='#' onclick='seleccionarNivel(event,this,"+nv.getId()+")'>Seleccionar</a></td>";
                msg += "<tr>"+idNv+fechaNv+estadoNv+alumnNv+claseNv+seleccion+"</tr>";
            }
            enviar("mostrarNiveles",msg);
        }
    }
    
    private void nivelSeleccionado(HttpServletRequest request) {
        try {
            long nvSel = Long.parseLong(request.getParameter("nivelseleccionado"));
            this.controlador.nivelSeleccionado(nvSel);
        } catch (Exception e) {
            enviar("mostrarAlert",e.getMessage());
        }
    }
    
    private void grupoSeleccionado(HttpServletRequest request) {
        try {
            long grSel = Long.parseLong(request.getParameter("gruposeleccionado"));
            this.controlador.grupoSeleccionado(grSel);
        } catch (Exception e) {
            enviar("mostrarAlert",e.getMessage());
        }
    }

    public void mostrarGruposNivel(List<Grupo> grupos) {
        if(grupos == null || grupos.isEmpty()){
            enviar("mostrarAlert","No existen grupos cragados");
        }else{
            String msg ="<tr><th>Nivel</th><th>Año</th>\n" +
                     "<th>Cantidad de Alumnos</th><th></th></tr>";  
            for (Grupo g : grupos) {
                String idG="<td>"+g.getId()+"</td>";
                String fechaG ="<td>"+g.getAnio()+"</td>";
                String cantAluG ="<td>"+g.getAlumnos().size()+"</td>";
                String seleccion ="<td><a href='#' onclick='grupoSeleccionado(event,this,"+g.getId()+")'>Seleccionar</a></td>";
                msg += "<tr>"+idG+fechaG+cantAluG+seleccion+"</tr>";
            }
            enviar("mostrarGrupos",msg);
        }
    }

    public void mostrarAlumnos(List<Alumno> alumnos) {
        if(alumnos == null || alumnos.isEmpty()){
            enviar("mostrarAlert","No existen aluemnos en el grupo");
        }else{
            String msg ="";  
            for (Alumno al : alumnos) {
                String miID = al.getId()+"";
                String idA="<td class='alumno-id' id='alumno-"+miID+"'>"+miID+"</td>";
                String matricula ="<td>"+al.getMatricula()+"</td>";
                String nombre ="<td>"+al.getPrimerNombre() + " " + al.getSegundoNombre() + " " + al.getPrimerApellido() +
                        " " + al.getSegundoApellido() +"</td>";
                String btn ="<td><a href='#' data-alumno-id='"+miID+"' onclick='editarAlumno(event,this);'>Seleccionar</a></td>";
                msg += "<tr data-alumno-id='"+ miID +"'>"+idA+matricula+nombre+btn+"</tr>";
            }
            String table_headings="<tr><th>ID</th><th>Matrícula</th><th>Nombre</th><th>Modificar</th></tr>";
            enviar("mostrarAlumnos",table_headings+msg);
        }
    }

    private void alumnoSeleccionado(HttpServletRequest request) {
        String id = request.getParameter("alumnoseleccionado");
        controlador.alumnoSeleccionado(id);
    }

    public void mostrarFormAlumno(Alumno alumno) {
        this.request.getSession(false).setAttribute("alumno", alumno);
        enviar("mostrarFormAlumno","-");
    }

    public void reconectar() {
        controlador.reconectar();
    }
}
