/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorModificarTutorLista;
import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Polachek
 */
public class VistaModificarTutorLista extends VistaAbstracta{
    private ControladorModificarTutorLista controlador;
    private AdultoResponsable tutor;

    public void inicializar() {
        Usuario usu = (Usuario)request.getSession(false).getAttribute("usuario");
        controlador = new ControladorModificarTutorLista(this, usu);
    }
    
    public void mostrarUsuario(String nombreUsu) {
        enviar("mostrarUsuario",nombreUsu.toUpperCase());
    }

    public void resultadoBusqueda(AdultoResponsable miTutor) {
        tutor = miTutor;
        String msg= "";
        msg += "<div id='tutor-item'>";
        String altphoto = "Foto de "+ miTutor.getPrimerNombre() + " " +miTutor.getPrimerApellido();
        msg += "<div id='tutor-foto'><img src='' alt='"+altphoto+"'/></div>";
        msg += "<div id='tutor-data'><span><b>Nombre: </b>"+ miTutor.getPrimerNombre() +"</span>";
        msg += "<span><b>Apellido: </b>"+ miTutor.getPrimerApellido() +"</span>";
        msg += "<span><b>Documento: </b>"+ miTutor.getDocumento().getNumero() +"</span>";
        msg += "</div>";
        msg += "<div class='clear'></div>";
        msg += "</div>";
        enviar("resultadoBusqueda",msg);
    }
    
    private void tutorSeleccionado(HttpServletRequest request) {
        request.getSession(true).setAttribute("tutor", tutor);
        enviar("redireccion","modificar-tutor.jsp");
    }

    public void error(String msg) {
        enviar("Error",msg);
    }
    
    public void alert(String msg) {
        enviar("alert",msg);
    }
    
    public void procesar(HttpServletRequest request, String accion) throws AppException {
        switch(accion){
            case "buscar" : buscar(request);break;
            case "tutorSeleccionado" : tutorSeleccionado(request);break;
        }
    }

    private void buscar(HttpServletRequest request) {
        Long ciTutor = Long.parseLong(request.getParameter("numDoc"));
        controlador.buscarTutor(ciTutor);
    }
    
    public void reconectar(){
        controlador.reconectar();
    }   
}
