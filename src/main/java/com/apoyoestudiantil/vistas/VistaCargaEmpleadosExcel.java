/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorCargaEmpleadosExcel;
import com.apoyoestudiantil.entity.Cargo;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @date 21 sept. 2019
 * @time 23:29:30
 * @author Leonardo Pérez
 */
public class VistaCargaEmpleadosExcel extends VistaAbstracta{
    private ControladorCargaEmpleadosExcel controlador;
    private HttpServletResponse response;
    public VistaCargaEmpleadosExcel(PrintWriter out, HttpServletRequest request) {
        super(out, request);
        this.controlador = new ControladorCargaEmpleadosExcel(this);
    }

    public VistaCargaEmpleadosExcel(HttpServletResponse response, PrintWriter out, HttpServletRequest request) {
       super(out, request);
       this.response = response;
       this.controlador = new ControladorCargaEmpleadosExcel(this);
    }

    public void cargarCargos(ArrayList<Cargo> cargos) {
       this.request.getSession().setAttribute("listaCargos", cargos);
    }

    

    public void inicializarProcesoCarga(InputStream filecontent, String tipoCarga, String idCargo, String rol) {
        Long cargoId = Long.parseLong(idCargo);
        controlador.importar(filecontent, tipoCarga, cargoId, rol);
    }

    @Override
    public void enviar(String evento, String dato) {
          try {
            
            response.sendRedirect("carga-empleados-desde-excel.jsp?evento=" + evento + "&mensaje=" + dato);
        } catch (IOException ex) {
            Logger.getLogger(VistaUploadAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    
    
}
