/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorEvaluarGrupo;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @date 30-jul-2019
 * @time 6:33:34
 * @author Leonardo Pérez
 */
public class VistaEvaluarGrupo extends VistaAbstracta {

    private HttpServletResponse response;
    private ControladorEvaluarGrupo controlador;

    public VistaEvaluarGrupo(HttpServletResponse response, PrintWriter out, HttpServletRequest request) {
        super(out, request);
        this.response = response;
        this.controlador = new ControladorEvaluarGrupo(this);
    }

    public void cargarLista(int indice, HttpServletRequest request) {
        this.controlador.cargarLista(indice, request);
    }

//    public void guardarEvaluacion(HttpServletRequest request) {
//       this.controlador.guardarEvaluacion(request);
//    }
//    
      @Override
    public void enviar(String evento, String mensaje) {
        try {
            //response.sendRedirect("evaluacion-grupo.jsp?evento=" + evento + "&mensaje=" + mensaje);
            //response.sendRedirect("cargargruposdocente?evento=" + evento + "&mensaje=" + mensaje);
//            String url = this.request.getServletContext().getServletContextName();//getRealPath("evaluacion-grupo.jsp");
//            System.out.println(url);
            this.request.getServletContext().getRequestDispatcher("/evaluacion-grupo.jsp?evento=" + evento + "&mensaje=" + mensaje).forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(VistaUploadAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    

    public void guardarEvaluacion(HttpServletRequest request, HttpServletResponse response, InputStream filecontent) {
        this.request =request;
        this.response = response;
        this.controlador.guardarEvaluacion(request, filecontent);
    }
    
}
