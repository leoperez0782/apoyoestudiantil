/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorAltaAdmin;
import com.apoyoestudiantil.entity.Cargo;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.AsyncContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Polachek
 */
public class VistaAltaAdmin {
    private HttpServletRequest request;
    private HttpServletResponse response;
    private RequestDispatcher dispatcher;
    private PrintWriter out;
    private ControladorAltaAdmin controlador;
    private boolean conected = false;
    
    public void reconectar(String accion){
        controlador.reconectar(accion);
    }
    
    public void inicializar(String accion) {
        if(accion.equals("admin") || accion.equals("adminO")){
            Usuario usu = (Usuario)request.getSession(false).getAttribute("usuario");
            controlador = new ControladorAltaAdmin(this,accion);
        }else{
            mostrarError("Error, no se pudo establecer coneccion");
        }
    }
    
    public void mostrarError(String mensaje) {
        enviar("Error",mensaje);
    }
    
    public void mostrarExito(String mensaje) {
        enviar("Exito",mensaje);
    }
    
    public void mostrarCargos(List<Cargo> cargos) {
        String msg ="";
        for (Cargo c : cargos) {
           msg += "<option value ='"+c.getId()+"'>"+c.getTipoCargo()+"</option>";
        }
        enviar("mostrarCargos",msg);
        
        //<option value ="<%out.print(c.getId());%>"><%out.print(c.getTipoCargo());%></option>
    }

    public void conectarSSE(HttpServletRequest request) throws IOException {        
        request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
        AsyncContext contexto = request.startAsync();
        this.request = (HttpServletRequest)contexto.getRequest();
        contexto.getResponse().setContentType("text/event-stream");
        contexto.getResponse().setCharacterEncoding("UTF-8");
        contexto.setTimeout(0);//SIN TIMEOUT
        out = contexto.getResponse().getWriter();
        this.conected = true;
    }
    
    public void procesar(HttpServletRequest request, String accion) throws AppException{
        switch(accion){
            case "alta" : alta(request);break;
            case "telefono" : telefono(request);break;
            case "direccion" : direccion(request);break;
        }
    }
    
    public void enviar(String evento, String dato) {
        out.write("event: " + evento + "\n");
        dato = dato.replace("\n", "");
        out.write("data: " + dato + "\n\n");
        if (out.checkError()) {//checkError llama a flush, si da false evio bien
            System.out.println("Falló Envío");            
        } else {
            //TODO OK!
        }
    }
    
    public boolean isConected() {
        return conected;
    }
    
    private void alta(HttpServletRequest request){
        // validar nuevamente, solo con javascript no es suficiente
        controlador.alta(request);
    }

    private void telefono(HttpServletRequest request) {
        String tipoTel = request.getParameter("tipotel");
        String numTel = request.getParameter("numtel");
        String obsTel = request.getParameter("obstel");
        controlador.agregarTelefono(tipoTel,numTel,obsTel);    
    }
    
    private void direccion(HttpServletRequest request) {
        controlador.agregarDireccion(request);
    }
}
