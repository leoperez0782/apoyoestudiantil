/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.vistas;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.AsyncContext;
import javax.servlet.http.HttpServletRequest;

/**
 * @date 12-jun-2019
 * @time 20:28:46
 * @author Leonardo Pérez
 */
public abstract class VistaAbstracta {

    protected PrintWriter out;
    protected HttpServletRequest request;
    private boolean conected = false;

    public VistaAbstracta() {
    }

    public VistaAbstracta(PrintWriter out, HttpServletRequest request) {
        this.out = out;
        this.request = request;
    }

    public PrintWriter getOut() {
        return out;
    }

    public void setOut(PrintWriter out) {
        this.out = out;
    }

    public HttpServletRequest getRequest() {
        return request;
    }

    public void setRequest(HttpServletRequest request) {
        this.request = request;
    }

    
    
    public void enviar(String evento, String dato) {
        out.write("event: " + evento + "\n");
        dato = dato.replace("\n", "");
        out.write("data: " + dato + "\n\n");
        if (out.checkError()) {//checkError llama a flush, si da false evio bien
            System.out.println("Falló Envío");
        }
    }
    
    public void conectarSSE(HttpServletRequest request) throws IOException {
        request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
        AsyncContext contexto = request.startAsync();
        this.request = (HttpServletRequest)contexto.getRequest();
        contexto.getResponse().setContentType("text/event-stream");
        contexto.getResponse().setCharacterEncoding("UTF-8");
        contexto.setTimeout(0);//SIN TIMEOUT
        out = contexto.getResponse().getWriter();
        this.conected = true;
    }
    
    public boolean isConected() {
        return conected;
    }
}
