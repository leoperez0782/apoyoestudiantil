/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorNotificacionAtencion;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @date 5 sept. 2019
 * @time 21:53:43
 * @author Leonardo Pérez
 */
public class VistaNotificacionAtencion extends VistaAbstracta{
    private ControladorNotificacionAtencion controlador;
    private HttpServletResponse response;
    public VistaNotificacionAtencion(HttpServletResponse response,PrintWriter out, HttpServletRequest request) {
        super(out, request);
        this.response = response;
        this.controlador = new ControladorNotificacionAtencion(this);
    }

    public void cargarLista(int indice, HttpServletRequest request) {
       this.controlador.cargarLista(indice, request);
    }

    @Override
    public void enviar(String evento, String dato) {
         try {
            //response.sendRedirect("notificacion-atencion.jsp?evento=" + evento + "&mensaje=" + dato);
            //response.sendRedirect("cargargruposdocente?evento=" + evento + "&mensaje=" + dato);
            this.request.getServletContext().getRequestDispatcher("/notificacion-atencion.jsp?evento=" + evento + "&mensaje=" + dato).forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(VistaNotificacionAtencion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void procesarMensajes(HttpServletRequest request) {
        this.controlador.procesarMensajes(request);
    }

    
}
