/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorListadoGrupo;
import java.io.PrintWriter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @date 13-jul-2019
 * @time 0:32:22
 * @author Leonardo Pérez
 */
public class VistaListadoGrupo extends VistaAbstracta{
    private HttpServletResponse response;
    private ControladorListadoGrupo controlador;

    public VistaListadoGrupo(HttpServletResponse response, PrintWriter out, HttpServletRequest request) {
        super(out, request);
        this.response = response;
        this.controlador = new ControladorListadoGrupo(this);
    }

    public void cargarLista(int indice) {
        this.controlador.cargarLista(indice, request);
    }
    
    
}
