/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorListadoGrupo;
import com.apoyoestudiantil.controladores.ControladorPasajeLista;
import com.apoyoestudiantil.entity.Alumno;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.AsyncContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @date 06-jul-2019
 * @time 5:24:49
 * @author Leonardo Pérez
 */
public class VistaPasajeLista extends VistaAbstracta{
    private HttpServletResponse response;
    private ControladorPasajeLista controlador;
    public VistaPasajeLista(HttpServletResponse response, PrintWriter out, HttpServletRequest request) {
        super(out, request);
        this.response = response;
        this.controlador = new ControladorPasajeLista(this);
    }

    public void cargarLista(int indice) {
        controlador.cargarLista(indice, request);
    }

    public void verificarFecha(String fecha) {
        //this.controlador.validarFecha(fecha, this.request);
    }

    public HttpServletResponse getResponse() {
        return response;
    }

    public void setResponse(HttpServletResponse response) {
        this.response = response;
    }

    public void procesarLista(HttpServletRequest request) {
        this.request = request;
       this.controlador.procesarLista(request);
    }

    @Override
    public void enviar(String evento, String mensaje) {
        try {
            //response.sendRedirect("pasaje-lista.jsp?evento=" + evento + "&mensaje=" + mensaje);
            //response.sendRedirect("cargargruposdocente?evento=" + evento + "&mensaje=" + mensaje);
            this.request.getServletContext().getRequestDispatcher("/pasaje-lista.jsp?evento=" + evento + "&mensaje=" + mensaje).forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(VistaUploadAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
