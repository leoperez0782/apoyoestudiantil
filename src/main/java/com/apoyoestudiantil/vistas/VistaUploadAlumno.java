/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorUploadAlumnos;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @date 12-jun-2019
 * @time 19:58:28
 * @author Leonardo Pérez
 */
public class VistaUploadAlumno extends VistaAbstracta {

    private ControladorUploadAlumnos controlador;
    private HttpServletResponse response;
    public VistaUploadAlumno() {
        
    }

    public VistaUploadAlumno(PrintWriter out, HttpServletRequest request) {
        super(out, request);
        controlador = new ControladorUploadAlumnos(this);
    }
    

    public VistaUploadAlumno(PrintWriter out, HttpServletRequest request, InputStream archivo, String tipoCarga) {
        super(out, request);
        controlador = new ControladorUploadAlumnos(this);
    }

    public VistaUploadAlumno(HttpServletResponse response, PrintWriter out, HttpServletRequest request) {
        super(out, request);
        this.response = response;
        controlador = new ControladorUploadAlumnos(this);
    }
    
    
    public void inicializarProcesoCarga(InputStream archivo, String tipoCarga){
        controlador.importar(archivo, tipoCarga);
    }
   @Override
   public void enviar( String evento,String mensaje){
        try {
            String dato = evento + " " + mensaje;
            response.sendRedirect("carga-alumnos-por-lote.jsp?mensaje=" +dato);
        } catch (IOException ex) {
            Logger.getLogger(VistaUploadAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
   }

    public void inicializar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void reconectar() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void procesar(HttpServletRequest request, String accion) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    
}
