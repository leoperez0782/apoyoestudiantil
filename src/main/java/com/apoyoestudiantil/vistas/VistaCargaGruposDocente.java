/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorCargaGruposDocente;
import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.entity.Libreta;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author leo
 */
public class VistaCargaGruposDocente extends VistaAbstracta {

    private ControladorCargaGruposDocente controlador;
    private HttpServletResponse response;

    public VistaCargaGruposDocente(HttpServletResponse response, PrintWriter out, HttpServletRequest request) {
        super(out, request);
        this.response = response;
        this.controlador = new ControladorCargaGruposDocente(this);
    }

    public void procesar() {
        try {
            Docente d = (Docente) request.getSession().getAttribute("usuario");
            List<Libreta> lista = controlador.cargarLibretas(d);
            request.getSession(true).setAttribute("listaLibretas", lista);
        } catch (Exception e) {
            Logger.getLogger(VistaCargaGruposDocente.class.getName()).log(Level.SEVERE, null, e);
        }
    }

}
