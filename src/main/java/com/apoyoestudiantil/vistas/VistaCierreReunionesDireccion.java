/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorCierreReunionesDireccion;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.entity.PeriodoCes;
import com.apoyoestudiantil.entity.Reunion;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @date 26-ago-2019
 * @time 20:18:25
 * @author Leonardo Pérez
 */
public class VistaCierreReunionesDireccion extends VistaAbstracta {

    private ControladorCierreReunionesDireccion controlador;
    private HttpServletResponse response;

    public VistaCierreReunionesDireccion() {
        this.controlador = new ControladorCierreReunionesDireccion(this);
    }

    public void inicializar() {
        this.controlador.inicializar();
    }

    public void reconectar() {
        this.controlador.reconectar();
    }

    public void procesar(HttpServletRequest request, String accion) {
        switch (accion) {
            case "nivels":
                nivelSeleccionado(request);
                break;
            case "grupo":
                grupoSeleccionado(request);
                break;
            case "periodo":
                periodoSeleccionado(request);
                break;
        }
    }

    public void mostrarNiveles(ArrayList<Nivel> niveles) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        if (niveles == null || niveles.isEmpty()) {
            enviar("mostrarAlert", "No existen niveles cragados");
        } else {
            String msg = "";
            for (Nivel nv : niveles) {

                String fechaNv = "<span>Fecha de creacion: " + dateFormat.format(nv.getFechaCreacion().getTime()) + "</span>";
                String estadoNv = "<span> Estado: " + nv.getEstado().toString() + "</span>";
                String alumnNv = "<span> Cantidad de Alumnos: " + nv.getAlumnos().size() + "</span>";
                String claseNv = "<span> Clase : " + nv.getClase().getNivel() + " - " + nv.getClase().getCodigoClase() + "</span>";
                msg += "<li data-nivel-id='" + nv.getId() + "'><a href='#' onclick='nivelSeleccionado(event,this)'>" + fechaNv + estadoNv + alumnNv + claseNv + "</a></li>";
            }
            enviar("mostrarNiveles", msg);
        }
    }

    private void nivelSeleccionado(HttpServletRequest request) {
        try {
            long nvSel = Long.parseLong(request.getParameter("nivelseleccionado"));
            this.controlador.nivelSeleccionado(nvSel);
        } catch (Exception e) {
            /*UtilidadesVista u = new UtilidadesVista();
            u.mostrarError(this, e.getMessage());*/
            enviar("mostrarAlert", e.getMessage());
        }
    }

    public void mostrarGruposNivel(List<Grupo> grupos) {
        if (grupos == null || grupos.isEmpty()) {
            enviar("mostrarAlert", "No existen grupos cragados");
        } else {
            String msg = "";
            for (Grupo g : grupos) {
                String idG = "<span class='grupo-id' id='grupo-" + g.getId() + "'>Turno: " + g.getTurno() + "</span>";
                String fechaG = "<span> Año: " + g.getAnio() + "</span>";
                String cantAluG = "<span> Cantidad de Alumnos: " + g.getAlumnos().size() + "</span>";
                msg += "<li data-grupo-id='" + g.getId() + "'><a href='#' onclick='grupoSeleccionado(event,this)'>" + idG + " - " + fechaG + " - " + cantAluG + "</a></li>";
            }
            enviar("mostrarGrupos", msg);
        }
    }

    private void grupoSeleccionado(HttpServletRequest request) {
        long grupoSel = Long.parseLong(request.getParameter("gruposeleccionado"));
        this.controlador.grupoSeleccionado(grupoSel, request);
    }

    public void avisarTodaslasReunionesCerradas() {
        enviar("mostrarAlert", "Se cerraron todas las reuniones del grupo");
    }

    public void cargarReuniones(List<Reunion> reunionesSinCerrar) {
        String msg = "";
        for (Reunion r : reunionesSinCerrar) {
            String nombre = r.getNombre();
            String periodo = r.getPeriodoEnLibreta();
            msg += "<li data-reunion-id ='" + r.getId() + "'><a href='#' onclick='reunionSeelccionada(event, this)'>" + nombre + " Periodo : " + periodo + "</a></li> ";
        }
        enviar("mostrarReuniones", msg);
    }

    public void cargarPeriodos(Libreta lib) {
        String msg = "";
        for (PeriodoCes periodo : lib.getPeriodos()) {
            msg += "<li data-periodo-id ='" + periodo.getNombre() + "'> <a href='#' onclick='periodoSeleccionado(event, this)'>" + periodo.getNombre() + "</a></li>";
        }
        enviar("mostrarPeriodos", msg);
    }

    private void periodoSeleccionado(HttpServletRequest request) {
        String nombreperiodo = request.getParameter("periodoseleccionado");
        request.getSession().setAttribute("periodoSeleccionado", nombreperiodo);
        this.controlador.periodoSeleccionado(nombreperiodo);
    }

    public void mostrarReunionesSinCerrar(List<Libreta> lista) {
        String msg = "";
        for (Libreta lib : lista) {
            String nomMat = lib.getMateria().getNombre();
            String idLib = String.valueOf(lib.getId());
            msg += "<li data-libreta-id ='" + idLib + "'> <a href='#'>" + nomMat + "</a></li>";
        }
        enviar("mostrarReuniones", msg);
    }

    public void cargarAlumnos(List<Alumno> alumnos) {
        String msg = "";
        for (Alumno a : alumnos) {
            String id = String.valueOf(a.getId());
            String mat = String.valueOf(a.getMatricula());
            String doc = String.valueOf(a.getDocumento().getNumero());
            String nomCompleto = a.getPrimerApellido() + " " + a.getSegundoApellido() + "," + a.getPrimerNombre();
            msg += "<tr id='row" + id + "'> <td>" + mat + "</td><td>" + doc + "</td><td>" + nomCompleto + "</td><td><input class='form-control' type='text' name='" + id + "' ></td></tr>";
        }
        enviar("cargarAlumnos", msg);
    }

    public void procesarPost(HttpServletRequest request, HttpServletResponse res) {
        this.request = request;
        this.response = res;
        this.controlador.procesarPost();

    }

    public HashMap<Alumno, String> asociarDatosPost(List<Alumno> alumnos) {
        HashMap<Alumno, String> juicios = new HashMap<>();
        for (Alumno a : alumnos) {
            String idJuicio = String.valueOf(a.getId());
            String valorJuicio = this.request.getParameter(idJuicio);
            if (!(valorJuicio == null) || !(valorJuicio.isEmpty())) {
                juicios.put(a, valorJuicio);
            }
        }
        return juicios;
    }

    public void devolverPost(String evento, String mensaje) {
        try {

            this.request.getServletContext().getRequestDispatcher("/cierre-reuniones-direccion.jsp?evento=" + evento + "&mensaje=" + mensaje).forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(VistaCierreReunionesDireccion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
