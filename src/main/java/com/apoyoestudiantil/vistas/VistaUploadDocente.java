/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorUploadDocentes;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @date 21-jun-2019
 * @time 19:28:00
 * @author Leonardo Pérez
 */
public class VistaUploadDocente extends VistaAbstracta {

    private HttpServletResponse response;
    private ControladorUploadDocentes controlador;

    public VistaUploadDocente(HttpServletResponse response, PrintWriter out, HttpServletRequest request) {
        super(out, request);
        this.response = response;
        controlador = new ControladorUploadDocentes(this);
    }

    public void inicializarProcesoCarga(InputStream archivo, String tipoCarga) {
        
        controlador.importar(tipoCarga, archivo);
    }

    @Override
    public void enviar(String evento, String mensaje) {
        try {
            String dato = evento + " " + mensaje;
            response.sendRedirect("carga-docentes-por-lote.jsp?mensaje=" + dato);
        } catch (IOException ex) {
            Logger.getLogger(VistaUploadAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
