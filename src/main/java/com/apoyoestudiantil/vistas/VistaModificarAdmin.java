/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorModificarAdmin;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.AsyncContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Polachek
 */
public class VistaModificarAdmin {
    private HttpServletRequest request;
    private HttpServletResponse response;
    private RequestDispatcher dispatcher;
    private PrintWriter out;
    private ControladorModificarAdmin controlador;
    private Usuario usumod;
    
    public void inicializar() {
        usumod = (Usuario)request.getSession(false).getAttribute("usermod");
        controlador = new ControladorModificarAdmin(this, usumod);
    }
    
    public void mostrarError(String mensaje) {
        enviar("Error",mensaje);
    }
    
    public void mostrarExito(String mensaje) {
        enviar("Exito",mensaje);
    }
    
    public void conectarSSE(HttpServletRequest request) throws IOException {        
        request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
        AsyncContext contexto = request.startAsync();
        this.request = (HttpServletRequest)contexto.getRequest();
        contexto.getResponse().setContentType("text/event-stream");
        contexto.getResponse().setCharacterEncoding("UTF-8");
        contexto.setTimeout(0);//SIN TIMEOUT
        out = contexto.getResponse().getWriter();
    }
    
    public void procesar(HttpServletRequest request, String accion) throws AppException {
        switch(accion){
            case "modificar" : modificar(request);break;
            case "telefono" : telefono(request);break;
            case "direccion" : direccion(request);break;      
        }
    }
    
    public void enviar(String evento, String dato) {
        out.write("event: " + evento + "\n");
        dato = dato.replace("\n", "");
        out.write("data: " + dato + "\n\n");
        if (out.checkError()) {//checkError llama a flush, si da false evio bien
            System.out.println("Falló Envío");            
        } else {
            //TODO OK!
        }
    }
    
    private void modificar(HttpServletRequest request){
        controlador.modificar(request);
    }

    private void telefono(HttpServletRequest request) {
        String tipoTel = request.getParameter("tipotel");
        String numTel = request.getParameter("numtel");
        String obsTel = request.getParameter("obstel");
        controlador.agregarTelefono(tipoTel,numTel,obsTel);    
    }
    
    private void direccion(HttpServletRequest request) {
        controlador.agregarDireccion(request);
    }
    
}
