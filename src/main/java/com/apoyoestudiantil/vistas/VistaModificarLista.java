/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorModificarLista;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @date 16-jul-2019
 * @time 20:02:33
 * @author Leonardo Pérez
 */
public class VistaModificarLista extends VistaAbstracta {

    private HttpServletResponse response;
    private ControladorModificarLista controlador;

    public VistaModificarLista(HttpServletResponse response, PrintWriter out, HttpServletRequest request) {
        super(out, request);
        this.response = response;
        this.controlador = new ControladorModificarLista(this);
    }

    public void cargarLista(int indice) {
        this.controlador.cargarLista(indice, request);
    }

    public void procesarLista(HttpServletRequest request) {
        this.controlador.procesarLista(request);
    }

    @Override
    public void enviar(String evento, String mensaje) {
        try {
            // response.sendRedirect("modificar-lista.jsp?evento=" + evento + "&mensaje=" + mensaje);
            //response.sendRedirect("cargargruposdocente?evento=" + evento + "&mensaje=" + mensaje);
            this.request.getServletContext().getRequestDispatcher("/modificar-lista.jsp?evento=" + evento + "&mensaje=" + mensaje).forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(VistaUploadAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
