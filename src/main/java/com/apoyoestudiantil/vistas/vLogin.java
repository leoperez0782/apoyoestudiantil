/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorLogin;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Polachek
 */
public class vLogin {
    private HttpServletRequest request;
    private HttpServletResponse response;
    private RequestDispatcher dispatcher;
    private ControladorLogin controlador;
    
    public vLogin() {
        controlador = new ControladorLogin(this);
    }
    
    public void mostrarError(String mensaje) {
        try {
            response.sendRedirect("index.jsp?mensaje=" + mensaje);
        } catch (IOException ex) {
        }
    }
    
    public void procesar(HttpServletRequest request, HttpServletResponse response) throws AppException{
        this.request = request;
        this.response = response;
        controlador.login(request.getParameter("usuario"),
                request.getParameter("password"));
    }

    public void ingresarEstudiante(Usuario usu) {
        try {
            request.getSession(true).setAttribute("usuario", usu);
            response.sendRedirect("inicio-alumno.jsp");
           
        } catch (IOException ex) {
        }
    }

    public void ingresarAdmin(Usuario usu) {
        try {
            request.getSession(true).setAttribute("usuario", usu);
            response.sendRedirect("administrador.jsp");
           
        } catch (IOException ex) {
        }
    }

    public void ingresarPadre(Usuario usu) {
        try {
            request.getSession(true).setAttribute("usuario", usu);
            response.sendRedirect("inicio-tutor.jsp");
           
        } catch (IOException ex) {
        }
    }

    public void ingresarAdminO(Usuario usu) {
        try {
            request.getSession(true).setAttribute("usuario", usu);
            response.sendRedirect("inicio-administrativo.jsp");
           
        } catch (IOException ex) {
        }
    }

    public void ingresarDocente(Usuario usu) {
        try {
            request.getSession(true).setAttribute("usuario", usu);
            //response.sendRedirect("inicio-docente.jsp");
            response.sendRedirect("cargargruposdocente");
           
        } catch (IOException ex) {
        }
    }


    public void volverLogin() {
        try {
            response.sendRedirect("index.jsp");
        } catch (IOException ex) {
            Logger.getLogger(vLogin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void cerrarSesion(HttpServletRequest request, HttpServletResponse response) {
         this.response = response;
         this.controlador.cerrarSesion(request);
    }
}
