/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorModificarTutor;
import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.AsyncContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Polachek
 */
public class VistaModificarTutor {    
    private HttpServletRequest request;
    private HttpServletResponse response;
    private RequestDispatcher dispatcher;
    private PrintWriter out;
    private ControladorModificarTutor controlador;
    private boolean conected = false;
    private AdultoResponsable tutor;
    
    public void mostrarError(String mensaje) {
        enviar("Error",mensaje);
    }
    
    public void mostrarExito(String mensaje) {
        enviar("Exito",mensaje);
    }
    
    public void inicializar() {
        Usuario usu = (Usuario)request.getSession(false).getAttribute("usuario");
        tutor = (AdultoResponsable)request.getSession(false).getAttribute("tutor");
        controlador = new ControladorModificarTutor(this, usu, tutor);
    }
    
    public void reconectar(){
        tutor = (AdultoResponsable)request.getSession(false).getAttribute("tutor");
        controlador.reconectar();
    }
    
    public void conectarSSE(HttpServletRequest request) throws IOException {        
        request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
        AsyncContext contexto = request.startAsync();
        this.request = (HttpServletRequest)contexto.getRequest();
        contexto.getResponse().setContentType("text/event-stream");
        contexto.getResponse().setCharacterEncoding("UTF-8");
        contexto.setTimeout(0);//SIN TIMEOUT
        out = contexto.getResponse().getWriter();
        this.conected = true;
    }
    
    public void procesar(HttpServletRequest request, String accion) throws AppException {
        switch(accion){
            case "modificar" : modificar(request);break;
            case "telefono" : telefono(request);break;
            case "buscar" : buscarAlumno(request);break;
            case "direccion" : direccion(request);break;
            case "agregarAlumno" : agregarAlumno(request);break;
            case "quitarAlumno" : quitarAlumno(request);break;
            case "limpiarsession" : limpiarsession();break;            
        }
    }

    public void mostrarUsuario(String nombreUsu) {
        enviar("mostrarUsuario",nombreUsu.toUpperCase());
    }
    
    private void limpiarsession() {
        if (request.getSession() != null) {
            request.getSession().invalidate();
        }
        enviar("redireccion","index.jsp");
    }
    
    public void enviar(String evento, String dato) {
        out.write("event: " + evento + "\n");
        dato = dato.replace("\n", "");
        out.write("data: " + dato + "\n\n");
        if (out.checkError()) {//checkError llama a flush, si da false evio bien
            System.out.println("Falló Envío");            
        } else {
            //TODO OK!
        }
    }
    
    public boolean isConected() {
        return conected;
    }
    
    private void modificar(HttpServletRequest request){
        controlador.modificar(request);
    }

    private void telefono(HttpServletRequest request) {
        String tipoTel = request.getParameter("tipotel");
        String numTel = request.getParameter("numtel");
        String obsTel = request.getParameter("obstel");
        controlador.agregarTelefono(tipoTel,numTel,obsTel);    
    }
    
    private void direccion(HttpServletRequest request) {
        controlador.agregarDireccion(request);
    }

    private void buscarAlumno(HttpServletRequest request) {
        String ciAlumno = request.getParameter("numDoc");
        controlador.buscarAlumno(ciAlumno);
    }

    public void busquedaSinAlumno() {
        enviar("noAlumnos","");
    }

    public void busquedaAlumno(Alumno miAlumno) {
        String nombre="<span><b>Nombre:</b> "+miAlumno.getPrimerNombre()+"</span><br />";
        String nombreSeg="<span><b>Segundo nombre:</b> "+miAlumno.getSegundoNombre()+"</span><br />";
        String apellido="<span><b>Apellido:</b> "+miAlumno.getPrimerApellido()+"</span><br />";
        String apellidoSeg="<span><b>Segundo apellido:</b> "+miAlumno.getSegundoApellido()+"</span><br />";
        String agregar = "<a href='#' onclick='agregarAlumno(event,"+miAlumno.getId()+","+miAlumno.getDocumento().getNumero()+")'>Agregar</a>";
        String msg = nombre+nombreSeg+apellido+apellidoSeg+agregar;
        enviar("resultadoBusqueda",msg);
    }

    public void mostrarAlumnos(List<Alumno> al) {
        String tabla="<table id='tabla_alumnos'><tr><th>Nombre</th><th>Apellido</th>\n" +
                     "<th>Cedula</th><th></th></tr>";
        String alm ="";        
        for (Alumno a : al) {
            alm+= "<tr>";
            alm+= "<td>"+a.getPrimerNombre()+"</td>";
            alm+= "<td>"+a.getPrimerApellido()+"</td>";
            alm+= "<td>"+a.getDocumento().getNumero()+"</td>";
            alm+= "<td><a href='#' onclick='quitarAlumno(event,this,"+a.getDocumento().getNumero()+")'>Quitar de la lista</a></td>";
            alm+= "</tr>";
        }
        String msg=tabla+alm+"</table>";
        enviar("listaAlumnos",msg);
    }    

    private void agregarAlumno(HttpServletRequest request) {
        Long idAlmuno = Long.parseLong(request.getParameter("idAlm"));
        controlador.agregarAlumno(idAlmuno);
    }

    public void agregarListaAlumno(Alumno a) {        
        String alm = "<td>"+a.getPrimerNombre()+"</td>";
        alm+= "<td>"+a.getPrimerApellido()+"</td>";
        alm+= "<td>"+a.getDocumento().getNumero()+"</td>";
        alm+= "<td><a href='#' onclick='quitarAlumno(event,this,"+a.getDocumento().getNumero()+")'>Quitar de la lista</a></td>";
        enviar("agregarListaAlumno",alm);
    }

    private void quitarAlumno(HttpServletRequest request) {
        Long docAlm = Long.parseLong(request.getParameter("docAlm"));
        controlador.quitarAlumno(docAlm);
    }   
}
