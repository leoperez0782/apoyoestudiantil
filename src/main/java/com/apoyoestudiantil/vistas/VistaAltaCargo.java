/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorAltaCargo;
import com.apoyoestudiantil.entity.Cargo;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @date 22-jun-2019
 * @time 23:09:48
 * @author Leonardo Pérez
 */
public class VistaAltaCargo extends VistaAbstracta{
    private HttpServletResponse response;
    private ControladorAltaCargo controlador;

    public VistaAltaCargo(HttpServletResponse response, PrintWriter out, HttpServletRequest request) {
        super(out, request);
        this.response = response;
        this.controlador = new ControladorAltaCargo(this);
    }

    public void guardarCargo(String tipo, String descripcion, boolean activo) {
       Cargo c = new Cargo();
       //c.setActivo(activo);
       c.setDescripcion(descripcion);
       c.setTipoCargo(tipo);
       controlador.guardar(c);
    }
    
    @Override
   public void enviar( String evento,String mensaje){
        try {
            String dato = evento + " " + mensaje;
            response.sendRedirect("alta-cargos.jsp?mensaje=" +dato);
        } catch (IOException ex) {
            Logger.getLogger(VistaUploadAlumno.class.getName()).log(Level.SEVERE, null, ex);
        }
   }
    
}
