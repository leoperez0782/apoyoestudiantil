/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.vistas;


import com.apoyoestudiantil.controladores.ControladorInicioTutor;
import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Cargo;
import com.apoyoestudiantil.entity.Notificacion;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.AsyncContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Polachek
 */
public class VistaInicioTutor {
    private HttpServletRequest request;
    private HttpServletResponse response;
    private RequestDispatcher dispatcher;
    private PrintWriter out;
    private ControladorInicioTutor controlador;
    private boolean conected = false;
    
    public void inicializar() {
        Usuario usu = (Usuario)request.getSession(false).getAttribute("usuario");
        controlador = new ControladorInicioTutor(this, (AdultoResponsable)usu);
    }
    
    public void reconectar(){
        controlador.reconectar();
    }
    
    public void mostrarError(String mensaje) {
        enviar("Error",mensaje);
    }
    
    public void mostrarExito(String mensaje) {
        enviar("Exito",mensaje);
    }

    public void conectarSSE(HttpServletRequest request) throws IOException {        
        request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
        AsyncContext contexto = request.startAsync();
        this.request = (HttpServletRequest)contexto.getRequest();
        contexto.getResponse().setContentType("text/event-stream");
        contexto.getResponse().setCharacterEncoding("UTF-8");
        contexto.setTimeout(0);//SIN TIMEOUT
        out = contexto.getResponse().getWriter();
        this.conected = true;
    }
    
    public void procesar(HttpServletRequest request, String accion) throws AppException{
        switch(accion){
            case "tutorAlumno" : iniciarTA(request);break;
            case "notificaciones" : notificaciones(request);break;
            case "verNotificacion" : verNotificacion(request);break; 
            case "comprobarNotificaciones" : comprobarNotificaciones(request);break;
        }
    }
    
    public void enviar(String evento, String dato) {
        out.write("event: " + evento + "\n");
        dato = dato.replace("\n", "");
        out.write("data: " + dato + "\n\n");
        if (out.checkError()) {//checkError llama a flush, si da false evio bien
            System.out.println("Falló Envío");            
        } else {
            //TODO OK!
        }
    }
    
    public boolean isConected() {
        return conected;
    }

    public void mostrarAlumnos(List<Alumno> al) {
        String alm ="";
        for (Alumno a : al) {
            alm+= "<div class='hijo-item'>";
            alm+= "<a href='#' class='item-dash' onclick='tutorEstudiante(event,"+a.getDocumento().getNumero()+")'>";
            alm+= "<div class='hijo-item-photo'>";
            alm+= "<img src='resources/images/student-logo.png' alt='Foto de "+a.getPrimerNombre() + " "+a.getPrimerApellido()+" ' />";
            alm+= "</div><div class='hijo-item-data'>";
            alm+= "<span class='hijo-name'>";
            alm+= a.getPrimerNombre()+" "+a.getPrimerApellido();
            alm+= "</span><span class='hijo-doc'>";
            alm+= a.getDocumento().getNumero();
            alm+= "</span></div><div class='clear'></div></a></div>";
        }
        enviar("listadoAlumnos",alm);
    }

    private void iniciarTA(HttpServletRequest request) {
        controlador.iniciarTA(request);
    }

    public void iniciarTAOK(Alumno miAlumno) {
        request.getSession(true).setAttribute("alumno", miAlumno);
        enviar("redireccion","inicio-tutor-estudiante.jsp");
    }

    private void notificaciones(HttpServletRequest request) {
        controlador.notificaciones(request);
    }

    public void mostrarNotificaciones(List<Notificacion> lista) {
        String alm ="";
        String tabla="<tr><th>Prioridad</th><th>Fecha</th><th>Asunto</th>\n" +
                     "<th>Remitente</th><th></th></tr>";
        for (Notificacion a : lista) {
            String color="";
            String fontW="normal";
            switch(a.getPrioridad().toString()){
                case "ALTA" : color="#f22800";break;
                case "NORMAL" : color="#000000";break;
                case "BAJA" : color="#D3D3D3";break;  
            }            
            if(!a.isLeida()){
                fontW = "bold";
            }
            alm+= "<tr style='font-weight:"+fontW+"'>";
            alm+= "<td style='color:"+color+"'>"+a.getPrioridad().toString()+"</td>";
            alm+= "<td>"+a.getFechaEnvio()+"</td>";
            alm+= "<td>"+a.getAsunto()+"</td>";
            alm+= "<td>"+a.getRemitente().getPrimerNombre()+" "+a.getRemitente().getPrimerApellido()+"</td>";
            alm+= "<td><a href='#' onclick='verNotificacion(event,"+a.getId()+")'>Ver Notificación</a></td>";
            alm+= "</tr>";
        }
        enviar("mostrarNotificaciones",tabla+alm);
    }

    public void hayNuevasNotificaciones() {
        enviar("hayNuevasNotificaciones","");
    }

    private void verNotificacion(HttpServletRequest request) {
        controlador.verNotificacion(request);
    }

    public void mostrarNotificacion(Notificacion not) {
        String msg="";
        msg += "<h1>"+not.getAsunto()+"</h1>";
        msg += "<p>"+not.getCuerpoMensaje()+"</p>";
        msg += "<div id='cerrar-noti'><a href='#' onclick='cerrarNotificacion(event)'>Cerrar</a>";
        enviar("mostrarNotificacion",msg);
    }

    public void noHayNuevasNotificaciones() {
        enviar("noHayNuevasNotificaciones","");
    }

    private void comprobarNotificaciones(HttpServletRequest request) {
        controlador.comprobarNotificaciones();
    }
    
}
