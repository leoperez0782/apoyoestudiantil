/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorListadoDocente;
import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.interactores.SistemaDocentes;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @date 28-jun-2019
 * @time 21:14:00
 * @author Leonardo Pérez
 */
public class VistaListadoDocente extends VistaAbstracta{
    private HttpServletResponse response;
    private ControladorListadoDocente controlador;
    

    public VistaListadoDocente(HttpServletResponse response, PrintWriter out, HttpServletRequest request) {
        super(out, request);
        this.response = response;
        this.controlador = new ControladorListadoDocente(this);
        
    }

    public List<Docente> cargarDocentes() {
       return this.controlador.cargarDocentes();
    }

   
    
    
}
