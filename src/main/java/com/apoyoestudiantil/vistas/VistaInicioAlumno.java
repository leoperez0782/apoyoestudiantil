/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorInicioAlumno;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Evaluacion;
import com.apoyoestudiantil.entity.Inasistencia;
import com.apoyoestudiantil.entity.ItemDesarrollo;
import com.apoyoestudiantil.entity.Notificacion;
import com.apoyoestudiantil.entity.Promedio;
import com.apoyoestudiantil.excepciones.AppException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.AsyncContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Polachek
 */
public class VistaInicioAlumno {
    
    private HttpServletRequest request;
    private HttpServletResponse response;
    private RequestDispatcher dispatcher;
    private PrintWriter out;
    private ControladorInicioAlumno controlador;
    private boolean conected = false;
    private Alumno alumno;
    
    public void inicializar() {
        alumno = (Alumno)request.getSession(false).getAttribute("usuario");
        controlador = new ControladorInicioAlumno(this,alumno);
    }
    
    public void reconectar(){
        controlador.reconectar();
    }
    
    public void mostrarError(String mensaje) {
        enviar("Error",mensaje);
    }
    
    public void mostrarExito(String mensaje) {
        enviar("Exito",mensaje);
    }

    public void conectarSSE(HttpServletRequest request) throws IOException {        
        request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
        AsyncContext contexto = request.startAsync();
        this.request = (HttpServletRequest)contexto.getRequest();
        contexto.getResponse().setContentType("text/event-stream");
        contexto.getResponse().setCharacterEncoding("UTF-8");
        contexto.setTimeout(0);//SIN TIMEOUT
        out = contexto.getResponse().getWriter();
        this.conected = true;
    }
    
    public void procesar(HttpServletRequest request, String accion) throws AppException{
        switch(accion){
            case "asistencias" : asistencias(request);break;
            case "evaluaciones" : evaluaciones(request);break;   
            case "promedios" : promedios();break;
            case "calendario" : calendario();break;
            case "notificaciones" : notificaciones(request);break;
            case "verNotificacion" : verNotificacion(request);break; 
            case "comprobarNotificaciones" : comprobarNotificaciones(request);break;
        }
    }
    
    public void enviar(String evento, String dato) {
        out.write("event: " + evento + "\n");
        dato = dato.replace("\n", "");
        out.write("data: " + dato + "\n\n");
        if (out.checkError()) {//checkError llama a flush, si da false evio bien
            System.out.println("Falló Envío");            
        } else {
            //TODO OK!
        }
    }
    
    public boolean isConected() {
        return conected;
    }  

    private void mostrarAlumno() {
        String alm = "";
        alm+= "<div class='hijo-item'>";
        alm+= "<a href='#' class='item-dash' onclick='tutorEstudiante(event)'>";
        alm+= "<div class='hijo-item-photo'>";
        alm+= "<img src='resources/images/student-logo.png' alt='Foto de "+alumno.getPrimerNombre() + " "+alumno.getPrimerApellido()+" ' />";
        alm+= "</div><div class='hijo-item-data'>";
        alm+= "<span class='hijo-name'>";
        alm+= alumno.getPrimerNombre()+" "+alumno.getPrimerApellido();
        alm+= "</span><span class='hijo-doc'>";
        alm+= alumno.getDocumento().getNumero();
        alm+= "</span></div><div class='clear'></div></a></div>";
        enviar("mostrarAlumno",alm);
    }

    private void asistencias(HttpServletRequest request) {
        controlador.asistencias();
    }

    public void mostrarInasistencias(List<Inasistencia> inasistencias) {
        String msg ="";
        String tabla="";
        String alm =""; 
        if(inasistencias.size() > 0){
            tabla+="<tr><th>Fecha</th><th>Tipo</th>\n" +
                     "<th>Justificacion</th></tr>";                   
            for (Inasistencia i : inasistencias) {
                alm+= "<tr>";
                alm+= "<td>"+i.getFecha()+"</td>";
                alm+= "<td>"+i.getTipo().toString()+"</td>";
                alm+= "<td>"+i.getJustificacion()+"</td>";
                alm+= "</tr>";
            }            
        }else{
            tabla+="<tr><th></th></tr>";
            alm+= "<tr><td>"+alumno.getPrimerNombre()+" "+alumno.getPrimerApellido();
             alm+= " no tiene inasistencias en nuestros registros<td></tr>";
        }        
        enviar("Inasistencias",tabla+alm);        
    }

    private void evaluaciones(HttpServletRequest request) {
         controlador.evaluaciones();
    }
    
    private void promedios() {
        controlador.promedios();
    }
    
    private void calendario() {
        controlador.calendario();
    }
    
    public void mostrarCalendario(List<ItemDesarrollo> items) {
        String tabla="<tr><th>Período</th><th>Fecha</th>\n" +
                     "<th>Tipo</th><th></th></tr>";
        String alm =""; 
        for (ItemDesarrollo i : items) {
            String fecha=""+i.getFecha().getDayOfMonth()+"-"+i.getFecha().getMonthValue()+"-"+i.getFecha().getYear();
            String hora = "";
            String horaF = "";
            String turno = i.getLibreta().getTurno().name();
            switch(turno){
                case "MATUTINO" : 
                    hora="08:00 AM";
                    horaF="09:00 AM";
                    break;
                case "VESPERTINO" : 
                    hora="13:00 PM";
                    horaF="14:00 PM";
                    break;
                case "NOCTURNO" : 
                    hora="19:00 PM";
                    horaF="20:00 PM";
                    break;
            }
            alm+= "<tr>";
            alm+= "<td>"+i.getPeriodo()+"</td>";
            alm+= "<td>"+i.getFecha().getDayOfMonth()+"/"+i.getFecha().getMonthValue()+"/"+i.getFecha().getYear()+"</td>";
            alm+= "<td>"+i.getTipo().name()+"</td>";
            alm+= "<td><div title='Agendar en el calendario' class='addeventatc'>";
            alm+= "Agendar en el calendario";
            alm+= "<span class='start'>"+fecha+" "+hora+"</span>\n" +
"                <span class='end'>"+fecha+" "+horaF+"</span>\n" +
"                <span class='timezone'>-03:00, Montevideo</span>\n" +
"                <span class='title'>"+i.getTipo().name()+" de "+alumno.getPrimerApellido()+"</span>";
            alm+= "</div></tr>";
        }
        enviar("mostrarCalendario",tabla+alm);
    }
    
    public void mostrarPromedios(List<Promedio> promedios){
        String tabla="<tr><th>Estado</th><th>Período</th><th>Materia</th>\n" +
                     "<th>Promedio</th><th>Conducta</th><th>Comentarios</th></tr>";
        String alm =""; 
        for (Promedio p : promedios) {
            alm+= "<tr>";
            alm+= "<td>"+p.getEstado()+"</td>";
            alm+= "<td>"+p.getPeriodo()+"</td>";
            alm+= "<td>"+p.getMateria()+"</td>";
            alm+= "<td>"+p.getPromedio()+"</td>";
            alm+= "<td>"+p.getConducta()+"</td>";
            alm+= "<td>"+p.getComentario()+"</td>";
            alm+= "</tr>";
        }
        enviar("mostrarPromedios",tabla+alm);
    }
    
    public void mostrarEvaluaciones(List<Evaluacion> lista) {
        String msg ="";
        String tabla="";
        String alm =""; 
        if(!lista.isEmpty()){
            tabla+="<tr><th>Fecha</th><th>Período</th>\n" +
                     "<th>Tipo</th><th>Calificación</th><th>Comentarios</th></tr>";
            for (Evaluacion e : lista) {
                alm+= "<tr>";
                alm+= "<td>"+e.getFecha().getDayOfMonth()+"/"+e.getFecha().getMonthValue()+"/"+e.getFecha().getYear()+"</td>";
                alm+= "<td>"+e.getPeriodo()+"</td>";
                alm+= "<td>"+e.getTipo().toString()+"</td>";
                alm+= "<td>"+e.getCalificacion()+"</td>";
                alm+= "<td>"+e.getComentario()+"</td>";
                alm+= "</tr>";
            }
        }else{
            tabla+="<tr><th></th></tr>";
            alm+= "<tr><td>No hay evaluaciones</td></tr>";
        }
        enviar("Evaluaciones",tabla+alm);
    }
    
    private void notificaciones(HttpServletRequest request) {
        controlador.notificaciones(request);
    }
    
    public void mostrarNotificaciones(List<Notificacion> lista) {
        String alm ="";
        String tabla="<tr><th>Prioridad</th><th>Fecha</th><th>Asunto</th>\n" +
                     "<th>Remitente</th><th></th></tr>";
        for (Notificacion a : lista) {
            String color="";
            String fontW="normal";
            switch(a.getPrioridad().toString()){
                case "ALTA" : color="#f22800";break;
                case "NORMAL" : color="#000000";break;
                case "BAJA" : color="#D3D3D3";break;  
            }            
            if(!a.isLeida()){
                fontW = "bold";
            }
            alm+= "<tr style='font-weight:"+fontW+"'>";
            alm+= "<td style='color:"+color+"'>"+a.getPrioridad().toString()+"</td>";
            alm+= "<td>"+a.getFechaEnvio()+"</td>";
            alm+= "<td>"+a.getAsunto()+"</td>";
            alm+= "<td>"+a.getRemitente().getPrimerNombre()+" "+a.getRemitente().getPrimerApellido()+"</td>";
            alm+= "<td><a href='#' onclick='verNotificacion(event,"+a.getId()+")'>Ver Notificación</a></td>";
            alm+= "</tr>";
        }
        enviar("mostrarNotificaciones",tabla+alm);
    }

    public void hayNuevasNotificaciones() {
        enviar("hayNuevasNotificaciones","");
    }

    private void verNotificacion(HttpServletRequest request) {
        controlador.verNotificacion(request);
    }
    
    public void mostrarNotificacion(Notificacion not) {
        String msg="";
        msg += "<h1>"+not.getAsunto()+"</h1>";
        msg += "<p>"+not.getCuerpoMensaje()+"</p>";
        msg += "<div id='cerrar-noti'><a href='#' onclick='cerrarNotificacion(event)'>Cerrar</a>";
        enviar("mostrarNotificacion",msg);
    }

    public void noHayNuevasNotificaciones() {
        enviar("noHayNuevasNotificaciones","");
    }

    private void comprobarNotificaciones(HttpServletRequest request) {
        controlador.comprobarNotificaciones();
    }
}
