/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.vistas;

import com.apoyoestudiantil.controladores.ControladorModificarAdminLista;
import com.apoyoestudiantil.entity.Administrador;
import com.apoyoestudiantil.entity.Administrativo;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.servlet.AsyncContext;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Polachek
 */
public class VistaModificarAdminLista {
    private HttpServletRequest request;
    private HttpServletResponse response;
    private RequestDispatcher dispatcher;
    private PrintWriter out;
    private ControladorModificarAdminLista controlador;
    
    public void inicializar(String rol) {
        controlador = new ControladorModificarAdminLista(this, rol);
    }
    
    public void mostrarError(String mensaje) {
        enviar("Error",mensaje);
    }
    
    public void mostrarExito(String mensaje) {
        enviar("Exito",mensaje);
    }
    
    public void conectarSSE(HttpServletRequest request) throws IOException {        
        request.setAttribute("org.apache.catalina.ASYNC_SUPPORTED", true);
        AsyncContext contexto = request.startAsync();
        this.request = (HttpServletRequest)contexto.getRequest();
        contexto.getResponse().setContentType("text/event-stream");
        contexto.getResponse().setCharacterEncoding("UTF-8");
        contexto.setTimeout(0);//SIN TIMEOUT
        out = contexto.getResponse().getWriter();
    }
    
    public void procesar(HttpServletRequest request, String accion) throws AppException {
        switch(accion){
            case "buscar" : buscar(request);break;
            case "mostrarTodos" : mostrarTodos();break;
            case "modificar" : modificar(request);break;
        }
    }
    
    public void enviar(String evento, String dato) {
        out.write("event: " + evento + "\n");
        dato = dato.replace("\n", "");
        out.write("data: " + dato + "\n\n");
        if (out.checkError()) {//checkError llama a flush, si da false evio bien
            System.out.println("Falló Envío");            
        } else {
            //TODO OK!
        }
    }

    private void buscar(HttpServletRequest request) {
        controlador.buscar(request);
    }

    private void mostrarTodos() {
        controlador.mostrarTodos();
    }

    public void mostrarAdministradores(List<Administrador> users) {
        String tabla="<table id='listado_usuarios' class='table_style_1'>\n"+
                     "<tr><th>Nombre</th><th>Apellido</th><th>Cedula</th>\n" +
                     "<th>Email</th><th></th></tr>";
        String alm ="";        
        for (Administrador a : users) {
            alm+= "<tr>";
            alm+= "<td>"+a.getPrimerNombre()+"</td>";
            alm+= "<td>"+a.getPrimerApellido()+"</td>";
            alm+= "<td>"+a.getDocumento().getNumero()+"</td>";
            alm+= "<td>"+a.getEmail()+"</td>";
            alm+= "<td><a href='#' onclick='modificarUsuario(event,"+a.getDocumento().getNumero()+")'>Modificar</a></td>";
            alm+= "</tr>";
        }
        String msg=tabla+alm+"</table>";
        enviar("mostrarListaUsuarios",msg);
    }
    
    public void mostrarAdministrativos(List<Administrativo> users) {
        String tabla="<table id='listado_usuarios' class='table_style_1'>\n"+
                     "<tr><th>Nombre</th><th>Apellido</th><th>Cedula</th>\n" +
                     "<th>Email</th><th></th></tr>";
        String alm ="";        
        for (Administrativo a : users) {
            alm+= "<tr>";
            alm+= "<td>"+a.getPrimerNombre()+"</td>";
            alm+= "<td>"+a.getPrimerApellido()+"</td>";
            alm+= "<td>"+a.getDocumento().getNumero()+"</td>";
            alm+= "<td>"+a.getEmail()+"</td>";
            alm+= "<td><a href='#' onclick='modificarUsuario(event,"+a.getDocumento().getNumero()+")'>Modificar</a></td>";
            alm+= "</tr>";
        }
        String msg=tabla+alm+"</table>";
        enviar("mostrarListaUsuarios",msg);
    }

    public void resultadoBusqueda(Usuario a) {
        String tabla="<table id='listado_usuarios' class='table_style_1'>\n"+
                     "<tr><th>Nombre</th><th>Apellido</th><th>Cedula</th>\n" +
                     "<th>Email</th><th></th></tr>";
        String alm ="";        
        alm+= "<tr>";
        alm+= "<td>"+a.getPrimerNombre()+"</td>";
        alm+= "<td>"+a.getPrimerApellido()+"</td>";
        alm+= "<td>"+a.getDocumento().getNumero()+"</td>";
        alm+= "<td>"+a.getEmail()+"</td>";
        alm+= "<td><a href='#' onclick='modificarUsuario(event,"+a.getDocumento().getNumero()+")'>Modificar</a></td>";
        alm+= "</tr>";
        String msg=tabla+alm+"</table>";
        enviar("mostrarListaUsuarios",msg);
    }

    private void modificar(HttpServletRequest request) {
        controlador.modificar(request);
    }

    public void modificarUser() {
        enviar("redirectModificar","");
    }
}
