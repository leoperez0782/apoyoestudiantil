/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.NivelDAO;
import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.entity.Telefono;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.interactores.SistemaUsuarios;
import com.apoyoestudiantil.vistas.VistaEstudianteModForm;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Polachek
 */
public class ControladorEstudianteModForm {
    private VistaEstudianteModForm vista;
    private Usuario usuario;
    private Institucion sistema = Institucion.getInstancia();
    private Alumno alumno;
    private List<Telefono> telefonos = new ArrayList<>();
    private List<Domicilio> domicilios = new ArrayList<>();
    private EntityManager mng = ContextoDB.getInstancia().getManager();
    EntityTransaction tx = mng.getTransaction();
    
    public ControladorEstudianteModForm(VistaEstudianteModForm vista, Alumno al, Usuario usu) {
        this.vista = vista;
        this.alumno = al;
        this.usuario = usu;
    }
    
    public void reconectar() {
    }
    
    protected Institucion getSistema(){
        return sistema;
    }

    public void modificar(HttpServletRequest request) {
        AlumnoDAO daoAlumno = new AlumnoDAO(mng);
        NivelDAO dao = new NivelDAO(mng);
        Alumno miAlumno = null;
        Nivel miNivel = null;
        try {
            miAlumno = (Alumno)daoAlumno.findUsuarioByNumeroDoc(alumno.getDocumento().getNumero());
        } catch (AppException ex) {
            Logger.getLogger(ControladorModificarTutor.class.getName()).log(Level.SEVERE, null, ex);
        }
        cargarDatos(miAlumno, request);
        if(!domicilios.isEmpty()){
            miAlumno.setDomicilios(domicilios);
        }        
        if(!telefonos.isEmpty()){
            miAlumno.setTelefonos(telefonos);
        }
        
        boolean agrego;
        String errmsg="";
        miNivel = miAlumno.getNivel();
        try{
            daoAlumno.updateTr(tx,mng,miAlumno);
            dao.updateTr(tx,mng,miNivel);
            //actualizamos el nivel
            agrego = dao.update(miNivel);
        }catch(Exception ex){
                agrego = false;
                errmsg = " - Ya existe un usuario con los datos ingresados";
        }catch(Throwable e){
                agrego = false;
                errmsg = " - Ya existe un usuario con los datos ingresados";
        } 
       
        if(agrego){
            request.getSession(true).setAttribute("alumno", miAlumno);
            // Si agrego actualizo entidad local
            cargarDatos(alumno, request);
            if(!domicilios.isEmpty()){
                alumno.setDomicilios(domicilios);
            }        
            if(!telefonos.isEmpty()){
                alumno.setTelefonos(telefonos);
            }
            // Fin - Si agrego actualizo entidad local
            vista.mostrarExito("Se actualizo el Alumno exitosamente"+errmsg);
        }else{
            vista.mostrarError("No se pudo actualizar el Alumno"+errmsg);
        }
        telefonos.clear();
        domicilios.clear();
    }
    
    private void cargarDatos(Alumno a, HttpServletRequest request) {
        //Datos Personales
        a.setEmail(request.getParameter("email"));
        a.setFechaNacimiento(request.getParameter("fechan"));
        a.setPrimerNombre(request.getParameter("nombre"));
        a.setSegundoNombre(request.getParameter("nombre_segundo"));
        a.setPrimerApellido(request.getParameter("apellido"));
        a.setSegundoApellido(request.getParameter("apellido_segundo"));
        a.setMatricula(Integer.parseInt(request.getParameter("matricula")));
        if(request.getParameter("clave")!= null){
            a.setClave(request.getParameter("clave"));
        }        
        if(request.getParameter("user_estado").equals("true")){
            a.setActivo(true);
        }else if(request.getParameter("user_estado").equals("false")){
            a.setActivo(false);
        }
    }

    public void agregarTelefono(String tipoTel, String numTel, String obsTel) {
        Telefono telefono = new Telefono();
        telefono.setTipo(Telefono.TipoTelefono.valueOf(tipoTel.toUpperCase()));
        telefono.setNumero(numTel);
        telefono.setObservaciones(obsTel);
        telefonos.add(telefono);
    }
    
    public void agregarDireccion(HttpServletRequest request) {
        Domicilio domi = new Domicilio();
        domi.setApartamento(request.getParameter("apto"));
        domi.setBarrio(request.getParameter("barrio"));
        domi.setBlock(request.getParameter("block"));
        domi.setCalle(request.getParameter("calle"));
        domi.setDepartamento(Domicilio.Departamento.valueOf(request.getParameter("departamento")));
        domi.setEdificio(request.getParameter("edificio"));
        domi.setEsquina(request.getParameter("esquina"));
        domi.setLocalidad(request.getParameter("localidad"));
        domi.setManzanaSolar(request.getParameter("manzanasolar"));
        domi.setNumero(request.getParameter("numero"));
        domi.setTorre(request.getParameter("isTorre"));
        domicilios.add(domi);
    }

    
}
