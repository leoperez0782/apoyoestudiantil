/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.FactoriaCargadoresArchivo;
import com.apoyoestudiantil.interactores.ICargadorArchivos;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaCargaAlumnosExcel;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @date 22-ago-2019
 * @time 11:02:20
 * @author Leonardo Pérez
 */
public class ControladorCargaAlumnosExcel {

    private InputStream archivo;
    private VistaCargaAlumnosExcel vista;
    private Institucion sistema;
    private Nivel nivel;
    private Grupo grupo;

    public ControladorCargaAlumnosExcel(VistaCargaAlumnosExcel vista) {
        this.vista = vista;
        this.sistema = Institucion.getInstancia();
        this.vista.mostrarNiveles(sistema.getNiveles());
    }

    public void reconectar() {
        vista.mostrarNiveles(sistema.getNiveles());
    }

    public void nivelSeleccionado(long nvSel) {
        nivel = sistema.buscarNivelID(nvSel);
        vista.mostrarGruposNivel(nivel.getGrupos());
    }

    public void grupoSeleccionado(long grupoSel) {
        grupo = nivel.getGrupo(grupoSel);
    }

    public void importar(InputStream archivo, String tipoCarga) {
        try {
            this.archivo = archivo;
            guardarDatos(tipoCarga);

            
            vista.devolverPost("Exito", "Se agregaron los datos correctamente");
            vista.mostrarNiveles(sistema.getNiveles());
        }catch(AppException ae){
            vista.devolverPost("Error", ae.getMessage());
        }catch (IllegalArgumentException ie) {//lo lanza cuando trata de convertir una celda vacia en enum de departamento o si el archivo no tiene el formato correcto.
            vista.devolverPost("Error", "El formato de las celdas de la planilla no es correcto, o el archivo elegido no es una planilla Excel");
        } catch (Exception ex) {
            Logger.getLogger(ControladorUploadAlumnos.class.getName()).log(Level.SEVERE, null, ex);
            
            vista.devolverPost("Error", ex.getMessage());
        }
    }

    private void guardarDatos(String tipoCarga) throws AppException {
        ICargadorArchivos.TipoCarga tipo = ICargadorArchivos.TipoCarga.valueOf(tipoCarga.toUpperCase());

        List<Alumno> lista = sistema.cargarArchivo(archivo, FactoriaCargadoresArchivo.Tipo.ESTUDIANTE, tipo);
        sistema.inscribirAlumnos(lista, this.nivel, this.grupo);
    }

}
