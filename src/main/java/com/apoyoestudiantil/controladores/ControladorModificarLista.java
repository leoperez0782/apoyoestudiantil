/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.entity.Inasistencia;
import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.entity.Lista;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaModificarLista;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 * @date 16-jul-2019
 * @time 20:08:13
 * @author Leonardo Pérez
 */
public class ControladorModificarLista {


    private Institucion sistema;
    private VistaModificarLista vista;
    private Libreta libretaActual;
    private Lista listaActual;

    public ControladorModificarLista(VistaModificarLista vista) {
        this.vista = vista;
        this.sistema = Institucion.getInstancia();
    }

    public void cargarLista(int indice, HttpServletRequest request) {
        try {
            List<Lista> listas = (List) request.getSession().getAttribute("pasajesLista");
          
            this.libretaActual = (Libreta) request.getSession().getAttribute("libretaActual");
            this.listaActual = libretaActual.getPasajesDeLista().get(indice);
            if(listaActual.getInasistencias().isEmpty()){
                this.sistema.actualizarListado(listaActual);
            }

            request.getSession().setAttribute("listaActual", this.listaActual);
            SimpleDateFormat formater = new SimpleDateFormat("dd/MM/yyyy");
            String fechaAMostrar = listaActual.getFecha();
            request.setAttribute("fecha", fechaAMostrar);
            String horario = listaActual.getHorario().getHoraInicio() + " " + listaActual.getHorario().getHoraFin();
            request.setAttribute("horario", horario);
        } catch (AppException ex) {
            Logger.getLogger(ControladorPasajeLista.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void procesarLista(HttpServletRequest request) {
        int tope = this.listaActual.getInasistencias().size();
        String nomTipoFalta;

        try {
            for (int i = 0; i < tope; i++) {

                nomTipoFalta = "tipoFalta" + i;

                String valorTipoFalta = request.getParameter(nomTipoFalta);

                Inasistencia inasist = listaActual.getInasistencias().get(i);
                inasist.setEstado(Inasistencia.devolverEstado(valorTipoFalta));
                inasist.setJustificacion(valorTipoFalta);
                inasist.setTipo(Inasistencia.TipoInasistencia.valueOf(valorTipoFalta));

            }
            if (this.sistema.actualizarLista(this.libretaActual, listaActual)) {
                this.vista.enviar("Exito", "Se actualizo la lista");
            } else {
                this.vista.enviar("Error", "No se pudo actualizar");
            }
        } catch (Exception e) {
            this.vista.enviar("Error", e.getMessage());
        }

    }

}
