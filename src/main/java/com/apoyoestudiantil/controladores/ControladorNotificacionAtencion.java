/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaNotificacionAtencion;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 * @date 5 sept. 2019
 * @time 21:54:15
 * @author Leonardo Pérez
 */
public class ControladorNotificacionAtencion {

    private VistaNotificacionAtencion vista;
    private Institucion sistema;
    private Libreta libretaActual;

    public ControladorNotificacionAtencion(VistaNotificacionAtencion vista) {
        this.vista = vista;
        this.sistema = Institucion.getInstancia();
    }

    public void cargarLista(int indice, HttpServletRequest request) {
        try {
            List<Libreta> libretas = (List) request.getSession().getAttribute("listaLibretas");
            this.libretaActual = libretas.get(indice);

            List<Alumno> alumnos = libretaActual.getGrupo().getAlumnos();
            request.getSession().setAttribute("libretaActual", libretaActual);
            request.getSession().setAttribute("alumnosGrupo", alumnos);

        } catch (Exception ex) {
            Logger.getLogger(ControladorNotificacionAtencion.class.getName()).log(Level.SEVERE, null, ex);
            this.vista.enviar("Error", ex.getMessage());
        }
    }

    public void procesarMensajes(HttpServletRequest request) {
        try {
            String valorMensajeGrup = request.getParameter("txtNotifGrupal");
            if (valorMensajeGrup == null || valorMensajeGrup.isEmpty()) {
                //busco mensajes individuales
                int indice = Integer.parseInt(request.getParameter("txtSeleccionado"));
                String idtexto = "txt" + indice;
                String mensaje = request.getParameter(idtexto);
                if (mensaje.isEmpty()) {
                    this.vista.enviar("Error", "El mensaje no puede estar vacio");
                } else {
                    this.sistema.enviarNotificacionAtencion(mensaje, this.libretaActual, indice);
                    this.vista.enviar("Exito", "Se envio la notificacion");
                }

            } else {
                this.sistema.enviarNotificacionAtencionGrupal(valorMensajeGrup, this.libretaActual);
                this.vista.enviar("Exito", "Se enviaron las notificaciones");
            }
        } catch(NumberFormatException ne){
            Logger.getLogger(ControladorNotificacionAtencion.class.getName()).log(Level.SEVERE, null, ne);
            String mensaje = "Si desea enviar un mensaje individual, debe utilizar el boton 'enviar', de lo contrario llene el cuadro de texto de 'mensaje grupal'";
             this.vista.enviar("Error",mensaje );
        }catch (Exception e) {
            Logger.getLogger(ControladorNotificacionAtencion.class.getName()).log(Level.SEVERE, null, e);
            this.vista.enviar("Error", e.getMessage());
        }
    }

}
