/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.entity.Cargo;
import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Empleado;
import com.apoyoestudiantil.entity.Telefono;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaModificarDocente;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 * @date 26-jun-2019
 * @time 18:55:23
 * @author Leonardo Pérez
 */
public class ControladorModificarDocente {

    private VistaModificarDocente vista;
    private Docente usumod;
    private List<Telefono> telefonos = new ArrayList<>();
    private List<Domicilio> domicilios = new ArrayList<>();
    private Institucion sistema = Institucion.getInstancia();
    private List<Cargo> cargos = new ArrayList<>();
    
    public ControladorModificarDocente(VistaModificarDocente vista, Docente usu) {
        this.vista = vista;
        this.usumod = usu;
        mostrarCargos();
    }
    
    protected Institucion getSistema(){
        return sistema;
    }
    
    private void mostrarCargos() {
        cargos = obtenerCargosDisponibles();
        if(!cargos.isEmpty()){
            vista.mostrarCargos(cargos);
        }else{
            vista.mostrarError("No hay cargos agregados al sistema");
        }
    }
    
    public List<Cargo> obtenerCargosDisponibles() {
        try {
            return sistema.getAllCargos();
        } catch (AppException ex) {
            Logger.getLogger(ControladorAltaDocente.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public void modificar(HttpServletRequest request) {
        try {
            boolean agrego=false;
            cargarDatos(usumod, request);
            asignarCargo(usumod, request);
            if(!domicilios.isEmpty()){
                usumod.setDomicilios(domicilios);
            }
            if(!telefonos.isEmpty()){
                usumod.setTelefonos(telefonos);
            }
       
            this.sistema.updateDocente(usumod);
            request.getSession(true).setAttribute("usermod", usumod);
            vista.mostrarExito("Se actualizo el Usuario exitosamente");
        }catch(Exception ex){
            vista.mostrarError("No se puede modificar el usuario, ya existe un ususario con los datos ingresados");
        }catch(Throwable e){
            vista.mostrarError("No se puedo modificar el usuario, ya existe un ususario con los datos ingresados");
        }
        telefonos.clear();
        domicilios.clear();
    }
    
    private void asignarCargo(Empleado d, HttpServletRequest request) throws AppException {
        Long cargoId = Long.parseLong(request.getParameter("cargo"));
        Cargo c = sistema.findCargoById(cargoId);
        d.setCargo(c);
    }
    
    private void cargarDatos(Usuario a, HttpServletRequest request) {
        //Datos Personales
        a.setEmail(request.getParameter("email"));
        a.setFechaNacimiento(request.getParameter("fechan"));
        a.setPrimerNombre(request.getParameter("nombre"));
        a.setSegundoNombre(request.getParameter("nombre_segundo"));
        a.setPrimerApellido(request.getParameter("apellido"));
        a.setSegundoApellido(request.getParameter("apellido_segundo"));
        if(request.getParameter("clave")!= null){
            a.setClave(request.getParameter("clave"));
        }
        if(request.getParameter("user_estado").equals("true")){
            a.setActivo(true);
        }else if(request.getParameter("user_estado").equals("false")){
            a.setActivo(false);
        }
        
    }

    public void agregarTelefono(String tipoTel, String numTel, String obsTel) {
        Telefono telefono = new Telefono();
        telefono.setTipo(Telefono.TipoTelefono.valueOf(tipoTel.toUpperCase()));
        telefono.setNumero(numTel);
        telefono.setObservaciones(obsTel);
        telefonos.add(telefono);
    }
    
    public void agregarDireccion(HttpServletRequest request) {
        Domicilio domi = new Domicilio();
        domi.setApartamento(request.getParameter("apto"));
        domi.setBarrio(request.getParameter("barrio"));
        domi.setBlock(request.getParameter("block"));
        domi.setCalle(request.getParameter("calle"));
        domi.setDepartamento(Domicilio.Departamento.valueOf(request.getParameter("departamento")));
        domi.setEdificio(request.getParameter("edificio"));
        domi.setEsquina(request.getParameter("esquina"));
        domi.setLocalidad(request.getParameter("localidad"));
        domi.setManzanaSolar(request.getParameter("manzanasolar"));
        domi.setNumero(request.getParameter("numero"));
        domi.setTorre(request.getParameter("isTorre"));
        domicilios.add(domi);
    }
}
