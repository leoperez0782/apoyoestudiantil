/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaCierreReunionesDireccion;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 * @date 26-ago-2019
 * @time 20:23:23
 * @author Leonardo Pérez
 */
public class ControladorCierreReunionesDireccion {

    private VistaCierreReunionesDireccion vista;
    private Institucion sistema;
    private Nivel nivel;
    private Grupo grupo;
    private String periodoElegido;
    private List<Libreta> libretasDelGrupo;

    public ControladorCierreReunionesDireccion(VistaCierreReunionesDireccion vista) {
        this.vista = vista;
        this.sistema = Institucion.getInstancia();
    }

    public void reconectar() {
        try {
            vista.mostrarNiveles(sistema.getNiveles());
        } catch (Exception e) {
            Logger.getLogger(ControladorCierreReunionesDireccion.class.getName()).log(Level.SEVERE, null, e);
            vista.enviar("Error", e.getMessage());
        }

    }

    public void inicializar() {
        try {
            vista.mostrarNiveles(sistema.getNiveles());
        } catch (Exception e) {
            Logger.getLogger(ControladorCierreReunionesDireccion.class.getName()).log(Level.SEVERE, null, e);
            vista.enviar("Error", e.getMessage());
        }

    }

    public void nivelSeleccionado(long nvSel) {
        try {
            nivel = sistema.buscarNivelID(nvSel);
            vista.mostrarGruposNivel(nivel.getGrupos());
        } catch (Exception e) {
            Logger.getLogger(ControladorCierreReunionesDireccion.class.getName()).log(Level.SEVERE, null, e);
            vista.enviar("Error", e.getMessage());
        }

    }

    public void grupoSeleccionado(long grupoSel) {
        try {
            grupo = nivel.getGrupo(grupoSel);

            libretasDelGrupo = sistema.cargarLibretasPorGrupo(grupo);
            if (!libretasDelGrupo.isEmpty()) {

                vista.cargarPeriodos(libretasDelGrupo.get(0));
            }
        } catch (Exception e) {
            Logger.getLogger(ControladorCierreReunionesDireccion.class.getName()).log(Level.SEVERE, null, e);
            vista.enviar("Error", e.getMessage());
        }

    }

    public void periodoSeleccionado(String nombreperiodo) {
        try {
            List<Libreta> lista = sistema.asignaturasConReunionesSinCerrar(libretasDelGrupo, nombreperiodo);
            this.periodoElegido = nombreperiodo;
            vista.mostrarReunionesSinCerrar(lista);
            vista.cargarAlumnos(grupo.getAlumnos());
        } catch (Exception e) {
            Logger.getLogger(ControladorCierreReunionesDireccion.class.getName()).log(Level.SEVERE, null, e);
            vista.enviar("Error", e.getMessage());
        }

    }

    public void grupoSeleccionado(long grupoSel, HttpServletRequest request) {
        try {
            grupo = nivel.getGrupo(grupoSel);

            libretasDelGrupo = sistema.cargarLibretasPorGrupo(grupo);
            if (!libretasDelGrupo.isEmpty()) {
                request.getSession().setAttribute("grupoSeleccionado", grupo);
                vista.cargarPeriodos(libretasDelGrupo.get(0));
            }
        } catch (Exception e) {
            Logger.getLogger(ControladorCierreReunionesDireccion.class.getName()).log(Level.SEVERE, null, e);
            vista.enviar("Error", e.getMessage());
        }

    }

    public void procesarPost() {

        try {
            HashMap<Alumno, String> juicios = this.vista.asociarDatosPost(this.grupo.getAlumnos());
            this.sistema.guardarJuiciosReunion(this.grupo, juicios, this.periodoElegido);

            vista.devolverPost("Exito", "Se guardaron los datos correctamente");
        } catch (NullPointerException npe) {
            vista.devolverPost("Error", "Debe realizar todos los pasos");
        } catch (Exception e) {
            vista.devolverPost("Error", e.getMessage());
        }
    }

}
