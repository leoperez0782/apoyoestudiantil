/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Inasistencia;
import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.entity.Lista;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaPasajeLista;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 * @date 14-jul-2019
 * @time 0:21:05
 * @author Leonardo Pérez
 */
public class ControladorPasajeLista {
    private VistaPasajeLista vista;

    private Libreta libretaActual;
    private Lista listaActual;
    private Institucion sistema;

    public ControladorPasajeLista(VistaPasajeLista vista) {
        this.vista = vista;

        this.sistema = Institucion.getInstancia();
    }

    public void cargarLista(int indice, HttpServletRequest request) {
        try {
            
            this.libretaActual =(Libreta) request.getSession().getAttribute("libretaActual");
            this.listaActual = libretaActual.getPasajesDeLista().get(indice);
            List<Alumno> alumnosAMostrar = libretaActual.getGrupo().getAlumnos();
            request.getSession().setAttribute("listaAlumnos", alumnosAMostrar);
            
            String fechaAMostrar = listaActual.getFecha();
            request.setAttribute("fecha", fechaAMostrar);
            String horario = listaActual.getHorario().getHoraInicio() + " " + listaActual.getHorario().getHoraFin();
            request.setAttribute("horario",horario );
        } catch (AppException ex) {
            Logger.getLogger(ControladorPasajeLista.class.getName()).log(Level.SEVERE, null, ex);
             this.vista.enviar("Error", ex.getMessage());
        }catch(Exception e){
            Logger.getLogger(ControladorPasajeLista.class.getName()).log(Level.SEVERE, null, e);
            this.vista.enviar("Error", e.getMessage());
        }
    }

    public void procesarLista(HttpServletRequest request) {
        int tope = this.libretaActual.getGrupo().getAlumnos().size();
        listaActual.setPasada(true);
        String nomCheckbox;
        String nomTipoFalta;
        //En el long del map guardo el id del alumno
        HashMap<Long, Inasistencia> mapa = new HashMap<>();
        try {
            for (int i = 0; i < tope; i++) {
                nomCheckbox = "chk" + i;
                nomTipoFalta = "tipoFalta" + i;
                
                String valorChecbox = request.getParameter(nomCheckbox);
                String valorTipoFalta = request.getParameter(nomTipoFalta);
                if (valorChecbox != null) {//no asistio
                    
                    String fecha = this.listaActual.getFecha();
                    //Crea la insasitencia
                    Inasistencia inasist = new Inasistencia();
                    inasist.setEstado(Inasistencia.devolverEstado(valorTipoFalta));

                    inasist.setFecha(LocalDate.parse(fecha));
                    
                    inasist.setHoraInicio(this.listaActual.getHorario().getHoraInicio());
                    inasist.setJustificacion(valorTipoFalta);
                    inasist.setTipo(Inasistencia.TipoInasistencia.valueOf(valorTipoFalta));
                    
                    
                    //busco el alumno
                    Alumno al = this.libretaActual.getGrupo().getAlumno(i);

                    mapa.put(al.getId(), inasist);
                }
            }
            if (this.sistema.actualizarLibreta(this.libretaActual, mapa, listaActual)) {
                this.vista.enviar("Exito", "Se actualizo la lista");
            } else {
                this.vista.enviar("Error", "No se pudo actualizar");
            }
        }catch(Exception e){
            this.vista.enviar("Error", e.getMessage());
        }

    }
    
    
}
