/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaCargaGruposDocente;
import java.util.List;

/**
 *
 * @author leo
 */
public class ControladorCargaGruposDocente {

    private VistaCargaGruposDocente vista;
    private Institucion sistema;
    
    public ControladorCargaGruposDocente(VistaCargaGruposDocente vista) {
        this.vista = vista;
        this.sistema = Institucion.getInstancia();
    }

    

    public List<Libreta> cargarLibretas(Docente d) {
        return this.sistema.cargarLibretasPorDocente(d);
    }

}
