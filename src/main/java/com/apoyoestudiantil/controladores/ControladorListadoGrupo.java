/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.entity.Lista;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaListadoGrupo;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 * @date 06-jul-2019
 * @time 5:25:56
 * @author Leonardo Pérez
 */
public class ControladorListadoGrupo {

    private VistaListadoGrupo vista;
    private Libreta libretaActual;
    private Institucion sistema;

    public ControladorListadoGrupo(VistaListadoGrupo aThis) {
        this.vista = aThis;
        this.sistema = Institucion.getInstancia();
    }

    public void cargarLista(int indice, HttpServletRequest request) {
        try {
            List<Libreta> libretas = (List) request.getSession().getAttribute("listaLibretas");
            this.libretaActual = libretas.get(indice);
            if(this.libretaActual.getPasajesDeLista().isEmpty()){
                this.sistema.cargarPasajesLista(libretaActual);
            }
            List<Lista> listas = this.libretaActual.getPasajesDeLista();
            request.getSession().setAttribute("libretaActual", libretaActual);//guardo la libreta para pasar la lista del grupo.
            request.getSession().setAttribute("pasajesLista", listas);
        } catch (AppException ex) {
            Logger.getLogger(ControladorListadoGrupo.class.getName()).log(Level.SEVERE, null, ex);
            this.vista.enviar("Error", ex.getMessage());
        }catch(Exception e){
            Logger.getLogger(ControladorListadoGrupo.class.getName()).log(Level.SEVERE, null, e);
            this.vista.enviar("Error", e.getMessage());
        }
    }

   
}
