/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Evaluacion;
import com.apoyoestudiantil.entity.Inasistencia;
import com.apoyoestudiantil.entity.ItemDesarrollo;
import com.apoyoestudiantil.entity.Promedio;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaInicioTutorEstudiante;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Polachek
 */
public class ControladorInicioTutorEstudiante {
    private VistaInicioTutorEstudiante vista;
    private Institucion sistema;
    private AdultoResponsable tutor;
    private Alumno alumno;
    private EntityManager mng = ContextoDB.getInstancia().getManager();
    
    public ControladorInicioTutorEstudiante(VistaInicioTutorEstudiante vista, Alumno alm) {
        this.vista = vista;
        this.sistema = Institucion.getInstancia();
        this.alumno = alm;
    }
    
    public void reconectar() {
        
    }

    public void asistencias() {
        List<Inasistencia> inasistencias = alumno.getInasistencias();
        vista.mostrarInasistencias(inasistencias);
    }

    public void evaluaciones() {
        try {
            List<Evaluacion> lista = sistema.getEvaluacionesAlumno(alumno);
            vista.mostrarEvaluaciones(lista);
        } catch (AppException ex) {
            vista.mostrarError(ex.getMessage());
        }       
    }

    public void promedios() {
        try {
            List<Promedio> promedios = sistema.obetenrPromediosAlumno(alumno);
            vista.mostrarPromedios(promedios);
        } catch (AppException ex) {
            vista.mostrarError(ex.getMessage());
        }              
    }

    public void calendario() {
        try {
            List<ItemDesarrollo> items = sistema.calendarioEvaluacionesAlumno(alumno);
            vista.mostrarCalendario(items);
        } catch (AppException ex) {
            vista.mostrarError(ex.getMessage());
        }
    }
        
}
