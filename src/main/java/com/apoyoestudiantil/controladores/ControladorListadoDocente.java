/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaListadoDocente;
import java.util.List;

/**
 * @date 28-jun-2019
 * @time 21:14:55
 * @author Leonardo Pérez
 */
public class ControladorListadoDocente {

    private VistaListadoDocente vista;
    private Institucion sistema;

    public ControladorListadoDocente(VistaListadoDocente aThis) {
        this.vista = aThis;
        this.sistema = Institucion.getInstancia();
        cargarDocentes();
    }

    public List<Docente> cargarDocentes() {

        return sistema.listarDocentes();

    }
}
