/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.GestorAlumnos;
import com.apoyoestudiantil.interactores.FactoriaCargadoresArchivo.Tipo;
import com.apoyoestudiantil.interactores.ICargadorArchivos;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaUploadAlumno;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;

/**
 * @date 12-jun-2019
 * @time 17:43:00
 * @author Leonardo Pérez
 */
public class ControladorUploadAlumnos {

    private VistaUploadAlumno vista;
    private InputStream archivo;
    private GestorAlumnos gestorAlumnos;
    private EntityManager mng;

    private Institucion sistema;

    public ControladorUploadAlumnos(VistaUploadAlumno vista) {
        this.vista = vista;
        this.sistema = Institucion.getInstancia();
        this.mng = ContextoDB.getInstancia().getManager();
        this.gestorAlumnos = new GestorAlumnos(mng);

    }

    public void importar(InputStream archivo, String tipoCarga) {
        try {
            this.archivo = archivo;
            guardarDatos(tipoCarga);

            vista.enviar("Exito", "Se agregaron los datos correctamente");
        } catch (AppException ex) {
            Logger.getLogger(ControladorUploadAlumnos.class.getName()).log(Level.SEVERE, null, ex);
            vista.enviar("Error", ex.getMessage());
        } catch (Exception e) {
            Logger.getLogger(ControladorUploadAlumnos.class.getName()).log(Level.SEVERE, null, e);
            vista.enviar("Error", e.getMessage());
        }
    }

    private void guardarDatos(String tipoCarga) throws AppException {
        try {
            ICargadorArchivos.TipoCarga tipo = ICargadorArchivos.TipoCarga.valueOf(tipoCarga.toUpperCase());

            List<Alumno> lista = sistema.cargarArchivo(archivo, Tipo.ESTUDIANTE, tipo);

            gestorAlumnos.saveAll(lista);
        } catch (Exception e) {
            Logger.getLogger(ControladorUploadAlumnos.class.getName()).log(Level.SEVERE, null, e);
            vista.enviar("Error", e.getMessage());
        }

    }

}
