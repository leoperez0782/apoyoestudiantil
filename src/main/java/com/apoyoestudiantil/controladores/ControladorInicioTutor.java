/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.NotificacionDAO;
import com.apoyoestudiantil.dao.UsuarioDAO;
import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Notificacion;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaInicioTutor;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Polachek
 */
public class ControladorInicioTutor {
    private VistaInicioTutor vista;
    private Institucion sistema;
    private AdultoResponsable tutor;
    private EntityManager mng = ContextoDB.getInstancia().getManager();
    
    public ControladorInicioTutor(VistaInicioTutor vista, AdultoResponsable usu) {
        this.vista = vista;
        this.sistema = Institucion.getInstancia();
        this.tutor = usu;
        try {
            mostrarAlumnos();
        }catch (AppException ex) {
            vista.mostrarError(ex.getMessage());
        }
        comprobarNotificaciones();
    }
    
    public void reconectar() {
        try {
            mostrarAlumnos();
        }catch (AppException ex) {
            vista.mostrarError(ex.getMessage());
        }
        comprobarNotificaciones();
    }

    private void mostrarAlumnos() throws AppException {
        AlumnoDAO daoAlumno = new AlumnoDAO(mng);
        List<Alumno> alumnos = daoAlumno.getAlumnosByTutor(tutor);
        vista.mostrarAlumnos(alumnos);
    }

    public void comprobarNotificaciones() {
        NotificacionDAO daoNotif = new NotificacionDAO(mng);
        List<Notificacion> lista = daoNotif.getAllByDestinatrio(tutor);
        boolean hayNuevas = false;        
        for (Notificacion a : lista) {
            if(!a.isLeida()){
                hayNuevas = true;
                break;
            }
        }
        if(hayNuevas){
            vista.hayNuevasNotificaciones();
        }else{
            vista.noHayNuevasNotificaciones();
        }
    }

    public void iniciarTA(HttpServletRequest request) {
        Long numDoc = Long.parseLong(request.getParameter("docAlm"));
        Alumno miAlumno;
        try {
            miAlumno = sistema.findAlumnoByNumeroDoc(numDoc);
            vista.iniciarTAOK(miAlumno);
        } catch (AppException ex) {
            vista.mostrarError(ex.getMessage());
        }        
    }

    public void notificaciones(HttpServletRequest request) {
        NotificacionDAO daoNotif = new NotificacionDAO(mng);
        List<Notificacion> lista = daoNotif.getAllByDestinatrio(tutor);
        vista.mostrarNotificaciones(lista);
    }

    public void verNotificacion(HttpServletRequest request) {
        Long idN = Long.parseLong(request.getParameter("idN"));
        NotificacionDAO daoNotif = new NotificacionDAO(mng);
        Notificacion miNot = daoNotif.findById(idN);
        if(!miNot.isLeida()){
            miNot.setLeida(true);
            daoNotif.update(miNot);
        }        
        vista.mostrarNotificacion(miNot);
    }
}
