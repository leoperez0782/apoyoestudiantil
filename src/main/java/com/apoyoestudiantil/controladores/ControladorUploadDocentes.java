/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.FactoriaCargadoresArchivo;
import com.apoyoestudiantil.interactores.ICargadorArchivos;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaUploadDocente;
import java.io.InputStream;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @date 21-jun-2019
 * @time 19:31:03
 * @author Leonardo Pérez
 */
public class ControladorUploadDocentes {

    private VistaUploadDocente vista;
    private Institucion sistema;
    private InputStream archivo;

    public ControladorUploadDocentes(VistaUploadDocente vista) {
        this.vista = vista;

        this.sistema = Institucion.getInstancia();

    }

    public void importar(String tipoCarga, InputStream archivo) {
        try {
            this.archivo = archivo;
            ICargadorArchivos.TipoCarga tipo = ICargadorArchivos.TipoCarga.valueOf(tipoCarga.toUpperCase());
            List<Docente> lista = sistema.cargarArchivoDocente(archivo, FactoriaCargadoresArchivo.Tipo.DOCENTE, tipo);
            sistema.saveAllDocentes(lista);
            vista.enviar("Exito", "Se agregaron los datos correctamente");
        } catch (AppException ex) {
            Logger.getLogger(ControladorUploadDocentes.class.getName()).log(Level.SEVERE, null, ex);
            vista.enviar("Error", ex.getMessage());
        } catch (IllegalArgumentException ie) {//lo lanza cuando trata de convertir una celda vacia en enum de departamento o si el archivo no tiene el formato correcto.
            vista.enviar("Error", "El formato de las celdas de la planilla no es correcto, o el archivo elegido no es una planilla Excel");
        } catch (Exception e) {
            vista.enviar("Error", e.getMessage());
        }
    }
}
