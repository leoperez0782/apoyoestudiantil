/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Rol;
import com.apoyoestudiantil.entity.Telefono;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaAltaTutor;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Polachek
 */
public class ControladorAltaTutor {
    private VistaAltaTutor vista;
    private Usuario usuario;
    private List<Alumno> alumnos = new ArrayList<>();
    private Alumno bAlumno;
    private Institucion sistema = Institucion.getInstancia();
    private List<Telefono> telefonos = new ArrayList<>();
    private List<Domicilio> domicilios = new ArrayList<>();

    public ControladorAltaTutor(VistaAltaTutor vista, Usuario usu) {
        this.vista = vista;
        this.usuario = usu;
    }
    
    protected Institucion getSistema(){
        return sistema;
    }
    
    public void alta(HttpServletRequest request) {
        if(!alumnos.isEmpty()){
            boolean agrego = false;
            String errmsg="";
            try{
                AdultoResponsable a = new AdultoResponsable();
                a.setActivo(true);
                cargarDatos(a, request);
                Rol r = sistema.findRolByRol("ADULTO_RESPONSABLE");
                a.addRol(r);
                if(!domicilios.isEmpty()){
                    a.setDomicilios(domicilios);
                }        
                if(!telefonos.isEmpty()){
                    a.setTelefonos(telefonos);
                }

                agrego = true;
                for(Alumno al: alumnos){
                   al.addPadre(a);
                }
                sistema.guardarUsuario(a);
            }catch(Exception ex){
                    agrego = false;
                    errmsg = " - Ya existe un ususario con los datos ingresados";
            }catch(Throwable e){
                    agrego = false;
                    errmsg = " - Ya existe un ususario con los datos ingresados";
            }
            
            if(agrego){
                vista.mostrarExito("Se agregó exitosamente el usuario");
            }else{
                vista.mostrarError("No se pudo agregar el usuario"+errmsg);
            }
        }else{
            vista.mostrarError("No se puede agregar a un tutor sin estudiantes asignados");
        } 
    }
    
    private void cargarDatos(AdultoResponsable a, HttpServletRequest request) {
        // Documento
        Documento doc = new Documento();
        doc.setNumero(Long.parseLong(request.getParameter("ci")));
        doc.setPaisEmisor(request.getParameter("ci_emisor"));
        doc.setTipo(Documento.TipoDocumento.valueOf(request.getParameter("ci_tipo")));
        a.setDocumento(doc);
        //Datos Personales
        a.setEmail(request.getParameter("email"));
        a.setFechaNacimiento(request.getParameter("fechan"));
        a.setPrimerNombre(request.getParameter("nombre"));
        a.setSegundoNombre(request.getParameter("nombre_segundo"));
        a.setPrimerApellido(request.getParameter("apellido"));
        a.setSegundoApellido(request.getParameter("apellido_segundo"));
        a.setClave(request.getParameter("clave"));
    }
   
    public void reconectar() {
    }

    public void agregarTelefono(String tipoTel, String numTel, String obsTel) {
        Telefono telefono = new Telefono();
        telefono.setTipo(Telefono.devolevertTipo(tipoTel));
        telefono.setNumero(numTel);
        telefono.setObservaciones(obsTel);
        telefonos.add(telefono);
    }
    
    public void agregarDireccion(HttpServletRequest request) {
        Domicilio domi = new Domicilio();
        domi.setApartamento(request.getParameter("apto"));
        domi.setBarrio(request.getParameter("barrio"));
        domi.setBlock(request.getParameter("block"));
        domi.setCalle(request.getParameter("calle"));
        domi.setDepartamento(Domicilio.Departamento.valueOf(request.getParameter("departamento")));
        domi.setEdificio(request.getParameter("edificio"));
        domi.setEsquina(request.getParameter("esquina"));
        domi.setLocalidad(request.getParameter("localidad"));
        domi.setManzanaSolar(request.getParameter("manzanasolar"));
        domi.setNumero(request.getParameter("numero"));
        domi.setTorre(request.getParameter("isTorre"));
        domicilios.add(domi);
    }  

    public void buscarAlumno(String ciAlumno) {
        Long numDoc = Long.parseLong(ciAlumno);
        Alumno miAlumno;
        try {
            miAlumno = sistema.findAlumnoByNumeroDoc(numDoc);
            vista.busquedaAlumno(miAlumno);
            bAlumno = miAlumno;
        } catch (AppException ex) {
            vista.busquedaSinAlumno();
        }
    }  

    public void agregarAlumno(Long idAlmuno) {
        if(bAlumno.getId()!=null && idAlmuno==bAlumno.getId()){
            alumnos.add(bAlumno);
            vista.agregarListaAlumno(bAlumno);
        }else{
            vista.mostrarError("No se pudo agregar el alumno");
        }
    }

    public void quitarAlumno(Long docAlm) {
        Alumno miAl = null;
        for(Alumno al: alumnos){
            if(Objects.equals(al.getDocumento().getNumero(), docAlm)){
                miAl = al;
            }
        }
        alumnos.remove(miAl);
    }
}
