/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.entity.Cargo;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.interactores.SistemaCargos;
import com.apoyoestudiantil.vistas.VistaAltaCargo;
import javax.persistence.EntityManager;

/**
 * @date 22-jun-2019
 * @time 23:23:18
 * @author Leonardo Pérez
 */
public class ControladorAltaCargo {

    private VistaAltaCargo vista;
   // private SistemaCargos sistema;
    private Institucion sistema;
    //private EntityManager mng;
    public ControladorAltaCargo(VistaAltaCargo vista) {
        this.vista = vista;
        //this.mng = ContextoDB.getInstancia().getManager();
        //this.sistema = new SistemaCargos(mng);
        this.sistema = Institucion.getInstancia();
    }

    public void guardar(Cargo c) {
        try {
            sistema.saveCargo(c);
            vista.enviar("Exito", "Se agrego el cargo");
        } catch (Exception e) {
            vista.enviar("Error", e.getMessage());
        }
    }

}
