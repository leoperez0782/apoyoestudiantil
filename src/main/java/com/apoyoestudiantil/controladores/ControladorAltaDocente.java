/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.RolDAO;
import com.apoyoestudiantil.entity.Cargo;
import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Rol;
import com.apoyoestudiantil.entity.Telefono;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaAltaDocente;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

/**
 * @date 23-jun-2019
 * @time 18:11:17
 * @author Leonardo Pérez
 */
public class ControladorAltaDocente {

    private VistaAltaDocente vista;
    private EntityManager mng;
    private Institucion sistema;
    private List<Telefono> telefonos = new ArrayList<>();
    private List<Domicilio> domicilios = new ArrayList<>();
    private List<Cargo> cargos = new ArrayList<>();

    public ControladorAltaDocente(VistaAltaDocente vista) {
        this.vista = vista;
        this.mng = ContextoDB.getInstancia().getManager();
//        this.sistemaCargos = new SistemaCargos(mng);
//        this.sisDocentes = new SistemaDocentes(mng);
        this.sistema = Institucion.getInstancia();
        mostrarCargos();
    }
    
    public void reconectar() {
        mostrarCargos();
    }
    
    private void mostrarCargos() {
        cargos = obtenerCargosDisponibles();
        if(!cargos.isEmpty()){
            vista.mostrarCargos(cargos);
        }else{
            vista.mostrarError("No hay cargos agregados al sistema");
        }
    }

    public List<Cargo> obtenerCargosDisponibles() {
        try {
            return sistema.getAllCargos();
        } catch (AppException ex) {
            Logger.getLogger(ControladorAltaDocente.class.getName()).log(Level.SEVERE, null, ex);
            //vista.enviar("Error", ex.getMessage());//Genera problemas en el servlet
            return null;
        }

    }

    public void alta(HttpServletRequest request) {
        try {
            Docente d = new Docente();
            d.setActivo(true);
            cargarDatosDelDocente(d, request);
            asignarCargoAlDocente(d, request);
            RolDAO daoRol = new RolDAO(mng);
            Rol r = daoRol.findByRol("DOCENTE");
            d.addRol(r);
            if(!domicilios.isEmpty()){
                d.setDomicilios(domicilios);
            }        
            if(!telefonos.isEmpty()){
                d.setTelefonos(telefonos);
            }
            sistema.saveDocente(d);
            vista.mostrarExito("Se agrego el docente");
        } catch (ParseException ex) {
            Logger.getLogger(VistaAltaDocente.class.getName()).log(Level.SEVERE, null, ex);
            vista.mostrarError("Error ingrese una fecha valida");
        } catch (AppException ae) {
            Logger.getLogger(VistaAltaDocente.class.getName()).log(Level.SEVERE, null, ae);
            vista.mostrarError("Error " + ae.getMessage());
        }
    }

    private void cargarDatosDelDocente(Docente a, HttpServletRequest request) throws ParseException {
        //Documento
        Documento d1 = new Documento();
        d1.setNumero(Long.parseLong(request.getParameter("ci")));
        d1.setPaisEmisor(request.getParameter("ci_emisor"));
        d1.setTipo(Documento.TipoDocumento.valueOf(request.getParameter("ci_tipo")));
        a.setDocumento(d1);
        
        //Datos Personales
        a.setEmail(request.getParameter("email"));
        a.setFechaNacimiento(request.getParameter("fechan"));
        a.setPrimerNombre(request.getParameter("nombre"));
        a.setSegundoNombre(request.getParameter("nombre_segundo"));
        a.setPrimerApellido(request.getParameter("apellido"));
        a.setSegundoApellido(request.getParameter("apellido_segundo"));
        String clave = request.getParameter("clave");
        a.setClave(clave);
    }

    private void asignarCargoAlDocente(Docente d, HttpServletRequest request) throws AppException {
        Long cargoId = Long.parseLong(request.getParameter("cargo"));
        Cargo c = sistema.findCargoById(cargoId);
        d.setCargo(c);
    }

    public void agregarTelefono(String tipoTel, String numTel, String obsTel) {
        Telefono telefono = new Telefono();
        telefono.setTipo(Telefono.TipoTelefono.valueOf(tipoTel.toUpperCase()));
        telefono.setNumero(numTel);
        telefono.setObservaciones(obsTel);
        telefonos.add(telefono);
    }
    
    public void agregarDireccion(HttpServletRequest request) {
        Domicilio domi = new Domicilio();
        domi.setApartamento(request.getParameter("apto"));
        domi.setBarrio(request.getParameter("barrio"));
        domi.setBlock(request.getParameter("block"));
        domi.setCalle(request.getParameter("calle"));
        domi.setDepartamento(Domicilio.Departamento.valueOf(request.getParameter("departamento")));
        domi.setEdificio(request.getParameter("edificio"));
        domi.setEsquina(request.getParameter("esquina"));
        domi.setLocalidad(request.getParameter("localidad"));
        domi.setManzanaSolar(request.getParameter("manzanasolar"));
        domi.setNumero(request.getParameter("numero"));
        domi.setTorre(request.getParameter("isTorre"));
        domicilios.add(domi);
    }  

}
