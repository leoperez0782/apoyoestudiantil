/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaEstudianteModificar;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 *
 * @author Polachek
 */
public class ControladorEstudianteModificar {
    private VistaEstudianteModificar vista;
    private Usuario usuario;
    private Institucion sistema = Institucion.getInstancia();
    private Nivel nivel;
    private Grupo grupo;
    private Alumno alumno;
    EntityManager manager = ContextoDB.getInstancia().getManager();
    EntityTransaction tx = manager.getTransaction();
    DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    
    public ControladorEstudianteModificar(VistaEstudianteModificar vista, Usuario usu) {
        this.vista = vista;
        this.usuario = usu;
        vista.mostrarNiveles(sistema.getNiveles());
    }
    
    public void reconectar() {
        vista.mostrarNiveles(sistema.getNiveles());
    }
    
    protected Institucion getSistema(){
        return sistema;
    }

    public void nivelSeleccionado(long nvSel) {
        this.nivel = sistema.buscarNivelID(nvSel);
        vista.mostrarGruposNivel(nivel.getGrupos());
    }

    public void grupoSeleccionado(long grSel) {
        this.grupo = nivel.getGrupo(grSel);
        vista.mostrarAlumnos(grupo.getAlumnos());
    }

    public void alumnoSeleccionado(String id) {
        alumno = grupo.getAlumno(Long.parseLong(id));
        vista.mostrarFormAlumno(alumno);
    }

}
