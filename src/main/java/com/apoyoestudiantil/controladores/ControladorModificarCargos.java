/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.entity.Cargo;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaModificarCargos;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Polachek
 */
public class ControladorModificarCargos {
    private Institucion sistema;
    private List<Cargo> cargos = new ArrayList<>();
    private VistaModificarCargos vista;
    private Cargo miCargo;
    
    public ControladorModificarCargos(VistaModificarCargos vista) {
        this.vista = vista;
        this.sistema = Institucion.getInstancia();
        mostrarCargos();    
    }
    
    private void mostrarCargos() {
        try {
            cargos = sistema.getAllCargos();
            if(!cargos.isEmpty()){
                vista.mostrarCargos(cargos);
            }else{
                vista.mostrarError("No hay cargos agregados al sistema");
            }
        } catch (AppException ex) {
            vista.mostrarError("No hay cargos agregados al sistema");
        }        
    }

    public void mostrar(Long idC) {
        try {
            miCargo = sistema.findCargoById(idC);
            vista.mostrarCargo(miCargo);
        } catch (AppException ex) {
            vista.mostrarError("Algo salió mal");
        }
    }

    public void guardar(HttpServletRequest request) {
        if(request.getParameter("tipo")!= null){
            miCargo.setTipoCargo(request.getParameter("tipo"));
        }
        if(request.getParameter("desc")!= null){
            miCargo.setDescripcion(request.getParameter("desc"));
        }else{
            miCargo.setDescripcion("");
        }
        boolean guardo = sistema.updateCargo(miCargo);
        if(guardo){
            vista.mostrarExito("Cargo actualizado");
            mostrarCargos();
        }else{
            vista.mostrarError("No se pudo actualizar el cargo");
        }
    }

    public void borrar() {
        boolean borro = sistema.borrarCargo(miCargo);
        if(borro){
            vista.mostrarExito("Cargo borrado");
            mostrarCargos();
        }else{
            vista.mostrarError("No se pudo borrar el cargo");
        }
    }

    
}
