/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.entity.Usuario_Rol;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.vLogin;
import java.util.List;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

/**
 * @date 10-jun-2019
 * @time 7:56:24
 * @author Leonardo Pérez
 */
public class ControladorLogin {

    private vLogin vista;
    private Institucion sistema = Institucion.getInstancia();
    private EntityManager mng;
    
    public ControladorLogin(vLogin v) {
        this.vista=v;
        this.mng = ContextoDB.getInstancia().getManager();
    }
    
    protected Institucion getInstancia(){
        return sistema;
    }
    
    public void login(String nombre, String pass) throws AppException {
            try{
                Usuario usu = this.getInstancia().login(nombre, pass, mng);
                List<Usuario_Rol> roles = usu.getRoles();
                if(roles.isEmpty()){
                    vista.mostrarError("Usuario sin rol asignado");
                }else{
                        Usuario_Rol usuRol = roles.get(0);
                        switch( usuRol.getRol().getRol() ) {
                        case "ADMINISTRADOR":
                             this.vista.ingresarAdmin(usu);
                             break;
                        case "ADMINISTRATIVO":
                             this.vista.ingresarAdminO(usu);
                             break;
                        case "ESTUDIANTE":
                             this.vista.ingresarEstudiante(usu);
                             break;
                        case "DOCENTE":
                             this.vista.ingresarDocente(usu);
                             break;
                        case "ADULTO_RESPONSABLE":
                             this.vista.ingresarPadre(usu);
                             break;
                        }
                }
            //this.vista.ingresar(usu);
             }catch(Exception e){
                 vista.mostrarError(e.getMessage());
             }

    }

    public void cerrarSesion(HttpServletRequest request) {
        request.getSession().invalidate();
        vista.volverLogin();
    }
}
