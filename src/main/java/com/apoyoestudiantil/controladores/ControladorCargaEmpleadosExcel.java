/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.entity.Administrador;
import com.apoyoestudiantil.entity.Administrativo;
import com.apoyoestudiantil.entity.Cargo;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.FactoriaCargadoresArchivo;
import com.apoyoestudiantil.interactores.ICargadorArchivos;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaCargaEmpleadosExcel;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @date 21 sept. 2019
 * @time 23:30:05
 * @author Leonardo Pérez
 */
public class ControladorCargaEmpleadosExcel {

    private VistaCargaEmpleadosExcel vista;
    private Institucion sistema;
    private InputStream archivo;

    public ControladorCargaEmpleadosExcel(VistaCargaEmpleadosExcel vista) {
        this.vista = vista;
        this.sistema = Institucion.getInstancia();
        cargarCargos();
    }

    private void cargarCargos() {
        try {
            ArrayList<Cargo> cargos = (ArrayList<Cargo>) sistema.getAllCargos();
            ArrayList<Cargo> cargosAdmitidos = new ArrayList<>();
            for(Cargo c: cargos){
                if(c.getTipoCargo().equals("Administrativo") || c.getTipoCargo().equals("Directivo")){
                    cargosAdmitidos.add(c);
                }
            }
                    
            vista.cargarCargos(cargosAdmitidos);
        } catch (AppException ex) {
            Logger.getLogger(ControladorCargaEmpleadosExcel.class.getName()).log(Level.SEVERE, null, ex);
            vista.enviar("Error", ex.getMessage());
        }catch(Exception e){
             Logger.getLogger(ControladorCargaEmpleadosExcel.class.getName()).log(Level.SEVERE, null, e);
             vista.enviar("Error", e.getMessage());
        }
    }

    public void importar(InputStream filecontent, String tipoCarga, Long cargoId, String rol) {
        try {
            this.archivo = filecontent;
            ICargadorArchivos.TipoCarga tipo = ICargadorArchivos.TipoCarga.valueOf(tipoCarga.toUpperCase());
            switch (rol) {
                case "ADMINISTRATIVO":
                    List<Administrativo> lista = sistema.cargarAdministrativos(archivo, FactoriaCargadoresArchivo.Tipo.ADMINISTRATIVO, tipo);
                    sistema.saveAllAdministrativos(lista);
                    vista.enviar("Exito", "Se agregaron los datos correctamente");
                    break;
                case "ADMINISTRADOR":
                    List<Administrador> listaAd = sistema.cargarAdministradores(archivo, FactoriaCargadoresArchivo.Tipo.ADMINISTRADOR, tipo);
                    sistema.saveAllAdministradores(listaAd);
                    vista.enviar("Exito", "Se agregaron los datos correctamente");
                    break;
                default:
                    vista.enviar("Error", "problemas con el rol seleccionado");
                    break;
            }

        } catch (AppException ex) {
            Logger.getLogger(ControladorUploadDocentes.class.getName()).log(Level.SEVERE, null, ex);
            vista.enviar("Error", ex.getMessage());
        } catch(IllegalArgumentException ie){//lo lanza cuando trata de convertir una celda vacia en enum de departamento o si el archivo no tiene el formato correcto.
            vista.enviar("Error", "El formato de las celdas de la planilla no es correcto, o el archivo elegido no es una planilla Excel");
        }catch (Exception e) {
            vista.enviar("Error", e.getMessage());
        }
    }

}
