/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.UsuarioDAO;
import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Telefono;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaModificarTutor;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Polachek
 */
public class ControladorModificarTutor {
    private VistaModificarTutor vista;
    private Usuario usuario;
    private List<Alumno> alumnos;
    private List<Alumno> alumnosBorrar = new ArrayList<>();
    private Alumno bAlumno;
    private Institucion sistema = Institucion.getInstancia();
    private List<Telefono> telefonos = new ArrayList<>();
    private List<Domicilio> domicilios = new ArrayList<>();
    private EntityManager mng = ContextoDB.getInstancia().getManager();
    private AdultoResponsable tutor;
    EntityTransaction tx = mng.getTransaction();
    
    public ControladorModificarTutor(VistaModificarTutor vista, Usuario usu, AdultoResponsable tuto) {
        this.vista = vista;
        this.usuario = usu;
        this.tutor = tuto;
        try {
            mostrarAlumnos();
        } catch (AppException ex) {
            vista.mostrarError(ex.getMessage());
        }
    }
    
    protected Institucion getSistema(){
        return sistema;
    }
    
    public void modificar(HttpServletRequest request) {
        boolean agrego = false;
        if(!alumnos.isEmpty()){
            String errmsg="";
            try{
                AlumnoDAO dao = new AlumnoDAO(mng);
                AdultoResponsable miTutor = null;
                try {
                    miTutor = (AdultoResponsable)dao.findUsuarioByNumeroDoc(tutor.getDocumento().getNumero());
                } catch (AppException ex) {
                    Logger.getLogger(ControladorModificarTutor.class.getName()).log(Level.SEVERE, null, ex);
                }
                cargarDatos(miTutor, request);
                if(!domicilios.isEmpty()){
                    miTutor.setDomicilios(domicilios);
                }        
                if(!telefonos.isEmpty()){
                    miTutor.setTelefonos(telefonos);
                }
                agrego = false;
                for(Alumno al: alumnos){
                   if(!al.getTutores().contains(miTutor)){
                       al.addPadre(miTutor);
                   }
                   agrego = dao.update(al);               
                   if(!agrego){
                       vista.mostrarError("No se pudo actualizar el Tutor");
                       break;
                    }
                }
                if(!alumnosBorrar.isEmpty()){
                    for(Alumno alm: alumnosBorrar){
                        if(alm.getTutores().contains(miTutor)){
                            alm.getTutores().remove(miTutor);
                            agrego = dao.update(alm);               
                            if(!agrego){
                                vista.mostrarError("No se pudo actualizar el Tutor");
                                break;
                            }
                        }                   
                    }
                }
                if(agrego){
                    request.getSession(true).setAttribute("tutor", miTutor);
                }
            }catch(Exception ex){
                    agrego = false;
                    errmsg = " - Ya existe un usuario con los datos ingresados";
            }catch(Throwable e){
                    agrego = false;
                    errmsg = " - Ya existe un usuario con los datos ingresados";
            }         
            
            if(agrego){
                vista.mostrarExito("Se actualizo el Tutor exitosamente");
            }else{
                vista.mostrarError("No se pudo actualizar el usuario"+errmsg);
            }
            telefonos.clear();
            domicilios.clear();
        }else{
            vista.mostrarError("No se puede agregar a un tutor sin estudiantes asignados");
        }        
    }
    
    private void cargarDatos(AdultoResponsable a, HttpServletRequest request) {
        //Datos Personales
        a.setEmail(request.getParameter("email"));
        a.setFechaNacimiento(request.getParameter("fechan"));
        a.setPrimerNombre(request.getParameter("nombre"));
        a.setSegundoNombre(request.getParameter("nombre_segundo"));
        a.setPrimerApellido(request.getParameter("apellido"));
        a.setSegundoApellido(request.getParameter("apellido_segundo"));
        if(request.getParameter("clave")!= null){
            a.setClave(request.getParameter("clave"));
        }
        if(request.getParameter("user_estado").equals("true")){
            a.setActivo(true);
        }else if(request.getParameter("user_estado").equals("false")){
            a.setActivo(false);
        }
    }
   
    public void reconectar() {
        try {
            mostrarAlumnos();
        } catch (AppException ex) {
            vista.mostrarError(ex.getMessage());
        }
    }

    public void agregarTelefono(String tipoTel, String numTel, String obsTel) {
        Telefono telefono = new Telefono();
        telefono.setTipo(Telefono.TipoTelefono.valueOf(tipoTel.toUpperCase()));
        telefono.setNumero(numTel);
        telefono.setObservaciones(obsTel);
        telefonos.add(telefono);
    }
    
    public void agregarDireccion(HttpServletRequest request) {
        Domicilio domi = new Domicilio();
        domi.setApartamento(request.getParameter("apto"));
        domi.setBarrio(request.getParameter("barrio"));
        domi.setBlock(request.getParameter("block"));
        domi.setCalle(request.getParameter("calle"));
        domi.setDepartamento(Domicilio.Departamento.valueOf(request.getParameter("departamento")));
        domi.setEdificio(request.getParameter("edificio"));
        domi.setEsquina(request.getParameter("esquina"));
        domi.setLocalidad(request.getParameter("localidad"));
        domi.setManzanaSolar(request.getParameter("manzanasolar"));
        domi.setNumero(request.getParameter("numero"));
        domi.setTorre(request.getParameter("isTorre"));
        domicilios.add(domi);
    }

    public void buscarAlumno(String ciAlumno) {
        Long numDoc = Long.parseLong(ciAlumno);
        Alumno miAlumno;
        try {
            miAlumno = sistema.findAlumnoByNumeroDoc(numDoc);
            vista.busquedaAlumno(miAlumno);
            bAlumno = miAlumno;
        } catch (AppException ex) {
            vista.busquedaSinAlumno();
        }
    }

    private void mostrarAlumnos() throws AppException {
        AlumnoDAO daoAlumno = new AlumnoDAO(mng);
        alumnos = daoAlumno.getAlumnosByTutor(tutor);
        vista.mostrarAlumnos(alumnos);
    }    

    public void agregarAlumno(Long idAlmuno) {
        if(bAlumno.getId()!=null && idAlmuno==bAlumno.getId()){
            alumnos.add(bAlumno);
            vista.agregarListaAlumno(bAlumno);
        }else{
            vista.mostrarError("No se pudo agregar el alumno");
        }
    }

    public void quitarAlumno(Long docAlm) {
        Alumno miAl = null;
        for(Alumno al: alumnos){
            if(Objects.equals(al.getDocumento().getNumero(), docAlm)){
                miAl = al;
            }
        }
        alumnos.remove(miAl);
        alumnosBorrar.add(miAl);
    }
}
