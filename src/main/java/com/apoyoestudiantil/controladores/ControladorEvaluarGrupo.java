/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Evaluacion;
import com.apoyoestudiantil.entity.ItemDesarrollo;
import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaEvaluarGrupo;
import java.io.InputStream;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import org.apache.commons.compress.utils.IOUtils;

/**
 * @date 30-jul-2019
 * @time 6:35:16
 * @author Leonardo Pérez
 */
public class ControladorEvaluarGrupo {

    private VistaEvaluarGrupo vista;
    private Institucion sistema;
    private Libreta libretaActual;

    public ControladorEvaluarGrupo(VistaEvaluarGrupo vista) {
        this.vista = vista;
        this.sistema = Institucion.getInstancia();
    }

    public void cargarLista(int indice, HttpServletRequest request) {
        try {
            List<Libreta> libretas = (List) request.getSession().getAttribute("listaLibretas");
            this.libretaActual = libretas.get(indice);

            List<Alumno> alumnos = libretaActual.getGrupo().getAlumnos();
            request.getSession().setAttribute("libretaActual", libretaActual);
            request.getSession().setAttribute("alumnosGrupo", alumnos);
            request.getSession().setAttribute("desarrolloEvaluaciones", libretaActual.getDesarrollos());
        } catch (Exception ex) {
            Logger.getLogger(ControladorEvaluarGrupo.class.getName()).log(Level.SEVERE, null, ex);
            this.vista.enviar("Error", ex.getMessage());
        }
    }


    public void guardarEvaluacion(HttpServletRequest request, InputStream filecontent) {
        try {
            String soloGuardarItemEv = request.getParameter("chkSoloEval");
            ItemDesarrollo item = cargarItemDesarrollo(request, filecontent, soloGuardarItemEv);
            
            if (soloGuardarItemEv != null && soloGuardarItemEv.equals("crear")) {
                if (this.sistema.guardarItemDesarrollo(this.libretaActual, item)) {//solo guarda los datos de la evaluacion.
                    this.vista.enviar("Exito", "Se guardaron los datos de la evaluacion");
                } else {
                    this.vista.enviar("Error", "No se pudo guardar los datos");
                }
            } else {
                HashMap<Integer, Evaluacion> mapa = cargarNotas(request);
                if (this.sistema.guardarEvaluaciones(this.libretaActual, mapa, item)) {//guarda los datos y las notas.
                    this.vista.enviar("Exito", "Se guardaron los datos de las evaluaciones");
                } else {
                    this.vista.enviar("Error", "No se pudo guardar los datos");
                }
            }
        } catch (Exception e) {
            Logger.getLogger(ControladorEvaluarGrupo.class.getName()).log(Level.SEVERE, null, e);

            this.vista.enviar("Error", e.getMessage());
        }
    }

    private HashMap<Integer, Evaluacion> cargarNotas(HttpServletRequest request) throws AppException {
        try {
            int tope = this.libretaActual.getGrupo().getAlumnos().size();
            String nomComentario;
            String nomCalificacion;
            HashMap<Integer, Evaluacion> mapa = new HashMap<>();
            
            for (int i = 0; i < tope; i++) {
                nomComentario = "cmt" + i;
                nomCalificacion = "cal" + i;
                String valorComentario = request.getParameter(nomComentario);
                int calificacion = Integer.parseInt(request.getParameter(nomCalificacion));

                Evaluacion ev = new Evaluacion();
                ev.setCalificacion(calificacion);
                ev.setComentario(valorComentario);

                mapa.put(i, ev);
            }
            return mapa;
        } catch (Exception ex) {
            Logger.getLogger(ControladorEvaluarGrupo.class.getName()).log(Level.SEVERE, null, ex);
            throw new AppException("Error al procesar las notas");
        }
    }

    private ItemDesarrollo cargarItemDesarrollo(HttpServletRequest request, InputStream filecontent) throws AppException {
        try {
            String fecha = request.getParameter("fecha");

            String periodo = request.getParameter("periodo");
            String tipoEvaluacion = request.getParameter("tipoEvaluacion");
            ItemDesarrollo item = new ItemDesarrollo();
            item.setArchivo(IOUtils.toByteArray(filecontent));
            item.setFecha(LocalDate.parse(fecha));
            item.setEvaluacion(true);
            item.setDesarrollo(request.getParameter("comentario"));
            item.setPeriodo(periodo);
            item.setTipo(Evaluacion.TipoEvaluacion.valueOf(tipoEvaluacion));
            

            return item;

        } catch (Exception ex) {
            Logger.getLogger(ControladorEvaluarGrupo.class.getName()).log(Level.SEVERE, null, ex);
            throw new AppException(ex.getMessage());
        }
    }

    private ItemDesarrollo cargarItemDesarrollo(HttpServletRequest request, InputStream filecontent, String soloGuardarItemEv) throws AppException {
        String idDesarrollo = request.getParameter("evSeleccionada");
        ItemDesarrollo retorno;
        if((idDesarrollo == null || idDesarrollo.isEmpty()) && soloGuardarItemEv == null){
            throw new AppException("Debe seleccionar una evaluacion o agregar una nueva");
        }else if(idDesarrollo != null && !(idDesarrollo.isEmpty())){//si es una evaluacion ya guardada solo guardo el id
            retorno = new ItemDesarrollo();
            retorno.setId(Long.parseLong(idDesarrollo));
        }else{//si no creo un item nuevo
            retorno = cargarItemDesarrollo(request, filecontent);
        }
        return retorno;
    }

}
