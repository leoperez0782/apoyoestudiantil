/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.dao.AdultoResponsableDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.UsuarioDAO;
import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Rol;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.entity.Usuario_Rol;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaModificarTutorLista;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 *
 * @author Polachek
 */
public class ControladorModificarTutorLista {
    private VistaModificarTutorLista vista;
    private Usuario usuario;
    private Institucion sistema = Institucion.getInstancia();
    private EntityManager mng = ContextoDB.getInstancia().getManager();
    EntityTransaction tx = mng.getTransaction();
    
    public ControladorModificarTutorLista(VistaModificarTutorLista vista, Usuario usu) {
        this.vista = vista;
        this.usuario = usu;
        vista.mostrarUsuario(usuario.getPrimerNombre() + " " + usuario.getPrimerApellido());
    }
    
    protected Institucion getSistema(){
        return sistema;
    }
    
    public void reconectar() {
        vista.mostrarUsuario(usuario.getPrimerNombre() + " " + usuario.getPrimerApellido());
    }
    
    public void buscarTutor(Long ciTutor) {
        UsuarioDAO dao = new UsuarioDAO(mng);
        AdultoResponsable miTutor;
        Usuario miUsuTutor;
        try {
            miUsuTutor = dao.findUsuarioByNumeroDoc(ciTutor);
            boolean rolOK = false;
            for(Usuario_Rol rol: miUsuTutor.getRoles()){
               if(rol.getRol().getRol().equals("ADULTO_RESPONSABLE")){
                   rolOK = true;
                   break;
               }
            }
            if(rolOK){
                miTutor = (AdultoResponsable) miUsuTutor;
                vista.resultadoBusqueda(miTutor);
            }else{
                vista.error("El usuario no tiene el rol de Adulto Responsable");
            }
        } catch (AppException ex) {
            vista.error(ex.getMessage());
        }
    }
}
