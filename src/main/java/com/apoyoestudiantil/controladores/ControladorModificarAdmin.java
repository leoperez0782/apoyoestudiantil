/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Telefono;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaModificarAdmin;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Polachek
 */
public class ControladorModificarAdmin {
    private VistaModificarAdmin vista;
    private Usuario usumod;
    private List<Telefono> telefonos = new ArrayList<>();
    private List<Domicilio> domicilios = new ArrayList<>();
    private Institucion sistema = Institucion.getInstancia();
    
    public ControladorModificarAdmin(VistaModificarAdmin vista, Usuario usu) {
        this.vista = vista;
        this.usumod = usu;
    }
    
    protected Institucion getSistema(){
        return sistema;
    }
    
    public void modificar(HttpServletRequest request) {
        try {
            boolean agrego=false;
            Usuario miU = sistema.buscarUsuarioporNDocumento(usumod.getDocumento().getNumero());
            cargarDatos(miU, request);
            if(!domicilios.isEmpty()){
                miU.setDomicilios(domicilios);
            }
            if(!telefonos.isEmpty()){
                miU.setTelefonos(telefonos);
            }
       
            agrego = sistema.actualizarUsuario(miU);
            
            if(agrego){
                request.getSession(true).setAttribute("usermod", miU);
                vista.mostrarExito("Se actualizo el Usuario exitosamente");
            }else{
                vista.mostrarError("No se pudo actualizar el Usuario");
            } 
        } catch (AppException ex) {
            vista.mostrarError(ex.getMessage());
        }catch(Exception ex){
            vista.mostrarError("No se puede modificar el usuario, ya existe un ususario con los datos ingresados");
        }catch(Throwable e){
            vista.mostrarError("No se puedo modificar el usuario, ya existe un ususario con los datos ingresados");
        }
        telefonos.clear();
        domicilios.clear();
    }
    
    private void cargarDatos(Usuario a, HttpServletRequest request) {
        //Datos Personales
        a.setEmail(request.getParameter("email"));
        a.setFechaNacimiento(request.getParameter("fechan"));
        a.setPrimerNombre(request.getParameter("nombre"));
        a.setSegundoNombre(request.getParameter("nombre_segundo"));
        a.setPrimerApellido(request.getParameter("apellido"));
        a.setSegundoApellido(request.getParameter("apellido_segundo"));
        if(request.getParameter("clave")!= null){
            a.setClave(request.getParameter("clave"));
        }
        if(request.getParameter("user_estado").equals("true")){
            a.setActivo(true);
        }else if(request.getParameter("user_estado").equals("false")){
            a.setActivo(false);
        }
        
    }

    public void agregarTelefono(String tipoTel, String numTel, String obsTel) {
        Telefono telefono = new Telefono();
        telefono.setTipo(Telefono.TipoTelefono.valueOf(tipoTel.toUpperCase()));
        telefono.setNumero(numTel);
        telefono.setObservaciones(obsTel);
        telefonos.add(telefono);
    }
    
    public void agregarDireccion(HttpServletRequest request) {
        Domicilio domi = new Domicilio();
        domi.setApartamento(request.getParameter("apto"));
        domi.setBarrio(request.getParameter("barrio"));
        domi.setBlock(request.getParameter("block"));
        domi.setCalle(request.getParameter("calle"));
        domi.setDepartamento(Domicilio.Departamento.valueOf(request.getParameter("departamento")));
        domi.setEdificio(request.getParameter("edificio"));
        domi.setEsquina(request.getParameter("esquina"));
        domi.setLocalidad(request.getParameter("localidad"));
        domi.setManzanaSolar(request.getParameter("manzanasolar"));
        domi.setNumero(request.getParameter("numero"));
        domi.setTorre(request.getParameter("isTorre"));
        domicilios.add(domi);
    }
}
