/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.entity.Administrador;
import com.apoyoestudiantil.entity.Administrativo;
import com.apoyoestudiantil.entity.Rol;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.entity.Usuario_Rol;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaModificarAdminLista;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Polachek
 */
public class ControladorModificarAdminLista {
    private VistaModificarAdminLista vista;
    private Rol rol;
    private Institucion sistema = Institucion.getInstancia();

    public ControladorModificarAdminLista(VistaModificarAdminLista vista, String mrol) {
        this.vista = vista;
        this.rol = sistema.findRolByRol(mrol);
    }
    
    protected Institucion getSistema(){
        return sistema;
    }

    public void mostrarTodos() {
        try {
            if(rol.getRol().equals("ADMINISTRADOR")){
                List<Administrador> users = sistema.getAllAdministradores();
                vista.mostrarAdministradores(users);
            }else if(rol.getRol().equals("ADMINISTRATIVO")){
                List<Administrativo> users = sistema.getAllAdministrativos();
                vista.mostrarAdministrativos(users);
            }           
        } catch (AppException ex) {
            vista.mostrarError(ex.getMessage());
        }
    }

    public void buscar(HttpServletRequest request) {
        Long numDoc = Long.parseLong(request.getParameter("numDoc"));
        try {
            Usuario miUsu = sistema.buscarUsuarioporNDocumento(numDoc);
            boolean usuOk = false;
            for (Usuario_Rol rolU : miUsu.getRoles()){
                if(rolU.getRol().getRol().equals(rol.getRol())){
                    usuOk = true;
                    vista.resultadoBusqueda(miUsu);
                    break;
                }
            }
            if(!usuOk){
                vista.mostrarError("No se encontró ningún usuario");
            }
            
        } catch (AppException ex) {
            vista.mostrarError(ex.getMessage());
        }
        
    }

    public void modificar(HttpServletRequest request) {
        Long numDoc = Long.parseLong(request.getParameter("numDoc"));
        try {
            Usuario miUsu = sistema.buscarUsuarioporNDocumento(numDoc);
            if(rol.getRol().equals("ADMINISTRADOR") || rol.getRol().equals("ADMINISTRATIVO")){
                vista.modificarUser();
                request.getSession(true).setAttribute("usermod", miUsu);
            }else{
                vista.mostrarError("Error en seleccion de usuario");
            }
        } catch (AppException ex) {
            vista.mostrarError(ex.getMessage());
        }
    }
    
}
