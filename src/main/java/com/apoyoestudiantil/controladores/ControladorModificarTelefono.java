/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.interactores.SistemaDocentes;
import com.apoyoestudiantil.vistas.VistaModificarTelefono;
import java.util.regex.Pattern;
import javax.servlet.http.HttpServletRequest;

/**
 * @date 01-jul-2019
 * @time 8:55:19
 * @author Leonardo Pérez
 */
public class ControladorModificarTelefono {
    private VistaModificarTelefono vista;
//    private SistemaDocentes sistema;
//    private EntityManager mng;
    private Institucion sistema;

    public ControladorModificarTelefono(VistaModificarTelefono aThis) {
        this.vista = aThis;
//        this.mng = ContextoDB.getInstancia().getManager();
//        this.sistema = new SistemaDocentes(mng);
        this.sistema = Institucion.getInstancia();
    }

    public void procesar(HttpServletRequest request) {
        
        Docente d = (Docente)request.getSession().getAttribute("docenteAmodificar");
        modificarTelefonos(d, request);
        this.sistema.updateDocente(d);
        vista.enviar("Exito", "Se modificaron los datos!!");
    }

    private void modificarTelefonos(Docente d, HttpServletRequest request) {
        String listaEliminados = request.getParameter("listaEliminados");
        if(listaEliminados != null && !(listaEliminados.equals(""))){
            String pattern = Pattern.quote(":");
            String[] idsAeliminar = listaEliminados.split(pattern);
            for(String s : idsAeliminar){
                Long idTel = Long.parseLong(s);
                d.removeTel(idTel);
            }
        }
    }

    
}
