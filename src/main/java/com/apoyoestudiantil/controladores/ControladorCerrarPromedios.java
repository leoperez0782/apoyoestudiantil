/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.entity.PeriodoCes;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaCerrarPromedios;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 * @date 10-ago-2019
 * @time 8:39:29
 * @author Leonardo Pérez
 */
public class ControladorCerrarPromedios {

    private Institucion sistema;
    private VistaCerrarPromedios vista;
    private Libreta libretaActual;

    public ControladorCerrarPromedios(VistaCerrarPromedios vista) {
        this.vista = vista;
        this.sistema = Institucion.getInstancia();
    }

    public void cargarLista(int indice, HttpServletRequest request) {
        try {
            List<Libreta> libretas = (List) request.getSession().getAttribute("listaLibretas");
            this.libretaActual = libretas.get(indice);
            
            List<Alumno> alumnos = libretaActual.getGrupo().getAlumnos();
            request.getSession().setAttribute("libretaActual", libretaActual);
            request.getSession().setAttribute("alumnosGrupo", alumnos);
            
        } catch (Exception ex) {
            Logger.getLogger(ControladorEvaluarGrupo.class.getName()).log(Level.SEVERE, null, ex);
            this.vista.enviar("Error", ex.getMessage());
        }
    }

    public void procesar(HttpServletRequest request) {
        try {
            cargarNotasYcomentarios(request);

        } catch (Exception ex) {
            Logger.getLogger(ControladorCerrarPromedios.class.getName()).log(Level.SEVERE, null, ex);
            vista.enviar("Error", ex.getMessage());
        }
    }

    private void cargarNotasYcomentarios(HttpServletRequest request) {

        try {
            List<Alumno> alumnos = libretaActual.getGrupo().getAlumnos();
            List<PeriodoCes> periodos = libretaActual.getPeriodos();

            for (int i = 0; i < alumnos.size(); i++) {
                for (PeriodoCes periodo : periodos) {
                    cargarNotasDeAlumno(alumnos.get(i), periodo, i, request);
                }
            }
            if (this.sistema.actualizarReuniones(libretaActual)) {
                vista.enviar("Exito", "Se actualizaron los datos");
            }
        } catch (AppException ex) {
            vista.enviar("Error", ex.getMessage());
        }
    }

    private void cargarNotasDeAlumno(Alumno alumno, PeriodoCes periodo, int i, HttpServletRequest request) throws AppException {
        try {
            String nomCheckBox = "btn#" + periodo.getNombre() + "#" + i;
            String valorCheckBox = request.getParameter(nomCheckBox);
            if (valorCheckBox != null) {
                String nomComent = "com#" + periodo.getNombre() + "#" + i;
                String nomCal = "cal#" + periodo.getNombre() + "#" + i;
                String nomConducta = "cond#" + periodo.getNombre() + "#" + i;
                String comentario = request.getParameter(nomComent);
                int calificacion = Integer.parseInt(request.getParameter(nomCal));
                int conducta = Integer.parseInt(request.getParameter(nomConducta));
                this.libretaActual.cerrarReunionDeAlumno(alumno, comentario, calificacion, periodo, conducta);
            }
        } catch (NumberFormatException ne) {
            throw new AppException("Error al procesar las notas, el valor debe ser numerico");
        }
    }

}
