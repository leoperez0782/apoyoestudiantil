/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.entity.Administrador;
import com.apoyoestudiantil.entity.Administrativo;
import com.apoyoestudiantil.entity.Cargo;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Empleado;
import com.apoyoestudiantil.entity.Rol;
import com.apoyoestudiantil.entity.Telefono;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaAltaAdmin;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Polachek
 */
public class ControladorAltaAdmin {
    private VistaAltaAdmin vista;
    private Institucion sistema;
    private List<Telefono> telefonos = new ArrayList<>();
    private List<Domicilio> domicilios = new ArrayList<>();
    private List<Cargo> cargos = new ArrayList<>();

    public ControladorAltaAdmin(VistaAltaAdmin vista, String tipoUsu) {
        this.vista = vista;
        this.sistema = Institucion.getInstancia();
        if(tipoUsu.equals("adminO")){
            mostrarCargos();
        }        
    }
    
    public void reconectar(String tipoUsu) {
        if(tipoUsu.equals("adminO")){
            mostrarCargos();
        }
    }
    
    private void mostrarCargos() {
        cargos = obtenerCargosDisponibles();
        if(!cargos.isEmpty()){
            vista.mostrarCargos(cargos);
        }else{
            vista.mostrarError("No hay cargos agregados al sistema");
        }
    }

    public List<Cargo> obtenerCargosDisponibles() {
        try {
            return sistema.getAllCargos();
        } catch (AppException ex) {
            Logger.getLogger(ControladorAltaDocente.class.getName()).log(Level.SEVERE, null, ex);
            //vista.enviar("Error", ex.getMessage());//Genera problemas en el servlet
            return null;
        }

    }
    
    public void alta(HttpServletRequest request) {
        if(null == request.getParameter("tipousu") || "".equals(request.getParameter("tipousu"))){
            vista.mostrarError("No se puede dar de alta a este usuario");
        }else {
            String tipoUsu = request.getParameter("tipousu");
            switch (tipoUsu) {
                case "adminO":
                    altaAdministrativo(request);
                    break;
                case "admin":
                    altaAdmin(request);
                    break;
            }
        }
    }  

    public void altaAdministrativo(HttpServletRequest request) {
        try {
            Administrativo d = new Administrativo();
            d.setActivo(true);
            cargarDatos(d, request);
            asignarCargo(d, request);
            Rol r = sistema.findRolByRol("ADMINISTRATIVO");
            d.addRol(r);
            if(!domicilios.isEmpty()){
                d.setDomicilios(domicilios);
            }        
            if(!telefonos.isEmpty()){
                d.setTelefonos(telefonos);
            }
            sistema.guardarUsuario(d);           
            vista.mostrarExito("Se ha agregado el usuario con éxito");
            
        }catch(AppException ae) {
            Logger.getLogger(VistaAltaAdmin.class.getName()).log(Level.SEVERE, null, ae);
            vista.mostrarError("Error " + ae.getMessage());
        }catch(Exception ex){
            vista.mostrarError("No se puedo dar de alta el usuario, ya existe un ususario con los datos ingresados");
        }catch(Throwable e){
            vista.mostrarError("No se puedo dar de alta el usuario, ya existe un ususario con los datos ingresados");
        }
    }
    
    private void altaAdmin(HttpServletRequest request) {
        try {
            Administrador d = new Administrador();
            d.setActivo(true);
            cargarDatos(d, request);
            Rol r = sistema.findRolByRol("ADMINISTRADOR");
            d.addRol(r);
            if(!domicilios.isEmpty()){
                d.setDomicilios(domicilios);
            }        
            if(!telefonos.isEmpty()){
                d.setTelefonos(telefonos);
            }
            sistema.guardarUsuario(d);
            vista.mostrarExito("Se ha agregado el usuario con éxito");
        }catch(Exception ex){
            vista.mostrarError("No se puedo dar de alta el usuario, ya existe un ususario con los datos ingresados");
        }catch(Throwable e){
            vista.mostrarError("No se puedo dar de alta el usuario, ya existe un ususario con los datos ingresados");
        }
    }

    private void cargarDatos(Usuario a, HttpServletRequest request){
        //Documento
        Documento d1 = new Documento();
        d1.setNumero(Long.parseLong(request.getParameter("ci")));
        d1.setPaisEmisor(request.getParameter("ci_emisor"));
        d1.setTipo(Documento.TipoDocumento.valueOf(request.getParameter("ci_tipo")));
        a.setDocumento(d1);
        
        //Datos Personales
        a.setEmail(request.getParameter("email"));
        a.setFechaNacimiento(request.getParameter("fechan"));
        a.setPrimerNombre(request.getParameter("nombre"));
        a.setSegundoNombre(request.getParameter("nombre_segundo"));
        a.setPrimerApellido(request.getParameter("apellido"));
        a.setSegundoApellido(request.getParameter("apellido_segundo"));
        String clave = request.getParameter("clave");
        a.setClave(clave);
    }

    private void asignarCargo(Empleado d, HttpServletRequest request) throws AppException {
        Long cargoId = Long.parseLong(request.getParameter("cargo"));
        Cargo c = sistema.findCargoById(cargoId);
        d.setCargo(c);
    }

    public void agregarTelefono(String tipoTel, String numTel, String obsTel) {
        Telefono telefono = new Telefono();
        telefono.setTipo(Telefono.TipoTelefono.valueOf(tipoTel.toUpperCase()));
        telefono.setNumero(numTel);
        telefono.setObservaciones(obsTel);
        telefonos.add(telefono);
    }
    
    public void agregarDireccion(HttpServletRequest request) {
        Domicilio domi = new Domicilio();
        domi.setApartamento(request.getParameter("apto"));
        domi.setBarrio(request.getParameter("barrio"));
        domi.setBlock(request.getParameter("block"));
        domi.setCalle(request.getParameter("calle"));
        domi.setDepartamento(Domicilio.Departamento.valueOf(request.getParameter("departamento")));
        domi.setEdificio(request.getParameter("edificio"));
        domi.setEsquina(request.getParameter("esquina"));
        domi.setLocalidad(request.getParameter("localidad"));
        domi.setManzanaSolar(request.getParameter("manzanasolar"));
        domi.setNumero(request.getParameter("numero"));
        domi.setTorre(request.getParameter("isTorre"));
        domicilios.add(domi);
    }
}
