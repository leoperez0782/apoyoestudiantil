/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.dao.AuxiliarMatriculaDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.GrupoDAO;
import com.apoyoestudiantil.dao.NivelDAO;
import com.apoyoestudiantil.dao.RolDAO;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.AuxiliarMatricula;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.ItemAuxiliarMatricula;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.entity.Rol;
import com.apoyoestudiantil.entity.Telefono;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaEstudianteAgregar;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Polachek
 */
public class ControladorEstudianteAgregar {
    private VistaEstudianteAgregar vista;
    private Usuario usuario;
    private Institucion sistema = Institucion.getInstancia();
    private Nivel nivel;
    private Grupo grupo;
    private List<Telefono> telefonos = new ArrayList<>();
    private List<Domicilio> domicilios = new ArrayList<>();
    EntityManager mng = ContextoDB.getInstancia().getManager();
    EntityTransaction tx = mng.getTransaction();
    
    public ControladorEstudianteAgregar(VistaEstudianteAgregar vista, Usuario usu) {
        this.vista = vista;
        this.usuario = usu;
        ArrayList<Nivel> niveles = sistema.getNiveles();
        vista.mostrarNiveles(niveles);
    }
    
    protected Institucion getSistema(){
        return sistema;
    }
    
    public void reconectar() {
        vista.mostrarNiveles(sistema.getNiveles());
    }

    public void nivelSeleccionado(long nvSel) {
        this.nivel = sistema.buscarNivelID(nvSel);
        vista.mostrarGruposNivel(nivel.getGrupos());
    }

    public void altaEstudiante(HttpServletRequest request){
        if(nivel==null){
            vista.mostrarError("No es posible dar de alta un estudiante sin nivel");
        }else{       
            AlumnoDAO daoAlumno = new AlumnoDAO(mng);
            NivelDAO dao = new NivelDAO(mng);
            RolDAO roldao = new RolDAO(mng);
            GrupoDAO daoGrup = new GrupoDAO(mng);
            AuxiliarMatriculaDAO auxDao = new AuxiliarMatriculaDAO(mng);
            Nivel miNivel;
            Rol r = roldao.findByRol("ESTUDIANTE");
            tx.begin();
            Alumno a = new Alumno();
            a.setActivo(true);
            a.addRol(r);
            cargarDatos(a, request);        
            if(!domicilios.isEmpty()){
                a.setDomicilios(domicilios);
            }        
            if(!telefonos.isEmpty()){
                a.setTelefonos(telefonos);
            }

            boolean agrego = false;       
            miNivel = dao.findById(nivel.getId());
            String errmsg="";

            if(grupo!=null){
                try{
                    a.setId(daoAlumno.persistWithTransaction(tx, mng, a));
                    Grupo grupobd = daoGrup.findByNivelYAnio(miNivel, 2019);
                    grupobd.addAlumno(a);
                    miNivel.addAlumno(a);
                    daoGrup.updateWithTransaction(tx, mng, grupobd);
                    AuxiliarMatricula aux = new AuxiliarMatricula();
                    aux.setAlumno(a);
                    ItemAuxiliarMatricula item = new ItemAuxiliarMatricula();
                    item.setNivel(miNivel);
                    item.setMatricula(aux);
                    aux.setId(auxDao.persistWithTransaction(tx, mng, aux));
                    tx.commit();
                    agrego = true;
                }catch(Exception ex){
                    agrego = false;
                    errmsg = " - Ya existe un ususario con los datos ingresados";
                }catch(Throwable e){
                    agrego = false;
                    errmsg = " - Ya existe un ususario con los datos ingresados";
                }            
            }else{
                try{
                    a.setId(daoAlumno.persistWithTransaction(tx, mng, a));
                    miNivel.addAlumno(a);
                    AuxiliarMatricula aux1 = new AuxiliarMatricula();
                    aux1.setAlumno(a);
                    ItemAuxiliarMatricula item1 = new ItemAuxiliarMatricula();
                    item1.setNivel(miNivel);
                    aux1.addItem(item1);
                    aux1.setId(auxDao.persistWithTransaction(tx, mng, aux1));
                    tx.commit();
                    agrego = true;
                }catch(Exception ex){
                    agrego = false;
                    errmsg = " - Ya existe un usuario con los datos ingresados";
                }catch(Throwable e){
                    agrego = false;
                    errmsg = " - Ya existe un usuario con los datos ingresados";
                } 
            }      

            if(agrego){
                vista.mostrarExito("Se agregó exitosamente el Alumno");
            }else{
                vista.mostrarError("No se pudo agregar el Alumno"+errmsg);
            }
        }
    }
    
    private void cargarDatos(Alumno a, HttpServletRequest request) {
        //Documento
        Documento d1 = new Documento();
        d1.setNumero(Long.parseLong(request.getParameter("ci")));
        d1.setPaisEmisor(request.getParameter("ci_emisor"));
        d1.setTipo(Documento.TipoDocumento.valueOf(request.getParameter("ci_tipo")));
        a.setDocumento(d1);
        
        //Datos Personales
        a.setEmail(request.getParameter("email"));
        a.setFechaNacimiento(request.getParameter("fechan"));
        a.setPrimerNombre(request.getParameter("nombre"));
        if(request.getParameter("nombre_segundo")!=null){
            a.setSegundoNombre(request.getParameter("nombre_segundo"));
        }
        a.setPrimerApellido(request.getParameter("apellido"));
        if(request.getParameter("apellido_segundo")!=null){
            a.setSegundoApellido(request.getParameter("apellido_segundo"));
        }        
        String clave = request.getParameter("clave");
        a.setClave(clave);
        a.setMatricula(Integer.parseInt(request.getParameter("matricula")));
    }

    public void agregarTelefono(String tipoTel, String numTel, String obsTel) {
        Telefono telefono = new Telefono();
        telefono.setTipo(Telefono.TipoTelefono.valueOf(tipoTel.toUpperCase()));
        telefono.setNumero(numTel);
        telefono.setObservaciones(obsTel);
        telefonos.add(telefono);
    }
    
    public void agregarDireccion(HttpServletRequest request) {
        Domicilio domi = new Domicilio();
        domi.setApartamento(request.getParameter("apto"));
        domi.setBarrio(request.getParameter("barrio"));
        domi.setBlock(request.getParameter("block"));
        domi.setCalle(request.getParameter("calle"));
        domi.setDepartamento(Domicilio.Departamento.valueOf(request.getParameter("departamento")));
        domi.setEdificio(request.getParameter("edificio"));
        domi.setEsquina(request.getParameter("esquina"));
        domi.setLocalidad(request.getParameter("localidad"));
        domi.setManzanaSolar(request.getParameter("manzanasolar"));
        domi.setNumero(request.getParameter("numero"));
        domi.setTorre(request.getParameter("isTorre"));
        domicilios.add(domi);
    }

    public void grupoSeleccionado(long grupoSel) {
        grupo = nivel.getGrupo(grupoSel);
    }
}
