/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.controladores;

import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.NotificacionDAO;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Evaluacion;
import com.apoyoestudiantil.entity.Inasistencia;
import com.apoyoestudiantil.entity.ItemDesarrollo;
import com.apoyoestudiantil.entity.Notificacion;
import com.apoyoestudiantil.entity.Promedio;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.Institucion;
import com.apoyoestudiantil.vistas.VistaInicioAlumno;
import java.util.List;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;

/**
 *
 * @author Polachek
 */
public class ControladorInicioAlumno {
    private VistaInicioAlumno vista;
    private Institucion sistema;
    private Alumno alumno;
    private EntityManager mng = ContextoDB.getInstancia().getManager();
    
    public ControladorInicioAlumno(VistaInicioAlumno vista, Alumno alm) {
        this.vista = vista;
        this.sistema = Institucion.getInstancia();
        this.alumno = alm;
        comprobarNotificaciones();
    }
    
    public void reconectar() {
        
    }

    public void asistencias() {
        List<Inasistencia> inasistencias = alumno.getInasistencias();
        vista.mostrarInasistencias(inasistencias);
    }
    
    public void evaluaciones() {
        try {
            List<Evaluacion> lista = sistema.getEvaluacionesAlumno(alumno);
            vista.mostrarEvaluaciones(lista);
        } catch (AppException ex) {
            vista.mostrarError(ex.getMessage());
        }       
    }
    
    public void promedios() {
        try {
            List<Promedio> promedios = sistema.obetenrPromediosAlumno(alumno);
            vista.mostrarPromedios(promedios);
        } catch (AppException ex) {
            vista.mostrarError(ex.getMessage());
        }              
    }

    public void calendario() {
        try {
            List<ItemDesarrollo> items = sistema.calendarioEvaluacionesAlumno(alumno);
            vista.mostrarCalendario(items);
        } catch (AppException ex) {
            vista.mostrarError(ex.getMessage());
        }
    }
    
    public void comprobarNotificaciones() {
        NotificacionDAO daoNotif = new NotificacionDAO(mng);
        List<Notificacion> lista = daoNotif.getAllByDestinatrio(alumno);
        boolean hayNuevas = false;        
        for (Notificacion a : lista) {
            if(!a.isLeida()){
                hayNuevas = true;
                break;
            }
        }
        if(hayNuevas){
            vista.hayNuevasNotificaciones();
        }else{
            vista.noHayNuevasNotificaciones();
        }
    }
    
    public void notificaciones(HttpServletRequest request) {
        NotificacionDAO daoNotif = new NotificacionDAO(mng);
        List<Notificacion> lista = daoNotif.getAllByDestinatrio(alumno);
        vista.mostrarNotificaciones(lista);
    }

    public void verNotificacion(HttpServletRequest request) {
        Long idN = Long.parseLong(request.getParameter("idN"));
        NotificacionDAO daoNotif = new NotificacionDAO(mng);
        Notificacion miNot = daoNotif.findById(idN);
        if(!miNot.isLeida()){
            miNot.setLeida(true);
            daoNotif.update(miNot);
        }        
        vista.mostrarNotificacion(miNot);
    }
    
}
