/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

import com.apoyoestudiantil.excepciones.AppException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 * @date 05-jun-2019
 * @time 21:42:14
 * @author Leonardo Pérez
 */
@NamedQueries({
    @NamedQuery(
            name = "get_all_by_docente",
            query = "select l from Libreta l where l.docente = :docente"
    ),
    @NamedQuery(
            name = "find_libreta_by_id",
            query = "select l from Libreta l where l.id = :id"
    ),
    @NamedQuery(
            name = "get_count_by_grupo",
            query = "select count(l) from Libreta l where l.grupo = :grupo"
    ),
    @NamedQuery(
            name = "get_all_by_alumnno",
            query = "select l from Libreta l where :alumno MEMBER OF l.grupo.alumnos"
    ),
    @NamedQuery(
            name = "get_all_by_grupo",
            query = "select l from Libreta l where l.grupo = :grupo"
    )
})
@Entity
public class Libreta implements Serializable {

   
    public enum Turno {
        MATUTINO, VESPERTINO, NOCTURNO
    }
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToOne
    @JoinColumn(name = "grupo_id")
    private Grupo grupo;
    @OneToOne
    @JoinColumn(name = "asignatura_id")
    private Asignatura materia;
    @ManyToOne
    @JoinColumn(name = "docente_id")
    private Docente docente;
    private boolean teorico;
    private boolean practico;
    @Enumerated(EnumType.STRING)
    private Turno turno;
    private String salon;
    @OneToMany(mappedBy = "libreta", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Reunion> reuniones = new ArrayList<>();
    @OneToMany(mappedBy = "libreta", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ItemDesarrollo> desarrollos = new ArrayList<>();
    @OneToMany(mappedBy = "libreta", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Horario> horarios = new ArrayList<>();
    @OneToMany(mappedBy = "libreta", cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER)
    private List<Lista> pasajesDeLista = new ArrayList<>();
    @OneToMany(mappedBy = "libreta", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Evaluacion> evaluaciones = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Grupo getGrupo() {
        return grupo;
    }

    public void setGrupo(Grupo grupo) {
        this.grupo = grupo;
    }

    public Asignatura getMateria() {
        return materia;
    }

    public void setMateria(Asignatura materia) {
        this.materia = materia;
    }

    public Docente getDocente() {
        return docente;
    }

    public void setDocente(Docente docente) {
        this.docente = docente;
    }

    public boolean isTeorico() {
        return teorico;
    }

    public void setTeorico(boolean teorico) {
        this.teorico = teorico;
    }

    public boolean isPractico() {
        return practico;
    }

    public void setPractico(boolean practico) {
        this.practico = practico;
    }

    public Turno getTurno() {
        return turno;
    }

    public void setTurno(Turno turno) {
        this.turno = turno;
    }

    public String getSalon() {
        return salon;
    }

    public void setSalon(String salon) {
        this.salon = salon;
    }

    public List<Reunion> getReuniones() {
        return reuniones;
    }

    public void setReuniones(List<Reunion> reuniones) {
        this.reuniones.clear();
        this.reuniones.addAll(reuniones);
    }

    public List<ItemDesarrollo> getDesarrollos() {
        return desarrollos;
    }

    public void setDesarrollos(List<ItemDesarrollo> desarrollos) {
        this.desarrollos = desarrollos;
    }

    public List<Horario> getHorarios() {
        return horarios;
    }

    public void setHorarios(List<Horario> horarios) {
        this.horarios = horarios;
    }

    public List<Lista> getPasajesDeLista() throws AppException {

        return pasajesDeLista;
    }

    public void setPasajesDeLista(List<Lista> pasajesDeLista) {
        
        this.pasajesDeLista.clear();
        this.pasajesDeLista.addAll(pasajesDeLista);
    }

    public boolean isPasajeListaEmpty() {
        return this.pasajesDeLista.isEmpty();
    }

    public void addHorario(Horario h) {
        this.horarios.add(h);
        h.setLibreta(this);
    }

    public void addEvaluacion(Evaluacion ev) {
        this.evaluaciones.add(ev);
        ev.setLibreta(this);
    }

    public void addItemDesarrollo(ItemDesarrollo item) {
        this.desarrollos.add(item);
        item.setLibreta(this);
    }

    public List<Evaluacion> getEvaluaciones() {
        return evaluaciones;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Libreta)) {
            return false;
        }
        Libreta other = (Libreta) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.apoyoestudiantil.entity.Libreta[ id=" + id + " ]";
    }

    public List<PeriodoCes> getPeriodos() {
        List<PeriodoCes> retorno;
        String programa = this.materia.getProgramaAnio();
        switch (programa) {
            case "ProgramaPrimeroSegundoCBReformulacion2006":
            case "ProgramaTerceroCBReformulacion2006":
                retorno = Arrays.asList(PeriodoCesCBR2006.values());
                break;
            case "ProgramaCuartoRef2006":
                retorno = Arrays.asList(PeriodoCuartoRef2006.values());
                break;
            case "ProgramaQuintoDiversifArteYExpresion":
            case "ProgramaQuintoDiversifBiologica":
            case "ProgramaQuintoDiversifCientifica":
            case "ProgramaQuintoDiversifHumanistica":
            case "ProgramaQuintoNucleoComunRef2006":
                retorno = Arrays.asList(PeriodoCesQuintoYSextoRef2006.values());
            default:
                retorno = new ArrayList<>();
                break;
        }
        return retorno;
    }

    public ItemDesarrollo getItemDesarrollo(Long id) {
        return this.desarrollos.stream()
                .filter(i -> i.getId().equals(id)).findFirst()
                .get();
    }

    public List<Integer> cargarNotas(Alumno alumno, String tipoEv, PeriodoCes periodo) {
        ArrayList<Integer> lista = new ArrayList();
        Evaluacion.TipoEvaluacion tipo = Evaluacion.TipoEvaluacion.valueOf(tipoEv);
        for (Evaluacion ev : evaluaciones) {
            if (ev.getPeriodo().equals(periodo.getNombre()) && alumno.getId().equals(ev.getAlumno().getId()) && ev.getTipo().equals(tipo)) {
                lista.add(ev.getCalificacion());
            }
        }
        return lista;
    }

    public int calcularPromedio(Alumno alumno, PeriodoCes periodo) {
        int total = 0;
        int cantidad = 0;

        for (Evaluacion ev : evaluaciones) {
            if (alumno.getId().equals(ev.getAlumno().getId()) && ev.getPeriodo().equals(periodo.getNombre())) {
                total += ev.getCalificacion();
                cantidad++;
            }
        }
        if (cantidad == 0) {
            return 0;
        } else {
            return total / cantidad;
        }
    }

    /**
     *Metodo que devuelve el promedio del alumno pasado por parametro , en el periodo correspondient a @param periodo.
     * Si el docente ya lo califico en un cierre de reunion, devuelve la calificacion asignada por el docente.
     * Si no, devuelve el promedio calculado en base a las calificaciones que obtuvo el alumno en dicho periodo.
     * Si no tiene ninguna evaluacion calificada, devuelve cero.
     * @param alumno
     * @param periodo
     * @return
     */
    public int recuperarPromedioReunion(Alumno alumno, PeriodoCes periodo) {

        Optional<Reunion> reunionOp = Optional.empty();
        //Si no existen reuniones guardadas en la libreta devuelve el promedio.
        if (this.reuniones.isEmpty()) {
            return calcularPromedio(alumno, periodo);
        }
        //busco la reunion del periodo.
        reunionOp = reuniones.stream()
                .filter(r -> r.getPeriodoEnLibreta().equals(periodo.getNombre()))
                .findFirst();
        //si existe reunion en ese periodo.
        if (reunionOp.isPresent()) {
            Reunion reunion = reunionOp.get();
            //Reviso si no hay items guardados, si no hay, devuelvo el promedio.
            if (reunion.getItems().size() == 0) {
                return calcularPromedio(alumno, periodo);
            }
            //Busco la calificacion del alumno en esta reunion.
            int calif = obtenerCalificacionDeReunion(alumno, reunion);
            //si tiene cero, es por que existe el item de la reunion, pero el docente no lo califico todavia.
            if (calif == 0) {
                //devuelvo el promedio
                return calcularPromedio(alumno, periodo);
            } else {
                //devuelvo la calificacion 
                return calif;
            }
        } else {//si no existe la reunion devuelvo el promedio.
            return calcularPromedio(alumno, periodo);
        }
    }

    private int obtenerCalificacionDeReunion(Alumno alumno, Reunion reunion) {

        Optional<ItemReunion> item = reunion.getItems().stream()
                .filter(r -> r.getAlumno().equals(alumno))
                .findFirst();
        if (item.isPresent()) {
            return item.get().getCalificacion();
        } else {
            return 0;
        }
    }

    public String recuperarComentarioReunion(Alumno alumno, PeriodoCes periodo) {
        Reunion reunion = null;
        if (this.reuniones.isEmpty()) {
            return "";
        }
        try {
            reunion = reuniones.stream()
                    .filter(r -> r.getPeriodoEnLibreta().equals(periodo.getNombre()))
                    .findFirst().get();
        } catch (Exception e) {//Si no lo encuentra tira error
            return "";
        }
        Optional<ItemReunion> op = reunion.getItems().stream().filter(i -> i.getAlumno().equals(alumno))
                .findFirst();
        String coment = null;
        if (op.isPresent()) {
            coment = op.get().getJuicioDocente();
        }
        if (coment == null) {
            coment = "";
        }
        return coment;
    }

    public int recuperarConductaReunion(Alumno alumno, PeriodoCes periodo) {
        Reunion reunion = null;
        int conducta = 0;
        if (this.reuniones.isEmpty()) {
            return 0;
        }
        try {
            reunion = reuniones.stream()
                    .filter(r -> r.getPeriodoEnLibreta().equals(periodo.getNombre()))
                    .findFirst().get();
        } catch (Exception e) {//Si no lo encuentra tira error
            return 0;
        }
        Optional<ItemReunion> op = reunion.getItems().stream().filter(i -> i.getAlumno().equals(alumno))
                .findFirst();
        if (op.isPresent()) {
            conducta = op.get().getConducta();
        }
        return conducta;
    }

    public void cerrarReunionDeAlumno(Alumno alumno, String comentario, int calificacion, PeriodoCes periodo, int conducta) {
        Optional<Reunion> reunionOp = reuniones.stream()
                .filter(r -> r.getPeriodoEnLibreta().equals(periodo.getNombre()))
                .findFirst();
        if (reunionOp.isPresent()) {//Si existe la reunion carga los datos en un item.
            Reunion r = reunionOp.get();
            r.actualizarItem(alumno, comentario, calificacion, conducta, this.getMateria());

        } else {//Si no existe la reunio la crea y carga los datos en un item.
            Reunion r = new Reunion();
            r.setLibreta(this);
            r.setPeriodoEnLibreta(periodo.getNombre());
            r.crearItem(alumno, comentario, calificacion, conducta, this.getMateria());
            r.asignarNombreSegunNombrePeriodo(this.materia.getProgramaAnio(), periodo.getNombre());
            this.reuniones.add(r);
        }
    }

    /**
     * Devuelve true, si el item de reunion correspondiente al alumno, en el
     * periodo pasados por parametros, fue cerrado.
     *
     * @param alumno
     * @param periodo
     * @return
     */
    public boolean esReunionCerrada(Alumno alumno, PeriodoCes periodo) {
        Optional<Reunion> reunionOp = reuniones.stream()
                .filter(r -> r.getPeriodoEnLibreta().equals(periodo.getNombre()))
                .findFirst();
        Optional<ItemReunion> item = Optional.empty();
        if (reunionOp.isPresent()) {
            Reunion r = reunionOp.get();
            item = r.getItems()
                    .stream()
                    .filter(i -> i.getAlumno().equals(alumno) && i.isCerrada())
                    .findFirst();
        }
        return item.isPresent();
    }

    public boolean esReunionCerrada(String nombrePeriodo) {
        Optional<Reunion> reunionOp = reuniones.stream()
                .filter(r -> r.getPeriodoEnLibreta().equals(nombrePeriodo))
                .findFirst();
        if (reunionOp.isPresent()) {
            return reunionOp.get().isCerrada();
        }
        return false;
    }

    public Optional<Reunion> getReunionDelPeriodo(String periodo) {
        Optional<Reunion> retorno = this.reuniones.stream()
                .filter(r -> r.getPeriodoEnLibreta().equals(periodo))
                .findFirst();
        return retorno;
    }

    public void cerrarItemReunion(Alumno a, String juicio, String periodoElegido) {
        Optional<Reunion> opReunion = this.getReunionDelPeriodo(periodoElegido);//busco si existe la reunion.
        if (opReunion.isPresent()) {
            Reunion r = opReunion.get();
            Optional<ItemReunion> item = r.getItemAlumno(a);
            if (item.isPresent()) {//si el item existe agrego el juicio de la reunion
                item.get().setJuicioReunion(juicio);
                item.get().setCerrada(true);
            } else {//si no existe lo creo y paso valores por defecto.
                PeriodoCes per = encontrarPeriodoCes(periodoElegido);
                //calculo el promedio basado en las evaluaciones del periodo.
                int calificacion = this.calcularPromedio(a, per);
                //la conducta se pasa cero al no estar calificada.
                //el item creado ya queda cerrado.
                r.crearItem(a, juicio, "sin calificar", calificacion, 0, materia);
            }
        } else {//si la reunion no existe, la creo y guardo el item.
            PeriodoCes per = encontrarPeriodoCes(periodoElegido);

            int calificacion = this.calcularPromedio(a, per);
            Reunion nueva = new Reunion();
            nueva.setLibreta(this);
            nueva.asignarNombreSegunNombrePeriodo(materia.getProgramaAnio(), periodoElegido);
            nueva.setPeriodoEnLibreta(periodoElegido);
            nueva.crearItem(a, juicio, "sin calificar", calificacion, 0, materia);
            this.reuniones.add(nueva);
        }
    }

    private PeriodoCes encontrarPeriodoCes(String periodoElegido) {
        return this.getPeriodos().stream()
                .filter(p -> p.getNombre().equals(periodoElegido))
                .findFirst().get();
    }

    /**
     * Cierra la reunion del periodo, si existe.
     *
     * @param periodoElegido
     */
    public void cerrarReunion(String periodoElegido) {
        Optional<Reunion> opReunion = this.getReunionDelPeriodo(periodoElegido);
        if (opReunion.isPresent()) {
            opReunion.get().setCerrada(true);
        }
    }
    
     public void validarJuicio(String juicio) throws AppException {
       if(juicio == null || juicio.isEmpty() || juicio == ""){
           throw new AppException("El juicio de reunion no puede estar en blanco");
       }
    }

}
