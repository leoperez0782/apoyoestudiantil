/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

/**
 *
 * @author Leonardo Pérez
 */
public enum PeriodoCesCBR2006 implements PeriodoCes {

    MAR_ABR("Mar-Abr"),
    MAY("May"),
    JUN_JUL("Jun-Jul"),
    AGO("Ago"),
    NOV_DIC("Nov-Dic");

    private final String value;

    private PeriodoCesCBR2006(String value) {
        this.value = value;
    }

    @Override
    public String getNombre() {
        return this.value;
    }

}
