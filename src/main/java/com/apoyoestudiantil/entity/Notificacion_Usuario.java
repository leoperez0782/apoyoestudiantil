/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.entity;

import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @date 30-may-2019
 * @time 22:44:05
 * @author Leonardo Pérez
 */
//@Entity
public class Notificacion_Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @ManyToOne
    private Usuario destinatario;
    
    @Id
    @ManyToOne
    private Notificacion notificacion;

    public Notificacion_Usuario() {
    }

    public Notificacion_Usuario(Usuario usuario, Notificacion notificacion) {
        this.destinatario = usuario;
        this.notificacion = notificacion;
    }
    
    
    

    
}
