/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.entity;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @date 05-jun-2019
 * @time 21:03:02
 * @author Leonardo Pérez
 */
@Entity
public class Inasistencia implements Serializable {

    

    
    public enum EstadoInasistencia{JUSTIFICADA, INJUSTIFICADA, EXONERADA}
    public enum TipoInasistencia {JUSTIFICADA, INJUSTIFICADA, EXONERADA, TARDE, MUY_TARDE}
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private LocalDate fecha;
    private String horaInicio;
    @Enumerated(EnumType.STRING)
    private EstadoInasistencia estado;
    @Enumerated(EnumType.STRING)
    private TipoInasistencia tipo;
    private String justificacion;
    @ManyToOne
    private Lista lista;
    @ManyToOne
    private Alumno alumno;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }



    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }
    

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String horaInicio) {
        this.horaInicio = horaInicio;
    }

    public EstadoInasistencia getEstado() {
        return estado;
    }

    public void setEstado(EstadoInasistencia estado) {
        this.estado = estado;
    }

    public String getJustificacion() {
        return justificacion;
    }

    public void setJustificacion(String justificacion) {
        this.justificacion = justificacion;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
        
    }

    public TipoInasistencia getTipo() {
        return tipo;
    }

    public void setTipo(TipoInasistencia tipo) {
        this.tipo = tipo;
    }

    public Lista getLista() {
        return lista;
    }

    public void setLista(Lista lista) {
        this.lista = lista;
    }

    
    /**
     * Devuelve justificada solo para el mismo valorTipoFalta
     * Todo lo demas se considera injustificado.
     * @param valorTipoFalta
     * @return 
     */
    public static EstadoInasistencia devolverEstado(String valorTipoFalta) {
        switch(valorTipoFalta){
            case "JUSTIFICADA":
                return EstadoInasistencia.JUSTIFICADA;
            default:
                return EstadoInasistencia.INJUSTIFICADA;
        }
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Inasistencia)) {
            return false;
        }
        Inasistencia other = (Inasistencia) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.apoyoestudiantil.entity.Inasistencia[ id=" + id + " ]";
    }

}
