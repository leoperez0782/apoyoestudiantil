/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

/**
 *
 * @author leo
 */
public enum ProgramaCuartoRef2006 implements ProgramaAsignaturas{
    LITERATURA("Literatura", 4),
    MATEMATICA("Matemática", 4),
    INGLES("Inglés", 3),
    HISTORIA("Historia", 4),
    ASTRONOMIA("Astronomía", 2),
    BIOLOGIA("Biología", 2),
    FISICA("Física", 3),
    QUIMICA("Química", 3),
    DIBUJO("Dibujo", 3),
    FILOSOFIA_Y_CRITICA_DE_LOS_SABERES("Filosofía y crítica de los saberes", 4),
    ESPACIO_EXTRACURRICULAR_OPTATIVO_COMUNICACION_ORAL_Y_ESCRITA("Comunicación oral y escrita", 2),
    ESPACIO_EXTRACURRICULAR_OPTATIVO_INFORMATICA("Informática", 2),
    EDUCACION_FISICA_Y_RECREACION("Educación Física  y Recreación", 3);

    private final String value;
    private final int horas;

    ProgramaCuartoRef2006(String value, int horas) {
        this.value = value;
        this.horas = horas;
    }
     @Override
    public String getNombre() {
        return this.value;
    }

    @Override
    public int getHoras() {
        return this.horas;
    }

    @Override
    public String getProgramaAnio() {
        return this.getClass().getSimpleName();
    }
}
