/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * @date 25-may-2019
 * @time 8:12:40
 * @author Leonardo Pérez
 */
@Entity
public class Domicilio implements Serializable {

    public enum Departamento {
        ARTIGAS, CANELONES, CERRO_LARGO, COLONIA,
        DURAZNO, FLORES, FLORIDA, LAVALLEJA, MALDONADO, MONTEVIDEO, PAYSANDU, RIO_NEGRO,
        RIVERA, ROCHA, SALTO, SAN_JOSE, SORIANO, TACUAREMBO, TREINTA_Y_TRES
    }
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Enumerated(EnumType.STRING)
    private Departamento departamento;
    //Localidad
    private String localidad;
    //BArrio
    private String barrio;
    //CAlle/Ruta
    private String calle;
    //numero/km
    private String numero;
    //esquina
    private String esquina;
    //block
    private String block;
    //manzana/solar
    private String manzanaSolar;
    //apto
    private String apartamento;
    //torre
    private String torre;
    //casa/Edificio
    private String edificio;

    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Departamento getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Departamento departamento) {
        this.departamento = departamento;
    }

    public String getLocalidad() {
        return localidad;
    }

    public void setLocalidad(String localidad) {
        this.localidad = localidad;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getCalle() {
        return calle;
    }

    public void setCalle(String calle) {
        this.calle = calle;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getEsquina() {
        return esquina;
    }

    public void setEsquina(String esquina) {
        this.esquina = esquina;
    }

    public String getBlock() {
        return block;
    }

    public void setBlock(String block) {
        this.block = block;
    }

    public String getManzanaSolar() {
        return manzanaSolar;
    }

    public void setManzanaSolar(String manzanaSolar) {
        this.manzanaSolar = manzanaSolar;
    }

    public String getApartamento() {
        return apartamento;
    }

    public void setApartamento(String apartamento) {
        this.apartamento = apartamento;
    }

    public String getTorre() {
        return torre;
    }

    public void setTorre(String torre) {
        this.torre = torre;
    }

    public String getEdificio() {
        return edificio;
    }

    public void setEdificio(String edificio) {
        this.edificio = edificio;
    }

  
    public Departamento devolverDepartamentoSegunString(String depart){
        
        return Departamento.valueOf(depart.toUpperCase());
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Domicilio)) {
            return false;
        }
        Domicilio other = (Domicilio) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.apoyoestudiantil.entity.Domicilio[ id=" + id + " ]";
    }

}
