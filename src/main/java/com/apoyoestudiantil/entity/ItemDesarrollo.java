/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

import java.io.Serializable;
import java.time.LocalDate;
import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;

/**
 * @date 08-jun-2019
 * @time 17:12:52
 * @author Leonardo Pérez En esta clase se guarda el desarrollo de las clases
 * dictadas por el docente.
 *
 */
@Entity
public class ItemDesarrollo implements Serializable {

    public enum TipoDeClase {
        COMUN, EPI, DUPLA, TRIPLA
    }
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String desarrollo;

    @Basic
    private LocalDate fecha;
    private boolean evaluacion;
    private boolean dictado;
    private int cantidadHorasDeClase;
    private boolean evalucionParaEntregarEnAdscripcion;
    private boolean corregida;
    @Enumerated(EnumType.STRING)
    private TipoDeClase tipoDeClase;
    @ManyToOne
    private Libreta libreta;
    @Lob
    private byte[] archivo;

    @Enumerated(EnumType.STRING)
    private Evaluacion.TipoEvaluacion tipo;

    private String periodo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDesarrollo() {
        return desarrollo;
    }

    public void setDesarrollo(String desarrollo) {
        this.desarrollo = desarrollo;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    
    public boolean isEvaluacion() {
        return evaluacion;
    }

    public void setEvaluacion(boolean evaluacion) {
        this.evaluacion = evaluacion;
    }

    public boolean isDictado() {
        return dictado;
    }

    public void setDictado(boolean dictado) {
        this.dictado = dictado;
    }

    public int getCantidadHorasDeClase() {
        return cantidadHorasDeClase;
    }

    public void setCantidadHorasDeClase(int cantidadHorasDeClase) {
        this.cantidadHorasDeClase = cantidadHorasDeClase;
    }

    public boolean isEvalucionParaEntregarEnAdscripcion() {
        return evalucionParaEntregarEnAdscripcion;
    }

    public void setEvalucionParaEntregarEnAdscripcion(boolean evalucionParaEntregarEnAdscripcion) {
        this.evalucionParaEntregarEnAdscripcion = evalucionParaEntregarEnAdscripcion;
    }

    public boolean isCorregida() {
        return corregida;
    }

    public void setCorregida(boolean corregida) {
        this.corregida = corregida;
    }

    public TipoDeClase getTipoDeClase() {
        return tipoDeClase;
    }

    public void setTipoDeClase(TipoDeClase tipoDeClase) {
        this.tipoDeClase = tipoDeClase;
    }

    public Libreta getLibreta() {
        return libreta;
    }

    public void setLibreta(Libreta libreta) {
        this.libreta = libreta;
    }

    public byte[] getArchivo() {
        return archivo;
    }

    public void setArchivo(byte[] archivo) {
        this.archivo = archivo;
    }

    public Evaluacion.TipoEvaluacion getTipo() {
        return tipo;
    }

    public void setTipo(Evaluacion.TipoEvaluacion tipo) {
        this.tipo = tipo;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemDesarrollo)) {
            return false;
        }
        ItemDesarrollo other = (ItemDesarrollo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.apoyoestudiantil.entity.ItemDesarrollo[ id=" + id + " ]";
    }

}
