/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

/**
 * @date 10-ago-2019
 * @time 3:35:59
 * @author Leonardo Pérez
 */
@Entity
public class ItemAuxiliarMatricula implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private AuxiliarMatricula matricula;
    
    
    @OneToMany(cascade = CascadeType.ALL)
    private List<ItemReunion> reuniones = new ArrayList<>();
    @ManyToOne
    private Nivel nivel;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public AuxiliarMatricula getMatricula() {
        return matricula;
    }

    public void setMatricula(AuxiliarMatricula matricula) {
        this.matricula = matricula;
    }

    public List<ItemReunion> getReuniones() {
        return reuniones;
    }

    public void setReuniones(List<ItemReunion> reuniones) {
        this.reuniones.clear();
        this.reuniones.addAll(reuniones);
    }

    public Nivel getNivel() {
        return nivel;
    }

    public void setNivel(Nivel nivel) {
        this.nivel = nivel;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemAuxiliarMatricula)) {
            return false;
        }
        ItemAuxiliarMatricula other = (ItemAuxiliarMatricula) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.apoyoestudiantil.entity.ItemAuxiliarMatricula[ id=" + id + " ]";
    }

    public void addAllItems(List<ItemReunion> items) {
        for(ItemReunion it: items){
            if(!this.reuniones.contains(it)){
                this.reuniones.add(it);
            }
        }
    }

}
