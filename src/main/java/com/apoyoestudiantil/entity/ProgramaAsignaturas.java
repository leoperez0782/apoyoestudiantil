/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

/**
 *
 * @author leo
 */
public interface ProgramaAsignaturas {
   public String getNombre();
   public int getHoras();
   public String getProgramaAnio();
}
