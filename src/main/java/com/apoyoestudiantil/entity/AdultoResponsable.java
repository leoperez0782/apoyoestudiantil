/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

/**
 * @author Guillermo Polachek
 */

@NamedQueries(
        {
            @NamedQuery(
                    name = "get_count_tutor",
                    query = "select count(d) from Padre d"
            ),
            @NamedQuery(
                    name = "get_all_tutor",
                    query = "select d from Padre d"
            ),
            @NamedQuery(
                    name = "find_tutor_by_id",
                    query = "select d from Padre d where d.id = :id"
            ),
            @NamedQuery(
                    name = "get_tutor_by_documento",
                    query = "select d from Padre d where d.documento = :doc"
            ),
             @NamedQuery(
                    name = "get_tutor_by_numero_documento",
                    query = "select d from Padre d where d.documento.numero = :num"
            )
        }
)
@Entity(name="Padre")
@Table(name="Padres")
public class AdultoResponsable extends Usuario implements Serializable {
    
    public static final String GET_COUNT_TUTOR = "get_count_tutor";
    public static final String GET_ALL_TUTOR = "get_all_tutor";
    public static final String FIND_TUTOR_BY_ID = "find_tutor_by_id";
    public static final String FIND_TUTOR_BY_DOCUMENTO = "get_tutor_by_documento";
    public static final String GET_TUTOR_BY_NUMERO_DOC = "get_tutor_by_numero_documento";
    
    
    public void addAlumno(Alumno hijo){
        
        hijo.getTutores().add(this);
    }
    

}
