/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @date 31-may-2019
 * @time 0:03:06
 * @author Leonardo Pérez
 */
@Entity
public class Actividad extends Notificacion implements Serializable {
    
    @Temporal(TemporalType.DATE)
    private Date fechaDeRealizacion;
    private String descripcion;
    private String urlImagen;


}
