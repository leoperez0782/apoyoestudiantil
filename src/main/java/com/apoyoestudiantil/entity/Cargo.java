/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @date 23-may-2019
 * @time 20:39:19
 * @author Leonardo Pérez
 */
@NamedQueries({
    @NamedQuery(name = "get_cargo_by_tipo", query = "select c from Cargo c where c.tipoCargo = :tipo")
    ,
    @NamedQuery(name = "get_all_cargos", query = "select c from Cargo c")
    ,
    @NamedQuery(name = "get_cargo_by_id", query = "select c from Cargo c where c.id = :id")
    ,
    @NamedQuery(name = "get_count_cargo", query = "select count(c) from Cargo c")
}
)
@Entity
public class Cargo implements Serializable {
    
    public static final String GET_COUNT_CARGO ="get_count_cargo";
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Column(unique = true)
    @NotEmpty
    private String tipoCargo;
    private String descripcion;
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTipoCargo() {
        return tipoCargo;
    }

    public void setTipoCargo(String tipoCargo) {
        this.tipoCargo = tipoCargo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

   

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cargo)) {
            return false;
        }
        Cargo other = (Cargo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.apoyoestudiantil.entity.Cargo[ id=" + id + " ]";
    }

}
