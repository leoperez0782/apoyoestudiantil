/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

/**
 *
 * @author leo
 */
public enum ProgramaQuintoDiversifArteYExpresion implements ProgramaAsignaturas{
    HISTORIA_DEL_ARTE("Historia del arte", 4),
    EXPRESION_MUSICAL("Expresión Musical", 3),
    EXPRESION_CORPORAL_Y_TEATRO("Expresión Corporal y Teatro", 3),
    COMUNICACION_VISUAL_I("Comunicación Visual I", 3),
    FISICA("Física", 3);
    
    private final String value;
    private final int horas;

    ProgramaQuintoDiversifArteYExpresion(String value, int horas) {
        this.value = value;
        this.horas = horas;
    }

    @Override
    public String getNombre() {
       return this.value;
    }

    @Override
    public int getHoras() {
        return this.horas;
    }
    @Override
    public String getProgramaAnio() {
        return this.getClass().getSimpleName();
    }
}
