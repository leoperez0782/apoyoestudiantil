/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @date 30-may-2019
 * @time 21:21:01
 * @author Leonardo Pérez
 */
//@Entity
public class Usuario_Domicilio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @ManyToOne
    private Usuario usuario;
    @Id
    @ManyToOne
    private Domicilio domicilio;
    
    public Usuario_Domicilio(){
        
    }

    public Usuario_Domicilio(Usuario usuario, Domicilio domicilio) {
        this.usuario = usuario;
        this.domicilio = domicilio;
    }
    
    
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private Long id;
//
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (id != null ? id.hashCode() : 0);
//        return hash;
//    }
//
//    @Override
//    public boolean equals(Object object) {
//        // TODO: Warning - this method won't work in the case the id fields are not set
//        if (!(object instanceof Usuario_Domicilio)) {
//            return false;
//        }
//        Usuario_Domicilio other = (Usuario_Domicilio) object;
//        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
//            return false;
//        }
//        return true;
//    }
//
//    @Override
//    public String toString() {
//        return "com.apoyoestudiantil.entity.Usuario_Domicilio[ id=" + id + " ]";
//    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public Domicilio getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(Domicilio domicilio) {
        this.domicilio = domicilio;
    }

    
}
