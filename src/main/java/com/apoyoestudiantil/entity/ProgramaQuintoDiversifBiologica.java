/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

/**
 *
 * @author leo
 */
public enum ProgramaQuintoDiversifBiologica implements ProgramaAsignaturas{
    FISICA("Física", 4),
    QUIMICA("Química", 4),
    BIOLOGIA("Biología", 5),
    COMUNICACION_VISUAL("Comunicación Visual", 3);
    
    private final String value;
    private final int horas;

    ProgramaQuintoDiversifBiologica(String value, int horas) {
        this.value = value;
        this.horas = horas;
    }
     @Override
    public String getNombre() {
        return this.value;
    }

    @Override
    public int getHoras() {
        return this.horas;
    }
    @Override
    public String getProgramaAnio() {
        return this.getClass().getSimpleName();
    }
}
