/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

/**
 *
 * @author Leonardo Pérez
 */
public enum PeriodoCuartoRef2006 implements PeriodoCes {
    MAR_ABR("Mar-Abr"),
    MAY("May"),
    JUN_JUL_AGO("Jun-Jul-Ago"),
    SET_NOV_DIC("Set-Nov-Dic");

    private final String value;

    private PeriodoCuartoRef2006(String value) {
        this.value = value;
    }

    @Override
    public String getNombre() {
        return this.value;
    }
}
