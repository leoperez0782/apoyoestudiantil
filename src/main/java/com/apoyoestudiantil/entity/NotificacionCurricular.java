/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * @date 08-ago-2019
 * @time 5:59:30
 * @author Leonardo Pérez
 */
@Entity
@Table(name = "NotificacionCurricular")
@Inheritance(strategy = InheritanceType.JOINED)
public class NotificacionCurricular extends Notificacion implements Serializable {

    private static final long serialVersionUID = 1L;
    
    @OneToOne
    private Alumno alumno;

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (getId() != null ? getId().hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof NotificacionCurricular)) {
            return false;
        }
        NotificacionCurricular other = (NotificacionCurricular) object;
        if ((this.getId() == null && other.getId() != null) || (this.getId() != null && !this.getId().equals(other.getId()))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.apoyoestudiantil.entity.NotificacionCurricular[ id=" + getId() + " ]";
    }

}
