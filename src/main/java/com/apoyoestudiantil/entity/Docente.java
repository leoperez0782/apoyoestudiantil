/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * @date 23-may-2019
 * @time 19:30:59
 * @author Leonardo Pérez
 */
@NamedQueries(
        {
            @NamedQuery(
                    name = "get_count_docente",
                    query = "select count(d) from Docente d"
            ),
            @NamedQuery(
                    name = "get_all_docente",
                    query = "select d from Docente d"
            ),
            @NamedQuery(
                    name = "find_docente_by_id",
                    query = "select d from Docente d where d.id = :id"
            ),
            @NamedQuery(
                    name = "get_docente_by_documento",
                    query = "select d from Docente d where d.documento = :doc"
            ),
             @NamedQuery(
                    name = "get_docente_by_numero_documento",
                    query = "select d from Docente d where d.documento.numero = :num"
            )
        }
)
@Entity
public class Docente extends Empleado implements Serializable {

    public static final String GET_COUNT_DOCENTE = "get_count_docente";
    public static final String GET_ALL_DOCENTE = "get_all_docente";
    public static final String FIND_DOCENTE_BY_ID = "find_docente_by_id";
    public static final String FIND_DOCENTE_BY_DOCUMENTO = "get_docente_by_documento";
    public static final String GET_DOCENTE_BY_NUMERO_DOC = "get_docente_by_numero_documento";
    
    

}
