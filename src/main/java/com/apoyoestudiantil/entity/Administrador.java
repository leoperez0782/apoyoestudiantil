/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 *
 * @author Polachek
 */
@NamedQueries({
    @NamedQuery(
            name = "get_All_Administrador",
            query = "select a from Administrador a"
    )
})

@Entity
public class Administrador extends Empleado implements Serializable {
    public Administrador() {
        super();
    }    
}
