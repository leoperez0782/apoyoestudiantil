/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

/**
 *
 * @author leo
 */
public enum ProgramaPrimeroSegundoCBReformulacion2006 implements ProgramaAsignaturas {
    IDIOMA_ESPANOL("Idioma español", 4),
    MATEMATICA("Matemática", 4),
    INGLES("Inglés", 4),
    HISTORIA("Historia", 3),
    GEOGRAFIA("Geografía", 3),
    BIOLOGIA("Biología", 3),
    CIENCIAS_FISICAS("Ciencias Físicas", 3),
    TALLER_DE_INFORMATICA("Taller de Informática", 4),
    EDUCACION_VISUAL_PLASTICA_Y_DIBUJO("Educación Visual, Plástica y Dibujo", 2),
    EDUCACIONSONORA("Educación Sonora", 2),
    ESPACIO_CURRICULAR_ABIERTO("Espacio Curricular Abierto", 2),
    ESPACIO_DE_ESTRATEGIAS_PEDAGOGICAS_INCLUSORAS("Espacio de Estratégias Pedagogicas Inclusoras", 2),
    EDUCACION_FISICA_Y_RECREACION("Educación Física  y Recreación", 3);

    private final String value;
    private final int horas;

    ProgramaPrimeroSegundoCBReformulacion2006(String value, int horas) {
        this.value = value;
        this.horas = horas;
    }

    @Override
    public String getNombre() {
        return this.value;
    }

    @Override
    public int getHoras() {
        return this.horas;
    }
    @Override
    public String getProgramaAnio() {
        return this.getClass().getSimpleName();
    }
}
