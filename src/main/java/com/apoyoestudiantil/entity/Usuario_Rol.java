/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @date 24-may-2019
 * @time 21:06:27
 * @author Leonardo Pérez
 */
@Entity(name = "Usuario_Rol")
public class Usuario_Rol implements Serializable {

    private static final long serialVersionUID = 1L;
//    @Id
//    @GeneratedValue(strategy = GenerationType.AUTO)
//    private Long id;
    @Id
    @ManyToOne
    private Usuario usuario;
    @Id
    @ManyToOne
    private Rol rol;

    public Usuario_Rol() {
    }

    public Usuario_Rol(Usuario usuario, Rol rol) {
        this.usuario = usuario;
        this.rol = rol;
    }

    public Rol getRol() {
        return rol;
    }
    
    
    
    
//    public Long getId() {
//        return id;
//    }
//
//    public void setId(Long id) {
//        this.id = id;
//    }
//
//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += (id != null ? id.hashCode() : 0);
//        return hash;
//    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario_Rol)) {
            return false;
        }
        Usuario_Rol other = (Usuario_Rol) object;
//        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
//            return false;
//        }
        if(this.rol.equals(other.rol) && this.usuario.equals(other.usuario));
        return true;
    }

//    @Override
//    public String toString() {
//        return "com.apoyoestudiantil.entity.Usuario_Rol[ id=" + id + " ]";
//    }

}
