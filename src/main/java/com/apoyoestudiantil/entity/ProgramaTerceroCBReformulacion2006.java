/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

/**
 *
 * @author leo
 */
public enum ProgramaTerceroCBReformulacion2006 implements ProgramaAsignaturas {
    IDIOMA_ESPANOL("Idioma español", 2),
    MATEMATICA("Matemática", 4),
    LITERATURA("Literatura", 4),
    INGLES("Inglés", 4),
    HISTORIA("Historia", 3),
    GEOGRAFIA("Geografía", 2),
    BIOLOGIA("Biología", 3),
    FISICA("Física", 3),
    QUIMICA("Química", 3),
    EDUCACION_VISUAL_PLASTICA_Y_DIBUJO("Educación Visual, Plástica y Dibujo", 2),
    EDUCACIONSONORA("Educación Sonora", 2),
    ESPACIO_DE_ESTRATEGIAS_PEDAGOGICAS_INCLUSORAS("Espacio de Estratégias Pedagogicas Inclusoras", 1),
    EDUCACION_FISICA_Y_RECREACION("Educación Física  y Recreación", 3),
    EDUCACION_SOCIAL_Y_CIVICA("Educación Social y Cívica", 3);

    private final String value;
    private final int horas;

    ProgramaTerceroCBReformulacion2006(String value, int horas) {
        this.value = value;
        this.horas = horas;
    }

    @Override
    public String getNombre() {
        return this.value;
    }

    @Override
    public int getHoras() {
        return this.horas;
    }
    @Override
    public String getProgramaAnio() {
        return this.getClass().getSimpleName();
    }
}
