/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @date 17-may-2019
 * @time 8:07:35
 * @author Leonardo Pérez
 */
@NamedQueries(
    {
        @NamedQuery(
            name = "get_all_by_nivel",
            query = "select a from Alumno a where a.nivel = :nivel and a.activo = true"
        ),
        @NamedQuery(
                name = "get_count_alumno",
                query = "select count(a) from Alumno a where a.activo = true"
        ),
        @NamedQuery(
                name = "get_alumno_by_numero_documento",
                query = "select d from Alumno d where d.documento.numero = :num"
        ),
        @NamedQuery(
                name = "get_alumnos_by_tutor",
                query = "select a from Alumno a where :tutor MEMBER OF a.tutores and a.activo = true"
        )
    }
        
)
@Entity(name = "Alumno")
@Table(name = "Alumnos")
public class Alumno extends Usuario implements Serializable {
    public static final String GET_ALUMNO_BY_NUMERO_DOC = "get_alumno_by_numero_documento";
    public static final String GET_ALUMNOS_BY_TUTOR = "get_alumnos_by_tutor";

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<AdultoResponsable> tutores = new ArrayList<>();
    private int matricula;

    @ManyToOne
    private Nivel nivel;
    @OneToMany(mappedBy = "alumno", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Inasistencia> inasistencias = new ArrayList<>();


    public Alumno() {
        super();
    }
    
    public List<AdultoResponsable> getTutores() {
        return tutores;
    }


    public void addPadre(AdultoResponsable p) {

          this.tutores.add(p);

    }
    
    public void addInasistencia(Inasistencia in){
        this.inasistencias.add(in);
        in.setAlumno(this);
    }

    public int getMatricula() {
        return matricula;
    }

    public void setMatricula(int matricula) {
        this.matricula = matricula;
    }

    public Nivel getNivel() {
        return nivel;
    }

    public void setNivel(Nivel nivel) {
        this.nivel = nivel;
    }

    public List<Inasistencia> getInasistencias() {
        return inasistencias;
    }

    public void setInasistencias(List<Inasistencia> inasistencias) {
        this.inasistencias.clear();
        this.inasistencias.addAll(inasistencias);
    }

    public int getFaltasFictas(Asignatura materia) {
        int retorno =(int) inasistencias.stream()
                .filter( i -> i.getLista().getLibreta().getMateria().equals(materia))
                .count();
        return retorno;
    }

    public int getFaltasJustificadas(Asignatura materia){
        int retorno = (int) inasistencias.stream()
                .filter( i -> i.getLista().getLibreta().getMateria().equals(materia) && i.getTipo().equals(Inasistencia.TipoInasistencia.JUSTIFICADA))
                .count();
        return retorno;
    }
    
    public int getFaltasInjustificadas(Asignatura materia){
        int retorno = (int) inasistencias.stream()
                .filter( i -> i.getLista().getLibreta().getMateria().equals(materia) && !(i.getTipo().equals(Inasistencia.TipoInasistencia.JUSTIFICADA)))
                .count();
        return retorno;
    }
}
