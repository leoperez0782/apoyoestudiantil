/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

/**
 *
 * @author leo
 */
public enum ProgramaQuintoDiversifHumanistica implements ProgramaAsignaturas{

    GEOGRAFIA_HUMANA_Y_ECONOMICA("Geografía Humana y Económica", 4),
    HISTORIA("Historia", 6),
    SOCIOLOGIA("Sociología", 4),
    BIOLOGIA("Biología", 2);
    private final String value;

    private final int horas;

    ProgramaQuintoDiversifHumanistica(String value, int horas) {
        this.value = value;
        this.horas = horas;
    }
    
     @Override
    public String getNombre() {
        return this.value;
    }

    @Override
    public int getHoras() {
        return this.horas;
    }
    @Override
    public String getProgramaAnio() {
        return this.getClass().getSimpleName();
    }
}
