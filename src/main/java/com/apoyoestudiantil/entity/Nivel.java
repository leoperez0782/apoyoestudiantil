/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;
import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.NaturalId;

/**
 * @date 25-may-2019
 * @time 11:44:06
 * @author Leonardo Pérez
 */
@NamedQueries({
        @NamedQuery(name = "get_nivel_by_clase", query = "select n from Nivel n where n.clase = :clase"),
    @NamedQuery(name = "get_all_niveles", query = "select n from Nivel n")
})
@Entity
public class Nivel implements Serializable {

    public enum Estado {ACTIVO, NO_ACTIVO,SIN_ASIGNAR}
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @Embedded
    @NaturalId
    private Clase clase;
    @Temporal(TemporalType.DATE)
    private Calendar fechaCreacion;
    @Enumerated(EnumType.STRING)
    private Estado estado;
    
    @OneToMany(mappedBy = "nivel", cascade = CascadeType.MERGE, orphanRemoval = true)
    private List<Alumno> alumnos = new ArrayList<>();
    public Long getId() {
        return id;
    }
    @OneToMany(mappedBy = "nivel", cascade = CascadeType.ALL)
    @Fetch(FetchMode.SUBSELECT)
    private List<Grupo> grupos = new ArrayList<>();
    
    public void addGrupo(Grupo g){
        grupos.add(g);
        g.setNivel(this);
    }
    
    
    public void setId(Long id) {
        this.id = id;
    }

    public Clase getClase() {
        return clase;
    }

    public void setClase(Clase clase) {
        this.clase = clase;
    }

    public Calendar getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Calendar fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Estado getEstado() {
        return estado;
    }

    public void setEstado(Estado estado) {
        this.estado = estado;
    }
    
    public void addAlumno(Alumno a){
        this.alumnos.add(a);
        a.setNivel(this);
    }

    public List<Alumno> getAlumnos() {
        return alumnos;
    }

    public void setAlumnos(List<Alumno> alumnos) {
        this.alumnos = alumnos;
    }

    public List<Grupo> getGrupos() {
        //Para evitar problema de que carga dos veces la misma entidad.
        return grupos.stream().distinct().collect(Collectors.toList());
    }

    public void setGrupos(List<Grupo> grupos) {
        //this.grupos = grupos;
        this.grupos.clear();
        this.grupos.addAll(grupos);
    }
    
    public Grupo getGrupo(long grSel) {
        Grupo retorno = null;
        for (Grupo g : grupos) {
            if (g.getId() == grSel) {
                retorno = g;
            }
        }
        return retorno;
    }
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Nivel)) {
            return false;
        }
        Nivel other = (Nivel) object;

        return (this.clase.equals(other.clase));
    }

    @Override
    public String toString() {
        return "com.apoyoestudiantil.entity.Nivel[ id=" + id + " ]";
    }

}
