/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

import Utilidades.UtilSeguridad;
import com.apoyoestudiantil.excepciones.AppException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import javax.validation.constraints.Size;

import org.hibernate.annotations.NaturalId;

/**
 * @date 15-may-2019
 * @time 22:11:19
 * @author Leonardo Pérez
 */
@NamedQueries({
    @NamedQuery(
            name = "get_Usuario_by_documento",
            query = "select u from Usuario u where u.documento = :documento"
    ),
    @NamedQuery(
        name = "get_Usuario_by_email",
        query = "select u from Usuario u where u.email = :email"
    ),
    @NamedQuery(
            name = "get_Usuario_by_numero_documento",
            query = "select u from Usuario u where u.documento.numero = :num"
    )
})
@Entity
@Table(name = "Usuarios")
@Inheritance(strategy = InheritanceType.JOINED)
public abstract class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Embedded
    @NaturalId
    private Documento documento;
    @Size(min = 2, max = 50)
    private String primerNombre;
    @Size(max = 50)
    private String segundoNombre;
    @Size(min = 2, max = 50)
    private String primerApellido;
    @Size(min = 2, max = 50)
    private String segundoApellido;
    @NotNull
    private String clave;
    
    @Column(unique = true)
    @Pattern(regexp = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\\.[a-zA-Z0-9-]+)*$", message = "ingrese una direccion de email valida")
    private String email;

    private String fechaNacimiento;
    private boolean activo;
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Domicilio> domicilios = new ArrayList<>();
    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    private List<Telefono> telefonos = new ArrayList<>();
    @OneToMany(
            mappedBy = "usuario",
            cascade = CascadeType.ALL,
            orphanRemoval = true
    )
    private List<Usuario_Rol> roles = new ArrayList<>();
    @OneToMany(mappedBy = "remitente", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Notificacion> notificacionesEnviadas = new ArrayList<>();

    @Transient
    private List<Notificacion> notificacionesRecibidas = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Documento getDocumento() {
        return documento;
    }

    public void setDocumento(Documento documento) {
        this.documento = documento;
    }

    public String getPrimerNombre() {
        return primerNombre;
    }

    public void setPrimerNombre(String primerNombre) {
        this.primerNombre = primerNombre;
    }

    public String getSegundoNombre() {
        return segundoNombre;
    }

    public void setSegundoNombre(String segundoNombre) {
        this.segundoNombre = segundoNombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        Utilidades.UtilSeguridad convertidor = new UtilSeguridad();

        try {
            this.clave = convertidor.crearClaveConSalt(clave);
        } catch (AppException ex) {
            Logger.getLogger(Usuario.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isActivo() {
        return activo;
    }

    public void setActivo(boolean activo) {
        this.activo = activo;
    }

    

    public void addDomicilio(Domicilio d) {
        this.domicilios.add(d);
//        Usuario_Domicilio ud = new Usuario_Domicilio(this, d);
//        this.domicilios.add(ud);
//        d.getUsuarios().add(ud);
//        this.domicilio =d;
//        d.setUsuario(this);
    }

    public void addRol(Rol r) {
        Usuario_Rol ur = new Usuario_Rol(this, r);
        roles.add(ur);
        r.getUsuarios().add(ur);
    }

    public void addNotificacionEnviada(Notificacion n) {
        this.notificacionesEnviadas.add(n);
        n.setRemitente(this);
    }

    public void addTelefono(Telefono t) {
        this.telefonos.add(t);
    }
    public void removeTel(Long idTel) {
        Telefono borrado = null;
       for(Telefono t : telefonos){
           if(t.getId() == idTel){
               borrado = t;
               break;
           }
       }
       telefonos.remove(borrado);
    }

    public List<Usuario_Rol> getRoles() {
        return roles;
    }
    
    public String getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(String fechaNacimiento) {    
        this.fechaNacimiento = fechaNacimiento;
    }

    public List<Telefono> getTelefonos() {
        return telefonos;
    }

    public void setTelefonos(List<Telefono> telefonos) {
        this.telefonos.clear();
        this.telefonos.addAll(telefonos);
    }

    public List<Domicilio> getDomicilios() {
        return domicilios;
    }

    public void setDomicilios(List<Domicilio> domicilios) {
        this.domicilios.clear();
        this.domicilios.addAll(domicilios);
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.apoyoestudiantil.entity.Persona[ id=" + id + " ]";
    }

}
