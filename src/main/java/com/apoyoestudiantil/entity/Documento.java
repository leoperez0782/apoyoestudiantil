/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * @date 23-may-2019
 * @time 19:43:25
 * @author Leonardo Pérez
 */
@Embeddable
public class Documento implements Serializable {
    
    public enum TipoDocumento{CEDULA, PASAPORTE, OTRO}
    
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_documento")
    private TipoDocumento tipo;
    @Column(name = "numero_documento")
    private long numero;
    @Column(name = "pais_emisor")
    private String paisEmisor;

    public TipoDocumento getTipo() {
        return tipo;
    }

    public void setTipo(TipoDocumento tipo) {
        this.tipo = tipo;
    }

    public Long getNumero() {
        return numero;
    }

    public void setNumero(Long numero) {
        this.numero = numero;
    }

    public String getPaisEmisor() {
        return paisEmisor;
    }

    public void setPaisEmisor(String paisEmisor) {
        this.paisEmisor = paisEmisor;
    }
    
    
    @Override
    public String toString() {
        return this.tipo + " " + this.numero + " " + this.paisEmisor;
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof Documento)) {
            return false;
        }
        Documento other = (Documento) obj;
        return this.toString().equals(other.toString());
    }

    @Override
    public int hashCode() {
        return super.hashCode(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
    public TipoDocumento devolverEnumTipoSegunString(String tipo){
        TipoDocumento tipoDoc;
        switch(tipo){
            case "CEDULA":
            case "cedula":
                tipoDoc = TipoDocumento.CEDULA;
                break;
            case "PASAPORTE":
            case "pasaporte":
                tipoDoc = TipoDocumento.PASAPORTE;
                break;
            default:
                tipoDoc = TipoDocumento.OTRO;
                break;
        }
        return tipoDoc;
    }
}
