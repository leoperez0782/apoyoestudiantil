/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

/**
 * @date 12-ago-2019
 * @time 6:02:40
 * @author Leonardo Pérez
 */
@NamedQueries({
    @NamedQuery(name = "get_all_items_by_reunion", query = "select i from ItemReunion i where i.reunion = :reunion")
})
@Entity
public class ItemReunion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToOne
    private Alumno alumno;
    private int cantidadFaltasJustificadas;
    private int cantidadFaltasSinJustificar;
    private int cantidadFaltasFictas;
    private String juicioDocente;
    private String juicioReunion;
    private int conducta;
    private int calificacion;
    private boolean cerrada;
    @ManyToOne
    private Reunion reunion;
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }


    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public boolean isCerrada() {
        return cerrada;
    }

    public void setCerrada(boolean cerrada) {
        this.cerrada = cerrada;
    }

    public Reunion getReunion() {
        return reunion;
    }

    public void setReunion(Reunion reunion) {
        this.reunion = reunion;
    }
    

    public int getCantidadFaltasJustificadas() {
        return cantidadFaltasJustificadas;
    }

    public void setCantidadFaltasJustificadas(int cantidadFaltasJustificadas) {
        this.cantidadFaltasJustificadas = cantidadFaltasJustificadas;
    }

    public int getCantidadFaltasSinJustificar() {
        return cantidadFaltasSinJustificar;
    }

    public void setCantidadFaltasSinJustificar(int cantidadFaltasSinJustificar) {
        this.cantidadFaltasSinJustificar = cantidadFaltasSinJustificar;
    }

    public int getCantidadFaltasFictas() {
        return cantidadFaltasFictas;
    }

    public void setCantidadFaltasFictas(int cantidadFaltasFictas) {
        this.cantidadFaltasFictas = cantidadFaltasFictas;
    }

    public String getJuicioDocente() {
        return juicioDocente;
    }

    public void setJuicioDocente(String juicioDocente) {
        this.juicioDocente = juicioDocente;
    }

    public String getJuicioReunion() {
        return juicioReunion;
    }

    public void setJuicioReunion(String juicioReunion) {
        this.juicioReunion = juicioReunion;
    }

    public int getConducta() {
        return conducta;
    }

    public void setConducta(int conducta) {
        this.conducta = conducta;
    }

    public int getCalificacion() {
        return calificacion;
    }

    public void setCalificacion(int calificacion) {
        this.calificacion = calificacion;
    }

    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ItemReunion)) {
            return false;
        }
        ItemReunion other = (ItemReunion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.apoyoestudiantil.entity.ItemReunion[ id=" + id + " ]";
    }

}
