/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author leo
 */
@NamedQueries({
    @NamedQuery( name = "get_auxiliar_by_alumno", query = "select a from AuxiliarMatricula a where a.alumno = :alumno")
})
@Entity
public class AuxiliarMatricula implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @OneToOne
    private Alumno alumno;
    @OneToMany(mappedBy = "matricula", cascade = CascadeType.ALL, orphanRemoval = true )
    private List<ItemAuxiliarMatricula> items = new ArrayList<>();
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Alumno getAlumno() {
        return alumno;
    }

    public void setAlumno(Alumno alumno) {
        this.alumno = alumno;
    }

    public List<ItemAuxiliarMatricula> getItems() {
        return items;
    }

    public void setItems(List<ItemAuxiliarMatricula> items) {
        this.items.clear();
        this.items.addAll(items);
    }
    
    public void addItem(ItemAuxiliarMatricula item){
        this.items.add(item);
        item.setMatricula(this);
        
    }
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof AuxiliarMatricula)) {
            return false;
        }
        AuxiliarMatricula other = (AuxiliarMatricula) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.apoyoestudiantil.entity.AuxiliarMatricula[ id=" + id + " ]";
    }
    
}
