/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

/**
 *
 * @author leo
 */
public enum ProgramaQuintoNucleoComunRef2006 implements ProgramaAsignaturas{
    LITERATURA("Literatura", 4),
    MATEMATICA("Matemática", 5),
    INGLES("Inglés", 3),
    FILOSOFIA("Filosofía", 3),
    EDUCACION_CIUDADANA("Educación Ciudadana", 3);

    private final String value;

    private final int horas;

    ProgramaQuintoNucleoComunRef2006(String value, int horas) {
        this.value = value;
        this.horas = horas;
    }
     @Override
    public String getNombre() {
        return this.value;
    }

    @Override
    public int getHoras() {
        return this.horas;
    }
    @Override
    public String getProgramaAnio() {
        return this.getClass().getSimpleName();
    }
}
