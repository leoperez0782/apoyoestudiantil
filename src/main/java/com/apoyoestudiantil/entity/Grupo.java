/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

import com.apoyoestudiantil.entity.Libreta.Turno;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import static java.util.stream.Collectors.toList;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

/**
 * @date 26-may-2019
 * @time 21:38:41
 * @author Leonardo Pérez
 */
@NamedQueries(
        @NamedQuery(name = "get_grupo_by_anio_y_nivel", query = "select g from Grupo g where g.nivel = :nivel and g.anio = :anio")
)
@Entity
@Table(
        name = "grupo",
        uniqueConstraints = @UniqueConstraint(
                name = "uk_grupo_nivel",
                columnNames = {
                    "anio",
                    "nivel_id"
                }
        )
)
public class Grupo implements Serializable {


    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @JoinColumn(
            name = "nivel_id",
            foreignKey = @ForeignKey(name = "fk_grupo_nivel_id")
    )
    private Nivel nivel;
    @Column(name = "anio")
    private int anio;
    private Turno turno;
    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Alumno> alumnos = new ArrayList<>();

    public void addAlumno(Alumno a) {
        alumnos.add(a);
        a.setNivel(this.nivel);
        
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public Nivel getNivel() {
        return nivel;
    }

    public void setNivel(Nivel nivel) {
        this.nivel = nivel;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public List<Alumno> getAlumnos() {
        //Para solucionar problema con recuperar solo alumnos activos.
        return alumnos.stream().filter(a -> a.isActivo()).collect(toList());
    }

    public void setAlumnos(List<Alumno> alumnos) {
        this.alumnos = alumnos;
    }

    public Alumno getAlumno(long idA) {
        Alumno retorno = null;
        for (Alumno a : alumnos) {
            if (a.getId() == idA) {
                retorno = a;
            }
        }
        return retorno;
    }

    public Alumno getAlumno(int indice) {
        //Para solucionar problema con recuperar solo alumnos activos.
        List<Alumno> alumnosActivos = this.alumnos.stream()
                .filter(a -> a.isActivo()).collect(toList());
        return alumnosActivos.get(indice);
    }


    public Turno getTurno() {
        return turno;
    }

    public void setTurno(Turno turno) {
        this.turno = turno;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Grupo)) {
            return false;
        }
        Grupo other = (Grupo) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.apoyoestudiantil.entity.Grupo[ id=" + id + " ]";
    }

}
