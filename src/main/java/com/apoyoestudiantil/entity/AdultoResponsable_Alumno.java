/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.entity;

import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * @date 26-may-2019
 * @time 16:50:14
 * @author Leonardo Pérez
 */
//@Entity
public class AdultoResponsable_Alumno implements Serializable {

    private static final long serialVersionUID = 1L;


    @Id
    @ManyToOne
    private  Alumno hijo;
    @Id
    @ManyToOne
    private AdultoResponsable padre;


    AdultoResponsable_Alumno(Alumno alumno, AdultoResponsable p) {
        this.hijo = alumno;
        this.padre = p;
    }

}
