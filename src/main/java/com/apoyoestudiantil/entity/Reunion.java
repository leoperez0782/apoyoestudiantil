/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author leo
 */
@NamedQueries({
    @NamedQuery(name = "get_all_reuniones_by_libreta", query = "select r from Reunion r where r.libreta = :libreta"),
    @NamedQuery(
            name = "get_count_cerradas_por_periodo_y_grupo",
            query = "select count(r) from Reunion r where r.periodoEnLibreta = :periodo and r.cerrada = true and r.libreta.grupo = :grupo"
    ),
    @NamedQuery(
            name = "get_count_por_periodo_y_grupo",
            query = "select count(r) from Reunion r where r.periodoEnLibreta = :periodo and r.libreta.grupo = :grupo"
    ),
    @NamedQuery(
            name = "get_all_by_grupo_y_periodo",
            query = "select r from Reunion r where r.periodoEnLibreta = :periodo and r.cerrada = true and r.libreta.grupo = :grupo"
    ),
    @NamedQuery(
            name = "get_no_cerradas_by_grupo",
            query = "select r from Reunion r where r.cerrada = false and r.libreta.grupo = :grupo"
    )
})
@Entity
public class Reunion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nombre;
    private String periodoEnLibreta;
    @ManyToOne
    private Libreta libreta;
    @OneToMany(mappedBy = "reunion", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ItemReunion> items = new ArrayList<>();
    private boolean cerrada;
    private boolean notificada;//Para setear luego de enviadas las notificaciones.

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void asignarNombreSegunNombrePeriodo(String programa, String periodo) {
        switch (programa) {
            case "ProgramaPrimeroSegundoCBReformulacion2006":
            case "ProgramaTerceroCBReformulacion2006":
                asignarNombreProgramaCB(periodo);
                break;
            case "ProgramaCuartoRef2006":
                asignarNombreProgramaCuartoRef2006(periodo);
                break;
            case "ProgramaQuintoDiversifArteYExpresion":
            case "ProgramaQuintoDiversifBiologica":
            case "ProgramaQuintoDiversifCientifica":
            case "ProgramaQuintoDiversifHumanistica":
            case "ProgramaQuintoNucleoComunRef2006":
                asignarNombreProgramasQuitoYSexto(periodo);
                break;
            default:
                break;
        }
    }

    public String getPeriodoEnLibreta() {
        return periodoEnLibreta;
    }

    public void setPeriodoEnLibreta(String periodoEnLibreta) {
        this.periodoEnLibreta = periodoEnLibreta;
    }

    public Libreta getLibreta() {
        return libreta;
    }

    public void setLibreta(Libreta libreta) {
        this.libreta = libreta;
    }

    public List<ItemReunion> getItems() {
        return items;
    }

    public void setItems(List<ItemReunion> items) {
        this.items.clear();
        this.items.addAll(items);
    }

    public void addItem(ItemReunion item) {
        this.items.add(item);
        item.setReunion(this);
    }

    public boolean isCerrada() {
        return cerrada;
    }

    public void setCerrada(boolean cerrada) {
        this.cerrada = cerrada;
    }

    public boolean isNotificada() {
        return notificada;
    }

    public void setNotificada(boolean notificada) {
        this.notificada = notificada;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Reunion)) {
            return false;
        }
        Reunion other = (Reunion) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.apoyoestudiantil.entity.Reunion[ id=" + id + " ]";
    }

    private void asignarNombreProgramaCB(String periodo) {
        switch (periodo) {
            case "Mar-Abr":
                this.nombre = "Primer Bimestre";
                break;
            case "May":
                this.nombre = "Primera Reunión";
                break;
            case "Jun-Jul":
                this.nombre = "Segundo Bimestre";
                break;
            case "Ago":
                this.nombre = "Segunda Reunión";
                break;
            case "Nov-Dic":
                this.nombre = "Reunión Final";
                break;
            default:
                this.nombre = "Reunión";
                break;
        }
    }

    private void asignarNombreProgramaCuartoRef2006(String periodo) {
        switch (periodo) {
            case "Mar-Abr":
                this.nombre = "Primer Bimestre";
                break;
            case "May":
                this.nombre = "Primera Reunión";
                break;
            case "Jun-Jul-Ago":
                this.nombre = "Segunda Reunión";
                break;
            case "Set-Oct-Nov":
                this.nombre = "Reunión Final";
                break;
            default:
                this.nombre = "Reunión";
                break;
        }
    }

    private void asignarNombreProgramasQuitoYSexto(String periodo) {
        switch (periodo) {
            case "Mar-Abr":
                this.nombre = "Primer Bimestre";
                break;
            case "May-Jun-Jul":
                this.nombre = "Primera Reunión";
                break;
            case "Ago-Set":
                this.nombre = "Segundo Bimestre";
                break;
            case "Oct-Nov":
                this.nombre = "Reunión Final";
                break;
            default:
                this.nombre = "Reunión";
                break;
        }
    }

    /**
     * Crea un item y lo deja cerrado.
     *
     * @param alumno
     * @param comentario
     * @param calificacion
     */
    void crearItem(Alumno alumno, String comentario, int calificacion, int conducta, Asignatura materia) {
        ItemReunion item = new ItemReunion();
        item.setAlumno(alumno);
        
        item.setCalificacion(calificacion);
        item.setJuicioDocente(comentario);
        item.setConducta(conducta);
        int cantidadFaltasFictas = alumno.getFaltasFictas(materia);
        int cantidadFaltasJustificadas = alumno.getFaltasJustificadas(materia);
        item.setCantidadFaltasFictas(cantidadFaltasFictas);
        item.setCantidadFaltasJustificadas(cantidadFaltasJustificadas);
        item.setCantidadFaltasSinJustificar(cantidadFaltasFictas - cantidadFaltasJustificadas);
        item.setCerrada(true);
        this.addItem(item);
    }

    void crearItem(Alumno alumno, String juicioReunion, String comentario, int calificacion, int conducta, Asignatura materia) {
        ItemReunion item = new ItemReunion();
        item.setAlumno(alumno);
        
        item.setCalificacion(calificacion);
        item.setJuicioDocente(comentario);
        item.setJuicioReunion(juicioReunion);
        item.setConducta(conducta);
        int cantidadFaltasFictas = alumno.getFaltasFictas(materia);
        int cantidadFaltasJustificadas = alumno.getFaltasJustificadas(materia);
        item.setCantidadFaltasFictas(cantidadFaltasFictas);
        item.setCantidadFaltasJustificadas(cantidadFaltasJustificadas);
        item.setCantidadFaltasSinJustificar(cantidadFaltasFictas - cantidadFaltasJustificadas);
        item.setCerrada(true);
        this.addItem(item);
    }

    /**
     * Busca el item correspondiente al alumno pasado por parametro y actualiza
     * los valores. Si no encuentra el item llama al metodo crearItem.
     *
     * @param alumno
     * @param comentario
     * @param calificacion
     * @param conducta
     */
    void actualizarItem(Alumno alumno, String comentario, int calificacion, int conducta, Asignatura materia) {
        Optional<ItemReunion> itemOp = items.stream()
                .filter(i -> i.getAlumno().equals(alumno))
                .findFirst();
        if (itemOp.isPresent()) {
            ItemReunion item = itemOp.get();
            item.setCalificacion(calificacion);
            item.setConducta(conducta);
            item.setJuicioDocente(comentario);
            int cantidadFaltasFictas = alumno.getFaltasFictas(materia);
            int cantidadFaltasJustificadas = alumno.getFaltasJustificadas(materia);
            item.setCantidadFaltasFictas(cantidadFaltasFictas);
            item.setCantidadFaltasJustificadas(cantidadFaltasJustificadas);
            item.setCantidadFaltasSinJustificar(cantidadFaltasFictas - cantidadFaltasJustificadas);
            item.setCerrada(true);
        } else {
            crearItem(alumno, comentario, calificacion, conducta, materia);
        }
    }

    /**
     * Revisa si todos los items estan cerrados. Si es asi, cierra la reunion.
     */
    public void verificarCierre() {
        int todosCerrados = (int) items.stream()
                .filter(i -> i.isCerrada())
                .count();
        this.cerrada = todosCerrados == items.size();
    }

    public Optional<ItemReunion> getItemAlumno(Alumno a) {
        Optional<ItemReunion> retorno = this.items.stream()
                .filter(i -> i.getAlumno().equals(a))
                .findFirst();
        return retorno;
    }

}
