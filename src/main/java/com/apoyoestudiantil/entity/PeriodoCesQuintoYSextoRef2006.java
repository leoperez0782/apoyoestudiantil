/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

/**
 *
 * @author Leonardo Pérez
 */
public enum PeriodoCesQuintoYSextoRef2006 implements PeriodoCes{

    
    MAR_ABR("Mar-Abr"),
    MAY_JUN_JUL("May-Jun-Jul"),
    AGO_SET("Ago-Set"),
    OCT_NOV("Oct-Nov");

    private final String value;

    private PeriodoCesQuintoYSextoRef2006(String value) {
        this.value = value;
    }
    
    @Override
    public String getNombre() {
        return this.value;
    }
    
}
