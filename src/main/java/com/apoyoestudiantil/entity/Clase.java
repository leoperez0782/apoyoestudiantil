/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

/**
 * @date 25-may-2019
 * @time 12:06:39
 * @author Leonardo Pérez
 */
@Embeddable
public class Clase implements Serializable {

    public enum TipoClase {
        COMUN, ESPECIAL
    }
   
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_de_clase")
    private TipoClase tipoDeClase;
    @Column(name = "nivel")
    private int nivel;
    @Column(name = "codigo_de_clase")
    private char codigoClase;

    public Clase() {
    }

    public Clase(TipoClase tipoDeClase, int nivel, char codigoClase) {
        this.tipoDeClase = tipoDeClase;
        this.nivel = nivel;
        this.codigoClase = codigoClase;
    }

    
//    @Override
//    public int hashCode() {
//        int hash = 0;
//        hash += ( != null ? id.hashCode() : 0);
//        return hash;
//    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Clase)) {
            return false;
        }
        Clase other = (Clase) object;
        return (this.codigoClase == other.codigoClase && other.tipoDeClase.equals(this.tipoDeClase) && this.nivel == other.nivel);
    }

//    @Override
//    public String toString() {
//        return "com.apoyoestudiantil.entity.Clase[ id=" + id + " ]";
//    }

    public TipoClase getTipoDeClase() {
        return tipoDeClase;
    }

    public void setTipoDeClase(TipoClase tipoDeClase) {
        this.tipoDeClase = tipoDeClase;
    }

    public int getNivel() {
        return nivel;
    }

    public void setNivel(int nivel) {
        this.nivel = nivel;
    }

    public char getCodigoClase() {
        return codigoClase;
    }

    public void setCodigoClase(char codigoClase) {
        this.codigoClase = codigoClase;
    }
}
