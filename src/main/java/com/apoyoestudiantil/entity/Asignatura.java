/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.entity;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

/**
 * @date 05-jun-2019
 * @time 22:51:41
 * @author Leonardo Pérez
 */
@NamedQueries(
        {
            @NamedQuery( 
                    name = "get_asignatura_by_nombre",
                    query = "select a from Asignatura a where a.nombre = :nombre"
            ),
            @NamedQuery( 
                    name = "get_count_all_asignaturas",
                    query = "select count(a) from Asignatura a"
            )
        }
)
@Entity
public class Asignatura implements Serializable {
    
    public static final String GET_ASIGNATURA_BY_NOMBRE = "get_asignatura_by_nombre";
    public static final String GET_COUNT_ALL_ASIGNATURAS = "get_count_all_asignaturas";
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private String nombre;
    private int horasSemanales;
    
    private String programaAnio;
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getHorasSemanales() {
        return horasSemanales;
    }

    public void setHorasSemanales(int horasSemanales) {
        this.horasSemanales = horasSemanales;
    }

    public String getProgramaAnio() {
        return programaAnio;
    }

    public void setProgramaAnio(String programaAnio) {
        this.programaAnio = programaAnio;
    }
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Asignatura)) {
            return false;
        }
        Asignatura other = (Asignatura) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.apoyoestudiantil.entity.Asignatura[ id=" + id + " ]";
    }

}
