/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

/**
 * @date 19-may-2019
 * @time 16:37:02
 * @author Leonardo Pérez
 */
@NamedQueries(
        @NamedQuery(name = "get_rol_by_rol", query = "select r from Rol r where r.rol = :rol")
)
@Entity(name="Rol")
@Table(name = "Roles")
public class Rol implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private String rol;
    

    @OneToMany(
		mappedBy = "rol",
		cascade = CascadeType.ALL,
		orphanRemoval = true
	)
    private List<Usuario_Rol> usuarios = new ArrayList<Usuario_Rol>();
    
   
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getRol() {
        return rol;
    }

    public void setRol(String rol) {
        this.rol = rol;
    }

    public List<Usuario_Rol> getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(List<Usuario_Rol> usuarios) {
        this.usuarios = usuarios;
    }

    

    public void removeUsuario(Usuario u){
        this.usuarios.remove(u);
       
    }
    

    
    
    
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Rol)) {
            return false;
        }
        Rol other = (Rol) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.apoyoestudiantil.entity.Rol[ id=" + id + " ]";
    }

}
