/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.interactores;

/**
 * @date 12-jun-2019
 * @time 20:44:26
 * @author Leonardo Pérez
 * Factoria de cargadores de excel.
 */
public class FactoriaCargadoresArchivo {

    public enum Tipo {
        ADMINISTRADOR, DOCENTE, ESTUDIANTE, ADULTO_RESPONSABLE, ADMINISTRATIVO
    }

    /**
     * Metodo que devuelve la clase para carga de archivos que corresponde 
     * a la entidad que se pasa como tipo.
     * @param tipo enum que describe la clase de la entidad a cargar.
     * @return 
     */
    public CargadorAbstractoExcel crearCargador(Tipo tipo) {
        switch(tipo){
            case ESTUDIANTE:
                return new CargadorAlumnosExcel();
            case DOCENTE:
                return new CargadorDocentesExcel();
            case ADMINISTRADOR:
                return new CargadorAdministradoresExcel();
            case ADMINISTRATIVO:
                return new CargadorAdministrativosExcel();
            default:
                return null;
        }
    }
}
