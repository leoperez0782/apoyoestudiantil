/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.interactores;

import com.apoyoestudiantil.entity.Notificacion;
import com.sun.mail.smtp.SMTPTransport;
import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.AddressException;

/**
 * @date 09-ago-2019
 * @time 2:13:51
 * @author Leonardo Pérez
 */
public class EnviadorEmail {

    private static final String SMTP_SERVER = "smtp.gmail.com";
    private static final String USERNAME = "correctoresap@gmail.com";
    private static final String PASSWORD = "2019correctoresproyectoap";

    private static final String EMAIL_FROM = "correctoresap@gmail.com";
    private static final String EMAIL_TO = "guillermollana@gmail.com";
    private static final String EMAIL_SUBJECT = "Prueba email Apoyo";
    private static final String EMAIL_TEXT = "Hola desde Apoyo estudiantil";
    private String emailTo;
    private String emailToCc = "";

    private String emailSubject;
    private String emailText;

    public EnviadorEmail() {

    }

    public void eviarMensaje(Notificacion notif) {
        
        
        Properties prop = System.getProperties();
        prop.put("mail.smtp.host", SMTP_SERVER); //optional, defined in SMTPTransport
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.port", "465"); // default google smtp

        Session session;
        Message msg ; 
        
         try {
		session = Session.getInstance(prop, null);
                msg = new MimeMessage(session);
			// from
            msg.setFrom(new InternetAddress(EMAIL_FROM));

			// to 
            msg.setRecipients(Message.RecipientType.TO,
                    cargarEmailTo(notif));

			// cc
            msg.setRecipients(Message.RecipientType.CC,
                    InternetAddress.parse(emailToCc, false));

			// subject
            msg.setSubject(notif.getAsunto());
			
			// content 
            msg.setText(notif.getCuerpoMensaje());
			
            msg.setSentDate(new Date());

			// Get SMTPTransport
            //Transport.send(msg, USERNAME, PASSWORD);
            SMTPTransport t = (SMTPTransport) session.getTransport("smtp");
			
			// connect
            t.connect(SMTP_SERVER, USERNAME, PASSWORD);
			
			// send
            t.sendMessage(msg, msg.getAllRecipients());

            System.out.println("Response: " + t.getLastServerResponse());

            t.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private InternetAddress[] cargarEmailTo(Notificacion notif) throws AddressException {
        InternetAddress[] addressTo = new InternetAddress[notif.getDestinatarios().size()];
        
        int tope = notif.getDestinatarios().size();
        for(int i=0; i< tope; i++){
            addressTo[i] = new InternetAddress(notif.getDestinatarios().get(i).getEmail());
        }
        return addressTo;
             
    }
    
    public void eviarMensaje() {       
        Properties prop = System.getProperties();
        prop.put("mail.smtp.host", SMTP_SERVER); //optional, defined in SMTPTransport
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.port", "465"); // default google smtp

        Session session = Session.getInstance(prop, null);
        Message msg = new MimeMessage(session);
        
         try {
		// from
            msg.setFrom(new InternetAddress(EMAIL_FROM));

			// to 
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(EMAIL_TO, false));


			// subject
            msg.setSubject(EMAIL_SUBJECT);
			
			// content 
            msg.setText(EMAIL_TEXT);
			
            msg.setSentDate(new Date());

			// Get SMTPTransport
            SMTPTransport t = (SMTPTransport) session.getTransport("smtp");
			
			// connect
            t.connect(SMTP_SERVER, USERNAME, PASSWORD);
			
			// send
            t.sendMessage(msg, msg.getAllRecipients());

            System.out.println("Response: " + t.getLastServerResponse());

            t.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
