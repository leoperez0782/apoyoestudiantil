/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.interactores;

import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Evaluacion;
import com.apoyoestudiantil.entity.ItemDesarrollo;
import com.apoyoestudiantil.entity.Notificacion;
import com.apoyoestudiantil.entity.NotificacionCurricular;
import java.util.Date;
import java.util.List;

/**
 * @date 09-ago-2019
 * @time 4:54:34
 * @author Leonardo Pérez
 */
public class CargadorNotificacionEvaluacion {

//    void formatearNotificacionEvaluacionNueva(ItemDesarrollo ultimaEv, List<Alumno> alumnos, NotificacionCurricular notif) {
//        notif.setAsunto("Aviso de proxima evaluación");
//        notif.setFechaEnvio(new Date());
//        notif.setPrioridad(Notificacion.Prioridad.ALTA);
//        notif.setRemitente(ultimaEv.getLibreta().getDocente());
//        String tipoEv = ultimaEv.getTipo().toString();
//        String fechaEv = ultimaEv.getFecha().toString();
//        String clase = cargarClase(ultimaEv);
//        String mensaje = String.format("Estimados el día %s se realizara %s en la clase de %s ", fechaEv, tipoEv, clase);
//        notif.setCuerpoMensaje(mensaje);
//        notif.setAlumno(null);
//        cargarDestinatarios(alumnos, notif);
//    }
    private String cargarClase(ItemDesarrollo ultimaEv) {
        return ultimaEv.getLibreta().getMateria().getNombre();
    }

    private void cargarDestinatarios(List<Alumno> alumnos, NotificacionCurricular notif) {

        for (Alumno a : alumnos) {
            notif.addDestinatario(a);
            for (AdultoResponsable adu : a.getTutores()) {
                notif.addDestinatario(adu);
            }
        }
    }

    void formatearNotificacionEvaluacionCalificadaParaAlumno(Evaluacion e, NotificacionCurricular notif) {
        notif.setAsunto("Resultado de Evaluacion");
        notif.setFechaEnvio(new Date());
        notif.setPrioridad(Notificacion.Prioridad.ALTA);
        notif.setRemitente(e.getLibreta().getDocente());
        String nombreAlumno = e.getAlumno().getPrimerNombre();
        String tipoEv = e.getTipo().toString();
        String fechaEv = e.getFecha().toString();
        String clase = e.getLibreta().getMateria().getNombre();
        String calif = String.valueOf(e.getCalificacion());
        String comentario = e.getComentario();
        String mensaje = String.format("Estimado/a %s la calificación de la evaluacion %s del día %s de la materia %s es de:  %s \n Comentario docente : %s",
                 nombreAlumno, tipoEv, fechaEv, clase, calif, comentario);
        notif.setCuerpoMensaje(mensaje);
        notif.setAlumno(e.getAlumno());
        notif.addDestinatario(e.getAlumno());

    }

    void formatearNotificacionEvaluacionCalificadaTutores(Evaluacion e, NotificacionCurricular notif) {
        notif.setAsunto("Resultado de Evaluacion");
        notif.setFechaEnvio(new Date());
        notif.setPrioridad(Notificacion.Prioridad.ALTA);
        notif.setRemitente(e.getLibreta().getDocente());
        String nombreAlumno = e.getAlumno().getPrimerNombre();
        String tipoEv = e.getTipo().toString();
        String fechaEv = e.getFecha().toString();
        String clase = e.getLibreta().getMateria().getNombre();
        String calif = String.valueOf(e.getCalificacion());
        String comentario = e.getComentario();
        String mensaje = String.format("Le cominicamos que el alumno %s recibio la calificacion de la evaluacion %s del día %s de la clase %s. Nota:  %s \n Comentario docente : %s",
                 nombreAlumno, tipoEv, fechaEv, clase, calif, comentario);
        notif.setCuerpoMensaje(mensaje);
        notif.setAlumno(null);//si no manda dos veces la misma notificacion a los alumnos.
        for (AdultoResponsable adu : e.getAlumno().getTutores()) {
            notif.addDestinatario(adu);
        }
    }

    void formatearNotificacionEvaluacionNueva(ItemDesarrollo ultimaEv, Alumno a, NotificacionCurricular notif) {
        notif.setAsunto("Aviso de proxima evaluación");
        notif.setFechaEnvio(new Date());
        notif.setPrioridad(Notificacion.Prioridad.ALTA);
        notif.setRemitente(ultimaEv.getLibreta().getDocente());
        String tipoEv = ultimaEv.getTipo().toString();
        String fechaEv = ultimaEv.getFecha().toString();
        String clase = cargarClase(ultimaEv);
        String mensaje = String.format("Estimados el día %s se realizara %s en la clase de %s ", fechaEv, tipoEv, clase);
        notif.setCuerpoMensaje(mensaje);
        notif.setAlumno(null);//revisar si agregarlo o no
        //cargarDEstinatariosEvaluacionNueva(a, notif);
        notif.addDestinatario(a);
    }

    private void cargarDEstinatariosEvaluacionNueva(List<AdultoResponsable> tutores, NotificacionCurricular notif) {
      
       tutores.forEach((adu) -> {
           notif.addDestinatario(adu);
        });
    }

    void formatearNotificacionNuevaEvaluacionParaTutores(List<AdultoResponsable> tutores, ItemDesarrollo ultimaEv, NotificacionCurricular notif) {
        notif.setAsunto("Aviso de proxima evaluación");
        notif.setFechaEnvio(new Date());
        notif.setPrioridad(Notificacion.Prioridad.ALTA);
        notif.setRemitente(ultimaEv.getLibreta().getDocente());
        String tipoEv = ultimaEv.getTipo().toString();
        String fechaEv = ultimaEv.getFecha().toString();
        String clase = cargarClase(ultimaEv);
        String grupo = cargarGrupo(ultimaEv);
        String mensaje = String.format("Estimados el día %s se realizara %s en la clase de %s del grupo %s", fechaEv, tipoEv, clase, grupo);
        notif.setCuerpoMensaje(mensaje);
        notif.setAlumno(null);//revisar si agregarlo o no
        cargarDEstinatariosEvaluacionNueva(tutores, notif);
    }

    private String cargarGrupo(ItemDesarrollo ultimaEv) {
        String grupo = ultimaEv.getLibreta().getGrupo().getNivel().getClase().getNivel()
                + " " + ultimaEv.getLibreta().getGrupo().getNivel().getClase().getCodigoClase();
        return grupo;
    }

}
