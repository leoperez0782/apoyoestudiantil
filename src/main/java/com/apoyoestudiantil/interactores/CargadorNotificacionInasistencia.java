/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.interactores;

import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Inasistencia;
import com.apoyoestudiantil.entity.Notificacion;
import com.apoyoestudiantil.entity.NotificacionCurricular;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 * @date 08-ago-2019
 * @time 6:05:32
 * @author Leonardo Pérez
 */
public class CargadorNotificacionInasistencia {

    
    public void formatearNotificacionInasistencia(NotificacionCurricular notif, Inasistencia inasist){
        notif.setAsunto("Inasistencia de Alumno");
        notif.setFechaEnvio(new Date());
        notif.setPrioridad(Notificacion.Prioridad.NORMAL);
        notif.setRemitente(inasist.getLista().getLibreta().getDocente());
        String nomAlumno = cargarNombreAlumno(inasist.getAlumno());
        String fechaFalta = cargarFecha(inasist.getFecha());
        String clase = cargarClase(inasist);
        String mensaje =String.format("Le comunicamos que el alumno %s falto a clase de %s el dia %s ", nomAlumno, clase, fechaFalta); 
        notif.setCuerpoMensaje(mensaje);
        notif.setAlumno(inasist.getAlumno());
        cargarDestinatarios(inasist.getAlumno().getTutores(), notif);
    }

    private String cargarNombreAlumno(Alumno alumno) {
        return alumno.getPrimerNombre() + " " + alumno.getPrimerApellido();
    }

    private String cargarFecha(LocalDate fecha) {
//       SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
       String ret = fecha.toString();//formatter.format(fecha.);
       return ret;
    }

    private String cargarClase(Inasistencia inasist) {
        String ret = inasist.getLista().getLibreta().getMateria().getNombre();
        return ret;
    }

    private void cargarDestinatarios(List<AdultoResponsable> tutores, NotificacionCurricular notif) {
        for(AdultoResponsable adu: tutores){
            notif.addDestinatario(adu);
        }
        //al final le podria agregar un usario de direccion/adscripcion?
    }

    
}
