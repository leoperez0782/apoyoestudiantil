/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.interactores;

import com.apoyoestudiantil.entity.Rol;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.compress.utils.IOUtils;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

/**
 * @param <T>
 * @date 03-jun-2019
 * @time 20:40:42
 * @author Leonardo Pérez
 */
public abstract class CargadorAbstractoExcel<T extends Usuario> implements ICargadorArchivos<T> {

    @Override
    public List<T> cargarDesdeArchivo(InputStream camino, Rol rol, TipoCarga tipocarga) throws AppException{
        switch(tipocarga){
            case SIMPLE:
                return cargarSimple(camino, rol);
            case COMPLETA:
                 return cargarCompleta(camino, rol);
            default:
                return cargarCompleta(camino, rol);
        }   
    }
    
   
   

    protected List<T> cargarSimple(InputStream camino, Rol rol) throws AppException {
        List<T> lista = new ArrayList<>();
        FileInputStream archivo;
        try {
            archivo = convertirArchivo(camino);//new FileInputStream(new File(camino));
            //obtener todas las filas de la hoja excel
            Iterator<Row> rowIterator = devuelveIteradorDeFilasDelArchivo(archivo);
            Row fila;
            T t;
            while (rowIterator.hasNext()) {
                fila = rowIterator.next();

                if (fila.getRowNum() != 0) {
                    t = cargarDatosDeDePlanillaSimplePorFila(fila);
                    t.addRol(rol);
                    lista.add(t);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CargadorAlumnosExcel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CargadorAlumnosExcel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    //protected  List<T> cargarCompleta(String camino, Rol rol){
    protected List<T> cargarCompleta(InputStream camino, Rol rol) throws AppException {
        List<T> lista = new ArrayList<>();
        FileInputStream archivo;
        try {
            archivo = convertirArchivo(camino);//new FileInputStream(new File(camino));
            //obtener todas las filas de la hoja excel
            Iterator<Row> rowIterator = devuelveIteradorDeFilasDelArchivo(archivo);
            Row fila;
            T t;
            while (rowIterator.hasNext()) {
                fila = rowIterator.next();

                if (fila.getRowNum() != 0) {
                    //cargo los primeros datos de la planilla.
                    t = cargarDatosDeDePlanillaSimplePorFila(fila);

                    cargarDatosAdicionales(fila, t);
                    t.addRol(rol);
                    lista.add(t);
                }
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(CargadorAlumnosExcel.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(CargadorAlumnosExcel.class.getName()).log(Level.SEVERE, null, ex);
        }
        return lista;
    }

    protected Iterator<Row> devuelveIteradorDeFilasDelArchivo(FileInputStream archivo) throws IOException {
        // leer archivo excel
        XSSFWorkbook libro = new XSSFWorkbook(archivo);
        //obtener la hoja que se va leer
        XSSFSheet hoja = libro.getSheetAt(0);
        //devolver iterador
        return hoja.iterator();
    }

    protected abstract T cargarDatosDeDePlanillaSimplePorFila(Row fila);

    protected abstract void cargarDatosAdicionales(Row fila, T t);

    private FileInputStream convertirArchivo(InputStream camino) throws AppException {
        FileOutputStream output = null;
        try {
            File file = File.createTempFile("temp", ".xlsx");
            output = new FileOutputStream(file);
            IOUtils.copy(camino, output);
            return new FileInputStream(file);
        } catch (IOException ex) {
            Logger.getLogger(CargadorAbstractoExcel.class.getName()).log(Level.SEVERE, null, ex);
            throw new AppException("Problema al convertir el archivo excel");
        }

    }
}
