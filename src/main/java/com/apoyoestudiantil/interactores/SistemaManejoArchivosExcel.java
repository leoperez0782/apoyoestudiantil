/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.interactores;

import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.entity.Administrador;
import com.apoyoestudiantil.entity.Administrativo;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Cargo;
import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.entity.Rol;
import com.apoyoestudiantil.excepciones.AppException;
import java.io.InputStream;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * @date 22-ago-2019
 * @time 11:30:00
 * @author Leonardo Pérez
 */
public class SistemaManejoArchivosExcel {

    private GestorAlumnos gestorAlumnos;
    private GestorCargaArchivos gestorCargaArchivos;
    private FactoriaCargadoresArchivo factoria;
    private EntityManager mng;
    public SistemaManejoArchivosExcel(){
        this.mng = ContextoDB.getInstancia().getManager();
        this.gestorAlumnos = new GestorAlumnos(mng);
        this.gestorCargaArchivos = new GestorCargaArchivos(mng);
        this.factoria = new FactoriaCargadoresArchivo();
    }

    List<Alumno> cargar(InputStream archivo, FactoriaCargadoresArchivo.Tipo tipoEntidad, ICargadorArchivos.TipoCarga tipoCarga) throws AppException {
        return gestorCargaArchivos.cargar(archivo, tipoEntidad, tipoCarga);
    }

    List<Docente> cargarDocentes(InputStream archivo, FactoriaCargadoresArchivo.Tipo tipoEntidad, ICargadorArchivos.TipoCarga tipoCarga) throws AppException {
        return gestorCargaArchivos.cargar(archivo, tipoEntidad, tipoCarga);
    }

 

    List<Administrativo> cargarAdministrativos(InputStream archivo, FactoriaCargadoresArchivo.Tipo tipo, ICargadorArchivos.TipoCarga tipoCarga) throws AppException {
        return gestorCargaArchivos.cargar(archivo, tipo, tipoCarga);
    }

    List<Administrador> cargarAdministradores(InputStream archivo, FactoriaCargadoresArchivo.Tipo tipo, ICargadorArchivos.TipoCarga tipoCarga) throws AppException {
        return gestorCargaArchivos.cargar(archivo, tipo, tipoCarga);
    }
}
