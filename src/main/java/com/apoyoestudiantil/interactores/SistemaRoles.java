/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.interactores;

import com.apoyoestudiantil.entity.Rol;

/**
 * @date 23-ago-2019
 * @time 19:40:55
 * @author Leonardo Pérez
 */
public class SistemaRoles {
    private GestorRoles gestor;

    public SistemaRoles() {
        this.gestor = new GestorRoles();
    }
    
    public Rol getRolByRol(String rol){
        return gestor.getRolByName(rol);
    }
}
