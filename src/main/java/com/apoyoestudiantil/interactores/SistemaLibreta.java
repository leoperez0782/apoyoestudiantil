/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.interactores;

import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.LibretaDAO;
import com.apoyoestudiantil.dao.ListaDAO;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.entity.Evaluacion;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Inasistencia;
import com.apoyoestudiantil.entity.ItemDesarrollo;
import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.entity.Lista;
import com.apoyoestudiantil.excepciones.AppException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import Utilidades.Observable;
import com.apoyoestudiantil.dao.AuxiliarMatriculaDAO;
import com.apoyoestudiantil.dao.EvaluacionDAO;
import com.apoyoestudiantil.dao.ItemReunionDAO;
import com.apoyoestudiantil.dao.ReunionDAO;
import com.apoyoestudiantil.entity.AuxiliarMatricula;
import com.apoyoestudiantil.entity.ItemAuxiliarMatricula;
import com.apoyoestudiantil.entity.ItemReunion;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.entity.PeriodoCes;
import com.apoyoestudiantil.entity.Promedio;
import com.apoyoestudiantil.entity.Reunion;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * @date 05-jul-2019
 * @time 19:49:54
 * @author Leonardo Pérez
 */
public class SistemaLibreta extends Observable {

    public enum Evento {
        INASISTENCIA, EVALUACION_NUEVA, EVALUACION_CORREGIDA, REUNION
    }

    private List<Libreta> lista = new ArrayList<>();
    private List<Lista> listadoInasistencia = new ArrayList<>(); //para usar en evento de inasistencia, guarda la ultima lista que se paso.
    private LibretaDAO dao;
    private ListaDAO daoLista;
    private EvaluacionDAO daoEva;
    private ReunionDAO daoReunion;
    private ItemReunionDAO daoItemReunion;
    private EntityManager mng;
    private Libreta ultimaActualizacion;//Para usar en notificacion evaluacion
    private ItemDesarrollo itemCorregido;//Para utilizar en notificacion evaluacion corregida
    private AuxiliarMatriculaDAO daoAuxiliarMatricula;

    public SistemaLibreta() {
        this.mng = ContextoDB.getInstancia().getManager();
        this.dao = new LibretaDAO(mng);
        this.daoLista = new ListaDAO(mng);
        this.daoReunion = new ReunionDAO(mng);
        this.daoItemReunion = new ItemReunionDAO(mng);
        this.daoAuxiliarMatricula = new AuxiliarMatriculaDAO(mng);
        this.daoEva = new EvaluacionDAO(mng);
    }

    public SistemaLibreta(EntityManager mng) {
        this.dao = new LibretaDAO(mng);
        this.daoLista = new ListaDAO(mng);
        this.mng = mng;
    }

    protected List<Libreta> cargarLibretasPorDocente(Docente d) {
        return this.dao.getAllByDocente(d);
    }

    public List<Lista> getListadoInasistencia() {
        return listadoInasistencia;
    }

    public void setListadoInasistencia(List<Lista> listadoInasistencia) {
        this.listadoInasistencia = listadoInasistencia;
    }

    public Libreta getUltimaActualizacion() {
        return ultimaActualizacion;
    }

    public ItemDesarrollo getItemCorregido() {
        return itemCorregido;
    }

    /**
     * Guarda las inasistencias en caso de haberlas, si es la primera vez que se
     * pasan las listas, guarda todas las listas correspondientes a la libreta
     * en la bd.
     *
     * @param libretaActual
     * @param mapa
     * @param listaActual
     * @return
     * @throws AppException
     */
    protected boolean actualizarLibreta(Libreta libretaActual, HashMap<Long, Inasistencia> mapa, Lista listaActual) throws AppException {
        Libreta lib = dao.findById(libretaActual.getId());
        Grupo grup = lib.getGrupo();
        final Lista listaElegida;
        boolean bandera = false;//lo uso para saber si hay que cargar las listas por primera vez.
        if (listaActual.getId() == null) {//si no se cargo de la bd es la primer carga de las listas.
            listaElegida = listaActual;
            bandera = true;
        } else {
            listaElegida = lib.getPasajesDeLista()//si no la busco tira error de objeto repetido dentro de la session hibernate
                    .stream().filter(l -> l.getId().equals(listaActual.getId()))
                    .findFirst().orElse(listaActual);
        }

        //agrego el alumno a la inasistencia
        for (Map.Entry<Long, Inasistencia> entry : mapa.entrySet()) {
            Alumno a = grup.getAlumno(entry.getKey());
            a.addInasistencia(entry.getValue());

            listaElegida.addInasistencia(entry.getValue());//agrego la inasistencia a la lista

        }
        listaElegida.setPasada(true);
        EntityTransaction trx = this.mng.getTransaction();
        trx.begin();
//        if (bandera) {//todavia no se guardaron los pasajes de lista
//            List<Lista> listadoNuevo = libretaActual.getPasajesDeLista();
//            daoLista.saveAll(listadoNuevo, trx, mng);
//            lib.setPasajesDeLista(listadoNuevo);
//        } else {
//
//            //actualizo solo la lista modificada
//            daoLista.updateWithTransaction(trx, mng, listaElegida);
//        }

        dao.updateWithTransaction(trx, mng, lib);
        trx.commit();
        if (listaElegida.getInasistencias().size() > 0) {//si hubo inasistencias notifico.
            this.listadoInasistencia.add(listaElegida);
            this.avisar(Evento.INASISTENCIA);
        }
        //si llega hasta aca sin tirar error actualizo.
        return true;
    }

    protected boolean actualizarLista(Libreta libretaActual, Lista listaActual) {
        return this.daoLista.update(listaActual);
    }

    protected void cargarPasajesLista(Libreta libretaActual) throws AppException {
        List<Lista> listado = this.daoLista.getAllByLibreta(libretaActual);
        libretaActual.setPasajesDeLista(listado);
    }

    /**
     * Carga las inasistencias guardadas en la bd, en la lista pasada por
     * paramtro
     *
     * @param listaActual
     */
    protected void actualizarListado(Lista listaActual) {
        List<Inasistencia> insaistencias = this.daoLista.cargarInasistencias(listaActual);
        listaActual.setInasistencias(insaistencias);
    }

//    protected boolean guardarEvaluaciones(Libreta libretaActual, HashMap<Integer, Evaluacion> mapa) {
//        Libreta lib = dao.findById(libretaActual.getId());
//        Grupo grup = lib.getGrupo();
//
//        for (Map.Entry<Integer, Evaluacion> entry : mapa.entrySet()) {
//            Alumno a = grup.getAlumno(entry.getKey());
//            //a.addEvaluacion(entry.getValue());
//            entry.getValue().setAlumno(a);
//            lib.addEvaluacion(entry.getValue());
//        }
//        dao.update(lib);
//        return true;
//    }
    /**
     * Asigna los alumnos a las evaluaciones y guarda las mismas en la libreta.
     * Tambien se guarda el item de desarrollo correspondiente. Si el item viene
     * sin archivo, se guarda solo la fecha y que es una evaluación, todos los
     * demas datos del item quedan con los valores por defecto.
     *
     * @param libretaActual
     * @param mapa
     * @param item
     * @return
     */
    boolean guardarEvaluaciones(Libreta libretaActual, HashMap<Integer, Evaluacion> mapa, ItemDesarrollo item) {
        Libreta lib = dao.findById(libretaActual.getId());
        Grupo grup = lib.getGrupo();
        ItemDesarrollo itemDes;
        if (item.getId() == null) {//si el item es nuevo
            itemDes = item;
            lib.addItemDesarrollo(itemDes);
        } else {
            itemDes = lib.getItemDesarrollo(item.getId());
        }
        itemDes.setCorregida(true);
        for (Map.Entry<Integer, Evaluacion> entry : mapa.entrySet()) {
            Alumno a = grup.getAlumno(entry.getKey());
            //a.addEvaluacion(entry.getValue());
            entry.getValue().setAlumno(a);
            //Agrego los datos del item de desarrollo
            entry.getValue().setFecha(itemDes.getFecha());
            entry.getValue().setPeriodo(itemDes.getPeriodo());
            entry.getValue().setTipo(itemDes.getTipo());
            lib.addEvaluacion(entry.getValue());
        }
        dao.update(lib);
        this.itemCorregido = itemDes;
        this.avisar(Evento.EVALUACION_CORREGIDA);
        return true;
    }

    boolean guardarItemDesarrollo(Libreta libretaActual, ItemDesarrollo item) {
        Libreta lib = dao.findById(libretaActual.getId());

        lib.addItemDesarrollo(item);

        dao.update(lib);
        if (item.isEvaluacion()) {
            this.ultimaActualizacion = lib;
            this.avisar(Evento.EVALUACION_NUEVA);

        }
        return true;
    }

    void limpiarInasistencias() {
        this.listadoInasistencia.clear();
    }

    /**
     * Actualiza los datos de las reuniones de la libreta pasada por parametro.
     * Guarda los datos delas reuniones en los auxiliares de matricula de los
     * alumnos.
     *
     * @param libretaActual
     * @return
     * @throws AppException
     */
    boolean actualizarReuniones(Libreta libretaActual) throws AppException {

        Nivel niv = libretaActual.getGrupo().getNivel();//Busco el nivel.
        List<Alumno> alumnos = libretaActual.getGrupo().getAlumnos();
        List<AuxiliarMatricula> listaAuxiliares = new ArrayList<>();
        //Busco el auxiliar de matricula del alumno.
        alumnos.forEach((a) -> {
            AuxiliarMatricula aux = daoAuxiliarMatricula.findByAlumno(a);
            listaAuxiliares.add(aux);
            ItemAuxiliarMatricula item;
            //Busco el item que corresponde al nivel cursado.
            Optional<ItemAuxiliarMatricula> itemOp = aux.getItems()
                    .stream().filter(i -> i.getNivel().equals(niv))
                    .findFirst();
            //Reviso si el item existe. Si el alumno se agrego al grupo luego de guardadas las primeras reuniones
            //no va a existir itemAuxiliar correspondiente al alunmo.
            if(itemOp.isPresent()){
                 item = itemOp.get();
            }else{//si no existe lo creo.
                item = new ItemAuxiliarMatricula();
                aux.addItem(item);
                item.setNivel(niv);
            }
            //cargo los datos de los items de la reunion de la libreta en el itemauxiliar de matrcicula
            cargarItemsEnAuxiliar(item, libretaActual, a);
        });
        EntityTransaction trx = mng.getTransaction();
        trx.begin();
        dao.updateWithTransaction(trx, mng, libretaActual);
        daoAuxiliarMatricula.updateAll(listaAuxiliares, trx, mng);
        trx.commit();

        //this.ultimaActualizacion = libretaActual;
        //this.avisar(Evento.REUNION); //avisa solo cuando las cierra la direccion.
        return true;
    }

    /**
     * Carga los datos de las reuniones de la libreta pasada por parametro
     * pertencientes a alumno tambien pasado por aparametro, en el
     * itemAuxiliarMatricula.
     *
     * @param aux
     * @param libretaActual
     * @param a
     */
    private void cargarItemsEnAuxiliar(ItemAuxiliarMatricula aux, Libreta libretaActual, Alumno a) {
        List<Reunion> lista = libretaActual.getReuniones();
        List<ItemReunion> items = new ArrayList<>();
        //Filtro las reuniones de la libreta buscando las que corresponden al alumno.
        for (Reunion r : lista) {
            r.verificarCierre();//Reviso si corresponde cerrar la reunion.
            List<ItemReunion> its = r.getItems()
                    .stream().filter(i -> i.getAlumno().equals(a))
                    .collect(Collectors.toList());
            items.addAll(its);
        }
        //actualizo los items de las reuniones.
        aux.addAllItems(items);
    }

    void cargarReuniones(Libreta libretaActual) {
        List<Reunion> listaReuniones = daoReunion.getAllByLibreta(libretaActual);
        listaReuniones.forEach((r) -> {
            List<ItemReunion> items = daoItemReunion.getAllByReunion(r);
            r.setItems(items);
        });
        libretaActual.setReuniones(listaReuniones);

    }

    boolean todasLasReunionesCerradas(String periodoEnLibreta, Libreta libreta) {
        int cantidadReunionesCerradasPeriodo = daoReunion.getCountCerradasPorPeriodo(periodoEnLibreta, libreta.getGrupo());
        int cantidadReunionesDelPeriodo = daoReunion.getCountReunionesPorPeriodo(periodoEnLibreta, libreta.getGrupo());
        int cantidadLibretasGrupo = dao.getCountPorGrupo(libreta.getGrupo());
        return cantidadLibretasGrupo == cantidadReunionesCerradasPeriodo;
    }

    /**
     * Devuelve la lista de reuniones del grupo al que corresponde la libreta pasada por parametro, 
     * en el periodo indicado.
     * @param periodoEnLibreta
     * @param libreta
     * @return 
     */
    List<Reunion> getReunionesCerradasDelGrupoEnUnPeriodo(String periodoEnLibreta, Libreta libreta) {
        List<Reunion> retorno = daoReunion.getAllByPeriodoLibreta(periodoEnLibreta, libreta.getGrupo());
        return retorno;
    }

    void marcarComoNotificadas(List<Reunion> reunionesDelPeriodo) {
        try {
            for (Reunion r : reunionesDelPeriodo) {
                r.setNotificada(true);
            }
            this.daoReunion.updateAll(reunionesDelPeriodo);
        } catch (AppException ex) {
            Logger.getLogger(SistemaLibreta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    List<Reunion> getReunionesSinCerrar(Grupo grupo) {
        return this.daoReunion.reunionesSinCerrarPorGrupo(grupo);
    }

    List<Libreta> cargarLibretasPorGrupo(Grupo grupo) {
        return this.dao.getAllByGrupo(grupo);
    }

    List<Libreta> asignaturasConReunionesSinCerrar(List<Libreta> libretasDelGrupo, String nombreperiodo) {
        return libretasDelGrupo.stream().filter(l -> !l.esReunionCerrada(nombreperiodo))
                .collect(Collectors.toList());
    }

    void guardarJuiciosDeReunion(Grupo grupo, HashMap<Alumno, String> juicios) {

    }
/**
 * Guarda los juicios de reunion y cierra las reuniones que no han sido cerradas en el grupo.
 * @param grupo
 * @param juicios
 * @param periodoElegido
 * @throws AppException 
 */
    void guardarJuiciosDeReunion(Grupo grupo, HashMap<Alumno, String> juicios, String periodoElegido) throws AppException {
        List<Libreta> libretasDelGrupo = this.cargarLibretasPorGrupo(grupo);
        List<Alumno> alumnos = grupo.getAlumnos();
        Nivel niv = grupo.getNivel();
        List<AuxiliarMatricula> listaAuxiliares = new ArrayList<>();
        for (Alumno a : alumnos) {
            AuxiliarMatricula aux = daoAuxiliarMatricula.findByAlumno(a);
            listaAuxiliares.add(aux);
            ItemAuxiliarMatricula item;
            //Busco el item que corresponde al nivel cursado.
            Optional<ItemAuxiliarMatricula> itemOp = aux.getItems()
                    .stream().filter(i -> i.getNivel().equals(niv))
                    .findFirst();
            //Reviso si el item existe. Si el alumno se agrego al grupo luego de guardadas las primeras reuniones
            //no va a existir itemAuxiliar correspondiente al alunmo.
            if(itemOp.isPresent()){
                 item = itemOp.get();
            }else{//si no existe lo creo.
                item = new ItemAuxiliarMatricula();
                aux.addItem(item);
                item.setNivel(niv);
            }
            for (Libreta lib : libretasDelGrupo) {
                String juicio = juicios.get(a);
                lib.validarJuicio(juicio);
                lib.cerrarItemReunion(a, juicio, periodoElegido);

                //cargo los datos.
                cargarItemsEnAuxiliar(item, lib, a);
            }
        }
        libretasDelGrupo.forEach((lib) -> {//Cierro las reuniones
            lib.cerrarReunion(periodoElegido);
        });
        EntityTransaction trx = mng.getTransaction();
        trx.begin();
        dao.updateAllWithTransaction(libretasDelGrupo, trx, mng);
        daoAuxiliarMatricula.updateAllWithTransaction(listaAuxiliares, trx, mng);
        trx.commit();

        this.ultimaActualizacion = libretasDelGrupo.get(0);//Pongo cualquier libreta por que el observador revisa si cerraron todas.
        this.avisar(Evento.REUNION);
    }
    
    public List<ItemDesarrollo> calendarioEvaluacionesAlumno(Alumno alumno)throws AppException {
        List<ItemDesarrollo> items = new ArrayList<>();
        List<Libreta> libretas = dao.getAllByAlumno(alumno);
        if(libretas.isEmpty()){
            throw new AppException("No existen libretas para el alumno");  
        }else{
            for (Libreta l : libretas) {
                for (ItemDesarrollo i: l.getDesarrollos()){
                    if(!i.isCorregida()){
                        items.add(i);
                    }                    
                }
            }
        }
        if(items.isEmpty()){
            throw new AppException("Aún no hay evaluaciones evaluaciones en el calendario");  
        }else{
            return items;
        }        
    }
    
    public List<Promedio> obetenrPromediosAlumno(Alumno alumno)throws AppException {
        List<Promedio> promedios = new ArrayList<>();
        List<Libreta> lista = dao.getAllByAlumno(alumno);
        if(lista.isEmpty()){
            throw new AppException("Aún no hay promedios disponibles para el alumno");
        }else{
            List<PeriodoCes> periodos = lista.get(0).getPeriodos();
            for (PeriodoCes p : periodos) {
                for (Libreta l : lista) {
                    Promedio promedio = new Promedio();
                    promedio.setPeriodo(p.getNombre());
                    promedio.setPromedio(l.recuperarPromedioReunion(alumno, p));
                    promedio.setConducta(l.recuperarConductaReunion(alumno, p));
                    promedio.setMateria(l.getMateria().getNombre());
                    l.getMateria().getNombre();
                    if(l.esReunionCerrada(alumno, p)){
                        promedio.setCerrado(true);
                        promedio.setComentario(l.recuperarComentarioReunion(alumno, p));
                    }else{
                        promedio.setCerrado(false);
                        promedio.setComentario("Promedio sin cerrar");
                    }
                    promedios.add(promedio);
                }
            }
            return promedios;
        }
    }
    
    public List<Evaluacion> getEvaluacionesAlumno(Alumno alumno) throws AppException {
        return daoEva.getAllByAlumno(alumno);
    }

}
