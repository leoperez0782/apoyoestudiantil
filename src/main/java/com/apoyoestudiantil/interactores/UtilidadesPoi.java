/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.interactores;

import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.poi.hssf.usermodel.HSSFDateUtil;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;

/**
 * @date 02-jun-2019
 * @time 23:17:59
 * @author Leonardo Pérez
 */
public class UtilidadesPoi {

    String devolverCadenaDeCelda(Cell celda) {
        CellType tipo = celda.getCellType();
        String cadenaAdevolver = "";
        switch (tipo) {
            case STRING:
                cadenaAdevolver = celda.getStringCellValue();
                break;
            case NUMERIC:
                cadenaAdevolver = convertirNumerico(celda);
                break;
            case BLANK:
                cadenaAdevolver = "n/c";
                break;
            default:
                cadenaAdevolver = "celda no compatible";
        }
        return cadenaAdevolver;
    }

    /**
     * Revisa si el valor de la celda es una fecha, si es asi, devuelve un string con 
     * indicando dd/MM/yyyy. Si no devuelve el valor numerico.
     * @param celda
     * @return 
     */
    private String convertirNumerico(Cell celda) {
        if (HSSFDateUtil.isCellDateFormatted(celda)) {
            Date fecha = HSSFDateUtil.getJavaDate(celda.getNumericCellValue());
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            String format = sdf.format(fecha);
            return format;
        } else {
            return String.valueOf((int) celda.getNumericCellValue());
        }
    }
}
