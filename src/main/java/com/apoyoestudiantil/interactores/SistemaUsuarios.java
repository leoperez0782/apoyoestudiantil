/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.interactores;

import Utilidades.UtilSeguridad;
import com.apoyoestudiantil.dao.AdministradorDAO;
import com.apoyoestudiantil.dao.AdministrativoDAO;
import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.dao.CargoDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.UsuarioDAO;
import com.apoyoestudiantil.entity.Administrador;
import com.apoyoestudiantil.entity.Administrativo;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Cargo;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Query;

/**
 *
 * @author Polachek
 */
public class SistemaUsuarios {

    private Institucion sistema;
    private ArrayList<Usuario> usuarios = new ArrayList<Usuario>();
    private EntityManager manager = ContextoDB.getInstancia().getManager();
    private UsuarioDAO dao;
    private AdministradorDAO daoAdmin;
    private AdministrativoDAO daoAdminO;
    private CargoDAO daoCargos;
    private AlumnoDAO daoAlumno;

    public SistemaUsuarios() {
        this.dao = new UsuarioDAO();
        this.daoAdmin = new AdministradorDAO();
        this.daoAdminO = new AdministrativoDAO();
        this.daoCargos = new CargoDAO(manager);
        this.daoAlumno = new AlumnoDAO();
    }

    protected Institucion getInstancia() {
        return sistema;
    }

    public Usuario loginUsuario(String user, String pass) throws AppException {
        // AGREGAR ------ SI EL TAMAÑO ES DISTINTO, GET COUNT
        if (usuarios == null || usuarios.isEmpty()) {
            Query q = ContextoDB.getInstancia().getManager().createQuery("select e from ususarios e");
            ArrayList<Usuario> employeeList = (ArrayList<Usuario>) q.getResultList();
            if (employeeList == null || employeeList.isEmpty()) {
                throw new AppException("Vacio o nulo desde Sistame Usuarios");
            } else {
                throw new AppException("No es nulo");
            }

            /*if(usuarios == null || usuarios.isEmpty()){
                throw new AplicationException("Vacio o nulo desde Sistame Usuarios");
            }else{
                throw new AplicationException("No es nulo");
            }*/
        } else {
            throw new AppException("No es nulo");
            /*for (Usuario usu : usuarios) {
                if (usu.getDocumento().getNumero().toString().equals(user) && usu.getClave().equals(pass)) {
                    return usu;
                }
            }*/
        }

        //throw new AplicationException("Usuario no encontrado o password incorrecto");
    }

    public void addUsuario(Usuario usu) {
        usuarios.add(usu);
    }
    
    public void guardarUsuario(Usuario u) {
        dao.save(u);
    }

    public Usuario loginUsuario(String user, String pass, EntityManager mng) throws AppException {
        dao = new UsuarioDAO(mng);

        try {
            Usuario miUsu = dao.findByEmail(user);
            // Buscar en lista
            Utilidades.UtilSeguridad convertidor = new UtilSeguridad();
            boolean esvalido = convertidor.compararClave(pass, miUsu.getClave());
            if (esvalido) {
                return miUsu;
            } else {
                throw new AppException("Clave incorrecta");
            }
        } catch (Exception e) {
            throw new AppException("Usuario o Clave incorrecta");
        }
    }

    public Usuario buscarUsuarioporNDocumento(Long ci) throws AppException {
        return dao.findUsuarioByNumeroDoc(ci);
    }

    public void actualizarTransaccion(Usuario usu) {
        dao.updateTr(manager.getTransaction(), manager, usu);
    }

    boolean updateUsuario(Usuario miU) {
        return dao.update(miU);
    }

    public List<Administrador> getAllAdministradores() throws AppException {
        return daoAdmin.get_All();
    }

    public List<Administrativo> getAllAdministrativos() throws AppException {
        return daoAdminO.get_All();
    }

    void saveAllAdministrativos(List<Administrativo> lista) throws AppException {
        for (Administrativo a : lista) {
            if (a.getCargo() != null) {
                String tipoCargo = a.getCargo().getTipoCargo();
                Cargo c = daoCargos.findByTipo(tipoCargo);
                a.setCargo(c);
            }
        }
        this.daoAdminO.saveAll(lista);
    }

    void saveAllAdministradores(List<Administrador> listaAd) throws AppException {
        daoAdmin.saveAll(listaAd);
    }
    
    public Alumno findAlumnoByNumeroDoc(Long numDoc) throws AppException {
        return dao.findAlumnoByNumeroDoc(numDoc);
    }
}
