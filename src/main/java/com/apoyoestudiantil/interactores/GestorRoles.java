/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.interactores;

import com.apoyoestudiantil.dao.RolDAO;
import com.apoyoestudiantil.entity.Rol;
import javax.persistence.EntityManager;

/**
 * @date 12-jun-2019
 * @time 22:25:02
 * @author Leonardo Pérez
 */
public class GestorRoles {
    
    private RolDAO dao;

    public GestorRoles() {
        this.dao = new RolDAO();
    }

    
    public GestorRoles(EntityManager mng) {
        this.dao = new RolDAO(mng);
        
    }
    
    public Rol getRolByName(String name){
        return dao.findByRol(name);
    }
}
