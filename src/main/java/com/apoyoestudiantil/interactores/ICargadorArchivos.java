/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.interactores;

import com.apoyoestudiantil.entity.Rol;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import java.io.InputStream;
import java.util.List;

/**
 *
 * @author leo
 */
public interface ICargadorArchivos <T extends Usuario>{
     /**
     * Con el enumerado se indica si se van a cargar todos los datos o solo algunos, a definir en la clase que la implementa.
     */
    enum TipoCarga{SIMPLE, COMPLETA}
    /**
     * 
     * @param camino
     * @param rol
     * @param tipo
     * @return 
     */
    List<T> cargarDesdeArchivo(InputStream camino, Rol rol, TipoCarga tipo) throws AppException;
    void exportarArchivo(String camino, List<T> lista) throws AppException;
}
