/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.interactores;

import com.apoyoestudiantil.dao.CargoDAO;
import com.apoyoestudiantil.dao.DocenteDAO;
import com.apoyoestudiantil.entity.Cargo;
import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.excepciones.AppException;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * @date 21-jun-2019
 * @time 20:02:17
 * @author Leonardo Pérez
 */
public class SistemaDocentes {

    private DocenteDAO dao;
    private CargoDAO  daoCargos;
    private List<Docente> lista;

    public SistemaDocentes() {
        this.dao = new DocenteDAO();
        this.daoCargos = new CargoDAO(dao.getManager());
    }
    
    public SistemaDocentes(EntityManager mng){
        this.dao = new DocenteDAO(mng);
        this.daoCargos = new CargoDAO(mng);
    }

    protected void saveAll(List<Docente> lista) throws AppException {
        //dao.saveAll(lista);
        for(Docente d : lista){
            if(d.getCargo() != null){
                Cargo c = daoCargos.findByTipo(d.getCargo().getTipoCargo());
                d.setCargo(c);
            }
            
        }
        dao.saveAll(lista);
    }

    protected void save(Docente d) {
      dao.save(d);
    }
    
    protected List<Docente> listar(){
        lista = dao.getAll();
        return lista;
    }

    protected Docente findById(Long idDocente) {
        return dao.findById(idDocente);
    }

    protected void update(Docente d) {
       dao.update(d);
    }
    
   
}
