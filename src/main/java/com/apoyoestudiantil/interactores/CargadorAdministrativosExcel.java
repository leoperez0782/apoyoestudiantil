/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.interactores;

import com.apoyoestudiantil.entity.Administrativo;
import com.apoyoestudiantil.entity.Cargo;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Telefono;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import java.util.Calendar;
import java.util.List;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/**
 * @date 22 sept. 2019
 * @time 0:14:18
 * @author Leonardo Pérez
 */
public class CargadorAdministrativosExcel extends CargadorAbstractoExcel<Usuario> {

    @Override
    protected Usuario cargarDatosDeDePlanillaSimplePorFila(Row fila) {
        //Obtengo las celdas.
        Cell celdaPrimerNombre = fila.getCell(4) == null ? fila.createCell(4) : fila.getCell(4);
        Cell celdaPrimerApellido = fila.getCell(5) == null ? fila.createCell(5) : fila.getCell(5);
        Cell celdaSegundoNombre = fila.getCell(6) == null ? fila.createCell(6) : fila.getCell(6);
        Cell celdaSegundoApellido = fila.getCell(7) == null ? fila.createCell(7) : fila.getCell(7);
        Cell celdaNroEmpleado = fila.getCell(0) == null ? fila.createCell(0) : fila.getCell(0);
        Cell celdaTipoDocumento = fila.getCell(2) == null ? fila.createCell(2) : fila.getCell(2);
        Cell celdaNumeroDocumento = fila.getCell(1) == null ? fila.createCell(1) : fila.getCell(1);
        Cell celdaPaisEmisor = fila.getCell(3) == null ? fila.createCell(3) : fila.getCell(3);
        Cell celdaEmail = fila.getCell(9) == null ? fila.createCell(9) : fila.getCell(9);
        Cell celdaFechaNAcimiento = fila.getCell(8) == null ? fila.createCell(8) : fila.getCell(8);
        Cell celdaCargo = fila.getCell(10) == null ? fila.createCell(10) : fila.getCell(10);
        Calendar calendar = Calendar.getInstance();

        UtilidadesPoi util = new UtilidadesPoi();
        Administrativo doc = new Administrativo();
        //CArgo los datos.
        Documento d = new Documento();
        d.setNumero(new Double(celdaNumeroDocumento.getNumericCellValue()).longValue());
        d.setPaisEmisor(celdaPaisEmisor.getStringCellValue());
        d.setTipo(d.devolverEnumTipoSegunString(celdaTipoDocumento.getStringCellValue()));
        doc.setDocumento(d);
        doc.setClave(String.valueOf(d.getNumero()));//Asumo que son docentes que no tienen usuario.
        doc.setEmail(celdaEmail.getStringCellValue());
        doc.setActivo(true);
        //calendar.setTime(celdaFechaNAcimiento.getDateCellValue());
        doc.setFechaNacimiento(util.devolverCadenaDeCelda(celdaFechaNAcimiento));

        doc.setNumeroEmpleado(new Double(celdaNroEmpleado.getNumericCellValue()).intValue());

        doc.setPrimerApellido(celdaPrimerApellido.getStringCellValue());
        doc.setPrimerNombre(celdaPrimerNombre.getStringCellValue());
        doc.setSegundoApellido(celdaSegundoApellido.getStringCellValue());

        doc.setSegundoNombre(util.devolverCadenaDeCelda(celdaSegundoNombre));
        String tipoCargo = util.devolverCadenaDeCelda(celdaCargo);
        if (!tipoCargo.equals("n/c")) {
            Cargo c = new Cargo();//hay que buscarlo en la bd antes de guardar el docente.
            c.setTipoCargo(tipoCargo);
            doc.setCargo(c);//Si hay tipo lo agego, si no queda en null
        }

        return doc;
    }

    @Override
    protected void cargarDatosAdicionales(Row fila, Usuario t) {
         //Obtengo las celdas.
        Cell celdaDepartamento = fila.getCell(11) == null ? fila.createCell(11) : fila.getCell(11);
        Cell celdaLocalidad = fila.getCell(12) == null ? fila.createCell(12) : fila.getCell(12);
        Cell celdaBarrio = fila.getCell(13) == null ? fila.createCell(13) : fila.getCell(13);
        Cell celdaCalle = fila.getCell(14) == null ? fila.createCell(14) : fila.getCell(14);
        Cell celdaNumero = fila.getCell(15) == null ? fila.createCell(15) : fila.getCell(15);
        Cell celdaEsquina = fila.getCell(16) == null ? fila.createCell(16) : fila.getCell(16);
        Cell celdaBlock = fila.getCell(17) == null ? fila.createCell(17) : fila.getCell(17);
        Cell celdaManzSolar = fila.getCell(18) == null ? fila.createCell(18) : fila.getCell(18);
        Cell celdaApartamento = fila.getCell(19) == null ? fila.createCell(19) : fila.getCell(19);
        Cell celdaTorre = fila.getCell(20) == null ? fila.createCell(20) : fila.getCell(20);
        Cell celdaEdificio = fila.getCell(21) == null ? fila.createCell(21) : fila.getCell(21);

        UtilidadesPoi util = new UtilidadesPoi();
        //creo el domicilio.
        Domicilio dom = new Domicilio();

        //dom.setApartamento(String.valueOf(celdaApartamento.getNumericCellValue()));
        dom.setApartamento(util.devolverCadenaDeCelda(celdaApartamento));
        dom.setBarrio(util.devolverCadenaDeCelda(celdaBarrio));
        //dom.setBlock(celdaBlock.getStringCellValue());
        dom.setBlock(util.devolverCadenaDeCelda(celdaBlock));
        dom.setCalle(util.devolverCadenaDeCelda(celdaCalle));
        dom.setDepartamento(dom.devolverDepartamentoSegunString(celdaDepartamento.getStringCellValue()));
        dom.setEsquina(util.devolverCadenaDeCelda(celdaEsquina));
        //dom.setIsEdificio(celdaEdificio.getBooleanCellValue());
        dom.setEdificio(util.devolverCadenaDeCelda(celdaEdificio));
        dom.setLocalidad(util.devolverCadenaDeCelda(celdaLocalidad));
        dom.setManzanaSolar(util.devolverCadenaDeCelda(celdaManzSolar));
        dom.setNumero(util.devolverCadenaDeCelda(celdaNumero));
        dom.setTorre(util.devolverCadenaDeCelda(celdaTorre));

        t.addDomicilio(dom);

        Telefono tel = cargarTelefono(fila);
        t.addTelefono(tel);
    }

    @Override
    public void exportarArchivo(String camino, List<Usuario> lista) throws AppException {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    private Telefono cargarTelefono(Row fila) {
        if (fila.getCell(22) == null) {//Si no hay numero de telefono no lo crea.
            return null;
        }
        Cell celdaNumero = fila.getCell(22);
        Cell celdaTipo = fila.getCell(23) == null ? fila.createCell(23) : fila.getCell(23);
        Cell celdaObs = fila.getCell(24) == null ? fila.createCell(24) : fila.getCell(24);
        UtilidadesPoi util = new UtilidadesPoi();
        Telefono t = new Telefono();
        t.setNumero(util.devolverCadenaDeCelda(celdaNumero));
        t.setTipo(Telefono.devolevertTipo(celdaTipo.getStringCellValue()));
        t.setObservaciones(util.devolverCadenaDeCelda(celdaObs));
        return t;
    }

}
