/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.interactores;

import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.excepciones.AppException;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * @date 12-jun-2019
 * @time 17:43:33
 * @author Leonardo Pérez
 *
 */
public class GestorAlumnos {

    private List<Alumno> alumnos = new ArrayList<>();
    private AlumnoDAO dao;

    public GestorAlumnos() {

    }

    public GestorAlumnos(EntityManager mng) {
        dao = new AlumnoDAO(mng);
    }

    public void saveAll(List<Alumno> lista) throws AppException {

        dao.saveAll(lista);

    }

}
