/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.interactores;

import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.ItemReunion;
import com.apoyoestudiantil.entity.Notificacion;
import com.apoyoestudiantil.entity.NotificacionCurricular;
import java.util.Date;
import java.util.List;

/**
 * @date 20-ago-2019
 * @time 0:29:40
 * @author Leonardo Pérez
 */
public class CargadorNotificacionesCierreReunion {

    void formatearNotificacion(NotificacionCurricular notif, List<ItemReunion> reunionesDelAlumno) {
         notif.setAsunto("Boletin de calificaciones");
        notif.setFechaEnvio(new Date());
        notif.setPrioridad(Notificacion.Prioridad.NORMAL);
        notif.setRemitente(null);
        String nomAlumno = cargarNombreAlumno(reunionesDelAlumno);
        
        String mensaje = crearMensaje(reunionesDelAlumno, nomAlumno); 
        notif.setCuerpoMensaje(mensaje);
        cargarAlumnoYDestinatarios(notif, reunionesDelAlumno);
    }

    private String cargarNombreAlumno(List<ItemReunion> reunionesDelAlumno) {
        Alumno a = reunionesDelAlumno.get(0).getAlumno();
        String nombre = a.getPrimerNombre() + " " + a.getPrimerApellido();
        return nombre;
    }

    private String crearMensaje(List<ItemReunion> reunionesDelAlumno, String nomAlumno) {
       ItemReunion it = reunionesDelAlumno.get(0);
       String cabecera = String.format("Le comunicamos los resultados del alumno %s en el cierre de %s \n", nomAlumno, it.getReunion().getNombre());
       String mensaje = "";
       for(ItemReunion itReu : reunionesDelAlumno){
           String men = String.format("Materia : %s  Calificacion : %s Conducta: %s Juicio del Docente : %s Faltas : %s" ,
                   itReu.getReunion().getLibreta().getMateria().getNombre(), itReu.getCalificacion(), itReu.getConducta(),
                   itReu.getJuicioDocente(), itReu.getCantidadFaltasFictas());
           mensaje += men + "\n";
       }
       mensaje += "Total faltas: "+ it.getAlumno().getInasistencias().size() + " \n Juicio de la reunion :" + it.getJuicioReunion();
       cabecera += mensaje;
       return cabecera;
    }

    private void cargarAlumnoYDestinatarios(NotificacionCurricular notif, List<ItemReunion> reunionesDelAlumno) {
       Alumno a = reunionesDelAlumno.get(0).getAlumno();
       notif.setAlumno(a);
       for(AdultoResponsable adu: a.getTutores()){
           notif.addDestinatario(adu);
       }
    }

}
