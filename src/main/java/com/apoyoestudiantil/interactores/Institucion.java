/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.interactores;

import com.apoyoestudiantil.entity.Administrador;
import com.apoyoestudiantil.entity.Administrativo;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Cargo;
import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.entity.Evaluacion;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Inasistencia;
import com.apoyoestudiantil.entity.ItemDesarrollo;
import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.entity.Lista;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.entity.Promedio;
import com.apoyoestudiantil.entity.Reunion;
import com.apoyoestudiantil.entity.Rol;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.ICargadorArchivos.TipoCarga;
import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * @date 19-may-2019
 * @time 17:26:03
 * @author Leonardo Pérez
 */
public class Institucion implements Serializable {

    private static final long serialVersionUID = 1L;

    private static Institucion instancia;
    private SistemaUsuarios sistemaUsuarios = new SistemaUsuarios();
    private SistemaNiveles sistemaNiveles = new SistemaNiveles();
    private SistemaCargos sistemaCargos = new SistemaCargos();
    private SistemaDocentes sistemaDocentes = new SistemaDocentes();
    private SistemaLibreta sistemaLibreta = new SistemaLibreta();
    private SistemaNotificaciones sistemaNotificaciones = new SistemaNotificaciones();
    private SistemaManejoArchivosExcel sistemaArchivosExcel = new SistemaManejoArchivosExcel();
    private SistemaRoles sistemaRoles = new SistemaRoles();

    private Institucion() {
        this.sistemaLibreta.agregar(sistemaNotificaciones);
    }

    public static Institucion getInstancia() {
        if (instancia == null) {
            instancia = new Institucion();
        }
        return instancia;
    }

    public Usuario login(String user, String pass) throws AppException {
        return sistemaUsuarios.loginUsuario(user, pass);
    }

    public void addUsuario(Usuario usu) {
        sistemaUsuarios.addUsuario(usu);
    }
    
    public void guardarUsuario(Usuario u) {
        sistemaUsuarios.guardarUsuario(u);
    }

    public Usuario login(String user, String pass, EntityManager mng) throws AppException {
        return sistemaUsuarios.loginUsuario(user, pass, mng);
    }
    
    public Usuario buscarUsuarioporNDocumento(Long ci) throws AppException{
        return this.sistemaUsuarios.buscarUsuarioporNDocumento(ci);
    }
    
    public void actTransaccionUsu(Usuario usu){
        this.sistemaUsuarios.actualizarTransaccion(usu);
    }

    public boolean actualizarUsuario(Usuario miU) {
        return this.sistemaUsuarios.updateUsuario(miU);
    }
    
    public List<Administrador> getAllAdministradores() throws AppException{
        return this.sistemaUsuarios.getAllAdministradores();
    }

    public List<Administrativo> getAllAdministrativos() throws AppException{
        return this.sistemaUsuarios.getAllAdministrativos();
    }

    public ArrayList<Nivel> getNiveles() {
        return sistemaNiveles.getNiveles();
    }

    public Nivel buscarNivelID(long nvSel) {
        return sistemaNiveles.getNivelesID(nvSel);
    }
    
    public Alumno findAlumnoByNumeroDoc(Long numDoc) throws AppException {
        return this.sistemaUsuarios.findAlumnoByNumeroDoc(numDoc);
    }

    // <editor-fold defaultstate="collapsed" desc="Sistema Cargos">
    public boolean saveCargo(Cargo c) {
        return sistemaCargos.save(c);
    }
    
    public boolean updateCargo(Cargo c){
        return sistemaCargos.update(c);
    }
    
    public boolean borrarCargo(Cargo c){
        return sistemaCargos.borrarCargo(c);
    }

    public List<Cargo> getAllCargos() throws AppException {
        return sistemaCargos.getAll();
    }

    public Cargo findCargoByCargo(String tipoCargo) throws AppException {
        return sistemaCargos.findByCargo(tipoCargo);
    }

    public Cargo findCargoById(Long cargoId) throws AppException {
        return sistemaCargos.findById(cargoId);
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Sistema docentes">
    public void saveAllDocentes(List<Docente> lista) throws AppException {
        sistemaDocentes.saveAll(lista);
    }

    public void saveDocente(Docente d) {
        sistemaDocentes.save(d);
    }

    public List<Docente> listarDocentes() {
        return sistemaDocentes.listar();
    }

    public Docente findDocenteById(Long idDocente) {
        return sistemaDocentes.findById(idDocente);
    }

    public void updateDocente(Docente d) {
        sistemaDocentes.update(d);
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Sistema libretas">
    public List<Libreta> cargarLibretasPorDocente(Docente d) {
        return sistemaLibreta.cargarLibretasPorDocente(d);
    }

    public boolean actualizarLibreta(Libreta libretaActual, HashMap<Long, Inasistencia> mapa, Lista listaActual) throws AppException {
        return sistemaLibreta.actualizarLibreta(libretaActual, mapa, listaActual);
    }

    public boolean actualizarLista(Libreta libretaActual, Lista listaActual) {
        return sistemaLibreta.actualizarLista(libretaActual, listaActual);
    }

    public void cargarPasajesLista(Libreta libretaActual) throws AppException {
        sistemaLibreta.cargarPasajesLista(libretaActual);
    }

    public void actualizarListado(Lista listaActual) {
        sistemaLibreta.actualizarListado(listaActual);
    }
    
    public List<ItemDesarrollo> calendarioEvaluacionesAlumno(Alumno alumno) throws AppException {
        return sistemaLibreta.calendarioEvaluacionesAlumno(alumno);
    }

    public List<Promedio> obetenrPromediosAlumno(Alumno alumno) throws AppException {
        return sistemaLibreta.obetenrPromediosAlumno(alumno);
    }

    public List<Evaluacion> getEvaluacionesAlumno(Alumno alumno) throws AppException {
        return sistemaLibreta.getEvaluacionesAlumno(alumno);
    }

//    public boolean guardarEvaluaciones(Libreta libretaActual, HashMap<Integer, Evaluacion> mapa) {
//        return sistemaLibreta.guardarEvaluaciones(libretaActual, mapa);
//    }
    public boolean guardarEvaluaciones(Libreta libretaActual, HashMap<Integer, Evaluacion> mapa, ItemDesarrollo item) {
        return sistemaLibreta.guardarEvaluaciones(libretaActual, mapa, item);
    }

    public boolean guardarItemDesarrollo(Libreta libretaActual, ItemDesarrollo item) {
        return sistemaLibreta.guardarItemDesarrollo(libretaActual, item);
    }

    public boolean actualizarReuniones(Libreta libretaActual) throws AppException {
        return sistemaLibreta.actualizarReuniones(libretaActual);
    }

    public void cargarReuniones(Libreta libretaActual) {
        sistemaLibreta.cargarReuniones(libretaActual);
    }

    public List<Libreta> cargarLibretasPorGrupo(Grupo grupo) {
        return this.sistemaLibreta.cargarLibretasPorGrupo(grupo);
    }

    public List<Libreta> asignaturasConReunionesSinCerrar(List<Libreta> libretasDelGrupo, String nombreperiodo) {
        return this.sistemaLibreta.asignaturasConReunionesSinCerrar(libretasDelGrupo, nombreperiodo);
    }

    public void guardarJuiciosReunion(Grupo grupo, HashMap<Alumno, String> juicios) {
        this.sistemaLibreta.guardarJuiciosDeReunion(grupo, juicios);
    }

    public void guardarJuiciosReunion(Grupo grupo, HashMap<Alumno, String> juicios, String periodoElegido) throws AppException {
        this.sistemaLibreta.guardarJuiciosDeReunion(grupo, juicios, periodoElegido);
    }

    public List<Reunion> getReunionesSinCerrar(Grupo grupo) {
        return this.sistemaLibreta.getReunionesSinCerrar(grupo);
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Sistema archivos">
    public List<Docente> cargarArchivoDocente(InputStream archivo, FactoriaCargadoresArchivo.Tipo tipoEntidad, ICargadorArchivos.TipoCarga tipoCarga) throws AppException {
        return this.sistemaArchivosExcel.cargarDocentes(archivo, tipoEntidad, tipoCarga);
    }

    public List<Alumno> cargarArchivo(InputStream archivo, FactoriaCargadoresArchivo.Tipo tipoEntidad, ICargadorArchivos.TipoCarga tipoCarga) throws AppException {
        return this.sistemaArchivosExcel.cargar(archivo, tipoEntidad, tipoCarga);
    }
    //</editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Sistema niveles">
    public void saveAllAlumnos(List<Alumno> lista) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void inscribirAlumnos(List<Alumno> lista, Nivel nivel, Grupo grupo) {
        this.sistemaNiveles.inscribirAlumnos(lista, nivel, grupo);
    }

    public void inscribirAlumno(Alumno a1, Nivel nivel, Grupo grupo) throws AppException {
        this.sistemaNiveles.inscribirAlumno(a1, nivel, grupo);
    }
    //</editor-fold>
    
    public Rol findRolByRol(String rol) {
        return this.sistemaRoles.getRolByRol(rol);
    }

    public void enviarNotificacionAtencionGrupal(String valorMensajeGrup, Libreta libretaActual) {
        this.sistemaNotificaciones.enviarNotificacionAtencionGrupal(valorMensajeGrup, libretaActual);
    }

    public void enviarNotificacionAtencion(String mensaje, Libreta libretaActual, int indice) {
        this.sistemaNotificaciones.enviarNotificacionAtencion(mensaje, libretaActual, indice);
    }   

    public List<Administrativo> cargarAdministrativos(InputStream archivo, FactoriaCargadoresArchivo.Tipo tipo, ICargadorArchivos.TipoCarga tipoCarga) throws AppException {
        return this.sistemaArchivosExcel.cargarAdministrativos(archivo, tipo, tipoCarga);
    }

    public void saveAllAdministrativos(List<Administrativo> lista) throws AppException {
       this.sistemaUsuarios.saveAllAdministrativos(lista);
    }

    public List<Administrador> cargarAdministradores(InputStream archivo, FactoriaCargadoresArchivo.Tipo tipo, TipoCarga tipoCarga) throws AppException {
        return this.sistemaArchivosExcel.cargarAdministradores(archivo, tipo, tipoCarga);
    }

    public void saveAllAdministradores(List<Administrador> listaAd) throws AppException {
       this.sistemaUsuarios.saveAllAdministradores(listaAd);
    }

}
