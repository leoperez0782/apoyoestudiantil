/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.interactores;


import com.apoyoestudiantil.dao.AlumnoDAO;
import com.apoyoestudiantil.dao.AuxiliarMatriculaDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.GrupoDAO;
import com.apoyoestudiantil.dao.NivelDAO;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.AuxiliarMatricula;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.ItemAuxiliarMatricula;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.excepciones.AppException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 *
 * @author Polachek
 */
public class SistemaNiveles {

    private Institucion sistema;
    private EntityManager manager = ContextoDB.getInstancia().getManager();
    private NivelDAO dao;
    private ArrayList<Nivel> niveles = new ArrayList<Nivel>();
    private AlumnoDAO daoAlumno;
    private GrupoDAO daoGrupo;
    private AuxiliarMatriculaDAO daoAux;

    protected Institucion getInstancia() {
        return sistema;
    }

    public SistemaNiveles() {
        this.dao = new NivelDAO(manager);
        this.daoAlumno = new AlumnoDAO(manager);
        this.daoAux = new AuxiliarMatriculaDAO(manager);
        this.daoGrupo = new GrupoDAO(manager);
    }

    public ArrayList<Nivel> getNiveles() {
        dao = new NivelDAO(manager);
        niveles = (ArrayList<Nivel>) dao.getAll();
        return niveles;
    }

    public Nivel getNivelesID(long nvSel) {
        for (Nivel nv : niveles) {
            if (nv.getId() == nvSel) {
                return nv;
            }
        }

        return null;
    }

    void inscribirAlumnos(List<Alumno> lista, Nivel nivel, Grupo grupo) {
        EntityTransaction trx = manager.getTransaction();
        trx.begin();
        daoAlumno.saveAll(lista, trx, manager);
        for (Alumno a : lista) {
            grupo.addAlumno(a);
        }

        daoGrupo.updateWithTransaction(trx, manager, grupo);

        for (Alumno a : lista) {
            AuxiliarMatricula aux = new AuxiliarMatricula();
            aux.setAlumno(a);
            ItemAuxiliarMatricula item = new ItemAuxiliarMatricula();
            item.setNivel(nivel);
            item.setMatricula(aux);
            aux.setId(daoAux.persistWithTransaction(trx, manager, aux));
        }

        trx.commit();
    }

    void inscribirAlumno(Alumno a1, Nivel nivel, Grupo grupo) throws AppException {
        try {
            EntityTransaction trx = manager.getTransaction();
            trx.begin();
            a1.setId(daoAlumno.persistWithTransaction(trx, manager, a1));

            grupo.addAlumno(a1);

            daoGrupo.updateWithTransaction(trx, manager, grupo);

            AuxiliarMatricula aux = new AuxiliarMatricula();
            aux.setAlumno(a1);
            ItemAuxiliarMatricula item = new ItemAuxiliarMatricula();
            item.setNivel(nivel);
            item.setMatricula(aux);
            aux.setId(daoAux.persistWithTransaction(trx, manager, aux));

            trx.commit();//si no tira error todo ok.
        } catch (Exception e) {
            Logger.getLogger(SistemaNiveles.class.getName()).log(Level.SEVERE, null, e);
            throw new AppException("Error al guardar : " + e.getMessage());
        }
    }

}
