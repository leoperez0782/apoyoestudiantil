/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.interactores;

import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.entity.Notificacion;
import com.apoyoestudiantil.entity.NotificacionCurricular;
import java.util.Date;

/**
 * @date 6 sept. 2019
 * @time 7:24:55
 * @author Leonardo Pérez
 */
public class CargadorNotificacionAtencion {

    public enum TipoNotificacionAtencion {
        GRUPAL, INDIVIDUAL
    }

    void formatearNotificacionAtencion(NotificacionCurricular notif, Alumno a, Libreta libretaActual, String valorMensajeGrup, TipoNotificacionAtencion tipo) {
        notif.setAsunto("Aviso de Atención en la materia");
        notif.setFechaEnvio(new Date());
        notif.setPrioridad(Notificacion.Prioridad.ALTA);
        notif.setRemitente(libretaActual.getDocente());
        String clase = cargarClase(libretaActual);
        String grupo = cargarGrupo(libretaActual);
        String docente = cargarDocente(libretaActual);
        String alumno = cargarAlumno(a);
        String mensaje = crearCuerpoMensajeSegunTipo(clase, grupo, docente, valorMensajeGrup, alumno, tipo); 
        notif.setCuerpoMensaje(mensaje);
        cargarDesinatarios(notif, a);
    }

    private String cargarClase(Libreta libretaActual) {
        return libretaActual.getMateria().getNombre();
    }

    private String cargarGrupo(Libreta libretaActual) {
        String grupo = libretaActual.getGrupo().getNivel().getClase().getNivel() + " "
                + libretaActual.getGrupo().getNivel().getClase().getCodigoClase();
        return grupo;
    }

    private String cargarDocente(Libreta libretaActual) {
        String doc = libretaActual.getDocente().getPrimerApellido() + ", " + libretaActual.getDocente().getPrimerNombre();
        return doc;
    }

    private String cargarAlumno(Alumno a) {
        String alumno = a.getPrimerNombre() + " " + a.getPrimerApellido();
        return alumno;
    }

    private void cargarDesinatarios(NotificacionCurricular notif, Alumno a) {
        notif.setAlumno(a);
        notif.addDestinatario(a);
        a.getTutores().forEach((adu) -> {
            notif.addDestinatario(adu);
        });
    }

    private String crearCuerpoMensajeSegunTipo(String clase, String grupo, String docente, String valorMensajeGrup, String alumno, TipoNotificacionAtencion tipo) {
        String mensaje;
        switch(tipo){
            case GRUPAL:
                mensaje = String.format("Mensaje de atención sobre la materia %s de la clase %s \n Docente: %s \n Notificacion grupal \n %s", clase, grupo, docente, valorMensajeGrup);
                break;
            case INDIVIDUAL:
                mensaje = String.format("Mensaje de atención sobre la materia %s de la clase %s \n Docente: %s \n Sobre el alumno: \n %s \n Mensaje: %s", clase, grupo, docente,alumno, valorMensajeGrup);
                break;
            default:
                mensaje =" Tipo de mensaje no reconocido";
                break;
        }
        return mensaje;
    }
}
