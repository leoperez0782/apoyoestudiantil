/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.interactores;

import com.apoyoestudiantil.dao.CargoDAO;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.entity.Cargo;
import com.apoyoestudiantil.excepciones.AppException;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;
import javax.persistence.EntityManager;

/**
 * @date 23-jun-2019
 * @time 1:26:09
 * @author Leonardo Pérez
 */
public class SistemaCargos {

    private CargoDAO dao;
    private List<Cargo> lista;

    public SistemaCargos() {
        this.dao = new CargoDAO(ContextoDB.getInstancia().getManager());
    }

    
    public SistemaCargos(EntityManager mng) {
        this.dao = new CargoDAO(mng);
    }

    protected boolean save(Cargo c) {
        return dao.save(c);
    }

    protected List<Cargo> getAll() throws AppException {
        try {
            if(this.lista == null || lista.isEmpty() || (this.lista.size() < dao.getCount())){
                this.lista = dao.getAll();
            }
            return this.lista; 
        } catch (Exception e) {
            throw new AppException(e.getMessage());
        }

    }

    protected Cargo findByCargo(String tipoCargo) throws AppException {
        try {
            if(this.lista == null || lista.isEmpty() || (this.lista.size() < dao.getCount())){
                return dao.findByTipo(tipoCargo);
            }else{
                Optional<Cargo> cargo =lista.stream()
                        .filter(c -> c.getTipoCargo().equalsIgnoreCase(tipoCargo))
                        .findFirst();
                return cargo.get();
            }
            
        } catch(NoSuchElementException ne){
            throw new AppException("No se encontro el cargo buscado");
        }
        catch (Exception e) {
            throw new AppException(e.getMessage());
        }
    }

    protected Cargo findById(Long cargoId) throws AppException {
        try {
            return dao.findById(cargoId);
        } catch (Exception e) {
            throw new AppException(e.getMessage());
        }
    }

    protected boolean update(Cargo c) {
        return dao.update(c);
    }

    protected boolean borrarCargo(Cargo c) {
        return dao.delete(c);
    }
}
