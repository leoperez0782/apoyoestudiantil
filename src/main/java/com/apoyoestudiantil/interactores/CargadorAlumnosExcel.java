/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.interactores;

import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Domicilio;
import com.apoyoestudiantil.entity.Telefono;
import java.util.Calendar;
import java.util.List;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;

/**
 * @date 01-jun-2019
 * @time 18:47:36
 * @author Leonardo Pérez
 * Clase para cargar datos de los alumnos desde archivos
 * de externos.
 *
 */
public class CargadorAlumnosExcel extends CargadorAbstractoExcel<Alumno > {


    /**
     * Crea un objeto alumno y le carga los datos que se encuentran en la fila pasada por parametro.
     * Carga los datos basicos, no se carga ni domicilio, ni padres.
     * @param fila
     * @return 
     */
    @Override
    protected Alumno cargarDatosDeDePlanillaSimplePorFila(Row fila) {
        //Obtengo las celdas.
        Cell celdaPrimerNombre = fila.getCell(4)== null ? fila.createCell(4): fila.getCell(4);
        Cell celdaPrimerApellido = fila.getCell(5)== null ? fila.createCell(5): fila.getCell(5);
        Cell celdaSegundoNombre = fila.getCell(6)== null ? fila.createCell(6): fila.getCell(6);
        Cell celdaSegundoApellido = fila.getCell(7)== null ? fila.createCell(7): fila.getCell(7);
        Cell celdaMatricula = fila.getCell(0)== null ? fila.createCell(0): fila.getCell(0);
        Cell celdaTipoDocumento = fila.getCell(2)== null ? fila.createCell(2): fila.getCell(2);
        Cell celdaNumeroDocumento = fila.getCell(1)== null ? fila.createCell(1): fila.getCell(1);
        Cell celdaPaisEmisor = fila.getCell(3)== null ? fila.createCell(3): fila.getCell(3);
        Cell celdaEmail = fila.getCell(9)== null ? fila.createCell(9): fila.getCell(9);
        Cell celdaFechaNAcimiento = fila.getCell(8)== null ? fila.createCell(8): fila.getCell(8);
        Calendar calendar = Calendar.getInstance();
        
        UtilidadesPoi util = new UtilidadesPoi();
        Alumno a = new Alumno();
        a.setActivo(true);
        //CArgo los datos.
        Documento d = new Documento();
        d.setNumero(new Double(celdaNumeroDocumento.getNumericCellValue()).longValue());
        d.setPaisEmisor(celdaPaisEmisor.getStringCellValue());
        d.setTipo(d.devolverEnumTipoSegunString(celdaTipoDocumento.getStringCellValue()));
        a.setDocumento(d);
        a.setClave(String.valueOf(d.getNumero()));//Asumo que son alumnos que no tienen usuario.
        a.setEmail(celdaEmail.getStringCellValue());

        //calendar.setTime(celdaFechaNAcimiento.getDateCellValue());
        a.setFechaNacimiento(util.devolverCadenaDeCelda(celdaFechaNAcimiento));

        a.setMatricula(new Double(celdaMatricula.getNumericCellValue()).intValue());
       

        a.setPrimerApellido(celdaPrimerApellido.getStringCellValue());
        a.setPrimerNombre(celdaPrimerNombre.getStringCellValue());
        a.setSegundoApellido(celdaSegundoApellido.getStringCellValue());
        
        a.setSegundoNombre(util.devolverCadenaDeCelda(celdaSegundoNombre));

        return a;
    }


    @Override
    protected void cargarDatosAdicionales(Row fila, Alumno t) {
        //Obtengo las celdas.
        Cell celdaDepartamento = fila.getCell(10) == null ? fila.createCell(10) : fila.getCell(10);
        Cell celdaLocalidad = fila.getCell(11) == null ? fila.createCell(11) : fila.getCell(11);
        Cell celdaBarrio = fila.getCell(12) == null ? fila.createCell(12) : fila.getCell(12);
        Cell celdaCalle = fila.getCell(13) == null ? fila.createCell(13) : fila.getCell(13);
        Cell celdaNumero = fila.getCell(14) == null ? fila.createCell(14) : fila.getCell(14);
        Cell celdaEsquina = fila.getCell(15) == null ? fila.createCell(15) : fila.getCell(15);
        Cell celdaBlock = fila.getCell(16) == null ? fila.createCell(16) : fila.getCell(16);
        Cell celdaManzSolar = fila.getCell(17) == null ? fila.createCell(17) : fila.getCell(17);
        Cell celdaApartamento = fila.getCell(18) == null ? fila.createCell(18) : fila.getCell(18);
        Cell celdaTorre = fila.getCell(19) == null ? fila.createCell(19) : fila.getCell(19);
        Cell celdaEdificio = fila.getCell(20) == null ? fila.createCell(20) : fila.getCell(20);
        
        UtilidadesPoi util = new UtilidadesPoi();
        //creo el domicilio.
        Domicilio dom = new Domicilio();
        
        //dom.setApartamento(String.valueOf(celdaApartamento.getNumericCellValue()));
        dom.setApartamento(util.devolverCadenaDeCelda(celdaApartamento));
        dom.setBarrio(util.devolverCadenaDeCelda(celdaBarrio));
        //dom.setBlock(celdaBlock.getStringCellValue());
        dom.setBlock(util.devolverCadenaDeCelda(celdaBlock));
        dom.setCalle(util.devolverCadenaDeCelda(celdaCalle));
        dom.setDepartamento(dom.devolverDepartamentoSegunString(celdaDepartamento.getStringCellValue()));
        dom.setEsquina(util.devolverCadenaDeCelda(celdaEsquina));
        //dom.setIsEdificio(celdaEdificio.getBooleanCellValue());
        dom.setEdificio(util.devolverCadenaDeCelda(celdaEdificio));
        dom.setLocalidad(util.devolverCadenaDeCelda(celdaLocalidad));
        dom.setManzanaSolar(util.devolverCadenaDeCelda(celdaManzSolar));
        dom.setNumero(util.devolverCadenaDeCelda(celdaNumero));
        dom.setTorre(util.devolverCadenaDeCelda(celdaTorre));
        
        t.addDomicilio(dom);
        
        AdultoResponsable adu = cargarAdultoResponsable(fila, t);
        
        if(adu != null){
            adu.addDomicilio(dom);//Asumo que el domicilio del adulto responsable y el alumno son los mismos.
            t.addPadre(adu);
        }
    }

    /**
     * Carga los datos del adulto responsable pero sin agregarle el rol.
     * @param fila
     * @param t
     * @return 
     */
    private AdultoResponsable cargarAdultoResponsable(Row fila, Alumno t) {
        if(fila.getCell(21)== null){//Si no esta el documento del tutor asumo que no estan los datos del mismo ingresados.
            return null;
        }
        Cell celdaNumeroDocumento =  fila.getCell(21);
        Cell celdaTipoDocumento = fila.getCell(22)== null ? fila.createCell(22): fila.getCell(22);
        Cell celdaPaisEmisor = fila.getCell(23)== null ? fila.createCell(23): fila.getCell(23);
        Cell celdaPrimerNombre = fila.getCell(24)== null ? fila.createCell(24): fila.getCell(24);
        Cell celdaPrimerApellido = fila.getCell(25)== null ? fila.createCell(25): fila.getCell(25);
        Cell celdaSegundoNombre = fila.getCell(26)== null ? fila.createCell(26): fila.getCell(26);
        Cell celdaSegundoApellido = fila.getCell(27)== null ? fila.createCell(27): fila.getCell(27);
        Cell celdaFechaNAcimiento = fila.getCell(28)== null ? fila.createCell(28): fila.getCell(28);
        Cell celdaEmail = fila.getCell(29)== null ? fila.createCell(29): fila.getCell(29);
        Calendar calendar = Calendar.getInstance();
        UtilidadesPoi util = new UtilidadesPoi();
        AdultoResponsable adu = new AdultoResponsable();
        
        adu.setClave("123");
        Documento doc = new Documento();
        doc.setNumero(new Double(celdaNumeroDocumento.getNumericCellValue()).longValue());
        doc.setPaisEmisor(util.devolverCadenaDeCelda(celdaPaisEmisor));
        doc.setTipo(doc.devolverEnumTipoSegunString(celdaTipoDocumento.getStringCellValue()));
        adu.setDocumento(doc);
        adu.setEmail(util.devolverCadenaDeCelda(celdaEmail));
        //calendar.setTime(celdaFechaNAcimiento.getDateCellValue());
        adu.setFechaNacimiento(util.devolverCadenaDeCelda(celdaFechaNAcimiento));
        adu.setPrimerApellido(util.devolverCadenaDeCelda(celdaPrimerApellido));
        adu.setPrimerNombre(util.devolverCadenaDeCelda(celdaPrimerNombre));
        adu.setSegundoApellido(util.devolverCadenaDeCelda(celdaSegundoApellido));
        adu.setSegundoNombre(util.devolverCadenaDeCelda(celdaSegundoNombre));
        Telefono tel = cargarTelefono(fila);
        adu.addTelefono(tel);
        return adu;
    }

    @Override
    public void exportarArchivo(String camino, List<Alumno> lista) {
        HSSFWorkbook workbook = new HSSFWorkbook();
        HSSFSheet sheet = workbook.createSheet();
        workbook.setSheetName(0, "Alumnos");
        
        throw new UnsupportedOperationException("Not supported yet."); 
    }

    private Telefono cargarTelefono(Row fila) {
        if(fila.getCell(30)== null){//Si no esta el telefono del tutor asumo que no estan los datos del mismo ingresados.
            return null;
        }
        Cell celdaTipoTelefono = fila.getCell(30);
        Cell celdaNumero = fila.getCell(31)== null ? fila.createCell(31): fila.getCell(31);
        Cell celdaObservaciones = fila.getCell(32)== null ? fila.createCell(32): fila.getCell(32);
        Telefono tel = new Telefono();
        UtilidadesPoi util = new UtilidadesPoi();
        tel.setNumero(util.devolverCadenaDeCelda(celdaNumero));
        tel.setObservaciones(util.devolverCadenaDeCelda(celdaObservaciones));
        tel.setTipo(Telefono.devolevertTipo(util.devolverCadenaDeCelda(celdaTipoTelefono)));
        
        return tel;
    }

   
    
    
}
