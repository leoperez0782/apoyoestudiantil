/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.interactores;

import com.apoyoestudiantil.entity.Rol;
import com.apoyoestudiantil.excepciones.AppException;
import java.io.InputStream;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * @date 12-jun-2019
 * @time 20:43:41
 * @author Leonardo Pérez
 * Clase que se encarga de gestionar la carga de archivos excel.
 */
public class GestorCargaArchivos<T> {

    private GestorRoles gestorRol;

    public GestorCargaArchivos() {
        this.gestorRol = new GestorRoles();
    }
    
    public GestorCargaArchivos(EntityManager mng) {
        this.gestorRol = new GestorRoles(mng);
    }

    /**
     * Devuelve una lista del tipo que se pasa en el parametro tipo.
     * La lista se genera a partir del archivo.
     * @param archivo
     * @param tipo
     * @param tipoCarga
     * @return
     * @throws AppException 
     */
    public List<T> cargar(InputStream archivo, FactoriaCargadoresArchivo.Tipo tipo, ICargadorArchivos.TipoCarga tipoCarga) throws AppException {
        FactoriaCargadoresArchivo factory = new FactoriaCargadoresArchivo();

        Rol r = gestorRol.getRolByName(tipo.toString());
        return factory.crearCargador(tipo).cargarDesdeArchivo(archivo, r, tipoCarga);

    }

}
