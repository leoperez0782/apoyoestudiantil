/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.interactores;

import Utilidades.Observable;
import Utilidades.Observador;
import com.apoyoestudiantil.dao.ContextoDB;
import com.apoyoestudiantil.dao.NotificacionCurricularDao;
import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Evaluacion;
import com.apoyoestudiantil.entity.Inasistencia;
import com.apoyoestudiantil.entity.ItemDesarrollo;
import com.apoyoestudiantil.entity.ItemReunion;
import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.entity.Lista;
import com.apoyoestudiantil.entity.NotificacionCurricular;
import com.apoyoestudiantil.entity.Reunion;
import com.apoyoestudiantil.excepciones.AppException;
import com.apoyoestudiantil.interactores.CargadorNotificacionAtencion.TipoNotificacionAtencion;
import com.apoyoestudiantil.interactores.SistemaLibreta.Evento;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;

/**
 * @date 08-ago-2019
 * @time 5:16:16
 * @author Leonardo Pérez
 */
public class SistemaNotificaciones implements Observador {

    private EntityManager mng;
    private NotificacionCurricularDao daoCurricular;
    private CargadorNotificacionInasistencia gestorInasistencias;

    public SistemaNotificaciones() {
        this.mng = ContextoDB.getInstancia().getManager();
        this.daoCurricular = new NotificacionCurricularDao(mng);
        this.gestorInasistencias = new CargadorNotificacionInasistencia();
    }

    @Override
    public void actualizar(Object evento, Observable origen) {
        Evento event = (Evento) evento;
        switch (event) {
            case INASISTENCIA:
                procesarInasistencias(origen);
                limpiarInasistencias(origen);
                break;
            case EVALUACION_NUEVA:
                procesarEvaluacionNueva(origen);
                break;
            case EVALUACION_CORREGIDA:
                procesarEvaluacionCorregida(origen);
                break;
            case REUNION:
                procesarCierreReuniones(origen);
            default:
                break;
        }
    }

    /**
     * Busca las inasistencias del sistemaLibreta, llama al metodo que carga las inasitencias en la notificaciones,
     * y luego llama al metodo que guarda las notificaciones.
     * @param origen 
     */
    private void procesarInasistencias(Observable origen) {
        SistemaLibreta sisLib = (SistemaLibreta) origen;
        List<Lista> listas = sisLib.getListadoInasistencia();
        List<NotificacionCurricular> notificaciones = new ArrayList<>();
        cargarInasistenciasEnNotificaciones(listas, notificaciones);
        guardarNotificaciones(notificaciones);
        //enviarNotificaciones(notificaciones);
    }

    /**
     * Crea una notificacion por cada inasistencia en cada lista de la lista de listas pasada por parametro, 
     * y la agrega a la lista de notificaciones pasada por parametro.
     * @param listas
     * @param notificaciones 
     */
    private void cargarInasistenciasEnNotificaciones(List<Lista> listas, List<NotificacionCurricular> notificaciones){
         for (Lista li : listas) {
            for (Inasistencia i : li.getInasistencias()) {
                NotificacionCurricular n = crearNotificacionInasistencia(i);
                notificaciones.add(n);
            }
        }
    }
    /**
     * Crea una notificacion curricular de inasistencia y le da formato.
     * @param i
     * @return 
     */
    private NotificacionCurricular crearNotificacionInasistencia(Inasistencia i) {
        NotificacionCurricular nueva = new NotificacionCurricular();
        gestorInasistencias.formatearNotificacionInasistencia(nueva, i);
        return nueva;
    }

    private void guardarNotificaciones(List<NotificacionCurricular> notificaciones) {
        try {
            this.daoCurricular.saveAll(notificaciones);
        } catch (AppException ex) {
            Logger.getLogger(SistemaNotificaciones.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * LLama al metodo que limpia la lista de listas actualizadas en SistemaLibreta,
     * para que en el proximo evento no se repitan las inasistencias.
     * @param origen 
     */
    private void limpiarInasistencias(Observable origen) {
        SistemaLibreta sisLib = (SistemaLibreta) origen;
        sisLib.limpiarInasistencias();
    }

    private void enviarNotificaciones(List<NotificacionCurricular> notificaciones) {
        EnviadorEmail enviador = new EnviadorEmail();
        for (NotificacionCurricular n : notificaciones) {
            enviador.eviarMensaje(n);
        }
    }

    private void procesarEvaluacionNueva(Observable origen) {
        SistemaLibreta sisLib = (SistemaLibreta) origen;
        Libreta lib = sisLib.getUltimaActualizacion();
        List<ItemDesarrollo> items = lib.getDesarrollos();
        
        //Busco el ultimo item guardado que sea evaluacion sin corregir
        ItemDesarrollo ultimaEv = buscarUltimaEvaluacionSinCorregir(items);
        List<NotificacionCurricular> notificaciones
                = crearNotificacionesNuevaEvaluacion(ultimaEv, sisLib.getUltimaActualizacion().getGrupo().getAlumnos());
        guardarNotificaciones(notificaciones);
    }

    /**
     * Busca la ultima evaluacion si corregir dentro de la lista de items que se pasa por parametro.
     * Asume que la ultima evaluacion es la que tiene mayor id.
     * @param items
     * @return 
     */
    private ItemDesarrollo buscarUltimaEvaluacionSinCorregir(List<ItemDesarrollo> items){
        final Comparator<ItemDesarrollo> comp = (i1, i2) -> Long.compare(i1.getId(), i2.getId());
        ItemDesarrollo retorno = items.stream()
                .filter(i -> i.isEvaluacion() && !(i.isCorregida()))
                .max(comp).get();
        return retorno;
    }
    /**
     * Devuelve una lista con las notificaciones para todos los destinatarios.
     * Los destinatarios son los alumnos y los tutores de los mismos.
     * La notificacion lleva todos los datos cargados.
     *
     * @param ultimaEv
     * @param alumnos
     * @return
     */
    private List<NotificacionCurricular> crearNotificacionesNuevaEvaluacion(ItemDesarrollo ultimaEv, List<Alumno> alumnos) {
        List<NotificacionCurricular> notificaciones = new ArrayList<>();
        CargadorNotificacionEvaluacion cargador = new CargadorNotificacionEvaluacion();
        for(Alumno a: alumnos){
            NotificacionCurricular notif = new NotificacionCurricular();
            cargador.formatearNotificacionEvaluacionNueva(ultimaEv, a, notif);//la notifcacion al alumno
            notificaciones.add(notif);
            NotificacionCurricular notifTutores = new NotificacionCurricular();
            cargador.formatearNotificacionNuevaEvaluacionParaTutores(a.getTutores(), ultimaEv, notifTutores);//notif del tutor
            notificaciones.add(notifTutores);
        }
        //NotificacionCurricular notif = new NotificacionCurricular();//cambiar para que sea una notificacion por usuario
        //cargador.formatearNotificacionEvaluacionNueva(ultimaEv, alumnos, notif);
        //notificaciones.add(notif);
        return notificaciones;
    }

    private void procesarEvaluacionCorregida(Observable origen) {
        SistemaLibreta sisLib = (SistemaLibreta) origen;
        ItemDesarrollo evaluacionCorregida = sisLib.getItemCorregido();
        List<NotificacionCurricular> notificaciones = crearNotificacionesEvaluacionCorregida(evaluacionCorregida);
        guardarNotificaciones(notificaciones);
    }

    private List<NotificacionCurricular> crearNotificacionesEvaluacionCorregida(ItemDesarrollo evaluacionCorregida) {
        List<NotificacionCurricular> notificaciones = new ArrayList<>();
        CargadorNotificacionEvaluacion cargador = new CargadorNotificacionEvaluacion();
        
        //Busco en la libreta las evaluaciones corregidas de esa fecha.
        List<Evaluacion> evaluaciones = evaluacionCorregida.getLibreta().getEvaluaciones().stream()
                .filter(e -> e.getFecha().equals(evaluacionCorregida.getFecha())).collect(Collectors.toList());
        for(Evaluacion e : evaluaciones){
            NotificacionCurricular notif = new NotificacionCurricular();
            NotificacionCurricular notifTutor = new NotificacionCurricular();
            cargador.formatearNotificacionEvaluacionCalificadaParaAlumno(e, notif);
            cargador.formatearNotificacionEvaluacionCalificadaTutores(e, notifTutor);
            notificaciones.add(notif);
            notificaciones.add(notifTutor);
        }
        return notificaciones;       
    }

    private void procesarCierreReuniones(Observable origen) {
        SistemaLibreta sisLib = (SistemaLibreta)origen;
        
        Libreta lib = sisLib.getUltimaActualizacion();
        
        Optional<Reunion> reunionOp = buscarUltimaReunionCerrada(lib.getReuniones());
        
        if(reunionOp.isPresent()){//si hay una reunion cerrada sin notificar
            Reunion r = reunionOp.get();
            //reviso si todas las reuniones del grupo estan cerradas
            if(sisLib.todasLasReunionesCerradas(r.getPeriodoEnLibreta(), lib)){
                List<Reunion> reunionesDelPeriodo = sisLib.getReunionesCerradasDelGrupoEnUnPeriodo(r.getPeriodoEnLibreta(), r.getLibreta());
                List<NotificacionCurricular> notificaciones = crearNotificacionesDeCierreDeReunion(reunionesDelPeriodo, lib.getGrupo().getAlumnos());
                
                guardarNotificaciones(notificaciones);
                sisLib.marcarComoNotificadas(reunionesDelPeriodo);
            }
            
        }
    }

    /**
     * Busca la ultima reunion que este cerrada, y que no haya sido notificada.
     * @param reuniones
     * @return 
     */
    private Optional<Reunion> buscarUltimaReunionCerrada(List<Reunion> reuniones) {
        final Comparator<Reunion> comp = (r1, r2) -> Long.compare(r1.getId(), r2.getId());
        Optional<Reunion> retorno = reuniones.stream()
                .filter(r -> r.isCerrada() && !(r.isNotificada()))
                .max(comp);
        return retorno;
    }

    private List<NotificacionCurricular> crearNotificacionesDeCierreDeReunion(List<Reunion> reunionesDelPeriodo, List<Alumno> alumnos) {
        List<NotificacionCurricular> lista = new ArrayList<>();
        CargadorNotificacionesCierreReunion cargador = new CargadorNotificacionesCierreReunion();
        for(Alumno a: alumnos){
            List<ItemReunion> reunionesDelAlumno = filtrarReunionesAlumno(a, reunionesDelPeriodo);
            NotificacionCurricular notif = new NotificacionCurricular();
            cargador.formatearNotificacion(notif, reunionesDelAlumno);
            lista.add(notif);
        }
        return lista;
    }

    private List<ItemReunion> filtrarReunionesAlumno(Alumno a, List<Reunion> reunionesDelPeriodo) {
        List<ItemReunion> lista = new ArrayList<>();
        reunionesDelPeriodo.forEach((r) -> {
            r.getItems().stream().filter((it) -> (it.getAlumno().equals(a))).forEachOrdered((it) -> {
                lista.add(it);
            });
        });
        return lista;
    }

    void enviarNotificacionAtencionGrupal(String valorMensajeGrup, Libreta libretaActual) {
        List<NotificacionCurricular> notificaciones = new ArrayList<>();
        List<Alumno> alumnos =libretaActual.getGrupo().getAlumnos();
        CargadorNotificacionAtencion cargador = new CargadorNotificacionAtencion();
        for(Alumno a: alumnos){
            NotificacionCurricular notif = new NotificacionCurricular();
            cargador.formatearNotificacionAtencion(notif, a, libretaActual, valorMensajeGrup, TipoNotificacionAtencion.GRUPAL);
            notificaciones.add(notif);
        }
        //faltaria enviar mail
        this.guardarNotificaciones(notificaciones);
    }

    void enviarNotificacionAtencion(String mensaje, Libreta libretaActual, int indice) {
        List<NotificacionCurricular> notificaciones = new ArrayList<>();
        Alumno a = libretaActual.getGrupo().getAlumno(indice);
        CargadorNotificacionAtencion cargador = new CargadorNotificacionAtencion();
        NotificacionCurricular notif = new NotificacionCurricular();
        cargador.formatearNotificacionAtencion(notif, a, libretaActual, mensaje,TipoNotificacionAtencion.INDIVIDUAL);
        notificaciones.add(notif);
        this.guardarNotificaciones(notificaciones);
    }

}
