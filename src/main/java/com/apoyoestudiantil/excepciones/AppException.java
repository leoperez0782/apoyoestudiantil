/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.excepciones;

/**
 * @date 01-jun-2019
 * @time 11:33:46
 * @author Leonardo Pérez
 */
public class AppException extends Exception{

    public AppException() {
    }

    public AppException(String message) {
        super(message);
    }

    
}
