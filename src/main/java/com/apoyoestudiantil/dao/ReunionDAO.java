/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.dao;

import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.entity.Reunion;
import com.apoyoestudiantil.excepciones.AppException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

/**
 * @date 19-ago-2019
 * @time 4:25:43
 * @author Leonardo Pérez
 */
public class ReunionDAO extends AbstractDAO<Reunion> {

    public ReunionDAO() {
    }

    public ReunionDAO(EntityManager mng) {
        super(mng);
    }

    @Override
    public Reunion findById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Reunion> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean save(Reunion entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(Reunion entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Reunion entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Reunion> getAllByLibreta(Libreta lib) {
        List<Reunion> lista = getManager().createNamedQuery("get_all_reuniones_by_libreta", Reunion.class)
                .setParameter("libreta", lib)
                .getResultList();
        if (lista == null) {
            lista = new ArrayList<>();
        }
        return lista;
    }

    public int getCountCerradasPorPeriodo(String periodoEnLibreta, Grupo grupo) {
        return getManager()
                .createNamedQuery("get_count_cerradas_por_periodo_y_grupo", Long.class)
                .setParameter("periodo", periodoEnLibreta)
                .setParameter("grupo", grupo)
                .getSingleResult().intValue();
//        Query consulta = getManager().createNamedQuery("get_count_cerradas_por_periodo_y_grupo", Long.class);
//        consulta.setParameter("periodo", periodoEnLibreta);
//        consulta.setParameter("grupo", grupo);
//        return (Long)consulta.getSingleResult();
    }

    public int getCountReunionesPorPeriodo(String periodoEnLibreta, Grupo grupo) {
        return getManager()
                .createNamedQuery("get_count_por_periodo_y_grupo", Long.class)
                .setParameter("periodo", periodoEnLibreta)
                .setParameter("grupo", grupo)
                .getSingleResult().intValue();
//        Query consulta = getManager().createNamedQuery("get_count_por_periodo_y_grupo", Long.class);
//        consulta.setParameter("periodo", periodoEnLibreta);
//        consulta.setParameter("grupo", grupo);
//        return (Long) consulta.getSingleResult();
    }

    public List<Reunion> getAllByPeriodoLibreta(String periodoEnLibreta, Grupo grupo) {
        //return getManager().createNamedQuery("get_all_by_grupo_y_periodo", resultClass)
        Query consulta = getManager().createNamedQuery("get_all_by_grupo_y_periodo", Reunion.class);
        consulta.setParameter("periodo", periodoEnLibreta);
        consulta.setParameter("grupo", grupo);
        return consulta.getResultList();
    }
    
    public boolean updateAll(List<Reunion> lista) throws AppException{
         int batchSize = 25;
         EntityTransaction txn = getManager().getTransaction();
        try {
            txn.begin();
            for (int i = 0; i < lista.size(); i++) {
                if (i > 0 && i % batchSize == 0) {
                    //libera la memoria
                    getManager().flush();
                    getManager().clear();
                }

                getManager().merge(lista.get(i));
            }
            txn.commit();
            return true;
        } catch (Exception e) {
            if (txn.isActive()) {
                txn.rollback();
            }
            Logger.getLogger(ReunionDAO.class.getName()).log(Level.SEVERE, null, e);
            throw new AppException("Problema al actualizar las reuniones");

        }
    }
    
    public List<Reunion> reunionesSinCerrarPorGrupo(Grupo grupo){
        return getManager().createNamedQuery("get_no_cerradas_by_grupo", Reunion.class)
                .setParameter("grupo", grupo)
                .getResultList();
    }
}
