/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.dao;

import com.apoyoestudiantil.entity.Asignatura;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * @date 08-jun-2019
 * @time 18:07:14
 * @author Leonardo Pérez
 */
public class AsignaturaDAO extends AbstractDAO<Asignatura>{

    public AsignaturaDAO() {
        super();
    }

    
    public AsignaturaDAO(EntityManager mng) {
        super(mng);
    }
    

    public Asignatura findByNombre(String nombre){
        return getManager().createNamedQuery(Asignatura.GET_ASIGNATURA_BY_NOMBRE, Asignatura.class)
                .setParameter("nombre", nombre)
                .getSingleResult();
    }
    @Override
    public Asignatura findById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Asignatura> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean save(Asignatura entity) {
        insertInsideTransaction(entity);
        return true;
    }

    @Override
    public boolean update(Asignatura entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Asignatura entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getCount() {
        return getManager().createNamedQuery(Asignatura.GET_COUNT_ALL_ASIGNATURAS, Long.class)
                .getSingleResult().intValue();
    }

}
