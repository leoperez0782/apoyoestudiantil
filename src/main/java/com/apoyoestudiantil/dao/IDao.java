/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.dao;

import java.util.List;
import java.util.Optional;

/**
 *
 * @author leo
 */
public interface IDao<T> {
    T findById(Long id);
    List<T> getAll();
    boolean save (T entity);
    boolean update(T entity);
    boolean delete(T entity);
    int getCount();
}
