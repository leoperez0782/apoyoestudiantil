/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.dao;

import com.apoyoestudiantil.entity.Notificacion;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * @date 08-ago-2019
 * @time 5:17:46
 * @author Leonardo Pérez
 */
public class NotificacionDAO extends AbstractDAO<Notificacion>{

    public NotificacionDAO() {
    }

    
    public NotificacionDAO(EntityManager mng) {
        super(mng);
    }

    @Override
    public Notificacion findById(Long id) {
        return getManager().find(Notificacion.class, id);
    }

    @Override
    public List<Notificacion> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean save(Notificacion entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(Notificacion entity) {
        updateInsideTransaction(entity);
        return true;
    }

    @Override
    public boolean delete(Notificacion entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Sobreescribe el metodo de la clase base, la unica diferencia es que el de la clase base
     * realiza la operacion persist, y este metodo realiza la operacion merge.
     * @param lista
     * @throws AppException 
     */
    @Override
    public void saveAll(List<Notificacion> lista) throws AppException {
        EntityTransaction txn = getManager().getTransaction();
        txn.begin();
        int batchSize = 25;
        try {
            for (int i = 0; i < lista.size(); i++) {
                if (i > 0 && i % batchSize == 0) {
                    //libera la memoria
                    getManager().flush();
                    getManager().clear();
                }

                getManager().merge(lista.get(i));
               
            }

            txn.commit();
        } catch (Exception e) {
            if (txn.isActive()) {
                txn.rollback();
            }
           Logger.getLogger(AbstractDAO.class.getName()).log(Level.SEVERE, null, e);
            throw new AppException("Problema al guardar los datos");
           
        }
    }
    
    public List<Notificacion> getAllByDestinatrio(Usuario u){
        return getManager().createNamedQuery("get_all_by_destinatario", Notificacion.class)
                .setParameter("usuario", u)
                .getResultList();
    }
}
