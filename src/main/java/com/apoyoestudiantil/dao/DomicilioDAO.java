/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.dao;

import com.apoyoestudiantil.entity.Domicilio;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * @date 25-may-2019
 * @time 11:00:24
 * @author Leonardo Pérez
 */
public class DomicilioDAO extends AbstractDAO<Domicilio> {

    public DomicilioDAO() {
        super();
    }

    public DomicilioDAO(EntityManager mng) {
        super(mng);
    }

    
    @Override
    public Domicilio findById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Domicilio> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean save(Domicilio entity) {
        executeInsideTransaction(em -> getManager().persist(entity));
        return true;
    }

    @Override
    public boolean update(Domicilio entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Domicilio entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public void saveAll(List<Domicilio> domicilios, EntityTransaction tx, EntityManager mng) {
        int batchSize = 25;
        try {
            for (int i = 0; i < domicilios.size(); i++) {
                if (i > 0 && i % batchSize == 0) {
                    //libera la memoria
                    mng.flush();
                    mng.clear();
                }
                Domicilio d = domicilios.get(i);
                d.setId(persistWithTransaction(tx, mng, d));
            }

           // txn.commit();
        } catch (Exception e) {
            if (tx.isActive()) {
                tx.rollback();
            }
            throw e;
        }
    }

   
}
