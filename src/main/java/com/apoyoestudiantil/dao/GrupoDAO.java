/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.dao;

import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Nivel;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * @date 26-may-2019
 * @time 22:09:47
 * @author Leonardo Pérez
 */
public class GrupoDAO extends AbstractDAO<Grupo>{

    public GrupoDAO() {
    }

    public GrupoDAO(EntityManager mng) {
        super(mng);
    }

//    public boolean addAlumnos(Grupo entity){
//        int registros= 0;
//        EntityTransaction tx = getManager().getTransaction();
//        tx.begin();
//        for(Alumno a: entity.getAlumnos())
//        
//        
//    }
    public Grupo findByNivelYAnio(Nivel nivel, int anio){
        return getManager()
                .createNamedQuery("get_grupo_by_anio_y_nivel", Grupo.class)
                .setParameter("nivel", nivel)
                .setParameter("anio", anio)
                .getSingleResult();
    }
    
   
    @Override
    public Grupo findById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Grupo> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean save(Grupo entity) {
        executeInsideTransaction(em -> getManager().persist(entity));
        return true;
    }

    @Override
    public boolean update(Grupo entity) {
        updateInsideTransaction(entity);
        return true;
    }

    @Override
    public boolean delete(Grupo entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
