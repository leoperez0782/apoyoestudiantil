/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.dao;

import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Documento;
import com.apoyoestudiantil.entity.Rol;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;

/**
 * @date 15-may-2019
 * @time 23:55:49
 * @author Leonardo Pérez
 */
public class UsuarioDAO extends AbstractDAO<Usuario> {

    public UsuarioDAO() {
        super();
    }
    
    public UsuarioDAO(EntityManager mng){
        super(mng);
    }
    public Usuario findByDocumento(Documento doc) {
        return getManager().createNamedQuery("get_Usuario_by_documento", Usuario.class)
                .setParameter("documento", doc)
                .getSingleResult();                
    }
    
    public Usuario findUsuarioByNumeroDoc(Long num)throws AppException{
        try{
            return getManager().createNamedQuery("get_Usuario_by_numero_documento", Usuario.class)
                .setParameter("num", num)
                .getSingleResult();
        }catch (NoResultException nre) {
            throw new AppException("No se encontró un usuario con esa cédula en nuestros registros");
        }        
    }

    @Override
    public Usuario findById(Long id) {
        return getManager().find(Usuario.class, id);
    }
    
    public Usuario findByEmail(String email) {
        return getManager().createNamedQuery("get_Usuario_by_email", Usuario.class)
                .setParameter("email", email)
                .getSingleResult();
    }

    @Override
    public List<Usuario> getAll() {
        return getManager().createQuery("SELECT a FROM usuarios a", Usuario.class).getResultList();
    }

    @Override
    public boolean save(Usuario entity) {
        //executeInsideTransaction(em -> getManager().persist(entity));
        insertInsideTransaction(entity);
        return true;//si llega hasta aca es por que se realizo la transaccion.
    }

    @Override
    public boolean update(Usuario entity) {
        updateInsideTransaction(entity);
        return true;
    }

    @Override
    public boolean delete(Usuario entity) {
        //executeInsideTransaction(em -> getManager().remove(entity));
        deleteInsideTransaction(entity);
        return true;//si llega hasta aca es por que se realizo la transaccion.
    }

    @Override
    public int getCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    public Alumno findAlumnoByNumeroDoc(Long num) throws AppException{
         try{
            return getManager().createNamedQuery(Alumno.GET_ALUMNO_BY_NUMERO_DOC, Alumno.class)
                .setParameter("num", num)
                .getSingleResult();
        }catch (NoResultException nre) {
            throw new AppException("No se encontró un alumno con esa cédula en nuestros registros");
        }
    }
    
    public boolean updateTr(EntityTransaction tx, EntityManager manager,Usuario entity) {
        updateWithTransaction(tx,manager,entity);
        return true;
    }
}
