/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.dao;

import com.apoyoestudiantil.entity.ItemReunion;
import com.apoyoestudiantil.entity.Reunion;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * @date 19-ago-2019
 * @time 4:45:04
 * @author Leonardo Pérez
 */
public class ItemReunionDAO extends AbstractDAO<ItemReunion> {

    public ItemReunionDAO() {
    }

    public ItemReunionDAO(EntityManager mng) {
        super(mng);
    }

    
    @Override
    public ItemReunion findById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<ItemReunion> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean save(ItemReunion entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(ItemReunion entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(ItemReunion entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<ItemReunion> getAllByReunion(Reunion r){
        List<ItemReunion> lista = getManager().createNamedQuery("get_all_items_by_reunion", ItemReunion.class)
                .setParameter("reunion", r)
                .getResultList();
        if(lista == null){
            lista = new ArrayList<>();
        }
        return lista;
    }
}
