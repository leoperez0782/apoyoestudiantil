/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.dao;

import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.entity.Grupo;
import com.apoyoestudiantil.entity.Libreta;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author leo
 */
public class LibretaDAO extends AbstractDAO<Libreta> {

    public LibretaDAO() {
        super();
    }

    public LibretaDAO(EntityManager mng) {
        super(mng);
    }

    @Override
    public Libreta findById(Long id) {
       return getManager().createNamedQuery("find_libreta_by_id", Libreta.class)
               .setParameter("id", id)
               .getSingleResult();
    }

    @Override
    public List<Libreta> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean save(Libreta entity) {
        executeInsideTransaction(em -> getManager().persist(entity));
        return true;
    }

    @Override
    public boolean update(Libreta entity) {
        updateInsideTransaction(entity);
        return true;
    }

    @Override
    public boolean delete(Libreta entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    public List<Libreta> getAllByDocente(Docente d) {
        return getManager().createNamedQuery("get_all_by_docente", Libreta.class)
                .setParameter("docente", d)
                .getResultList();
    }

    public int getCountPorGrupo(Grupo grupo) {
        return getManager().createNamedQuery("get_count_by_grupo", Long.class)
                .setParameter("grupo", grupo)
                .getSingleResult().intValue();
    }
    
    public List<Libreta> getAllByGrupo(Grupo grupo){
        return getManager().createNamedQuery("get_all_by_grupo", Libreta.class)
                .setParameter("grupo", grupo)
                .getResultList();
    }
    
    public List<Libreta> getAllByAlumno(Alumno a){
        return getManager().createNamedQuery("get_all_by_alumnno", Libreta.class)
                .setParameter("alumno", a)
                .getResultList();
    }
}
