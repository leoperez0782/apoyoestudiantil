/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.dao;

import Utilidades.UtilidadCargaListas;
import com.apoyoestudiantil.entity.Inasistencia;
import com.apoyoestudiantil.entity.Libreta;
import com.apoyoestudiantil.entity.Lista;
import com.apoyoestudiantil.excepciones.AppException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * @date 14-jul-2019
 * @time 20:14:56
 * @author Leonardo Pérez
 */
public class ListaDAO extends AbstractDAO<Lista>{

    public ListaDAO() {
    }

    public ListaDAO(EntityManager mng) {
        super(mng);
    }

    
    @Override
    public Lista findById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<Lista> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean save(Lista entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(Lista entity) {
        updateInsideTransaction(entity);
        return true;
    }

    @Override
    public boolean delete(Lista entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

     /**
     * Guarda la lista de pasajes de lista con la transaccion pasada por param. La
     * transaccion debe venir abierta y hay que realizarle el commit fuera del
     * metodo.
     *Le asigna el id creado por la bd a cada entidad de la lista.
     * @param listado
     * @param txn
     * @param mng
     */
    public void saveAll(List<Lista> listado, EntityTransaction txn, EntityManager mng) {

        int batchSize = 25;
        try {
            for (int i = 0; i < listado.size(); i++) {
                if (i > 0 && i % batchSize == 0) {
                    //libera la memoria
                    mng.flush();
                    mng.clear();
                }
                Lista l = listado.get(i);
                l.setId(persistWithTransaction(txn, mng, l));
            }

            // txn.commit();
        } catch (Exception e) {
            if (txn.isActive()) {
                txn.rollback();
            }
            throw e;
        }
    }
    
    public List<Lista> getAllByLibreta(Libreta lib) throws AppException{
        List<Lista> listado = getManager().createNamedQuery("get_all_by_libreta", Lista.class)
                .setParameter("libreta", lib)
                .getResultList();
        if(listado == null || listado.isEmpty()){
            Utilidades.UtilidadCargaListas util = new UtilidadCargaListas();
            listado = util.cargarListasVacias(lib.getHorarios());
        }
        return listado;
    }
    /**
     * Carga la lista de insasitencias correspondiente a la libreta pasada por parametro.
     * @param listaActual
     * @return 
     */
    public List<Inasistencia> cargarInasistencias(Lista listaActual) {
        List<Inasistencia> listado = getManager().createNamedQuery("get_inasistencias", Inasistencia.class)
                .setParameter("lista", listaActual)
                .getResultList();
        return listado;
    }
}
