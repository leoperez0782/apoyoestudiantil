/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.dao;

import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Documento;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * @author Polachek
 */
public class AdultoResponsableDAO extends AbstractDAO<AdultoResponsable> {

    public AdultoResponsableDAO() {
        super();
    }

    public AdultoResponsableDAO(EntityManager mng) {
        super(mng);
    }
    
    public AdultoResponsable findByDocumento(Documento doc){
         return getManager().createNamedQuery(AdultoResponsable.FIND_TUTOR_BY_DOCUMENTO, AdultoResponsable.class)
                .setParameter("doc", doc)
                .getSingleResult();
    }
    
    public AdultoResponsable findByNumeroDoc(Long num){
         return getManager().createNamedQuery(AdultoResponsable.GET_TUTOR_BY_NUMERO_DOC, AdultoResponsable.class)
                .setParameter("num", num)
                .getSingleResult();
    }
    
    @Override
    public AdultoResponsable findById(Long id) {
        return getManager().createNamedQuery(AdultoResponsable.FIND_TUTOR_BY_ID, AdultoResponsable.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public List<AdultoResponsable> getAll() {
        return getManager()
                .createNamedQuery(AdultoResponsable.GET_ALL_TUTOR, AdultoResponsable.class)
                .getResultList();
    }

    @Override
    public boolean save(AdultoResponsable entity) {
        executeInsideTransaction(em -> getManager().persist(entity));
        return true;//si llega hasta aca es por que se realizo la transaccion.
    }

    @Override
    public boolean update(AdultoResponsable entity) {
        updateInsideTransaction(entity);
        return true;
    }

    @Override
    public boolean delete(AdultoResponsable entity) {
        executeInsideTransaction(em -> getManager().remove(entity));
        return true;
    }

    @Override
    public int getCount() {
        return getManager()
                .createNamedQuery(AdultoResponsable.GET_COUNT_TUTOR, Long.class)
                .getSingleResult().intValue();
    }
    
}
