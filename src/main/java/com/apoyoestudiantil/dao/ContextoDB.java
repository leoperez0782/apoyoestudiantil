/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.dao;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * @date 19-may-2019
 * @time 21:10:44
 * @author Leonardo Pérez
 */
public class ContextoDB {
    
    private static ContextoDB instancia;
    private EntityManager manager;
    private EntityManagerFactory emf;
    
    private ContextoDB(){
        try {
            
            emf = Persistence.createEntityManagerFactory("JPA_APOYO");

        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public static ContextoDB getInstancia() {
        if(instancia == null){
            instancia = new ContextoDB();
        }
        return instancia;
    }

    public EntityManager getManager() {
        return emf.createEntityManager();
    }
    
    
}
