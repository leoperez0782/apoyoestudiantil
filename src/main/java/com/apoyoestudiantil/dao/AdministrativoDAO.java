/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.dao;

import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;

/**
 * @param <Alumno>
 * @date 18-may-2019
 * @time 16:25:44
 * @author Leonardo Pérez
 */
public class AdministrativoDAO<Administrativo extends Usuario> extends UsuarioDAO {

    public AdministrativoDAO() {
        super();
    }

    public AdministrativoDAO(EntityManager mng) {
        super(mng);
    }
    
    public List<Administrativo> get_All() throws AppException{
        try{
        return getManager()
                .createNamedQuery("get_All_Administrativo")
                .getResultList();
        }catch (NoResultException nre) {
            throw new AppException("No se pudo recuperar el listado");
        }
    }
}
