/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.dao;

import com.apoyoestudiantil.entity.Alumno;
import com.apoyoestudiantil.entity.AuxiliarMatricula;
import com.apoyoestudiantil.excepciones.AppException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import org.hibernate.Session;

/**
 * @date 10-ago-2019
 * @time 6:42:55
 * @author Leonardo Pérez
 */
public class AuxiliarMatriculaDAO extends AbstractDAO<AuxiliarMatricula> {

    public AuxiliarMatriculaDAO() {
        super();
    }

    public AuxiliarMatriculaDAO(EntityManager mng) {
        super(mng);
    }

    public AuxiliarMatricula findByAlumno(Alumno alumno) {
        return getManager().createNamedQuery("get_auxiliar_by_alumno", AuxiliarMatricula.class)
                .setParameter("alumno", alumno)
                .getSingleResult();
    }

    @Override
    public AuxiliarMatricula findById(Long id) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public List<AuxiliarMatricula> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean save(AuxiliarMatricula entity) {
        insertInsideTransaction(entity);
        return true;
    }

    @Override
    public boolean update(AuxiliarMatricula entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(AuxiliarMatricula entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     * Actualiza las entidades pasadas en la lista. La transaccion debe venir abierta 
     * y se debe realizar el commit fuera del metodo.
     *
     * @param lista
     * @param txn
     * @param mng
     * @throws AppException
     */
    public void updateAll(List<AuxiliarMatricula> lista, EntityTransaction txn, EntityManager mng) throws AppException {

        Session session = mng.unwrap(org.hibernate.Session.class);
            
        int batchSize = 25;
        try {
            for (int i = 0; i < lista.size(); i++) {
                if (i > 0 && i % batchSize == 0) {
                    //libera la memoria
                    mng.flush();
                    mng.clear();
                }

                //mng.merge(lista.get(i));
                session.saveOrUpdate(lista.get(i));
            }

        } catch (Exception e) {
            if (txn.isActive()) {
                txn.rollback();
            }
            Logger.getLogger(AbstractDAO.class.getName()).log(Level.SEVERE, null, e);
            throw new AppException("Problema al guardar los datos");

        }
    }
}
