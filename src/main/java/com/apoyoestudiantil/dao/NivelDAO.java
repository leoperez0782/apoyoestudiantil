/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.dao;

import com.apoyoestudiantil.entity.Clase;
import com.apoyoestudiantil.entity.Nivel;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 * @date 26-may-2019
 * @time 13:37:49
 * @author Leonardo Pérez
 */
public class NivelDAO extends AbstractDAO<Nivel> {

    public NivelDAO() {
    }

    public NivelDAO(EntityManager mng) {
        super(mng);
    }

    
    
    public Nivel findByClase(Clase clase){
        return getManager().createNamedQuery("get_nivel_by_clase", Nivel.class)
                .setParameter("clase", clase)
                .getSingleResult();
    }
    @Override
    public Nivel findById(Long id) {
        return getManager().find(Nivel.class, id);
    }

    @Override
    public List<Nivel> getAll() {
        //return getManager().createQuery( "from Nivel", Nivel.class ).getResultList();
        return getManager().createNamedQuery("get_all_niveles", Nivel.class)
                .getResultList();
    }

    @Override
    public boolean save(Nivel entity) {
         insertInsideTransaction(entity);
         return true;
    }

    @Override
    public boolean update(Nivel entity) {
        updateInsideTransaction(entity);
        return true;
    }
    
    public boolean updateTr(EntityTransaction tx, EntityManager manager,Nivel entity) {
        updateWithTransaction(tx,manager,entity);
        return true;
    }

    @Override
    public boolean delete(Nivel entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    

}
