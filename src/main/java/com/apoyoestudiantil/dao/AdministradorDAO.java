/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.dao;

import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

/**
 *
 * @author Polachek
 * @param <Administrador>
 */
public class AdministradorDAO <Administrador extends Usuario> extends UsuarioDAO {
    public AdministradorDAO() {
        super();
    }

    public AdministradorDAO(EntityManager mng) {
        super(mng);
    }
    
    public List<Administrador> get_All() throws AppException{
        try{
        return getManager()
                .createNamedQuery("get_All_Administrador")
                .getResultList();
        }catch (NoResultException nre) {
            throw new AppException("No se pudo recuperar el listado");
        }
    }
}
