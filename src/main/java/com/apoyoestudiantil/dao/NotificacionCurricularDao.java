/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.dao;

import com.apoyoestudiantil.entity.Notificacion;
import javax.persistence.EntityManager;

/**
 * @date 08-ago-2019
 * @time 6:30:45
 * @author Leonardo Pérez
 */
public class NotificacionCurricularDao<NotificacionCurricular extends Notificacion>  extends NotificacionDAO{

    public NotificacionCurricularDao() {
    }

    public NotificacionCurricularDao(EntityManager mng) {
        super(mng);
    }
    
}
