/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.apoyoestudiantil.dao;

import com.apoyoestudiantil.entity.Cargo;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * @date 22-jun-2019
 * @time 22:27:57
 * @author Leonardo Pérez
 */
public class CargoDAO extends AbstractDAO<Cargo>{

    public CargoDAO(EntityManager mng) {
        super(mng);
    }

    public Cargo findByTipo(String tipoCargo){
        return getManager().createNamedQuery("get_cargo_by_tipo", Cargo.class)
                .setParameter("tipo", tipoCargo)
                .getSingleResult();
    }
    @Override
    public Cargo findById(Long id) {
        return getManager().createNamedQuery("get_cargo_by_id", Cargo.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public List<Cargo> getAll() {
        return getManager().createNamedQuery("get_all_cargos", Cargo.class)
                .getResultList();
    }

    @Override
    public boolean save(Cargo entity) {
        insertInsideTransaction(entity);
        return true;
    }

    @Override
    public boolean update(Cargo entity) {
        updateInsideTransaction(entity);
        return true;
    }

    @Override
    public boolean delete(Cargo entity) {
        deleteInsideTransaction(entity);
        return true;
    }

    @Override
    public int getCount() {
       return getManager().createNamedQuery(Cargo.GET_COUNT_CARGO, Long.class)
               .getSingleResult()
               .intValue();
    }

}
