/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.dao;

import com.apoyoestudiantil.entity.Docente;
import com.apoyoestudiantil.entity.Documento;
import java.util.List;
import javax.persistence.EntityManager;

/**
 * @date 18-may-2019
 * @time 18:31:24
 * @author Leonardo Pérez
 */
public class DocenteDAO extends AbstractDAO<Docente> {

    public DocenteDAO() {
        super();
    }

    public DocenteDAO(EntityManager mng) {
        super(mng);
    }
    
    public Docente findByDocumento(Documento doc){
         return getManager().createNamedQuery(Docente.FIND_DOCENTE_BY_DOCUMENTO, Docente.class)
                .setParameter("doc", doc)
                .getSingleResult();
    }
     public Docente findByNumeroDoc(Long num){
         return getManager().createNamedQuery(Docente.GET_DOCENTE_BY_NUMERO_DOC, Docente.class)
                .setParameter("num", num)
                .getSingleResult();
    }
    @Override
    public Docente findById(Long id) {
        return getManager().createNamedQuery(Docente.FIND_DOCENTE_BY_ID, Docente.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public List<Docente> getAll() {
        return getManager()
                .createNamedQuery(Docente.GET_ALL_DOCENTE, Docente.class)
                .getResultList();
    }

    @Override
    public boolean save(Docente entity) {
        executeInsideTransaction(em -> getManager().persist(entity));
        return true;//si llega hasta aca es por que se realizo la transaccion.
    }

    @Override
    public boolean update(Docente entity) {
        updateInsideTransaction(entity);
        return true;
    }

    @Override
    public boolean delete(Docente entity) {
        executeInsideTransaction(em -> getManager().remove(entity));
        return true;
    }

    @Override
    public int getCount() {
        return getManager()
                .createNamedQuery(Docente.GET_COUNT_DOCENTE, Long.class)
                .getSingleResult().intValue();
    }

}
