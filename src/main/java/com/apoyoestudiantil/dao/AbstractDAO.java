/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.dao;

import com.apoyoestudiantil.excepciones.AppException;
import java.util.List;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceContext;
import org.hibernate.Session;

/**
 * @param <T>
 * @date 15-may-2019
 * @time 18:04:22
 * @author Leonardo Pérez
 */
public abstract class AbstractDAO<T> implements IDao<T> {

    @PersistenceContext(unitName = "JPA_APOYO")
    private EntityManager manager;
    private ContextoDB contexto;

    //private 
    public AbstractDAO() {
        try {
            this.contexto = ContextoDB.getInstancia();
            this.manager = contexto.getManager();
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public AbstractDAO(EntityManager mng) {
        try {
            //this.contexto = ContextoDB.getInstancia();
            this.manager = mng;
        } catch (Throwable e) {
            e.printStackTrace();
        }
    }

    public EntityManager getManager() {//cambiar a protected
        return manager;
    }

    public void setManager(EntityManager manager) {
        this.manager = manager;
    }

    protected void insertInsideTransaction(T entity) {
        EntityTransaction tx = getManager().getTransaction();
        try {
            tx.begin();
            getManager().persist(entity);
            tx.commit();

        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } catch (Exception e) {
            tx.rollback();
            throw e;
        }
    }

    /**
     * Ejecuta la accion que se pasa por parametro dentro de una transaccion.
     * Persist, Merge, Remove, Refresh
     *
     * @param action
     */
    protected void executeInsideTransaction(Consumer<EntityManager> action) {
        EntityTransaction tx = getManager().getTransaction();
        try {
            tx.begin();
            action.accept(getManager());
            tx.commit();

        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } catch (Exception e) {
            tx.rollback();
            throw e;
        }
    }

    protected void updateInsideTransaction(T entity) {
        EntityTransaction tx = getManager().getTransaction();
        try {
            tx.begin();
            getManager().merge(entity);
            tx.commit();

        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } catch (Exception e) {
            tx.rollback();
            throw e;
        }
    }

    protected void deleteInsideTransaction(T entity) {
        EntityTransaction tx = getManager().getTransaction();
        try {
            tx.begin();
            getManager().remove(entity);
            tx.commit();

        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } catch (Exception e) {
            tx.rollback();
            throw e;
        }
    }

    /**
     * Persiste la entidad pasada por parametro y devuelve el id de la misma, NO
     * REALIZA COMMIT. La transaccion que recibe debe haber comenzado, en caso
     * de un error al persistir, el metodo llama al rollback() de la
     * transaccion. Luego de usado este metodo, hay que realizar el commit de la
     * transaccion manualmente
     *
     * @param tx
     * @param manager
     * @param entity
     * @return
     */
    public Long persistWithTransaction(EntityTransaction tx, EntityManager manager, T entity) {
        try {
            Session session = manager.unwrap(org.hibernate.Session.class);
            Long id = (Long) session.save(entity);
            return id;
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } catch (Exception e) {
            tx.rollback();
            throw e;
        }
    }

    public void updateWithTransaction(EntityTransaction tx, EntityManager manager, T entity) {
        try {
            Session session = manager.unwrap(org.hibernate.Session.class);
            session.saveOrUpdate(entity);
            //return id;
        } catch (RuntimeException e) {
            tx.rollback();
            throw e;
        } catch (Exception e) {
            tx.rollback();
            throw e;
        }
    }

    /**
     * Metodo para guardar varias entidades en una transaccion. No asigna os id
     * a las entidades guardadas.
     *
     * @param lista
     */
    public void saveAll(List<T> lista) throws AppException {
        EntityTransaction txn = getManager().getTransaction();
        txn.begin();
        int batchSize = 25;
        try {
            for (int i = 0; i < lista.size(); i++) {
                if (i > 0 && i % batchSize == 0) {
                    //libera la memoria
                    getManager().flush();
                    getManager().clear();
                }

                getManager().persist(lista.get(i));

            }

            txn.commit();
        } catch (Exception e) {
            if (txn.isActive()) {
                txn.rollback();
            }
            Logger.getLogger(AbstractDAO.class.getName()).log(Level.SEVERE, null, e);
            throw new AppException("Problema al guardar los datos");

        }
    }

    public void updateAllWithTransaction(List<T> lista, EntityTransaction txn, EntityManager mng) throws AppException {
        Session session = mng.unwrap(org.hibernate.Session.class);

        int batchSize = 25;
        try {
            for (int i = 0; i < lista.size(); i++) {
                if (i > 0 && i % batchSize == 0) {
                    //libera la memoria
                    mng.flush();
                    mng.clear();
                }

                //mng.merge(lista.get(i));
                session.saveOrUpdate(lista.get(i));
            }

        } catch (Exception e) {
            if (txn.isActive()) {
                txn.rollback();
            }
            Logger.getLogger(AbstractDAO.class.getName()).log(Level.SEVERE, null, e);
            throw new AppException("Problema al guardar los datos");

        }
    }
}
