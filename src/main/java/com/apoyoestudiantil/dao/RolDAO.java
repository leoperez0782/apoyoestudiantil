/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.dao;

import com.apoyoestudiantil.entity.Rol;
import java.util.List;
import java.util.Optional;
import javax.persistence.EntityManager;

/**
 * @date 23-may-2019
 * @time 23:34:13
 * @author Leonardo Pérez
 */
public class RolDAO extends AbstractDAO<Rol> {

    public RolDAO() {
        super();
    }

    public RolDAO(EntityManager mng) {
        super(mng);
    }

    
    public Rol findByRol(String rol){
        return getManager().createNamedQuery("get_rol_by_rol", Rol.class)
                .setParameter("rol", rol)
                .getSingleResult();
    }
    @Override
    public Rol findById(Long id) {
        return getManager().find(Rol.class, id);
    }

    @Override
    public List<Rol> getAll() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean save(Rol entity) {
        executeInsideTransaction(em -> getManager().persist(entity));
        return true;//si llega hasta aca es por que se realizo la transaccion.
    }

    @Override
    public boolean update(Rol entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(Rol entity) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int getCount() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
