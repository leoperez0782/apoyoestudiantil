/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apoyoestudiantil.dao;

import com.apoyoestudiantil.entity.AdultoResponsable;
import com.apoyoestudiantil.entity.Nivel;
import com.apoyoestudiantil.entity.Usuario;
import com.apoyoestudiantil.excepciones.AppException;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;

/**
 * @param <Alumno>
 * @date 18-may-2019
 * @time 16:25:44
 * @author Leonardo Pérez
 */
public class AlumnoDAO<Alumno extends Usuario> extends UsuarioDAO {

    public AlumnoDAO() {
        super();
    }

    public AlumnoDAO(EntityManager mng) {
        super(mng);
    }

    public List<Alumno> getAllbyNivel(Nivel nivel) {
        return getManager()
                .createNamedQuery("get_all_by_nivel")
                .setParameter("nivel", nivel)
                .getResultList();

    }
    
    
    public List<Alumno> getAlumnosByTutor(AdultoResponsable tutor)throws AppException{
        try{
            return getManager()
                .createNamedQuery("get_alumnos_by_tutor")
                .setParameter("tutor", tutor)
                .getResultList();
        }catch (NoResultException nre) {
            throw new AppException("No se encontron estudiantes designados a este tutor");
        }
    }
//    public void saveAll(List<Alumno> lista) {
//        EntityTransaction txn = getManager().getTransaction();
//        txn.begin();
//        int batchSize = 25;
//        try {
//            for (int i = 0; i < lista.size(); i++) {
//                if (i > 0 && i % batchSize == 0) {
//                    //libera la memoria
//                    getManager().flush();
//                    getManager().clear();
//                }
//
//                //getManager().persist(lista.get(i));
//                Alumno a = lista.get(i);
//                a.setId(persistWithTransaction(txn, getManager(), a));
//            }
//
//            txn.commit();
//        } catch (Exception e) {
//            if (txn.isActive()) {
//                txn.rollback();
//            }
//            throw e;
//        }
//    }

    /**
     * Guarda la lista de alumnos con la transaccion pasada por param. La
     * transaccion debe venir abierta y hay que realizarle el commit fuera del
     * metodo.
     *Le asigna el id creado por la bd a cada entidad de la lista.
     * @param lista
     * @param txn
     * @param mng
     */
    public void saveAll(List<Alumno> lista, EntityTransaction txn, EntityManager mng) {

        int batchSize = 25;
        try {
            for (int i = 0; i < lista.size(); i++) {
                if (i > 0 && i % batchSize == 0) {
                    //libera la memoria
                    mng.flush();
                    mng.clear();
                }
                Alumno a = lista.get(i);
                a.setId(persistWithTransaction(txn, mng, a));
            }

            // txn.commit();
        } catch (Exception e) {
            if (txn.isActive()) {
                txn.rollback();
            }
            throw e;
        }
    }

//    @Override
//    public Alumno findById(Long id) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public List<Alumno> getAll() {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public boolean save(Alumno entity) {
//       executeInsideTransaction(em -> getManager().persist(entity));
//       return true;
//    }
//
//    @Override
//    public boolean update(Alumno entity) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public boolean delete(Alumno entity) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
    @Override
    public int getCount() {
        return getManager()
                .createNamedQuery("get_count_alumno", Long.class
                )
                .getSingleResult().intValue();
    }
}
