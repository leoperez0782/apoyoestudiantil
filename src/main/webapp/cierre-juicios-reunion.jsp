<%-- 
    Document   : cierre-juicios-reunion
    Created on : 27-ago-2019, 17:46:01
    Author     : Leonardo Pérez
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <c:import url="/parts/head.jsp">

        </c:import>
    </head>
    <body>
        <div class="container-fluid">
            <c:import url="/parts/header.jsp">

            </c:import> 
            <form method="post" action="cierrejuiciosreunion">
                <!--inicio mensajes -->
                <div class="form-row">
                    <fieldset class="form-group">
                        <c:choose>
                            <c:when test="${param.evento eq 'Exito'}">
                                <div class="bg-success">
                                    <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                                </div>

                            </c:when>
                            <c:when test="${param.evento eq 'Error'}">
                                <div class="bg-danger">
                                    <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                                </div> 
                            </c:when>
                            <c:otherwise>
                                <label id="lblAviso"></label>
                            </c:otherwise>
                        </c:choose>
                    </fieldset>
                </div>
                <!--fin mensajes -->
                <!-- inicio listado alumnos -->
                <div class="form-row">
                    <div class="form-group">
                        <div class="panel panel-primary">
                             <div class="panel-heading">
                                <h3 class="panel-title">Cierre juicios de reunion</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsivetable">
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </body>
</html>
