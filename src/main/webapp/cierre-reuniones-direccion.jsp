<%-- 
    Document   : cierre-reuniones-direccion
    Created on : 26-ago-2019, 19:27:31
    Author     : Leonardo Pérez
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <c:import url="/parts/head.jsp">

        </c:import>
        <script type="text/javascript">
            var vistaWeb = new EventSource("cierrereunionesdireccion");

            //Mostrar Niveles
            vistaWeb.addEventListener("mostrarNiveles", function (evento) {
                document.getElementById("listaNiveles").innerHTML = evento.data;
            }, false);
            //Mostrar Grupos
            vistaWeb.addEventListener("mostrarGrupos", function (evento) {
                document.getElementById("listaGrupos").innerHTML = evento.data;
            }, false);
            //Mostrar periodos
            vistaWeb.addEventListener("mostrarPeriodos", function (evento) {
                document.getElementById("listaPeriodos").innerHTML = evento.data;
            }, false);
            vistaWeb.addEventListener("mostrarReuniones", function (evento) {
                document.getElementById("listaMaterias").innerHTML = evento.data;
            }, false);
            vistaWeb.addEventListener("cargarAlumnos", function (evento) {
                document.getElementById("cuerpoTabla").innerHTML = evento.data;
            }, false);
            vistaWeb.addEventListener("mostrarAlert", function (evento) {
                alert(evento.data);
            }, false);
            function nivelSeleccionado(e, element) {
                e.preventDefault();
                if ($(element).closest("li").hasClass("selected")) {
                    $("#sel-niveles ul li").removeClass("selected");
                    $("#sel-niveles ul").slideUp("slow", function () {
                        $("#sel-niveles ul li").show();
                        $("#sel-niveles ul").slideDown("slow", function () {});
                    });
                } else {
                    $("#sel-niveles ul li").removeClass("selected");
                    $(element).closest("li").addClass('selected');
                    $("#sel-niveles ul").slideUp("slow", function () {
                        $("#sel-niveles ul li").hide();
                        $("#sel-niveles ul li.selected").show();
                        $("#sel-niveles ul").slideDown("slow", function () {});
                        $("#pasos #pasos_paso_2 a").click();
                        $.get("cierrereunionesdireccion?accion=nivels&nivelseleccionado=" + $("#sel-niveles ul li.selected").attr("data-nivel-id"), function (data) {});
                    });
                }
            }
            function grupoSeleccionado(e, element) {
                e.preventDefault();
                $("#sel-grupo ul li").removeClass("selected");
                $(element).closest("li").addClass('selected');
                $.get("cierrereunionesdireccion?accion=grupo&gruposeleccionado=" + $("#sel-grupo ul li.selected").attr("data-grupo-id"), function (data) {});
            }

            function periodoSeleccionado(e, element) {
                e.preventDefault();
                $("#sel-periodo ul li").removeClass("selected");
                $(element).closest("li").addClass('selected');
                $.get("cierrereunionesdireccion?accion=periodo&periodoseleccionado=" + $("#sel-periodo ul li.selected").attr("data-periodo-id"), function (data) {});
            }
            function pasoSeleccionado(e, element) {
                e.preventDefault();
                $(".paso_content").hide();
                $("#pasos .paso").removeClass("selected");
                $(element).closest("div").addClass('selected');
                var selectedID = $("#pasos .selected").attr('id');
                switch (selectedID) {
                    case 'pasos_paso_1':
                        $(".paso_1").show();
                        break;
                    case 'pasos_paso_2':
                        $(".paso_2").show();
                        break;
                    case 'pasos_paso_3':
                        $(".paso_3").show();
                        break;
                    case 'pasos_paso_4':
                        $(".paso_4").show();
                        break;
                    case 'pasos_paso_5':
                        $(".paso_5").show();
                        break;
                }
            }
            function siguientePaso(e, paso_id) {
                e.preventDefault();
                $(paso_id).click();
            }

            $("#btnEviar").click(function (event) {
                event.preventDefault();
                var formulario = document.getElementById("form-juicios");
                formulario.submit();
            });
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <c:import url="/parts/header.jsp">

            </c:import>  
            <div>
                <c:choose>
                    <c:when test="${param.evento eq 'Exito'}">
                        <div class="bg-success">
                            <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                        </div>

                    </c:when>
                    <c:when test="${param.evento eq 'Error'}">
                        <div class="bg-danger">
                            <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                        </div> 
                    </c:when>
                    <c:otherwise>
                        <label id="lblAviso"></label>
                    </c:otherwise>
                </c:choose>
            </div>
            <div id="pasos">
                <div class="paso selected" id="pasos_paso_1">
                    <a href="#" onclick="pasoSeleccionado(event, this)">Paso 1</a>
                </div>
                <div class="paso" id="pasos_paso_2">
                    <a href="#" onclick="pasoSeleccionado(event, this)">Paso 2</a>
                </div>
                <div class="paso" id="pasos_paso_3">
                    <a href="#" onclick="pasoSeleccionado(event, this)">Paso 3</a>
                </div>
                <div class="paso" id="pasos_paso_4">
                    <a href="#" onclick="pasoSeleccionado(event, this)">Paso 4</a>
                </div>

            </div>

            <section id="sel-niveles" class="paso_1 paso_content">
                <h4>Seleccione el nivel</h4>
                <ul id="listaNiveles">

                </ul>
                <div class="paso_siguiente_cont">
                    <a href="#" class="paso_siguiente" onclick="siguientePaso(event, '#pasos #pasos_paso_2 a')">Siguiente</a>
                </div>
            </section>

            <section id="sel-grupo" class="paso_2 paso_content" style="display: none;">
                <h4>Seleccione el Grupo - <a href="#" onclick="siguientePaso(event, '#pasos #pasos_paso_3 a')">Siguiente</a></h4>
                <ul id="listaGrupos">

                </ul>
                <div class="paso_siguiente_cont">
                    <a href="#" class="paso_siguiente" onclick="siguientePaso(event, '#pasos #pasos_paso_3 a')">Siguiente</a>
                </div>

            </section>
            <section id="sel-periodo" class="paso_3 paso_content" style="display: none;">
                <h4>Seleccione el periodo - <a href="#" onclick="siguientePaso(event, '#pasos #pasos_paso_4 a')">Siguiente</a></h4>
                <ul id="listaPeriodos">

                </ul>
                <div class="paso_siguiente_cont">
                    <a href="#" class="paso_siguiente" onclick="siguientePaso(event, '#pasos #pasos_paso_4 a')">Siguiente</a>
                </div>

            </section>
            <section id="sel-materias" class="paso_4 paso_content" style="display: none;">
                <h4>Materias sin cierre de promedios - <a href="#" onclick="siguientePaso(event, '#pasos #pasos_paso_4 a')">Siguiente</a></h4>
                <ul id="listaMaterias">

                </ul>
                <div >
                    <form method="post" action="cierrereunionesdireccion" id="form-juicios">

                        <div class="form-row">
                            <fieldset class="form-group">
                                <h3>Agregar juicios</h3>
                            </fieldset>
                        </div>

                        <!-- inicio listado alumnos -->
                        <div class="form-row">
                            <div class="form-group">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Cierre juicios de reunion</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="table-responsivetable">
                                            <table class="table table-bordered table-hover">
                                                <thead>
                                                <td>Matricula</td>
                                                <td>Documento</td>
                                                <td>Nombre </td>
                                                <td>Juicio Reunion</td>
                                                </thead>
                                                <tbody id="cuerpoTabla">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-row">
                            <!--<div class="form-group">
                                <button class="btn btn-primary" id="btnGuardar" onclick="enviar(event)" type="submit">
                                    <span class="glyphicon glyphicon-floppy-disk"></span> Guardar todos los cambios  
                                </button>-->
                            <div class="form-group">
                                <input  class="btn btn-primary" type="submit" id="btnEnviar" />
                            </div>
                        </div>
                </div>
                </form>
        </div>

    </section>
</div>
</body>
</html>
