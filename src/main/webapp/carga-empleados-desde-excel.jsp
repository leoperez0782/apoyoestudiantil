<%-- 
    Document   : carga-empleados-desde-excel
    Created on : 21 sept. 2019, 23:23:51
    Author     : Leonardo Pérez
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <c:import url="/parts/head.jsp">

        </c:import>
        
    </head>
    <body>
        <div class="container-fluid">
            <c:import url="/parts/header-docente.jsp">

            </c:import> 
            <form action="uploadempleados" method="post" enctype="multipart/form-data">
                 <div class="form-row">
                    <fieldset class="form-group">
                        <c:choose>
                            <c:when test="${param.evento eq 'Exito'}">
                                <div class="bg-success">
                                    <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                                </div>

                            </c:when>
                            <c:when test="${param.evento eq 'Error'}">
                                <div class="bg-danger">
                                    <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                                </div> 
                            </c:when>
                            <c:otherwise>
                                <label id="lblAviso"></label>
                            </c:otherwise>
                        </c:choose>
                    </fieldset>
                </div>
                <div class="form-row">
                    <div class="form-group">
                        <!--<input class="form-control" type="text" name="description" />-->
                        <input class="form-control-file" type="file" name="file" />

                    </div>
                </div>
                <div class="form-row">
                    <select id="slcCargo" class="form-control" name="slcCargo">
                        <option value ="seleccione el cargo">Seleccione el cargo</option>
                        <c:forEach var="cargo" items="${listaCargos}">
                            <option value="${cargo.id}">${cargo.tipoCargo}</option>
                        </c:forEach>
                    </select>
                </div>
                <div class="form-row">
                    <select id="slcRol" class="form-control" name="slcRol">
                        <option value ="seleccione el Rol">Seleccione el rol</option>
                        <option value ="ADMINISTRADOR">DIRECTIVO - ADMINISTRADOR</option>
                        <option value ="ADMINISTRATIVO">ADMINISTRATIVO</option>
                    </select>
                </div>
                <div class="form-row">
                    <div class="form-group">
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="optradio" value="Simple">Carga Simple
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" checked="true" name="optradio" value="Completa">Carga completa
                            </label>
                        </div>
                    </div>
                    <input  class="btn btn-primary" type="submit" />
                </div>
            </form>
        </div>
    </body>
</html>
