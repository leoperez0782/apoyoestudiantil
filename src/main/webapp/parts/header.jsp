<%-- 
    Document   : header
    Created on : 09-jun-2019, 19:27:10
    Author     : Polachek
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<header class="main-header">
    <nav class="navbar">
        <div class="container-fluid">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">
                    <img src="resources/images/ap-logo.png" alt="Apoyo Estudiantil"/>
                </a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li><a id="usuario" href="#">${usuario.primerNombre} ${usuario.primerApellido}</a></li>
                        <c:forEach var="rol" items="${usuario.roles}">
                            <c:choose>
                                <c:when test="${rol.rol.rol eq 'ADMINISTRADOR'}">
                                <li>
                                    <a href="administrador.jsp">Inicio Administrador</a>
                                </li>
                            </c:when>
                                <c:when test="${rol.rol.rol eq 'ADMINISTRATIVO'}">
                                <li>
                                    <a href="inicio-administrativo.jsp">Inicio Administrativo</a>
                                </li>
                            </c:when>
                                <c:when test="${rol.rol.rol eq 'DOCENTE'}">
                                <li>
                                    <a href="cargargruposdocente">Inicio Docente</a>
                                </li>
                            </c:when>
                                <c:when test="${rol.rol.rol eq 'ESTUDIANTE'}">
                                <li>
                                    <a href="inicio-alumno.jsp">Inicio Estudiante</a>
                                </li>
                            </c:when>
                                <c:when test="${rol.rol.rol eq 'ADULTO_RESPONSABLE'}">
                                <li>
                                    <a href="inicio-tutor.jsp">Inicio Tutor</a>
                                </li>
                            </c:when>
                        </c:choose>
                    </c:forEach>

                    <li><a href="login?accion=logout">Cerrar sesión</a></li>        
                </ul>
            </div>
        </div>
    </nav>
</header>