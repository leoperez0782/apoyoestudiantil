<%-- 
    Document   : index-docente
    Created on : 30-jul-2019, 5:53:54
    Author     : Leonardo Pérez
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <c:import url="/parts/head.jsp"></c:import>

        </head>
        <body>
            <div class="container-fluid">
            <c:import url="/parts/header-docente.jsp">

            </c:import>
            <div id ="contenedor-grupo">
                <div class ="row">
                    <c:choose>
                        <c:when test="${param.evento eq 'Exito'}">
                            <div class="bg-success">
                                <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                            </div>

                        </c:when>
                        <c:when test="${param.evento eq 'Error'}">
                            <div class="bg-danger">
                                <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                            </div> 
                        </c:when>
                        <c:otherwise>
                            <label id="lblAviso"></label>
                        </c:otherwise>
                    </c:choose>
                </div>
                <c:forEach var="libreta" items="${listaLibretas}" varStatus="loop">
                    <div class="row">
                        <ul class="nav nav-tabs">
                            <li role="presentation" class="active"><a  href="
                                                                       <c:url value='listadogrupo'>
                                                                           <c:param name="index" value="${loop.index}"/>
                                                                       </c:url>"
                                                                       >Pasar lista</a></li>
                            <li role="presentation"><a  href="
                                                        <c:url value='evaluargrupo'>
                                                            <c:param name="index" value="${loop.index}"/>
                                                        </c:url>"
                                                        >Evaluar todo el grupo</a></li>
                            <li role="presentation"><a href="
                                                       <c:url value="cerrarpromedios">
                                                           <c:param name="index" value="${loop.index}"/>                  
                                                       </c:url>"
                                                       >Cerrar promedios</a></li>
                            <li role="presentation"><a href="
                                                       <c:url value="notificacionatencion">
                                                           <c:param name="index" value="${loop.index}"/>                  
                                                       </c:url>"
                                                       >Notificacion atencion sobre la materia</a></li>
                        </ul>
                    </div>
                    <div class="row">
                        <div class="table-responsivetable">
                            <table class="table table-bordered table-hover">
                                <thead>
                                <td>Clase</td>
                                <td>Turno</td>
                                <td>Salon</td>
                                <td>Asignatura</td>

                                </thead>
                                <tbody>

                                    <tr>
                                        <td>${libreta.grupo.nivel.clase.nivel} ${libreta.grupo.nivel.clase.codigoClase}</td>  
                                        <td>${libreta.turno}</td>  
                                        <td>${libreta.salon}</td>  
                                        <td>${libreta.materia.nombre}</td>

                                    </tr>

                            </table>
                        </div>
                    </div>
                    <hr>
                </c:forEach>
            </div>
        </div>

    </body>
</html>
