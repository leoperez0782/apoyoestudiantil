<%-- 
    Document   : carga-alumnos-excel
    Created on : 21-ago-2019, 18:36:42
    Author     : Leonardo Pérez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@ include file="parts/head.jsp" %>
        <script type="text/javascript">
            var vistaWeb = new EventSource("uploadalumnosexcel");

            //Mostrar Niveles
            vistaWeb.addEventListener("mostrarNiveles", function (evento) {
                document.getElementById("listaNiveles").innerHTML = evento.data;
            }, false);
            //Mostrar Grupos
            vistaWeb.addEventListener("mostrarGrupos", function (evento) {
                document.getElementById("listaGrupos").innerHTML = evento.data;
            }, false);

            vistaWeb.addEventListener("mostrarAlert", function (evento) {
                alert(evento.data);
            }, false);
            function nivelSeleccionado(e, element) {
                e.preventDefault();
                if ($(element).closest("li").hasClass("selected")) {
                    $("#sel-niveles ul li").removeClass("selected");
                    $("#sel-niveles ul").slideUp("slow", function () {
                        $("#sel-niveles ul li").show();
                        $("#sel-niveles ul").slideDown("slow", function () {});
                    });
                } else {
                    $("#sel-niveles ul li").removeClass("selected");
                    $(element).closest("li").addClass('selected');
                    $("#sel-niveles ul").slideUp("slow", function () {
                        $("#sel-niveles ul li").hide();
                        $("#sel-niveles ul li.selected").show();
                        $("#sel-niveles ul").slideDown("slow", function () {});
                        $("#pasos #pasos_paso_2 a").click();
                        $.get("uploadalumnosexcel?accion=nivels&nivelseleccionado=" + $("#sel-niveles ul li.selected").attr("data-nivel-id"), function (data) {});
                    });
                }
            }

            function grupoSeleccionado(e, element) {
                e.preventDefault();
                $("#sel-grupo ul li").removeClass("selected");
                $(element).closest("li").addClass('selected');
                $.get("uploadalumnosexcel?accion=grupo&gruposeleccionado=" + $("#sel-grupo ul li.selected").attr("data-grupo-id"), function (data) {});
            }

            function pasoSeleccionado(e, element) {
                e.preventDefault();
                $(".paso_content").hide();
                $("#pasos .paso").removeClass("selected");
                $(element).closest("div").addClass('selected');
                var selectedID = $("#pasos .selected").attr('id');
                switch (selectedID) {
                    case 'pasos_paso_1':
                        $(".paso_1").show();
                        break;
                    case 'pasos_paso_2':
                        $(".paso_2").show();
                        break;
                    case 'pasos_paso_3':
                        $(".paso_3").show();
                        break;
                    case 'pasos_paso_4':
                        $(".paso_4").show();
                        break;
                    case 'pasos_paso_5':
                        $(".paso_5").show();
                        break;
                }
            }
            function siguientePaso(e, paso_id) {
                e.preventDefault();
                $(paso_id).click();
            }
            $("#btnEviar").click(function (event) {
                event.preventDefault();
                var formulario = document.getElementById("formulario-upload");
                formulario.submit();
            });
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <%@ include file="parts/header.jsp" %>
            <div>
                <c:choose>
                    <c:when test="${param.evento eq 'Exito'}">
                        <div class="bg-success">
                            <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                        </div>

                    </c:when>
                    <c:when test="${param.evento eq 'Error'}">
                        <div class="bg-danger">
                            <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                        </div> 
                    </c:when>
                    <c:otherwise>
                        <label id="lblAviso"></label>
                    </c:otherwise>
                </c:choose>
            </div>
            <div id="pasos">
                <div class="paso selected" id="pasos_paso_1">
                    <a href="#" onclick="pasoSeleccionado(event, this)">Paso 1</a>
                </div>
                <div class="paso" id="pasos_paso_2">
                    <a href="#" onclick="pasoSeleccionado(event, this)">Paso 2</a>
                </div>
                <div class="paso" id="pasos_paso_3">
                    <a href="#" onclick="pasoSeleccionado(event, this)">Paso 3</a>
                </div>

            </div>

            <section id="sel-niveles" class="paso_1 paso_content">
                <h4>Seleccione el nivel</h4>
                <ul id="listaNiveles">

                </ul>
                <div class="paso_siguiente_cont">
                    <a href="#" class="paso_siguiente" onclick="siguientePaso(event, '#pasos #pasos_paso_2 a')">Siguiente</a>
                </div>
            </section>

            <section id="sel-grupo" class="paso_2 paso_content" style="display: none;">
                <h4>Seleccione el Grupo - <a href="#" onclick="siguientePaso(event, '#pasos #pasos_paso_3 a')">Siguiente</a></h4>
                <ul id="listaGrupos">

                </ul>
                <div class="paso_siguiente_cont">
                    <a href="#" class="paso_siguiente" onclick="siguientePaso(event, '#pasos #pasos_paso_3 a')">Siguiente</a>
                </div>

            </section>

            <section id="formulario-upload" class="paso_3 paso_content" style="display: none;" >
                <form action="uploadalumnosexcel" enctype="multipart/form-data" method="post">
                    <div class="form-group">
                        <!--<input class="form-control" type="text" name="description" />-->
                        <input class="form-control-file" type="file" name="file" />

                    </div>
                    <div class="form-group">
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="optradio" value="Simple">Carga Simple
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" checked="true" name="optradio" value="Completa">Carga completa
                            </label>
                        </div>
                    </div>
                    <div class="form-group">
                        <input  class="btn btn-primary" type="submit" id="btnEnviar" />
                    </div>

                </form>
            </section>
        </div>
    </body>
</html>
