<%-- 
    Document   : modificar-telefonos
    Created on : 01-jul-2019, 17:27:05
    Author     : leo
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@ include file="parts/head.jsp" %>
    </head>
    <body>
        <div class="container-fluid">
            <%@ include file="parts/header.jsp" %>
            <form action="modificartelefonos" method="post">
                <fieldset class="form-group"> 
                    <legend>Informacion sobre los telefonos del docente: ${docenteAmodificar.primerNombre} ${docenteAmodificar.primerApellido}</legend>
                </fieldset>
                <c:forEach var="telefono" items="${telefonosAmodificar}" varStatus="loop">
                    <div id="telefonos" class="form-row">
                        <fieldset id="${telefono.id}">
                            <legend >Telefono ${loop.index}</legend>
                            <div  class="form-group">
                                <div class="col-md-2">
                                    <label for="slcTipoTel">Numero telefono</label>
                                    <select class="form-control" id="slcTipoTel${telefono.id}" name="tipoDoc">
                                        <option selected>${telefono.tipo}</option>
                                        <option value="MOVIL">Movil</option>
                                        <option value="FIJO">Fijo</option>

                                    </select>


                                </div> 
                                <div class="col-md-4">
                                    <label for="txtNumeroTel">Numero telefono</label>
                                    <input type="text" class="form-control" id="txtNumeroTel${telefono.id}" name="numeroTel" value="${telefono.numero}" >
                                </div>
                                <div class="col-md-4">
                                    <label for="txtObservaciones">Observaciones</label>
                                    <input type="text" class="form-control" id="txtObservaciones${telefono.id}" name="observaciones" value="${telefono.observaciones}" >
                                </div>
                                <div class="col-md-2">

                                    <button class="btn btn-primary btnDel" id="btn${telefono.id}"><span class="glyphicon glyphicon-minus-sign"></span> Eliminar</button>
                                </div>
                            </div>
                        </fieldset>
                    </div>
                </c:forEach>



                <div class="form-row">
                    <div class="col-md-6">
                        <button class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span> Guardar cambios  
                        </button>
                    </div>
                    <div class="col-md-6">
                        <button id="btnAddTel" class="btn btn-primary" type="submit">
                            <span class="glyphicon glyphicon-plus-sign"></span> Agregar telefono  
                        </button>
                    </div>
                </div>
                <input type="hidden" id="listaEliminados" name="listaEliminados" value="">
                <input type="hidden" id="listaAgregados" name="listaTelefonosAgregados" value="">
            </form>
            <div class="form-row">
                <fieldset class="form-group">
                    <c:choose>
                        <c:when test="${param.evento eq 'Exito'}">
                            <div class="bg-success">
                                <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                            </div>

                        </c:when>
                        <c:when test="${param.evento eq 'Error'}">
                            <div class="bg-danger">
                                <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                            </div> 
                        </c:when>
                        <c:otherwise>
                            <label id="lblAviso"></label>
                        </c:otherwise>
                    </c:choose>
                </fieldset>
            </div>

        </div>
        <script type="text/javascript">
            $(".btnDel").on("click", function (event) {
                event.preventDefault();
                var idBoton = $(this).attr("id");
                var idAOcultar = idBoton.substring(3);
                var idsAremover = $("#listaEliminados").val();
                idsAremover += idAOcultar + ":";
                $("#listaEliminados").val(idsAremover);
                $("#" + idAOcultar).hide();
            });
            $("#btnAddTel").on("click", function (event) {
                event.preventDefault();

            });
        </script>
    </body>
</html>
