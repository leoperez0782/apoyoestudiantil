<%-- 
    Document   : error-page-403
    Created on : 4 sept. 2019, 0:26:04
    Author     : Leonardo Pérez
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page isErrorPage="true"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Error 403</title>
         <%@ include file="parts/head.jsp" %>
    </head>
    <body>
        <h1>Esta tratando de acceder a una página a la que no tiene permiso</h1>
        <div>
            ${error.message}
        </div>
        <div >
            <img src="resources/images/ap-logo.jpg" />
        </div>
    </body>
</html>
