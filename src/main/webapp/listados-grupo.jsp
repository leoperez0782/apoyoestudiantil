<%-- 
    Document   : listados-grupo
    Created on : 12-jul-2019, 20:07:53
    Author     : Leonardo Pérez
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <c:import url="/parts/head.jsp"></c:import>
        </head>
        <body>
            <div class="container-fluid">
           <c:import url="/parts/header-docente.jsp"></c:import>
            <div class="row">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title"> Listados del grupo</h3>
                    </div>
                    <div class="panel-body">
                        <p>Abajo se muestran las listas del grupo. Se pueden modificar las que ya fueron pasadas o simplemente pasar lista.</p>
                        
                    </div>
                </div>
            </div>
                <table class="table table-bordered table-hover">
                    <thead>
                    <td>Fecha</td>
                    <td>Paso</td>
                    <td>Dia</td>
                    <td>Hora inicio</td>
                    <td>Hora Fin</td>
                    <td>Accion</td>
                    </thead>
                    <tbody>
                    <c:forEach var="lista" items="${pasajesLista}" varStatus="loop">
                        <tr>
                            <td>${lista.fecha}</td>  
                            <td>${lista.pasada}</td>  
                            <td>${lista.horario.dia}</td>  
                            <td>${lista.horario.horaInicio}</td>
                            <td>${lista.horario.horaFin}</td>
                            <!--<td>
                                <a class="btn btn-primary" href="
                                   <c:url value='pasajelista'>
                                       <c:param name="index" value="${loop.index}"/>
                                   </c:url>"
                                   >Pasar lista</a>
                            </td> --> 
                            <td>
                                <c:choose>
                                    <c:when test="${lista.pasada}">
                                        <!--<a class="btn disabled" href="
                                           #"
                                           >Lista pasada</a>-->
                                        <a class="btn btn-warning" href="
                                           <c:url value='modificarlista'>
                                               <c:param name="index" value="${loop.index}"/>
                                           </c:url>"
                                           >Modificar lista</a>
                                    </c:when>
                                    <c:otherwise>
                                        <a class="btn btn-primary" href="
                                           <c:url value='pasajelista'>
                                               <c:param name="index" value="${loop.index}"/>
                                           </c:url>"
                                           >Pasar lista</a>
                                    </c:otherwise>
                                </c:choose>
                            </td>
                        </tr>
                    </c:forEach>
            </table>
        </div>
    </div>

</body>
</html>
