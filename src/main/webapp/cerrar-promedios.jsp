<%-- 
    Document   : cerrar-promedios
    Created on : 10-ago-2019, 9:11:17
    Author     : Leonardo Pérez
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <c:import url="/parts/head.jsp">

        </c:import>
        
    </head>
    <body>
        <div class="container-fluid">
            <c:import url="/parts/header-docente.jsp">

            </c:import> 
            <form action="cerrarpromedios" method="post" id="formulario">
                <!--inicio mensajes -->
                <div class="form-row">
                    <fieldset class="form-group">
                        <c:choose>
                            <c:when test="${param.evento eq 'Exito'}">
                                <div class="bg-success">
                                    <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                                </div>

                            </c:when>
                            <c:when test="${param.evento eq 'Error'}">
                                <div class="bg-danger">
                                    <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                                </div> 
                            </c:when>
                            <c:otherwise>
                                <label id="lblAviso"></label>
                            </c:otherwise>
                        </c:choose>
                    </fieldset>
                </div>
                <!--fin mensajes -->
                <!-- Inicio listado alumnos -->
                <div class="form-row" id="panelListadoAlumnos">
                    <div class="form-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Cierre de promedios</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsivetable">
                                    <!-- Listado alumnos -->
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        <td>Foto alumno</td>
                                        <td>Nombre alumno</td>
                                        <td>Inasistencias</td>
                                        <!-- Cargo las cabeceras con los periodos de la libreta -->
                                        <c:forEach var="periodo" items="${libretaActual.getPeriodos()}">
                                            <td>${periodo.getNombre()}</td>
                                        </c:forEach>

                                        </thead>
                                        <tbody id="tablaAlumnos">
                                            <c:forEach var="alumno" items="${libretaActual.grupo.alumnos}" varStatus="loop">
                                                <tr id="${loop.index}">
                                                    <td>
                                                        Foto
                                                    </td>
                                                    <td>
                                                        ${alumno.primerApellido} ${alumno.segundoApellido}, ${alumno.primerNombre} ${alumno.segundoNombre} 
                                                    </td>
                                                    <td>
                                                        <p>                         
                                                            <strong>Total :</strong> <span class="badge">${alumno.getFaltasFictas(libretaActual.getMateria())}</span>
                                                        </p>
                                                        <hr>
                                                        <p>
                                                             <strong>Justif :</strong> <span class="badge">${alumno.getFaltasJustificadas(libretaActual.getMateria())}</span>
                                                        </p>
                                                        <p>
                                                             <strong>N.Justif :</strong> <span class="badge">${alumno.getFaltasInjustificadas(libretaActual.getMateria())}</span>
                                                        </p>
                                                        
                                                    </td>
                                                    <!-- por cada periodo muestro las notas de cada evaluacion de cada tipo de evaluacion --> 
                                                    <c:forEach var="periodo" items="${libretaActual.getPeriodos()}">
                                                        <td>
                                                            <div class="panel panel-default">
                                                                <div class="panel-heading">
                                                                    <h5 class="panel-title">${periodo.getNombre()}</h5>
                                                                </div>
                                                                <div class="panel-body">
                                                                    <p>
                                                                        <strong>
                                                                            Escritos 
                                                                        </strong> 
                                                                        <c:forEach var="nota" items="${libretaActual.cargarNotas(alumno,'ESCRITO',periodo)}">
                                                                            <span class="badge">${nota}</span>
                                                                        </c:forEach>
                                                                    </p>
                                                                    <p>
                                                                        <strong>
                                                                            Tareas domiciliarias
                                                                        </strong> 
                                                                        <c:forEach var="nota" items="${libretaActual.cargarNotas(alumno,'TAREA_DOMICILIARIA',periodo)}">
                                                                            <span class="badge">${nota}</span>
                                                                        </c:forEach>
                                                                    </p>
                                                                    <p>
                                                                        <strong>
                                                                            Carpetas 
                                                                        </strong> 
                                                                        <c:forEach var="nota" items="${libretaActual.cargarNotas(alumno,'CARPETA',periodo)}">
                                                                            <span class="badge">${nota}</span>
                                                                        </c:forEach>
                                                                    </p>
                                                                    <p>
                                                                        <strong>
                                                                            Actividades 
                                                                        </strong> 
                                                                        <c:forEach var="nota" items="${libretaActual.cargarNotas(alumno,'ACTIVIDAD',periodo)}">
                                                                            <span class="badge">${nota}</span>
                                                                        </c:forEach>
                                                                    </p>
                                                                    <p>
                                                                        <strong>
                                                                            Lectura 
                                                                        </strong> 
                                                                        <c:forEach var="nota" items="${libretaActual.cargarNotas(alumno,'LECTURA',periodo)}">
                                                                            <span class="badge">${nota}</span>
                                                                        </c:forEach>
                                                                    </p>
                                                                    <p>
                                                                        <strong>
                                                                            Oral 
                                                                        </strong> 
                                                                        <c:forEach var="nota" items="${libretaActual.cargarNotas(alumno,'ORAL',periodo)}">
                                                                            <span class="badge">${nota}</span>
                                                                        </c:forEach>
                                                                    </p>
                                                                    <p>
                                                                        <strong>
                                                                            Presentación 
                                                                        </strong> 
                                                                        <c:forEach var="nota" items="${libretaActual.cargarNotas(alumno,'PRESENTACION',periodo)}">
                                                                            <span class="badge">${nota}</span>
                                                                        </c:forEach>
                                                                    </p>
                                                                    <p>
                                                                        <strong>
                                                                            Proyecto 
                                                                        </strong> 
                                                                        <c:forEach var="nota" items="${libretaActual.cargarNotas(alumno,'PROYECTO',periodo)}">
                                                                            <span class="badge">${nota}</span>
                                                                        </c:forEach>
                                                                    </p>
                                                                    <p>
                                                                        <strong>
                                                                            Practico 
                                                                        </strong> 
                                                                        <c:forEach var="nota" items="${libretaActual.cargarNotas(alumno,'PRACTICO',periodo)}">
                                                                            <span class="badge">${nota}</span>
                                                                        </c:forEach>
                                                                    </p>
                                                                    <hr>

                                                                    <p>
                                                                        <label for="cal#${periodo.getNombre()}#${loop.index}">Promedio :</label>
                                                                        <input type="text" class="form-control" name="cal#${periodo.getNombre()}#${loop.index}" id="cal#${periodo.getNombre()}#${loop.index}" value="${libretaActual.recuperarPromedioReunion(alumno, periodo)}"/>
                                                                    </p>
                                                                    <p>
                                                                        <label for="com#${periodo.getNombre()}#${loop.index}">Comentario :</label>
                                                                        <input type="text" class="form-control" name="com#${periodo.getNombre()}#${loop.index}" id="com#${periodo.getNombre()}#${loop.index}" value="${libretaActual.recuperarComentarioReunion(alumno, periodo)}"/>
                                                                    </p>
                                                                    <p>
                                                                        <label for="cond#${periodo.getNombre()}#${loop.index}">Conducta :</label>
                                                                        <input type="text" class="form-control" name="cond#${periodo.getNombre()}#${loop.index}" id="cond#${periodo.getNombre()}#${loop.index}" value="${libretaActual.recuperarConductaReunion(alumno, periodo)}"/>
                                                                    </p>
                                                                </div>
                                                                <!-- final panel-body -->
                                                                <div class="panel-footer">
                                                                    <c:choose>
                                                                        <c:when test="${libretaActual.esReunionCerrada(alumno, periodo)}">
                                                                            <label class="btn btn-default" >
                                                                                <input disabled="true"  type="checkbox" id="btn#${periodo.getNombre()}#${loop.index}" name="btn#${periodo.getNombre()}#${loop.index}">
                                                                                <span class="glyphicon glyphicon-floppy-disk"></span> Reunion cerrada.  

                                                                            </label>     

                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <label class="btn btn-primary" >
                                                                                <input  type="checkbox" id="btn#${periodo.getNombre()}#${loop.index}" name="btn#${periodo.getNombre()}#${loop.index}">
                                                                                <span class="glyphicon glyphicon-floppy-disk"></span> Guardar cambios  

                                                                            </label>   
                                                                        </c:otherwise>
                                                                    </c:choose>

                                                                </div>
                                                            </div>
                                                        </td>
                                                    </c:forEach>
                                                    <!-- fin listado periodos -->
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <!-- fin listado alumnos -->
                <input type="hidden" name="opSeleccionada" id="opSeleccionada">
                <div class="form-row">
                    <div class="form-group">
                        <button class="btn btn-primary" id="btnGuardar" type="submit">
                            <span class="glyphicon glyphicon-floppy-disk"></span> Guardar todos los cambios  
                        </button>
                    </div>
                </div>
            </form>
        </div>

    </body>
</html>
