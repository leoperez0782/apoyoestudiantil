<%-- 
    Document   : test
    Created on : 09-jun-2019, 19:25:14
    Author     : Polachek
--%>
<%
    String mensaje = request.getParameter("mensaje");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>ORT</title>
        <%@ include file="parts/head.jsp" %>
    </head>
    <body class="login">
        <%@ include file="parts/header.jsp" %>
       
        <div class="container-fluid">
            <video autoplay muted loop id="myVideo">
                <source src="resources/assets/video-loop.mp4" type="video/mp4">
            </video>
            
            <section id="login">
                    <img id="logo-login" src="resources/images/ap-logo-img.png" alt="Apoyo Estudiantil"/>
                    <h2>LOGIN</h2>
                    <form method="get" action="login" onsubmit="showLoader();">
                            <input class="input-login" type="text" name="usuario" placeholder="Usuario"></input>
                            <br>
                            <input class="input-login" type="password" name="password" placeholder="Contraseña"></input>
                            <br>
                            <input type="submit" id="submit" value="Ingresar"> </input>
                            <p class="login-error">
                            <%
                                if(mensaje!=null){
                                    out.println("Error: " + mensaje);
                                }
                            %>
                            </p>
                    </form>
            </section>
        </div>
        <div id="wp-loader" style="display: none;">
            <div class="loader"></div>
        </div>
    </body>
</html>
