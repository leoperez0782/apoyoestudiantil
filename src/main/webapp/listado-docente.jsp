<%-- 
    Document   : listado-docente
    Created on : 28-jun-2019, 20:31:22
    Author     : leo
--%>
<%--<%@page import="com.apoyoestudiantil.entity.Docente"%>
<%@page import="java.util.List"%>--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
    String mensaje = request.getParameter("mensaje");
    List<Docente> listado = (List) request.getSession(false).getAttribute("listado");
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    <c:import url="/parts/head.jsp"></c:import>
</head>
<body>
    <div class="container-fluid">
    <c:import url="/parts/header.jsp"></c:import>
        <div class="table-responsivetable">
            <table class="table table-bordered table-hover">
                <thead>
                <td>Nro Documento</td>
                <td>Primer nombre</td>
                <td>Segundo nombre</td>
                <td>Primer apellido</td>
                <td>Segundo apellido</td>
                <td>Email</td>
                <td>Cargo</td>
                </thead>
                <tbody>
                <c:forEach var="docente" items="${listado}" varStatus="loop">
                    <tr>
                        <td>${docente.documento.numero}</td>  
                        <td>${docente.primerNombre}</td>  
                        <td>${docente.segundoNombre}</td>  
                        <td>${docente.primerApellido}</td>  
                        <td>${docente.segundoApellido}</td>  
                        <td>${docente.email}</td>  
                        <td>${docente.cargo.tipoCargo}</td>  
                        <td>
                            <a class="btn btn-primary" href="
                               <c:url value='modificardocentes'>
                                   <c:param name="index" value="${loop.index}"/>
                               </c:url>"
                               >Modificar</a>
                        </td>  
                        <td></td>  
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>

</body>
</html>
