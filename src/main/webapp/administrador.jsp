<%-- 
    Document   : administrador
    Created on : 17-jun-2019, 3:45:42
    Author     : Polachek
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrador</title>
        <%@ include file="parts/head.jsp" %>

        <script type="text/javascript">
            var vistaWeb = new EventSource("administrador?accion=new");
        </script>
        <script type="text/javascript">
            // FUNCIONES - FUNCTIONS 
            function panelsec(e,panel) {
                e.preventDefault();
                $("#panel_central").slideUp( "slow", function() {
                    $(panel).slideDown( "slow" );
                });
            }
            
            function panelcentral(e) {
                e.preventDefault();
                $(".panel_secundario").slideUp( "slow", function() {
                     $("#panel_central").slideDown( "slow" );
                });
            }          
            
        </script>
    </head>
    <body class="admin dashboard">
        <%@ include file="parts/header.jsp" %>

        <div class="container-fluid">
            <div id="panel_central">
                <a href="#" onclick="panelsec(event,'#panel_admin')" id="add-est" class="item-dash">
                    <img src="resources/images/admin.png" alt="Administrador" />
                    <span>Administrador</span>
                </a>
                
                <a href="#" onclick="panelsec(event,'#panel_adminO')" id="add-est" class="item-dash">
                    <img src="resources/images/administrativo.png" alt="Administrativo" />
                    <span>Administrativo</span>
                </a>
                
                <a href="#" onclick="panelsec(event,'#panel_estudiante')" id="add-est" class="item-dash">
                    <img src="resources/images/student-add.png" alt="Estudiante" />
                    <span>Estudiante</span>
                </a>
                
                <a href="#" onclick="panelsec(event,'#panel_tutor')" id="add-est" class="item-dash">
                    <img src="resources/images/tutor.png" alt="Tutor" />
                    <span>Tutor</span>
                </a>
                
                <a href="#" onclick="panelsec(event,'#panel_docente')" id="add-est" class="item-dash">
                    <img src="resources/images/docente-add.png" alt="Docente" />
                    <span>Docente</span>
                </a>
                
                <a href="#" onclick="panelsec(event,'#panel_cargo')" id="add-est" class="item-dash">
                    <img src="resources/images/cargo.png" alt="Cargos" />
                    <span>Cargos</span>
                </a>
                
                <a href="uploadempleados" id="del-cargo" class="item-dash">
                    <img src="resources/images/cargo.png" alt="Carga de empleados desde excel" />
                    <span>Carga de empleados desde Excel</span>
                </a> 
            </div>   
            
            
            <!-- ** ADMINISTRADOR **-->
            <div class="panel_secundario" id="panel_admin">
                <a href="#" onclick="panelcentral(event)" id="doc-noti" class="item-dash">
                    <img src="resources/images/back-icon.png" alt="Volver" />
                    <span>Volver</span>
                </a>
                <a href="alta-administrador.jsp" id="add-est" class="item-dash">
                    <img src="resources/images/admin.png" alt="Agregar administrador" />
                    <span>Agregar administrador</span>
                </a>
                <a href="modificar-admin.jsp?ADMINISTRADOR" id="edit-est" class="item-dash">
                    <img src="resources/images/admin.png" alt="Modificar administrador" />
                    <span>Modificar administrador</span>
                </a>                
            </div>
            <!-- ** /ADMINISTRADOR **-->
            
            <!-- ** ADMINISTRATIVO **-->
            <div class="panel_secundario" id="panel_adminO"> 
                <a href="#" onclick="panelcentral(event)" id="doc-noti" class="item-dash">
                    <img src="resources/images/back-icon.png" alt="Volver" />
                    <span>Volver</span>
                </a>
                <a href="alta-administrativo.jsp" id="add-est" class="item-dash">
                    <img src="resources/images/administrativo.png" alt="Agregar administrativo" />
                    <span>Agregar administrativo</span>
                </a>
                <a href="modificar-admin.jsp?ADMINISTRATIVO" id="edit-est" class="item-dash">
                    <img src="resources/images/administrativo.png" alt="Modificar administrativo" />
                    <span>Modificar administrativo</span>
                </a>                
            </div>
            <!-- ** /ADMINISTRATIVO **-->

            <!-- ** ESTUDIANTE **-->
            <div class="panel_secundario" id="panel_estudiante">
                <a href="#" onclick="panelcentral(event)" id="doc-noti" class="item-dash">
                    <img src="resources/images/back-icon.png" alt="Volver" />
                    <span>Volver</span>
                </a>                
                <a href="estudiante-agregar.jsp" id="add-est" class="item-dash">
                    <img src="resources/images/student-add.png" alt="Agregar estudiante" />
                    <span>Agregar estudiante</span>
                </a>
                <a href="estudiante-modificar.jsp" id="edit-est" class="item-dash">
                    <img src="resources/images/student-edit.png" alt="Modificar estudiante" />
                    <span>Modificar estudiante</span>
                </a>
                <a href="carga-alumnos-excel.jsp" id="load-est" class="item-dash">
                    <img src="resources/images/students-lote.png" alt="Carga alumnos por lote" />
                    <span>Carga alumnos por lote</span>
                </a>
                <!--<a href="uploadalumnos" id="load-est" class="item-dash">Carga alumnos por lote</a>-->  
            </div>            
            <!-- ** /ESTUDIANTE **-->

            <!-- ** TUTOR **-->
            <div class="panel_secundario" id="panel_tutor">
                <a href="#" onclick="panelcentral(event)" id="doc-noti" class="item-dash">
                    <img src="resources/images/back-icon.png" alt="Volver" />
                    <span>Volver</span>
                </a>
                <a href="alta-tutor.jsp" id="add-tutor" class="item-dash">
                    <img src="resources/images/tutor.png" alt="Agregar tutor" />
                    <span>Agregar tutor</span>
                </a>
                <a href="modificar-tutor-lista.jsp" id="mod-tutor" class="item-dash">
                    <img src="resources/images/tutor.png" alt="Modificar tutor" />
                    <span>Modificar tutor</span>
                </a>                
            </div>
            <!-- ** /TUTOR **-->

            <!-- ** DOCENTE **-->
            <div class="panel_secundario" id="panel_docente">
                <a href="#" onclick="panelcentral(event)" id="doc-noti" class="item-dash">
                    <img src="resources/images/back-icon.png" alt="Volver" />
                    <span>Volver</span>
                </a>
                <a href="alta-docente.jsp" id="add-teach" class="item-dash">
                    <img src="resources/images/docente-add.png" alt="Agregar docente" />
                    <span>Agregar docente</span>
                </a>
                <a href="uploaddocentes" id="load-teach" class="item-dash">
                    <img src="resources/images/docentes-lote.png" alt="Carga docentes por lote" />
                    <span>Carga docentes por lote</span>
                </a>
                <a href="listadodocente" id="list-teach" class="item-dash">
                    <img src="resources/images/docentes-lote.png" alt="Listado docente" />
                    <span>Listado docente</span>
                </a>  
                
                <a href="cierre-reuniones-direccion.jsp" id="list-teach" class="item-dash">
                    <img src="resources/images/docentes-lote.png" alt="Cierre de reuniones" />
                    <span>Cierre de reuniones</span>
                </a>
            </div> 
            <!-- ** /DOCENTE **-->

            <!-- ** CARGO **-->
            <div class="panel_secundario" id="panel_cargo">
                <a href="#" onclick="panelcentral(event)" id="doc-noti" class="item-dash">
                    <img src="resources/images/back-icon.png" alt="Volver" />
                    <span>Volver</span>
                </a>
                <a href="altacargo" id="add-cargo" class="item-dash">
                    <img src="resources/images/cargo.png" alt="Alta cargos" />
                    <span>Alta cargo</span>
                </a>
                <a href="modificar-cargos.jsp" id="mod-cargo" class="item-dash">
                    <img src="resources/images/cargo.png" alt="Modificar cargos" />
                    <span>Modificar cargo</span>
                </a>
                <a href="borrar-cargo.jsp" id="del-cargo" class="item-dash">
                    <img src="resources/images/cargo.png" alt="Eliminar cargos" />
                    <span>Eliminar cargo</span>
                </a>
            </div> 
            <!-- ** /CARGO **-->    
        </div>
    </body>
</html>
