<%-- 
    Document   : estudiante-agregar
    Created on : 18-jun-2019, 15:48:48
    Author     : Polachek
--%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agregar Estudiante</title>
         <%@ include file="parts/head.jsp" %>
        
        <script type="text/javascript">
            // VISTA - VIEW
            var vistaWeb = new EventSource("estudianteagregar");
            
            //Mostrar SaludoUsuario
            vistaWeb.addEventListener("mostrarUsuario", function (evento){
                document.getElementById("usuario").innerHTML=evento.data;                
            },false);
            
            vistaWeb.addEventListener("Error", function (evento) {
                document.getElementById('wp-loader').style.display = "none";
                document.getElementById("mensajeError").innerHTML = "<p>"+evento.data+"</p>";                
            }, false);
            
            vistaWeb.addEventListener("Exito", function (evento) {
                document.getElementById('wp-loader').style.display = "none";
                document.getElementById("mensajeExito").innerHTML = "<p>"+evento.data+"</p>"; 
            }, false);
            
            //Mostrar Niveles
            vistaWeb.addEventListener("mostrarNiveles", function (evento){
                document.getElementById("listaNiveles").innerHTML=evento.data;                
            },false);
            
            //Mostrar Grupos
            vistaWeb.addEventListener("mostrarGrupos", function (evento){
                document.getElementById("listaGrupos").innerHTML=evento.data;                
            },false);
            
            vistaWeb.addEventListener("redireccion", function (evento) {
                window.location.replace(evento.data);
            }, false);
            
        </script>
        <script type="text/javascript">
            // FUNCIONES - FUNCTIONS            
            function nivelSeleccionado(e,element) {
                e.preventDefault();                
                if($(element).closest("li").hasClass("selected")){
                    $( "#sel-niveles ul li" ).removeClass("selected");
                    $( "#sel-niveles ul" ).slideUp( "slow", function() {
                        $( "#sel-niveles ul li" ).show();
                        $( "#sel-niveles ul" ).slideDown( "slow", function() {});
                    }); 
                }else{
                    $( "#sel-niveles ul li" ).removeClass("selected");
                    $(element).closest("li").addClass('selected');
                    $( "#sel-niveles ul" ).slideUp( "slow", function() {
                        $( "#sel-niveles ul li" ).hide();
                        $( "#sel-niveles ul li.selected" ).show();
                        $( "#sel-niveles ul" ).slideDown( "slow", function() {});
                        $( "#pasos #pasos_paso_2 a" ).click();
                        $.get("estudianteagregar?accion=nivels&nivelseleccionado=" + $("#sel-niveles ul li.selected").attr("data-nivel-id") , function (data){});
                    });    
                }
            }
            
            function grupoSeleccionado(e,element) {
                e.preventDefault();
                $( "#sel-grupo ul li" ).removeClass("selected");
                $(element).closest("li").addClass('selected');
            }
            
            function pasoSeleccionado(e,element) {
                e.preventDefault();
                $(".paso_content").hide();
                $( "#pasos .paso" ).removeClass("selected");
                $(element).closest("div").addClass('selected');
                var selectedID = $("#pasos .selected").attr('id');
                switch (selectedID) {
                    case 'pasos_paso_1':
                      $(".paso_1").show();
                      break;
                    case 'pasos_paso_2':
                      $(".paso_2").show();
                      break;
                    case 'pasos_paso_3':
                      $(".paso_3").show();
                      break;
                    case 'pasos_paso_4':
                      $(".paso_4").show();
                      break;
                    case 'pasos_paso_5':
                      $(".paso_5").show();
                      break;
                }
            }
            
            function agregarTelefono(e) {
                e.preventDefault();
                $.get("resources/partials/telefono.html", function (data) {
                    $("#telefonos_wrap").append(data);
                });                
            }
            
            function borrarTelefono(e,element) {
                e.preventDefault();
                $(element).closest("div.telefono_item").remove();
            }
            
            function agregarDireccion(e) {
                e.preventDefault();
                $.get("resources/partials/direccion.html", function (data) {
                    $("#direcciones_wrap").append(data);
                });                
            }
            
            function borrarDireccion(e,element) {
                e.preventDefault();
                $(element).closest("div.direccion_item").remove();
            }
            
            function siguientePaso(e,paso_id) {
                e.preventDefault();
                $( paso_id ).click();
            }
            
            function formAgregarEstudiante(miForm) {
                $("#mensajeExito").html("");
                $("#mensajeError").html("");
                $( "#wp-loader" ).show();
                formLimpiaErrores();                
                var valida = validarForm(miForm);                
                if(!validaNumero(miForm.matricula.value) || !miForm.matricula.value){
                    valida = false;
                    miForm.matricula.classList.add("error");
                }                
                if(!valida){
                    $('#pasos_paso_3').addClass('error');
                }

                if($('#form-telefono .telefono_item:first .tel_num').val()==="" || !$('#form-telefono .telefono_item:first').length){
                   $('#pasos_paso_5').addClass('error');
                   valida = false;
                }
                if(!$('#listaNiveles .selected').length){
                   $('#pasos_paso_1').addClass('error');
                   valida = false;
                }
                if(!miForm.clave.value){
                    miForm.clave.classList.add("error");
                    valida = false;
                }
                
                if(!valida){
                    $("#mensajeError").html("Error: Por favor revise los campos resaltados en rojo");
                    $( "#wp-loader" ).show(0).delay(300).hide(0);
                }else{
                    var ci = miForm.documento.value;
                    var ci_emisor = miForm.ci_emisor.value;
                    var ci_tipo = miForm.ci_tipo.options[miForm.ci_tipo.selectedIndex].value;
                    var nombre = miForm.nombre.value;
                    var nombre_segundo = miForm.nombre_segundo.value;
                    var apellido = miForm.apellido.value;
                    var apellido_segundo = miForm.apellido_segundo.value;
                    var fechan = miForm.fechan.value;
                    var matricula = miForm.matricula.value;
                    var email = miForm.email.value;
                    var clave = miForm.clave.value;
                  
                    var params = "";
                    params += "ci="+ci+"&ci_emisor="+ci_emisor+"&ci_tipo="+ci_tipo+"&nombre="+nombre;
                    params += "&nombre_segundo="+nombre_segundo+"&apellido="+apellido+"&apellido_segundo="+apellido_segundo;
                    params += "&fechan="+fechan+"&matricula="+matricula+"&email="+email;
                    params += "&clave="+clave;
                    
                    $( "#form-telefono div.telefono_item" ).each(function( index ) {                        
                        var mi_tel_tipo = $(this).find('.tel_tipo option:selected').text();
                        if($(this).find(".tel_num").val()){
                            var mi_tel_num = $(this).find(".tel_num").val();
                        }                      
                        var mi_tel_obs = "";
                        if($(this).find(".tel_obs").val()){
                            mi_tel_obs = $(this).find(".tel_obs").val();
                        }
                        var tel_params = "tipotel="+mi_tel_tipo;
                        tel_params += "&numtel="+mi_tel_num;
                        tel_params += "&obstel="+mi_tel_obs;
                        $.get("estudianteagregar?accion=telefono&"+tel_params , function (data){});    
                    });
                    
                    $( "#direcciones_wrap div.direccion_item" ).each(function( index ) {
                        var calle ="";
                        if($(this).find(".calle").val()){
                            calle = $(this).find(".calle").val();
                        } 
                        var esquina ="";
                        if($(this).find(".esquina").val()){
                            esquina = $(this).find(".esquina").val();
                        }
                        var barrio ="";
                        if($(this).find(".barrio").val()){
                            barrio = $(this).find(".barrio").val();
                        }
                        var numero ="";
                        if($(this).find(".numero").val()){
                            numero = $(this).find(".numero").val();
                        }
                        var apto ="";
                        if($(this).find(".apto").val()){
                            apto = $(this).find(".apto").val();
                        }
                        var block ="";
                        if($(this).find(".block").val()){
                            block = $(this).find(".block").val();
                        }
                        var departamento = $(this).find('.departamento option:selected').text();
                        var localidad ="";
                        if($(this).find(".localidad").val()){
                            localidad = $(this).find(".localidad").val();
                        }
                        var manzanasolar ="";
                        if($(this).find(".manzanasolar").val()){
                            manzanasolar = $(this).find(".manzanasolar").val();
                        }
                        var edificio ="";
                        if($(this).find(".edificio").val()){
                            edificio = $(this).find(".edificio").val();
                        }
                        var isTorre ="";
                        if($(this).find(".isTorre").val()){
                            isTorre = $(this).find(".isTorre").val();
                        }
                        
                        var dir_params = "&calle="+calle+"&esquina="+esquina;
                        dir_params += "&barrio="+barrio+"&numero="+numero+"&apto="+apto;
                        dir_params += "&block="+block+"&departamento="+departamento+"&localidad="+localidad;
                        dir_params += "&manzanasolar="+manzanasolar+"&edificio="+edificio+"&isTorre="+isTorre;
                        if(calle!==""){
                            $.get("estudianteagregar?accion=direccion&"+dir_params , function (data){});
                        }                        
                    }); 
                    
                    if($("#sel-grupo ul li.selected")){
                        $.get("estudianteagregar?accion=grupo&gruposeleccionado=" + $("#sel-grupo ul li.selected").attr("data-grupo-id") , function (data){});
                    }                    
                    $.get("estudianteagregar?accion=alta_alumno&"+params , function (data){});             
                }        
            }
 
            
        </script>
    </head>
    <body class="estudiante-agregar">
        <%@ include file="parts/header.jsp" %>
        
        <div class="container-fluid">
            <h1 id="usuario"></h1>           
            <div id="pasos">
                <div class="paso selected" id="pasos_paso_1">
                    <a href="#" onclick="pasoSeleccionado(event,this)">
                        Paso 1
                        <span class="subtit">Nivel</span>
                    </a>
                </div>
                <div class="paso" id="pasos_paso_2">
                    <a href="#" onclick="pasoSeleccionado(event,this)">
                        Paso 2
                        <span class="subtit">Grupo</span>
                    </a>
                </div>
                <div class="paso" id="pasos_paso_3">
                    <a href="#" onclick="pasoSeleccionado(event,this)">
                        Paso 3
                        <span class="subtit">Datos personales</span>
                    </a>
                </div>
                <div class="paso" id="pasos_paso_4">
                    <a href="#" onclick="pasoSeleccionado(event,this)">
                        Paso 4
                        <span class="subtit">Dirección</span>
                    </a>
                </div>
                <div class="paso" id="pasos_paso_5">
                    <a href="#" onclick="pasoSeleccionado(event,this)">
                        Paso 5
                        <span class="subtit">Teléfono</span>
                    </a>
                </div>
            </div>
            
            <!-- Mensajes -->
            <div id="mensajeExito"></div>
            <div id="mensajeError"></div>
            
            <section id="sel-niveles" class="paso_1 paso_content">
                <h4>Seleccione el nivel</h4>
                <ul id="listaNiveles">
                
                </ul>
                <div class="paso_siguiente_cont">
                    <a href="#" class="paso_siguiente" onclick="siguientePaso(event,'#pasos #pasos_paso_2 a')">Siguiente</a>
                </div>
            </section>
            
            <section id="sel-grupo" class="paso_2 paso_content" style="display: none;">
                <h4>Seleccione el Grupo - <a href="#" onclick="siguientePaso(event,'#pasos #pasos_paso_3 a')">(Omitir)</a></h4>
                <ul id="listaGrupos">
                
                </ul>
                <div class="paso_siguiente_cont">
                    <a href="#" class="paso_siguiente" onclick="siguientePaso(event,'#pasos #pasos_paso_3 a')">Siguiente</a>
                </div>
                
            </section>
            
            <section id="formulario-alta">
                <form onsubmit="event.preventDefault(); formAgregarEstudiante(this);" class="form-layout-1">
                    <div class="paso_3 paso_content" style="display: none;">
                        <fieldset>
                            <legend>Datos personales</legend>
                            
                            <div class="half-screen">
                                <label for="documento">Documento</label>
                                <input class="input-half" type="text" id="documento" name="documento" placeholder="Documento">

                                <label for="ci_emisor">País emisor</label>
                                <input class="input-half" type="text" id="ci_emisor" name="ci_emisor" value="URUGUAY">

                                <label for="ci_tipo">Tipo de documento</label>
                                <select class="input" id="ci_tipo" name="ci_tipo">
                                    <option value="CEDULA" selected="selected">CÉDULA</option>
                                    <option value="PASAPORTE">PASAPORTE</option>
                                    <option value="OTRO">OTRO</option>
                                </select>
                                
                                <label for="matricula">Matrícula</label> 
                                <input class="input-half" type="text" id="matricula" name="matricula" placeholder="Matricula">

                                <label for="email">E-mail</label> 
                                <input class="input-half" type="text" id="email" name="email" placeholder="E-mail">

                                <label for="clave">Clave</label> 
                                <input class="input-half" type="text" id="clave" name="clave" placeholder="Clave">
                            </div>
                            <div class="half-screen">
                                <label for="nombre">Primer nombre</label>
                                <input class="input-half" type="text" id="nombre" name="nombre" placeholder="Primer nombre">

                                <label for="nombre_segundo">Segundo nombre</label>
                                <input class="input-half" type="text" id="nombre_segundo" name="nombre_segundo" placeholder="Segundo nombre">

                                <label for="apellido">Primer apellido</label>
                                    <input class="input-half" type="text" id="apellido" name="apellido" placeholder="Primer apellido">

                                <label for="apellido_segundo">Segundo apellido</label> 
                                <input class="input-half" type="text" id="apellido_segundo" name="apellido_segundo" placeholder="Segundo apellido">

                                <label for="fechan">Fecha de nacimiento</label> 
                                <input class="input-half" type="date" id="fechan" name="fechan">
                            </div>
                            <div class="clear"></div>
                        </fieldset>
                        <div class="paso_siguiente_cont">
                            <a href="#" class="paso_siguiente" onclick="siguientePaso(event,'#pasos #pasos_paso_4 a')">Siguiente</a>
                        </div>
                    </div>
                    <div class="paso_4 paso_content" style="display: none;">
                        <fieldset id="direcciones_wrap">
                            <legend>Direcciones  - <a href="#" class="agregar_item" onclick="agregarDireccion(event)">Agregar nueva dirección</a></legend>
                            <div class="direccion_item">
                                <h1 class="direccion-title">Dirección</h1>
                                <div class="half-screen">
                                    <label>Calle
                                        <input class="input-half calle" type="text" placeholder="Ejemplo: Av.Italia">
                                    </label>

                                    <label>Esquina
                                        <input class="input-half esquina" type="text" placeholder="Ejemplo: Av.Centenario">
                                    </label> 

                                    <label>Barrio
                                        <input class="input-half barrio" type="text" placeholder="Ejemplo: Malvin">
                                    </label>

                                    <label>Número de puerta
                                        <input class="input-half numero" type="text" placeholder="Número de puerta">
                                    </label>

                                    <label>Nº de Apartamento, si corresponde
                                        <input class="input-half apto" type="text" placeholder="Nº de Apartamento, si corresponde">
                                    </label>

                                    <label>Torre
                                        <input class="input-half isTorre" type="text" placeholder="Torre">
                                    </label>
                                </div>
                                <div class="half-screen">
                                    <label>Block
                                        <input class="input-half block" type="text" placeholder="Block">
                                    </label>

                                    <label>Departamento</label>
                                    <select class="input departamento">
                                        <option value="MONTEVIDEO" selected="selected">MONTEVIDEO</option>
                                        <option value="ARTIGAS">ARTIGAS</option>
                                        <option value="CANELONES">CANELONES</option>
                                        <option value="CERRO_LARGO">CERRO_LARGO</option>
                                        <option value="COLONIA">COLONIA</option>
                                        <option value="DURAZNO">DURAZNO</option>
                                        <option value="FLORES">FLORES</option>
                                        <option value="FLORIDA">FLORIDA</option>
                                        <option value="LAVALLEJA">LAVALLEJA</option>
                                        <option value="MALDONADO">MALDONADO</option>
                                        <option value="PAYSANDU">PAYSANDU</option>
                                        <option value="RIO_NEGRO">RIO_NEGRO</option>
                                        <option value="RIVERA">RIVERA</option>
                                        <option value="ROCHA">ROCHA</option>
                                        <option value="SALTO">SALTO</option>
                                        <option value="SAN_JOSE">SAN_JOSE</option>
                                        <option value="SORIANO">SORIANO</option>
                                        <option value="TACUAREMBO">TACUAREMBO</option>
                                        <option value="TREINTA_Y_TRES">TREINTA_Y_TRES</option>
                                    </select>

                                    <label>Edificio
                                        <input class="input-half edificio" type="text" placeholder="Edificio">
                                    </label>

                                    <label>Localidad
                                        <input class="input-half localidad" type="text" placeholder="Localidad">
                                    </label>

                                    <label>Manzana Solar
                                        <input class="input-half manzanasolar" type="text" placeholder="Manzana Solar">
                                    </label>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </fieldset>
                        <div class="paso_siguiente_cont">
                            <a href="#" class="paso_siguiente" onclick="siguientePaso(event,'#pasos #pasos_paso_5 a')">Siguiente</a>
                        </div>
                    </div>
                    <div id="form-telefono" class="paso_5 paso_content" style="display: none;">
                        <fieldset id="telefonos_wrap">
                            <legend>Teléfonos - <a href="#" class="agregar_item" onclick="agregarTelefono(event)">Agregar nuevo teléfono</a></legend>
                            <div class="telefono_item">
                                <div class="innner_content">
                                    <label for="tipo_tel_1">Tipo de teléfono</label>
                                    <select class="input tel_tipo" id="tipo_tel_1" name="tipo_tel_1">
                                        <option value="MOVIL" selected="selected">MOVIL</option>
                                        <option value="FIJO">FIJO</option>
                                    </select>
                                    <label for="num_tel_1">Nº de teléfono</label>
                                    <input class="input-half tel_num" type="text" id="num_tel_1" name="num_tel_1" placeholder="NNNNNNNNN">
                                    <label for="obs_tel_1">Observaciones</label>
                                    <input class="input-half tel_obs" type="text" id="obs_tel_1" name="obs_tel_1" placeholder="Observaciones">
                                </div>
                            </div>
                        </fieldset>
                        <div class="clear"></div>
                        <div class="submit_form">
                            <input type="submit" id="submit" value="Agregar"> 
                        </div>
                    </div>
                    <p class="login-error">
                    </p>
                </form>
            </section>

        </div>
            
        <div id="wp-loader" style="display: none;">
            <div class="loader"></div>
        </div>
    </body>
</html>
