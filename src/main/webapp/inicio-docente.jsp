<%-- 
    Document   : inicio-docente
    Created on : 05-jul-2019, 17:47:26
    Author     : leo
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
         <c:import url="/parts/head.jsp"></c:import>
    </head>
    <body class="docente dashboard">
        <div class="container-fluid">
            <c:import url="/parts/header-docente.jsp"></c:import>
           
            <a href="#" id="doc-grupo" class="item-dash">
                <img src="resources/images/students-lote.png" alt="Ver grupos asignados" />
                <span>Ver grupos asignados</span>
            </a>
            <a href="cargargruposdocente" id="doc-lista" class="item-dash">
                <img src="resources/images/presente.svg" alt="Pasar lista" />
                <span>Pasar lista</span>
            </a>
            <a href="#" id="doc-evaluacion" class="item-dash">
                <img src="resources/images/evaluacion.png" alt="Crear evaluacion" />
                <span>Crear evaluacion</span>
            </a>
            <a href="#" id="doc-noti" class="item-dash">
                <img src="resources/images/notificacion.png" alt="Crear notificacion de materia" />
                <span>Crear notificacion de materia</span>
            </a>
        </div>
    </body>
</html>
