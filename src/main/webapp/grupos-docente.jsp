<%-- 
    Document   : grupos-docente
    Created on : 05-jul-2019, 19:16:12
    Author     : leo
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <c:import url="/parts/head.jsp"></c:import>
    </head>
    <body>
        <div class="container-fluid">
             <c:import url="/parts/header-docente.jsp"></c:import>
             <div class="table-responsivetable">
            <table class="table table-bordered table-hover">
                <thead>
                <td>Clase</td>
                <td>Turno</td>
                <td>Salon</td>
                <td>Asignatura</td>
                <td>Accion</td>
                </thead>
                <tbody>
                <c:forEach var="libreta" items="${listaLibretas}" varStatus="loop">
                    <tr>
                        <td>${libreta.grupo.nivel.clase.nivel} ${libreta.grupo.nivel.clase.codigoClase}</td>  
                        <td>${libreta.turno}</td>  
                        <td>${libreta.salon}</td>  
                        <td>${libreta.materia.nombre}</td>
                        <td>
                            <a class="btn btn-primary" href="
                               <c:url value='listadogrupo'>
                                   <c:param name="index" value="${loop.index}"/>
                               </c:url>"
                               >Pasar lista</a>
                        </td>  
                          
                    </tr>
                </c:forEach>
            </table>
        </div>
        </div>

    </body>
</html>
