<%-- 
    Document   : modificar-lista
    Created on : 16-jul-2019, 19:55:28
    Author     : Leonardo Pérez
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <c:import url="/parts/head.jsp"></c:import>
        </head>
        <body>
            <div class="container-fluid">
           <c:import url="/parts/header-docente.jsp"></c:import>
            <form action="modificarlista" method="post">
                <div class="form-row">
                    <div class="form-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <c:choose>
                                    <c:when test="${listaActual.inasistencias.isEmpty()}">
                                        <h3 class="panel-title">No hay inasistencias registradas</h3>
                                    </c:when>
                                    <c:otherwise>
                                        <h3 class="panel-title">Modificando lista</h3>
                                    </c:otherwise>
                                </c:choose>

                            </div>
                            <div class="panel-body">
                                Fecha : ${fecha}  Horario : ${horario}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-row">
                    <fieldset class="form-group">
                        <c:choose>
                            <c:when test="${param.evento eq 'Exito'}">
                                <div class="bg-success">
                                    <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                                </div>

                            </c:when>
                            <c:when test="${param.evento eq 'Error'}">
                                <div class="bg-danger">
                                    <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                                </div> 
                            </c:when>
                            <c:otherwise>
                                <label id="lblAviso"></label>
                            </c:otherwise>
                        </c:choose>
                    </fieldset>
                </div>
                <div class="form-row">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <td>Matricula</td>
                            <td>Primer nombre</td>
                            <td>Segundo nombre</td>
                            <td>Primer apellido</td>
                            <td>Segundo apellido</td>
                            <!--<td>Asistio</td>-->
                            <td>Tipo de falta</td>

                            </thead>
                            <tbody id="cuerpoTabla">
                                <c:forEach var="inasist" items="${listaActual.inasistencias}" varStatus="loop">
                                    <tr id="tr${loop.index}">
                                        <td>${inasist.alumno.matricula}</td>
                                        <td>${inasist.alumno.primerNombre}</td>  
                                        <td>${inasist.alumno.segundoNombre}</td>  
                                        <td>${inasist.alumno.primerApellido}</td>  
                                        <td>${inasist.alumno.segundoApellido}</td>
                                        <!--<td><input type="checkbox" name="chk${loop.index}" id ="chk${loop.index}"/></td>-->
                                        <td>
                                            <select class="form-control" id="sltTipoFalta${loop.index}" name="tipoFalta${loop.index}">
                                                <option>${inasist.tipo}</option>
                                                <option value="JUSTIFICADA">Justificada</option>
                                                <option value="INJUSTIFICADA">Injustificada</option>
                                                <option value="TARDE">Tarde</option>
                                                <option value="MUY_TARDE">Muy tarde</option>
                                            </select>
                                        </td>  

                                    </tr>
                                </c:forEach>
                        </table>
                    </div>

                </div>
                <div class="form-row">
                    <fieldset class="form-group">
                        <div class="col-md-4">

                            <button class="btn btn-primary" id="btnGuardar" type="submit">
                                <span class="glyphicon glyphicon-floppy-disk"></span> Guardar cambios  
                            </button>
                        </div>

                    </fieldset>
                </div>        
            </form>
        </div>
    </body>
</html>
