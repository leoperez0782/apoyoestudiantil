<%-- 
    Document   : carga-docentes-por-lote
    Created on : 21-jun-2019, 18:31:27
    Author     : leo
--%>
<%
    String mensaje = request.getParameter("mensaje");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@ include file="parts/head.jsp" %>
    </head>
    <body>
         
         <div class="container-fluid">
            <%@ include file="parts/header.jsp" %>
            <div>
                <form  action="uploaddocentes" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <!--<input class="form-control" type="text" name="description" />-->
                        <input class="form-control-file" type="file" name="file" />

                    </div>
                    <div class="form-group">
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="optradio" value="Simple">Carga Simple
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" checked="true" name="optradio" value="Completa">Carga completa
                            </label>
                        </div>
                    </div>
                    <input  class="btn btn-primary" type="submit" />
                </form>
                <%
                    if (mensaje != null) {
                        out.println( mensaje);
                    }
                %>
            </div>
    </body>
</html>
