<%-- 
    Document   : alta-cargos
    Created on : 22-jun-2019, 21:06:27
    Author     : leo
--%>
<%
    String mensaje = request.getParameter("mensaje");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@ include file="parts/head.jsp" %>
    </head>
    <body>
        <div class="container-fluid">
            <%@ include file="parts/header.jsp" %>
            <form  action="altacargo" method="post">
                <div class="row">

                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txtTipoCargo">Tipo de cargo</label>
                            <input type="text" class="form-control" id="txtTipoCargo" name="tipoCargo" placeholder="Ingrese el tipo de cargo" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="txtDescripcion">Descripción breve</label>
                            <input type="text" class="form-control" id="txtDescripcion" name="descripcion" placeholder="Ingrese la descripción">
                        </div>
                    </div>
                    <!--<div class="col-md-4">
                        <div class="form-group">
                            <label class="form-check-label" for="chkActivo">Activo</label>
                            <input type="checkbox" name="isActivo" class="form-check-input" id="chkActivo">
                        </div>
                    </div>-->
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit">Guardar</button>
                        </div> 
                    </div>
            </form>
            <%
                if (mensaje != null) {
                    out.println(mensaje);
                }
            %>
        </div>
    </body>
</html>
