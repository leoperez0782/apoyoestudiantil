<%-- 
    Document   : estudiante-agregar
    Created on : 18-jun-2019, 15:48:48
    Author     : Polachek
--%>
<%
    String mensaje = request.getParameter("mensaje");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Agregar Tutor</title>
         <%@ include file="parts/head.jsp" %>
        
        <!-- <script src="http://code.jquery.com/jquery-latest.min.js"></script> -->
        <script type="text/javascript">
            // VISTA - VIEW
            var vistaWeb = new EventSource("altatutor");
            
            vistaWeb.addEventListener("Error", function (evento) {
                document.getElementById('wp-loader').style.display = "none";
                document.getElementById("mensajeError").innerHTML = "<p>"+evento.data+"</p>";                
            }, false);
            
            vistaWeb.addEventListener("Exito", function (evento) {
                document.getElementById('wp-loader').style.display = "none";
                document.getElementById("mensajeExito").innerHTML = "<p>"+evento.data+"</p>"; 
            }, false);
            
            vistaWeb.addEventListener("resultadoBusqueda", function (evento){
                document.getElementById("resultadoAlumno").innerHTML=evento.data;                
            },false);
            
            vistaWeb.addEventListener("noAlumnos", function (evento){
                document.getElementById("error-no-alumnos").style.display = "block";
                document.getElementById("resultadoAlumno").innerHTML="";
            },false);
            
            vistaWeb.addEventListener("agregarListaAlumno", function (evento) {
                var myTabla = document.getElementById("tabla_alumnos");
                if(myTabla){
                    var newRow=document.getElementById('tabla_alumnos').insertRow();
                    newRow.innerHTML = evento.data;
                }else{
                    var laTabla = "<table id='tabla_alumnos' class='table_style_1'><tr><th>Nombre</th><th>Apellido</th>\n" +
                     "<th>Cédula</th></tr><tr>";
                     laTabla += evento.data;
                     laTabla += "</tr></table>";
                     document.getElementById("list-hijos-asignados").innerHTML = laTabla;
                }               
            }, false);
            
            vistaWeb.addEventListener("listaAlumnos", function (evento) {
                document.getElementById("list-hijos-asignados").innerHTML = evento.data;
            }, false);
        </script>
        <script type="text/javascript">
            // FUNCIONES - FUNCTIONS            
            function pasoSeleccionado(e,element) {
                e.preventDefault();
                $(".paso_content").hide();
                $( "#pasos .paso" ).removeClass("selected");
                $(element).closest("div").addClass('selected');
                var selectedID = $("#pasos .selected").attr('id');
                switch (selectedID) {
                    case 'pasos_paso_1':
                      $(".paso_1").show();
                      break;
                    case 'pasos_paso_2':
                      $(".paso_2").show();
                      break;
                    case 'pasos_paso_3':
                      $(".paso_3").show();
                      break;
                    case 'pasos_paso_4':
                      $(".paso_4").show();
                      break;
                }
            }
            
            function buscarEstudiante() {
                $('#error-alumnos-novacio').hide();
                $('#error-no-alumnos').hide();
                if($('#sel-alumno #buscar-alumno').val()){
                    var busqueda = "numDoc="+ $('#sel-alumno #buscar-alumno').val();
                    $.get("altatutor?accion=buscar&"+busqueda , function (data){});
                  }else{
                    $('#error-alumnos-novacio').show();
                }                
            }
            
            function agregarAlumno(e,idAlumno,doc){
                e.preventDefault();
                $("#mensajeExito").html("");
                $("#mensajeError").html(""); 
                var params = "idAlm="+idAlumno;
                var esta = false;
                $('#tabla_alumnos td').each(function() {
                    if($(this).is(':contains('+doc+')')) 
                        esta = true;         
                });
                if(!esta){
                    $.get("altatutor?accion=agregarAlumno&"+params , function (data){});
                }else{
                    $("#mensajeError").html("El alumno seleccionado ya se encuentra en la lista"); 
                }               
            }
            function quitarAlumno(e,element,doc) {
                e.preventDefault();
                $(element).closest("tr").remove();
                var params = "docAlm="+doc;
                $.get("altatutor?accion=quitarAlumno&"+params , function (data){});
            }
            
            function agregarTelefono(e) {
                e.preventDefault();
                $.get("resources/partials/telefono.html", function (data) {
                    $("#telefonos_wrap").append(data);
                });                
            }
            
            function borrarTelefono(e,element) {
                e.preventDefault();
                $(element).closest("div.telefono_item").remove();
            }
            
            function agregarDireccion(e) {
                e.preventDefault();
                $.get("resources/partials/direccion.html", function (data) {
                    $("#direcciones_wrap").append(data);
                });                
            }
            
            function borrarDireccion(e,element) {
                e.preventDefault();
                $(element).closest("div.direccion_item").remove();
            }
            
            function siguientePaso(e,paso_id) {
                e.preventDefault();
                $( paso_id ).click();
            }
            
            function formAgregar(miForm) {
                $("#mensajeExito").html("");
                $("#mensajeError").html("");
                $( "#wp-loader" ).show();
                formLimpiaErrores();
                
                var valida = validarForm(miForm);
                if(!valida){
                    $('#pasos_paso_2').addClass('error');
                }
                if($('#form-telefono .telefono_item:first .tel_num').val()===""){
                   $('#pasos_paso_4').addClass('error');
                    valida = false;
                }
                if(!miForm.clave.value){
                    miForm.clave.classList.add("error");
                    valida = false;
                }
                
                if(!valida){
                    $("#mensajeError").html("Error: Por favor revise los campos resaltados en rojo");
                    $( "#wp-loader" ).show(0).delay(300).hide(0);
                }else{
                    var ci = miForm.documento.value;
                    var ci_emisor = miForm.ci_emisor.value;
                    var ci_tipo = miForm.ci_tipo.options[miForm.ci_tipo.selectedIndex].value;
                    var nombre = miForm.nombre.value;
                    var nombre_segundo = miForm.nombre_segundo.value;
                    var apellido = miForm.apellido.value;
                    var apellido_segundo = miForm.apellido_segundo.value;
                    var fechan = miForm.fechan.value;
                    var email = miForm.email.value;
                    var clave = miForm.clave.value;
                    
                    var params = "";
                    params += "ci="+ci+"&ci_emisor="+ci_emisor+"&ci_tipo="+ci_tipo+"&nombre="+nombre;
                    params += "&nombre_segundo="+nombre_segundo+"&apellido="+apellido+"&apellido_segundo="+apellido_segundo;
                    params += "&fechan="+fechan+"&email="+email;
                    params += "&clave="+clave;
                                        
                    $( "#form-telefono div.telefono_item" ).each(function( index ) {                        
                        var mi_tel_tipo = $(this).find('.tel_tipo option:selected').text();
                        if($(this).find(".tel_num").val()){
                            var mi_tel_num = $(this).find(".tel_num").val();
                        }                      
                        var mi_tel_obs = "";
                        if($(this).find(".tel_obs").val()){
                            mi_tel_obs = $(this).find(".tel_obs").val();
                        }
                        var tel_params = "tipotel="+mi_tel_tipo;
                        tel_params += "&numtel="+mi_tel_num;
                        tel_params += "&obstel="+mi_tel_obs;
                        $.get("altatutor?accion=telefono&"+tel_params , function (data){});    
                    });
                    
                    $( "#direcciones_wrap div.direccion_item" ).each(function( index ) {
                        var calle ="";
                        if($(this).find(".calle").val()){
                            calle = $(this).find(".calle").val();
                        } 
                        var esquina ="";
                        if($(this).find(".esquina").val()){
                            esquina = $(this).find(".esquina").val();
                        }
                        var barrio ="";
                        if($(this).find(".barrio").val()){
                            barrio = $(this).find(".barrio").val();
                        }
                        var numero ="";
                        if($(this).find(".numero").val()){
                            numero = $(this).find(".numero").val();
                        }
                        var apto ="";
                        if($(this).find(".apto").val()){
                            apto = $(this).find(".apto").val();
                        }
                        var block ="";
                        if($(this).find(".block").val()){
                            block = $(this).find(".block").val();
                        }
                        var departamento = $(this).find('.departamento option:selected').text();
                        var localidad ="";
                        if($(this).find(".localidad").val()){
                            localidad = $(this).find(".localidad").val();
                        }
                        var manzanasolar ="";
                        if($(this).find(".manzanasolar").val()){
                            manzanasolar = $(this).find(".manzanasolar").val();
                        }
                        var edificio ="";
                        if($(this).find(".edificio").val()){
                            edificio = $(this).find(".edificio").val();
                        }
                        var isTorre ="";
                        if($(this).find(".isTorre").val()){
                            isTorre = $(this).find(".isTorre").val();
                        }
                        
                        var dir_params = "&calle="+calle+"&esquina="+esquina;
                        dir_params += "&barrio="+barrio+"&numero="+numero+"&apto="+apto;
                        dir_params += "&block="+block+"&departamento="+departamento+"&localidad="+localidad;
                        dir_params += "&manzanasolar="+manzanasolar+"&edificio="+edificio+"&isTorre="+isTorre;
                        $.get("altatutor?accion=direccion&"+dir_params , function (data){});
                    }); 
                    
                    $.get("altatutor?accion=alta&"+params , function (data){});             
                    
                }        
            }
 
            
        </script>
    </head>
    <body class="estudiante-agregar tutor-agregar">
        <%@ include file="parts/header.jsp" %>
        
        <div class="container-fluid">
            <h1 id="usuario"></h1>
            <%
                if(mensaje!=null){
                    out.println("Error: " + mensaje);
                }
            %>
            
            <div id="pasos">
                <div class="paso selected" id="pasos_paso_1">
                    <a href="#" onclick="pasoSeleccionado(event,this)">
                        Paso 1
                        <span class="subtit">Estudiantes</span>
                    </a>
                </div>
                <div class="paso" id="pasos_paso_2">
                    <a href="#" onclick="pasoSeleccionado(event,this)">
                        Paso 2
                        <span class="subtit">Datos personales</span>
                    </a>
                </div>
                <div class="paso" id="pasos_paso_3">
                    <a href="#" onclick="pasoSeleccionado(event,this)">
                        Paso 3
                        <span class="subtit">Dirección</span>
                    </a>
                </div>
                <div class="paso" id="pasos_paso_4">
                    <a href="#" onclick="pasoSeleccionado(event,this)">
                        Paso 4
                        <span class="subtit">Teléfono</span>
                    </a>
                </div>
            </div>
            
            <!-- Mensajes -->
            <div id="mensajeExito"></div>
            <div id="mensajeError"></div>
            
            <section id="sel-alumno" class="paso_1 paso_content">
                <div id="hijos-asignados">
                    <h1 style="text-align:center">Estudiantes asignados</h1>
                    <div id="list-hijos-asignados">
                        
                    </div>
                </div>
                
                <h4>Agregar estudiantes</h4>
                <form class="center" onsubmit="event.preventDefault(); buscarEstudiante();">
                    <input type="text" id="buscar-alumno" placeholder="Buscar.." name="Buscar">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
                <p id="error-alumnos-novacio" class="center" style="display:none">El campo no puede estar vacio</p>
                <p id="error-no-alumnos" class="center" style="display:none">No se encontraron alumnos con esa cédula</p>
                <div id="resultadoAlumno" class="center">
                    
                </div>
                <div class="paso_siguiente_cont">
                    <a href="#" class="paso_siguiente" onclick="siguientePaso(event,'#pasos #pasos_paso_2 a')">Siguiente</a>
                </div>
            </section>
            
            <section id="formulario-alta">
                <form onsubmit="event.preventDefault(); formAgregar(this);" class="form-layout-1">
                    <div class="paso_2 paso_content" style="display: none;">
                        <fieldset>
                            <legend>Datos personales</legend>
                            
                            <div class="half-screen">
                                <label for="documento">Documento</label>
                                <input class="input-half" type="text" id="documento" name="documento" placeholder="Documento">

                                <label for="ci_emisor">País emisor</label>
                                <input class="input-half" type="text" id="ci_emisor" name="ci_emisor" value="URUGUAY">

                                <label for="ci_tipo">Tipo de documento</label>
                                <select class="input" id="ci_tipo" name="ci_tipo">
                                    <option value="CEDULA" selected="selected">CÉDULA</option>
                                    <option value="PASAPORTE">PASAPORTE</option>
                                    <option value="OTRO">OTRO</option>
                                </select>

                                <label for="email">E-mail</label> 
                                <input class="input-half" type="text" id="email" name="email" placeholder="E-mail">

                                <label for="clave">Clave</label> 
                                <input class="input-half" type="text" id="clave" name="clave" placeholder="Clave">
                            </div>
                            <div class="half-screen">
                                <label for="nombre">Primer nombre</label>
                                <input class="input-half" type="text" id="nombre" name="nombre" placeholder="Primer nombre">

                                <label for="nombre_segundo">Segundo nombre</label>
                                <input class="input-half" type="text" id="nombre_segundo" name="nombre_segundo" placeholder="Segundo nombre">

                                <label for="apellido">Primer apellido</label>
                                    <input class="input-half" type="text" id="apellido" name="apellido" placeholder="Primer apellido">

                                <label for="apellido_segundo">Segundo apellido</label> 
                                <input class="input-half" type="text" id="apellido_segundo" name="apellido_segundo" placeholder="Segundo apellido">

                                <label for="fechan">Fecha de nacimiento</label> 
                                <input class="input-half" type="date" id="fechan" name="fechan">
                            </div>
                            <div class="clear"></div>
                        </fieldset>
                        <div class="paso_siguiente_cont">
                            <a href="#" class="paso_siguiente" onclick="siguientePaso(event,'#pasos #pasos_paso_3 a')">Siguiente</a>
                        </div>
                    </div>
                    <div class="paso_3 paso_content" style="display: none;">
                        <fieldset id="direcciones_wrap">
                            <legend>Direcciones  - <a href="#" class="agregar_item" onclick="agregarDireccion(event)">Agregar nueva dirección</a></legend>
                            <div class="direccion_item">
                                <h1 class="direccion-title">Dirección</h1>
                                <div class="half-screen">
                                    <label>Calle
                                        <input class="input-half calle" type="text" placeholder="Ejemplo: Av.Italia">
                                    </label>

                                    <label>Esquina
                                        <input class="input-half esquina" type="text" placeholder="Ejemplo: Av.Centenario">
                                    </label> 

                                    <label>Barrio
                                        <input class="input-half barrio" type="text" placeholder="Ejemplo: Malvin">
                                    </label>

                                    <label>Número de puerta
                                        <input class="input-half numero" type="text" placeholder="Número de puerta">
                                    </label>

                                    <label>Nº de Apartamento, si corresponde
                                        <input class="input-half apto" type="text" placeholder="Nº de Apartamento, si corresponde">
                                    </label>

                                    <label>Torre
                                        <input class="input-half isTorre" type="text" placeholder="Torre">
                                    </label>
                                </div>
                                <div class="half-screen">
                                    <label>Block
                                        <input class="input-half block" type="text" placeholder="Block">
                                    </label>

                                    <label>Departamento</label>
                                    <select class="input departamento">
                                        <option value="MONTEVIDEO" selected="selected">MONTEVIDEO</option>
                                        <option value="ARTIGAS">ARTIGAS</option>
                                        <option value="CANELONES">CANELONES</option>
                                        <option value="CERRO_LARGO">CERRO_LARGO</option>
                                        <option value="COLONIA">COLONIA</option>
                                        <option value="DURAZNO">DURAZNO</option>
                                        <option value="FLORES">FLORES</option>
                                        <option value="FLORIDA">FLORIDA</option>
                                        <option value="LAVALLEJA">LAVALLEJA</option>
                                        <option value="MALDONADO">MALDONADO</option>
                                        <option value="PAYSANDU">PAYSANDU</option>
                                        <option value="RIO_NEGRO">RIO_NEGRO</option>
                                        <option value="RIVERA">RIVERA</option>
                                        <option value="ROCHA">ROCHA</option>
                                        <option value="SALTO">SALTO</option>
                                        <option value="SAN_JOSE">SAN_JOSE</option>
                                        <option value="SORIANO">SORIANO</option>
                                        <option value="TACUAREMBO">TACUAREMBO</option>
                                        <option value="TREINTA_Y_TRES">TREINTA_Y_TRES</option>
                                    </select>

                                    <label>Edificio
                                        <input class="input-half edificio" type="text" placeholder="Edificio">
                                    </label>

                                    <label>Localidad
                                        <input class="input-half localidad" type="text" placeholder="Localidad">
                                    </label>

                                    <label>Manzana Solar
                                        <input class="input-half manzanasolar" type="text" placeholder="Manzana Solar">
                                    </label>
                                </div>
                                <div class="clear"></div>
                            </div>
                        </fieldset>
                        <div class="paso_siguiente_cont">
                            <a href="#" class="paso_siguiente" onclick="siguientePaso(event,'#pasos #pasos_paso_4 a')">Siguiente</a>
                        </div>
                    </div>
                    <div id="form-telefono" class="paso_4 paso_content" style="display: none;">
                        <fieldset id="telefonos_wrap">
                            <legend>Teléfonos - <a href="#" class="agregar_item" onclick="agregarTelefono(event)">Agregar nuevo teléfono</a></legend>
                            <div class="telefono_item">
                                <div class="innner_content">
                                    <label for="tipo_tel_1">Tipo de teléfono</label>
                                    <select class="input tel_tipo" id="tipo_tel_1" name="tipo_tel_1">
                                        <option value="MOVIL" selected="selected">MOVIL</option>
                                        <option value="FIJO">FIJO</option>
                                    </select>
                                    <label for="num_tel_1">Nº de teléfono</label>
                                    <input class="input-half tel_num" type="text" id="num_tel_1" name="num_tel_1" placeholder="NNNNNNNNN">
                                    <label for="obs_tel_1">Observaciones</label>
                                    <input class="input-half tel_obs" type="text" id="obs_tel_1" name="obs_tel_1" placeholder="Observaciones">
                                </div>
                            </div>
                        </fieldset>
                        <div class="clear"></div>
                        <div class="submit_form">
                            <input type="submit" id="submit" value="Agregar">
                        </div>
                    </div>
                    <p class="login-error">
                    </p>
                </form>
            </section>

        </div>
            
        <div id="wp-loader" style="display: none;">
            <div class="loader"></div>
        </div>
    </body>
</html>
