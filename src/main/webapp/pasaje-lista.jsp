<%-- 
    Document   : pasaje-lista
    Created on : 06-jul-2019, 5:54:26
    Author     : Leonardo Pérez
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <c:import url="/parts/head.jsp"></c:import>

        </head>
        <body>
            <div class="container-fluid">
            <c:import url="/parts/header-docente.jsp"></c:import>

                <div class="row">
                    <div class="col-md-12">
                        <label id="lblAviso"></label>
                    </div>
                </div>
                <form method="post" action="pasajelista" id="formLista">
                    <div class="form-row">
                        <fieldset class="form-group">
                        <c:choose>
                            <c:when test="${param.evento eq 'Exito'}">
                                <div class="bg-success">
                                    <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                                </div>

                            </c:when>
                            <c:when test="${param.evento eq 'Error'}">
                                <div class="bg-danger">
                                    <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                                </div> 
                            </c:when>
                            <c:otherwise>
                                <label id="lblAviso"></label>
                            </c:otherwise>
                        </c:choose>
                    </fieldset>
                </div>
                <div class="form-row">
                    <div class="form-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Pasando lista</h3>
                            </div>
                            <div class="panel-body">
                                Fecha : ${fecha}  Horario : ${horario}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="table-responsivetable">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <td>Matricula</td>
                        <td>Primer nombre</td>
                        <td>Segundo nombre</td>
                        <td>Primer apellido</td>
                        <td>Segundo apellido</td>
                        <td>Falta</td>
                        <td>Tipo de falta</td>

                        </thead>
                        <tbody id="cuerpoTabla">
                            <c:forEach var="alumno" items="${listaAlumnos}" varStatus="loop">
                                <tr id="tr${loop.index}">
                                    <td>${alumno.matricula}</td>
                                    <td>${alumno.primerNombre}</td>  
                                    <td>${alumno.segundoNombre}</td>  
                                    <td>${alumno.primerApellido}</td>  
                                    <td>${alumno.segundoApellido}</td>
                                    <td><input type="checkbox" name="chk${loop.index}" id ="chk${loop.index}"/></td>
                                    <td>
                                        <select class="form-control" id="sltTipoFalta${loop.index}" name="tipoFalta${loop.index}">
                                            <option>Seleccionar tipo falta</option>
                                            <option value="JUSTIFICADA">Justificada</option>
                                            <option value="INJUSTIFICADA">Injustificada</option>
                                            <option value="TARDE">Tarde</option>
                                            <option value="MUY_TARDE">Muy tarde</option>
                                        </select>
                                    </td>  

                                </tr>
                            </c:forEach>
                    </table>
                </div>
                <div class="form-row">
                    <fieldset class="form-group">
                        <div class="col-md-4">

                            <button class="btn btn-primary" id="btnGuardar" type="submit">
                                <span class="glyphicon glyphicon-floppy-disk"></span> Guardar cambios  
                            </button>
                        </div>

                    </fieldset>
                </div>
            </form>
        </div>
        <script src="resources/js/pasajeLista.js"></script>
        <script type="text/javascript">
            // VISTA - VIEW
           /* var vistaWeb = new EventSource("listadogrupo");

            vistaWeb.addEventListener("AvisoFechaRepetida", function (evento) {
                $("#lblAviso").addClass("bg-danger");
                $("#lblAviso").html(evento.data);
            });
            vistaWeb.addEventListener("FechaOK", function (evento) {
                $("#lblAviso").addClass("bg-info");
                $("#lblAviso").html(evento.data);
            });
            vistaWeb.addEventListener("Exito", function (evento) {
                $("#lblAviso").addClass("bg-success");
                $("#lblAviso").html(evento.data);
            });
            vistaWeb.addEventListener("Error", function (evento) {
                $("#lblAviso").addClass("bg-danger");
                $("#lblAviso").html(evento.data);
            });*/
        </script>

    </body>
</html>
