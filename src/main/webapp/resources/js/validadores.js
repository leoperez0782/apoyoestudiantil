/* 
 * Author: Polachek
 */

function validaCI(miCi) {
    if(!miCi.length){
        console.log("nulo");
        return false;
    }else{
        var isnum = /^\d+$/.test(miCi);
        if(isnum){
            return true;
        }else{
            console.log("Mal");
            return false;
        }              
    }
}

function validaEmail(email) {
  var match1 = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)(?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|1[0-4][0-9]|[01]?[0-9][0-9]?))\])/.test(email);
  var match2 =  /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/.test(email);
  if(match1 || match2){
      return true;
  }else{
      return false;
  }
}

function validaPalabra(inputtxt){ 
  var letras = /^[A-Za-z]+$/;
  if(inputtxt.match(letras)){
    return true;
  }
  else{
    return false;
  }
}

function validaNumero(inputtxt){ 
  if(isNaN(inputtxt)){
    return false;
  }else{
    return true;
  }
}

function validarForm(miForm) {
    var valido = true;
    if(!miForm.documento.value){
        miForm.documento.classList.add("error");
        valido = false;
    }
    if(!miForm.ci_emisor.value){
        miForm.documento.classList.add("error");
        valido = false;
    }
    if(!miForm.ci_tipo.options[miForm.ci_tipo.selectedIndex].value){
        miForm.ci_tipo.classList.add("error");
        valido = false;
    }
    if(!miForm.nombre.value){
        miForm.nombre.classList.add("error");
        valido = false;
    }else if(!validaPalabra(miForm.nombre.value)){
        miForm.nombre.classList.add("error");
        valido = false;
    }
    if(!miForm.apellido.value){
        miForm.apellido.classList.add("error");
        valido = false;
    }else if(!validaPalabra(miForm.apellido.value)){
        miForm.apellido.classList.add("error");
        valido = false;
    }
    if(!miForm.fechan.value){
        miForm.fechan.classList.add("error");
        valido = false;
    }
    if(!miForm.email.value){
        miForm.email.classList.add("error");
        valido = false;
    }else if(!validaEmail(miForm.email.value)){
        miForm.email.classList.add("error");
        valido = false;
    }

    return valido;
}

function formLimpiaErrores() {
    $('#pasos .paso').removeClass('error');
    $('form.form-layout-1 input').removeClass('error');
    $('form.form-layout-1 select').removeClass('error');
}
