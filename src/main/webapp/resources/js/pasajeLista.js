$("#btnGuardar").on("click", function (event) {
    event.preventDefault();
    $("#lblAviso").html("");
    $("lblAviso").removeClass("bg-warning");
    var contador = 0;
    var valido = true;
    var fecha = $("#txtFecha").val();
    if (fecha === "") {
        $("#lblAviso").html("Debe seleccionar una fecha");
        $("#lblAviso").addClass("bg-danger");
    } else {
        $("#cuerpoTabla").find("tr").each(function () {
            $(this).removeClass("bg-danger");
            var idChk = "#chk" + contador;
            var idSlc = "#sltTipoFalta" + contador;
            var falto = $(this).find(idChk).prop("checked");
            var tipoFalta = $(this).find(idSlc).val();
            var idFila = $(this).attr("id");

            if ((falto && (tipoFalta === "Seleccionar tipo falta") || (!falto && (tipoFalta !== "Seleccionar tipo falta")))) {
                valido = false;

                $(this).addClass("bg-danger");
            }
            contador++;
        });


        if (valido) {
            //$("#formLista").submit();
            var form = document.getElementById("formLista");
            form.submit();
        } else {
            $("#lblAviso").html("En las filas marcadas en rojo no corresponde la asistencia con el tipo de falta");
            $("#lblAviso").addClass("bg-danger");
        }
    }

});
$("#txtFecha").on("change", function (event) {
    event.preventDefault();
    var fecha = $(this).val();
    
    $.get("listadogrupo?fecha=" + fecha);
    
});



