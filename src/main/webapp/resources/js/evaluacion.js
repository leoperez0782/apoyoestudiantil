$(document).ready(function(){
    ocultar("#panelListadoEv");
});

$("#crearEv").click(verCrear);
$("#mostrarEv").click(verMostrar);
$(".chkEval").change(verificarCheckBox);

function verCrear(event){
    event.preventDefault();
    mostrar("#panelCrear");
    ocultar("#panelListadoEv");
    $("#txtFecha").prop("required", true);
    limpiarSeleccion('rbtEval');
}
function verMostrar(event){
    event.preventDefault();
    mostrar("#panelListadoEv");
    ocultar("#panelCrear");
    $("#txtFecha").prop("required", false);
    limpiarSeleccion('chkSoloEval');
}

function ocultar(id){
    $(id).hide();
}

function mostrar(id){
    $(id).show();
}

function verificarCheckBox(){
    if($(this).is(":checked")){
        var id = $(this).attr("id").substring(3);
        $("#evSeleccionada").val(id);
    }else{
        $("#evSeleccionada").val("");
    }
}

function limpiarSeleccion(nombre){
    var nombreRbt = '[name=' + nombre + ']';
    // document.querySelectorAll('[name=rbtEval]').forEach((x) => x.checked = false);
     document.querySelectorAll(nombreRbt).forEach((x) => x.checked = false);
     $("#evSeleccionada").val("");
}