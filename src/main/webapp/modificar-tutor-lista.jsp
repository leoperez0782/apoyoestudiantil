<%-- 
    Document   : modificar-tutor-lista
    Created on : 28-jul-2019, 17:42:55
    Author     : Polachek
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@ include file="parts/head.jsp" %>
        <script type="text/javascript">
            var vistaWeb = new EventSource("modtutorlista");
            //Mostrar SaludoUsuario
            vistaWeb.addEventListener("mostrarUsuario", function (evento) {
                document.getElementById("usuario").innerHTML = evento.data;

            }, false);
          
            vistaWeb.addEventListener("Error", function (evento) {
                $('#error-tutor').html(evento.data);
                $('#error-tutor').show();
            }, false);
            
            vistaWeb.addEventListener("resultadoBusqueda", function (evento) {
                $('#resulatdoBusqueda').html(evento.data);
                if($('#tutor-item #tutor-foto img').attr('src') === '') { 
                    var imgPath = "resources/images/user-photo.png";
                    $('#tutor-item #tutor-foto img').attr("src",imgPath);
                }
                $('#seleccionar_tutor').show();
            }, false);
            
            vistaWeb.addEventListener("Exito", function (evento) {
                $("#lblAviso").addClass("bg-success");
                $("#lblAviso").val(evento.data);
            }, false);
            
            vistaWeb.addEventListener("redireccion", function (evento) {
                window.location.replace(evento.data);
            }, false);
            
            //borrar
            vistaWeb.addEventListener("alert", function (evento) {
                alert(evento.data);
            }, false);
        </script> 
        <script type="text/javascript">
            // FUNCIONES - FUNCTIONS 
            function buscarTutor() {
                $('#error-tutor').hide();
                $('#seleccionar_tutor').hide();
                $('#resulatdoBusqueda').html("");
                var ciFormat = $('#buscar-tutor').val().replace('-', '');
                ciFormat = ciFormat.replace('.', '');
                ciFormat = ciFormat.replace(/ /g, '');
                if(validaCI(ciFormat)){
                    var busqueda = "numDoc="+ ciFormat;
                    $.get("modtutorlista?accion=buscar&"+busqueda , function (data){});
                }else{
                    $('#error-tutor').html("Debe ingresar una cédula en el formato N.NNN.NNN-N o NNNNNNNN");
                    $('#error-tutor').show();
                }
            }
            
            function seleccionarTutor(e) {
                e.preventDefault();
                $.get("modtutorlista?accion=tutorSeleccionado", function (data){});
            }
            
        </script>
    </head>
    <body class="modificar-tutor modificar-tutor-lista">
        <%@ include file="parts/header.jsp" %>
        <div class="container-fluid">
            <h1 id="main-title">Buscar tutor por cédula</h1>
            <form onsubmit="event.preventDefault(); buscarTutor();">
                <input type="text" id="buscar-tutor" placeholder="Buscar.." name="Buscar">
                <button type="submit"><i class="fa fa-search"></i></button>
            </form>
            <p id="error-tutor" style="display:none"></p>
            <div id="resulatdoBusqueda">
                
            </div>
            <div id="seleccionar_tutor" style="display:none">
                <a href="#" id="seleccionar_tutor_link" onclick="seleccionarTutor(event)">Seleccionar</a>
            </div>
        </div>
    </body>
</html>
