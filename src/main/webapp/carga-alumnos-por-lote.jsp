<%-- 
    Document   : carga-alumnos-por-lote
    Created on : 12-jun-2019, 17:46:04
    Author     : leo
--%>
<%
    String mensaje = request.getParameter("mensaje");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@ include file="parts/head.jsp" %>
    </head>
    <body>
        <div class="container-fluid">
            <%@ include file="parts/header.jsp" %>
            <div>
                <form  action="uploadalumnos" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <!--<input class="form-control" type="text" name="description" />-->
                        <input class="form-control-file" type="file" name="file" />

                    </div>
                    <div class="form-group">
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" name="optradio" value="Simple">Carga Simple
                            </label>
                        </div>
                        <div class="form-check-inline">
                            <label class="form-check-label">
                                <input type="radio" class="form-check-input" checked="true" name="optradio" value="Completa">Carga completa
                            </label>
                        </div>
                    </div>
                    <input  class="btn btn-primary" type="submit" />
                </form>
                <%
                    if (mensaje != null) {
                        out.println( mensaje);
                    }
                %>
            </div>
            <div id="mensajes"></div>
        </div>
        <script type="text/javascript">
            var vistaWeb = new EventSource("uploadalumnos");

            vistaWeb.onerror = function (evento) {
               // alert("Sin conexion con el servidor o UnsupportedOperationException");
                //vistaWeb.close();
                //document.location = "/carga-alumnos-por-lote";
            };

            vistaWeb.addEventListener("Exito", function(evento){
                document.getElementById("mensajes").innerHTML = evento.data;
            });
            vistaWeb.addEventListener("Error", function(evento){
                document.getElementById("mensajes").innerHTML = evento.data;
            });
        </script>    
    </body>
</html>
