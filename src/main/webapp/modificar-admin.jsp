<%-- 
    Document   : modificar-admin
    Created on : 31-ago-2019, 18:55:11
    Author     : Polachek
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Modificar Usuario</title>
        <%@ include file="parts/head.jsp" %>
         <script type="text/javascript">
            // VISTA - VIEW
            var url = window.location.href;
            var arguments = url.split('?')[1];
            var vistaWeb = new EventSource("modadminlista?accion="+arguments); 
           
            vistaWeb.addEventListener("Error", function (evento) {
                document.getElementById('wp-loader').style.display = "none";
                document.getElementById("mensajeError").innerHTML = "<p>"+evento.data+"</p>";                
            }, false);
            
            vistaWeb.addEventListener("Exito", function (evento) {
                document.getElementById('wp-loader').style.display = "none";
                document.getElementById("mensajeExito").innerHTML = "<p>"+evento.data+"</p>"; 
            }, false);  
            
            vistaWeb.addEventListener("mostrarListaUsuarios", function (evento){
                hideLoader();
                document.getElementById("listaUsuarios").innerHTML=evento.data;                
            },false);
            
            vistaWeb.addEventListener("redirectModificar", function (evento) {
                window.location.replace("modificar-admin-form.jsp");
            }, false);
            
        </script>
        <script type="text/javascript">
            // FUNCIONES - FUNCTIONS                       
            function buscar() {
                showLoader();
                $('#mensajeExito').html("");
                $('#mensajeError').html("");
                $('#listaUsuarios').html("");
                var ciFormat = $('#buscar-ci').val().replace('-', '');
                ciFormat = ciFormat.replace('.', '');
                ciFormat = ciFormat.replace(/ /g, '');
                if(validaCI(ciFormat)){
                    var busqueda = "numDoc="+ ciFormat;
                    $.get("modadminlista?accion=buscar&"+busqueda , function (data){});
                }else{
                    $('#mensajeError').html("Debe ingresar una cédula en el formato N.NNN.NNN-N o NNNNNNNN");
                }
            }
            
            function mostrarTodos(e) {
                e.preventDefault();
                showLoader();
                $('#mensajeExito').html("");
                $('#mensajeError').html("");
                $.get("modadminlista?accion=mostrarTodos", function (data){});
            }
            
            function modificarUsuario(e,numDoc){
                e.preventDefault();
                showLoader();
                $('#mensajeExito').html("");
                $('#mensajeError').html("");
                 var params = "numDoc="+ numDoc;
                $.get("modadminlista?accion=modificar&"+params, function (data){});
            }
        </script>
    </head>
    <body class="mod_admin_lista">
        <%@ include file="parts/header.jsp" %>
        
        <div class="container-fluid">
            <!-- Mensajes -->
            <div id="mensajeExito"></div>
            <div id="mensajeError"></div>
            
            <div class="col-md-6 center">
                <a href="#" class="show_all inline-block" onclick="mostrarTodos(event)">Mostrar todos</a>
            </div>
            <div class="col-md-6 center">
                <h1 id="main-title">Buscar por numero cédula</h1>
                <form onsubmit="event.preventDefault(); buscar();">
                    <input type="text" id="buscar-ci" placeholder="Buscar.." name="Buscar">
                    <button type="submit"><i class="fa fa-search"></i></button>
                </form>
            </div>
            <div id="listaUsuarios">
                
            </div>
        </div>
        <div id="wp-loader" style="display: none;">
            <div class="loader"></div>
        </div>
    </body>
</html>
