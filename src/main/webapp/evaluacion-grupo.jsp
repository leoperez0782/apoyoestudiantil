<%-- 
    Document   : evaluacion-grupo
    Created on : 30-jul-2019, 23:07:54
    Author     : Leonardo Pérez
--%>
<%@page import="java.util.Arrays"%>
<%@page import="com.apoyoestudiantil.entity.Evaluacion"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%> 
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="com.apoyoestudiantil.entity.Evaluacion.TipoEvaluacion"%>
<% TipoEvaluacion[] tiposEv = Evaluacion.TipoEvaluacion.values();
    request.getSession().setAttribute("tiposEv", tiposEv);
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <c:import url="/parts/head.jsp"></c:import>

        </head>
        <body>
            <div class="container-fluid">
            <c:import url="/parts/header-docente.jsp">

            </c:import>  
            <form action="evaluargrupo" method="post" enctype="multipart/form-data">
                <!--inicio mensajes -->
                <div class="form-row">
                    <fieldset class="form-group">
                        <c:choose>
                            <c:when test="${param.evento eq 'Exito'}">
                                <div class="bg-success">
                                    <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                                </div>

                            </c:when>
                            <c:when test="${param.evento eq 'Error'}">
                                <div class="bg-danger">
                                    <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                                </div> 
                            </c:when>
                            <c:otherwise>
                                <label id="lblAviso"></label>
                            </c:otherwise>
                        </c:choose>
                    </fieldset>
                </div>
                <!--fin mensajes -->
                <!-- pestanas -->
                <div class="form-row">
                    <ul class="nav nav-tabs">
                        <li role="presentation"  class="active"><a  href="#" id="crearEv">Crear evaluacion</a></li>
                        <li role="presentation"><a href="#" id="mostrarEv">Evaluaciones sin calificar</a></li>

                    </ul>
                </div>
                <!-- inicio  crear evaluacion -->
                <div class="form-row" id="panelCrear"> 
                    <div class="form-group">
                        <div class="panel panel-primary">

                            <div class="panel-heading">
                                <h3 class="panel-title">Creando nueva evaluación</h3>
                            </div>
                            <div class="panel-body">
                                <p>
                                    <label for="slcPeriodo">Periodo</label>
                                    <select class="form-control" id="slcPeriodo" name="periodo">
                                        <c:forEach var="periodo" items="${libretaActual.getPeriodos()}" varStatus="loop">
                                            <option value="${periodo.getNombre()}">${periodo.getNombre()}</option>
                                        </c:forEach>
                                    </select>
                                </p>
                                <p>
                                    <label for="slcEvaluacion">Tipo evaluacion</label>
                                    <select class="form-control" id="slcEvaluacion" name="tipoEvaluacion">
                                        <c:forEach var="tipo" items="${tiposEv}" varStatus="loop">
                                            <option value="${tipo.toString()}">${tipo.toString()}</option>
                                        </c:forEach>
                                    </select>
                                </p>
                                <p>
                                    <label for="txtFecha">Fecha</label>
                                    <input type="date" class="form-control" id="txtFecha" name="fecha" placeholder="Ingrese la fecha" required>
                                </p>
                                <p>
                                    <label for="txtComentario">Comentario</label>
                                    <textarea type="text" class="form-control" id="txtComentario" name="comentario" placeholder="Comentario adicional" >
                                        
                                    </textarea>
                                </p>
                                <p>
                                    <label for="fulFile">Seleccione el archivo</label>
                                    <input class="form-control-file" type="file" id="fulFile" name="fulFile" />
                                </p>
                                <p>
                                    <label for="chkSoloEval">Solo crear evaluacion</label>
                                    <input type="radio" id="chkSoloEval" name="chkSoloEval" value="crear"/>
                                </p>
                                <p>
                                    <label for="chkCrearYCalif">Crear y calificarevaluacion</label>
                                    <input type="radio" id="chkCrearYCalif" name="chkSoloEval" value="calificar"/>
                                </p>
                            </div>
                        </div>

                    </div>
                </div>
                <!--fin datos evaluacion -->
                <!-- inicio listado evaluaciones -->
                <div class="form-row" id="panelListadoEv">
                    <div class="form-group">
                        <div class="panel panel-primary">
                            <div class="panel-heading">
                                <h3 class="panel-title">Evaluaciones pendientes de calificación</h3>
                            </div>
                            <div class="panel-body">
                                <div class="table-responsivetable">
                                    <table class="table table-bordered table-hover">
                                        <thead>
                                        
                                        <td>Fecha</td>
                                        <td>Seleccionar</td>

                                        </thead>
                                        <tbody id="tablaEvaluaciones">
                                            <c:forEach var="desarrollo" items="${desarrolloEvaluaciones}" varStatus="loop">
                                                <c:choose>
                                                    <c:when test="${desarrollo.evaluacion and not desarrollo.corregida}">
                                                        <tr id="tr${loop.index}">

                                                            <td> 
                                                                <fmt:parseDate value="${desarrollo.fecha}" type="date" pattern="yyyy-MM-dd" var="fechaConvertida" />
                                                                <fmt:formatDate type="date" value="${fechaConvertida}" pattern="yyyy-MM-dd" />
                                                            </td>
                                                            <td>
                                                                <input type="radio" class="chkEval" name="rbtEval" id ="chk${desarrollo.id}"/>
                                                            </td>  

                                                        </tr>
                                                    </c:when>
                                                </c:choose>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                                <input type="hidden" name="evSeleccionada" id="evSeleccionada">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- fin listado evaluaciones -->
                <!-- tabla alumnos -->
                <div class="table-responsivetable">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <td>Foto</td>
                        <td>Estudiante</td>
                        <td>Comentario</td>
                        <td>Calificacion</td>


                        </thead>
                        <tbody id="cuerpoTabla">
                            <c:forEach var="alumno" items="${alumnosGrupo}" varStatus="loop">
                                <tr id="tr${loop.index}">
                                    <td>foto</td>
                                    <td>${alumno.primerNombre} ${alumno.primerApellido} ${alumno.segundoApellido}</td>  

                                    <td><input type="text" name="cmt${loop.index}" id ="cmt${loop.index}"/></td>
                                    <td>
                                        <input type="text" name="cal${loop.index}" id ="cal${loop.index}"/>
                                    </td>  

                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <!-- fin tabla alumnos -->
                <div class="form-row">
                    <fieldset class="form-group">
                        <div class="col-md-4">

                            <button class="btn btn-primary" id="btnGuardar" type="submit">
                                <span class="glyphicon glyphicon-floppy-disk"></span> Guardar cambios  
                            </button>
                        </div>

                    </fieldset>
                </div>    
            </form>
        </div>
        <script  src="resources/js/evaluacion.js">

        </script>
    </body>
</html>
