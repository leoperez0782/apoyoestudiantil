<%-- 
    Document   : inicio-tutor
    Created on : 04-ago-2019, 19:48:17
    Author     : Polachek
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tutor</title>
        <%@ include file="parts/head.jsp" %>
        <script type="text/javascript">
            var vistaWeb = new EventSource("initutor");
            
            vistaWeb.addEventListener("Error", function (evento) {
                document.getElementById("mensajeError").innerHTML = "<p>"+evento.data+"</p>";                
            }, false);
            
            vistaWeb.addEventListener("Exito", function (evento) {
                document.getElementById("mensajeExito").innerHTML = "<p>"+evento.data+"</p>"; 
            }, false);
            
            vistaWeb.addEventListener("listadoAlumnos", function (evento) {
                document.getElementById("hijos-lista").innerHTML = evento.data;
            }, false);
            
            vistaWeb.addEventListener("mostrarNotificaciones", function (evento) {
                notificaciones(evento.data);
            }, false);
            
            vistaWeb.addEventListener("mostrarNotificacion", function (evento) {
                mostrarNotificacion(evento.data);
            }, false);
            
            vistaWeb.addEventListener("hayNuevasNotificaciones", function (evento) {
                hayNuevasNotificaciones();
            }, false);
            
            vistaWeb.addEventListener("noHayNuevasNotificaciones", function (evento) {
                noHayNuevasNotificaciones();
            }, false);
            
            vistaWeb.addEventListener("redireccion", function (evento) {
                window.location.replace(evento.data);
            }, false);
        </script>
        <script type="text/javascript">
            // FUNCIONES - FUNCTIONS    
            function tutorEstudiante(e,doc) {
                e.preventDefault();
                var params = "docAlm="+doc;
                $.get("initutor?accion=tutorAlumno&"+params , function (data){});
            }
            
            function panelCentral(e) {
                e.preventDefault();
                $.get("initutor?accion=comprobarNotificaciones" , function (data){});
                $("#panel-secundario").slideUp( "slow", function() {
                    $("#panel-central").slideDown( "slow" );
                    $(".pan-sec").hide();
                    $(".panel-terciario").hide();                    
                });
            }
            
            function panel(e,accion) {
                e.preventDefault();
                $.get("initutor?accion="+accion , function (data){});
            }
            
            function notificaciones(datos) {
                $(".pan-sec").hide();
                $("#tabla-notificaciones").show();
                $("#tabla-notificaciones").html(datos);
                $("#panel-central").slideUp( "slow", function() {
                    $("#panel-secundario").slideDown( "slow" );
                });
            }
            
            function hayNuevasNotificaciones() {
                $("#img-notificaciones").attr("src", "resources/images/notificaciones-nueva.png");
            }
            
            function noHayNuevasNotificaciones() {
                $("#img-notificaciones").attr("src", "resources/images/notificaciones.png");
            }
            
            function verNotificacion(e,idN) {
                e.preventDefault();
                var params = "idN="+idN;
                $.get("initutor?accion=verNotificacion&"+params , function (data){});
            }
            
            function mostrarNotificacion(datos) {
                $("#ver-notificacion").html(datos);
                $("#tabla-notificaciones").slideUp( "slow", function() {
                    $("#ver-notificacion").slideDown( "slow" );
                });
            }
            
            function cerrarNotificacion(e) {
                e.preventDefault();
                $("#ver-notificacion").slideUp( "slow", function() {
                    $("#panel-secundario").hide();
                    panel(e,'notificaciones');                  
                });
            }            
        </script>
    </head>
    <body class="tutor-dash dashboard">
        <%@ include file="parts/header.jsp" %>
        <!-- Mensajes -->
        <div id="mensajeExito"></div>
        <div id="mensajeError"></div>
        
        <div class="col-md-10" id="panel-central">
            <div id="hijos-lista">

            </div>

            <a href="#" onclick="panel(event,'notificaciones')" class="item-dash">
                <img src="resources/images/notificaciones.png" id="img-notificaciones" alt="Notificaciones" />
                <span>Notificaciones</span>
            </a>
        </div>
        <div class="col-md-10" id="panel-secundario" style="display:none">
            <a href="#" onclick="panelCentral(event)" style="font-size: 20px;">&larr; Volver </a>
            <table class="pan-sec table_style_1" id="tabla-notificaciones">

            </table>
            <div id="ver-notificacion" class="panel-terciario" style="display:none">

            </div>
        </div>
    </body>
</html>
