<%-- 
    Document   : ini-administrativo
    Created on : 04-ago-2019, 19:36:52
    Author     : Polachek
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrativo</title>
        <%@ include file="parts/head.jsp" %>
        <script type="text/javascript">
            // FUNCIONES - FUNCTIONS 
            function panelsec(e,panel) {
                e.preventDefault();
                $("#panel_central").slideUp( "slow", function() {
                    $(panel).slideDown( "slow" );
                });
            }
            
            function panelcentral(e) {
                e.preventDefault();
                $(".panel_secundario").slideUp( "slow", function() {
                     $("#panel_central").slideDown( "slow" );
                });
            }          
            
        </script>
    </head>
    <body class="admin dashboard">
        <%@ include file="parts/header.jsp" %>

        <div class="container-fluid">
            <div id="panel_central">                
                <a href="#" onclick="panelsec(event,'#panel_estudiante')" id="add-est" class="item-dash">
                    <img src="resources/images/student-add.png" alt="Estudiante" />
                    <span>Estudiante</span>
                </a>
                
                <a href="#" onclick="panelsec(event,'#panel_tutor')" id="add-est" class="item-dash">
                    <img src="resources/images/tutor.png" alt="Tutor" />
                    <span>Tutor</span>
                </a>
                
                <a href="#" onclick="panelsec(event,'#panel_docente')" id="add-est" class="item-dash">
                    <img src="resources/images/docente-add.png" alt="Docente" />
                    <span>Docente</span>
                </a>
            </div>   
            
            <!-- ** ESTUDIANTE **-->
            <div class="panel_secundario" id="panel_estudiante">
                <a href="#" onclick="panelcentral(event)" id="doc-noti" class="item-dash">
                    <img src="resources/images/back-icon.png" alt="Volver" />
                    <span>Volver</span>
                </a>                
                <a href="estudiante-agregar.jsp" id="add-est" class="item-dash">
                    <img src="resources/images/student-add.png" alt="Agregar estudiante" />
                    <span>Agregar estudiante</span>
                </a>
                <a href="estudiante-modificar.jsp" id="edit-est" class="item-dash">
                    <img src="resources/images/student-edit.png" alt="Modificar estudiante" />
                    <span>Modificar estudiante</span>
                </a>
                <a href="carga-alumnos-excel.jsp" id="load-est" class="item-dash">
                    <img src="resources/images/students-lote.png" alt="Carga alumnos por lote" />
                    <span>Carga alumnos por lote</span>
                </a>
                <!--<a href="uploadalumnos" id="load-est" class="item-dash">Carga alumnos por lote</a>-->  
            </div>            
            <!-- ** /ESTUDIANTE **-->

            <!-- ** TUTOR **-->
            <div class="panel_secundario" id="panel_tutor">
                <a href="#" onclick="panelcentral(event)" id="doc-noti" class="item-dash">
                    <img src="resources/images/back-icon.png" alt="Volver" />
                    <span>Volver</span>
                </a>
                <a href="alta-tutor.jsp" id="add-tutor" class="item-dash">
                    <img src="resources/images/tutor.png" alt="Agregar tutor" />
                    <span>Agregar tutor</span>
                </a>
                <a href="modificar-tutor-lista.jsp" id="mod-tutor" class="item-dash">
                    <img src="resources/images/tutor.png" alt="Modificar tutor" />
                    <span>Modificar tutor</span>
                </a>                
            </div>
            <!-- ** /TUTOR **-->

            <!-- ** DOCENTE **-->
            <div class="panel_secundario" id="panel_docente">
                <a href="#" onclick="panelcentral(event)" id="doc-noti" class="item-dash">
                    <img src="resources/images/back-icon.png" alt="Volver" />
                    <span>Volver</span>
                </a>
                <a href="alta-docente.jsp" id="add-teach" class="item-dash">
                    <img src="resources/images/docente-add.png" alt="Agregar docente" />
                    <span>Agregar docente</span>
                </a>
                <a href="uploaddocentes" id="load-teach" class="item-dash">
                    <img src="resources/images/docentes-lote.png" alt="Carga docentes por lote" />
                    <span>Carga docentes por lote</span>
                </a>
                <a href="listadodocente" id="list-teach" class="item-dash">
                    <img src="resources/images/docentes-lote.png" alt="Listado docente" />
                    <span>Listado docente</span>
                </a>  
                
                <a href="cierre-reuniones-direccion.jsp" id="list-teach" class="item-dash">
                    <img src="resources/images/docentes-lote.png" alt="Cierre de reuniones" />
                    <span>Cierre de reuniones</span>
                </a>
            </div> 
            <!-- ** /DOCENTE **-->
        </div>
    </body>
</html>