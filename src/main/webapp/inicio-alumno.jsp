<%-- 
    Document   : ini-alumno
    Created on : 04-ago-2019, 19:40:13
    Author     : Polachek
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Alumno</title>
        <%@ include file="parts/head.jsp" %>
        <script type="text/javascript">
            var vistaWeb = new EventSource("inialumno");
          
            vistaWeb.addEventListener("Error", function (evento) {
                document.getElementById('wp-loader').style.display = "none";
                document.getElementById("mensajeError").innerHTML = "<p>"+evento.data+"</p>";                
            }, false);
            
            vistaWeb.addEventListener("Exito", function (evento) {
                document.getElementById('wp-loader').style.display = "none";
                document.getElementById("mensajeExito").innerHTML = "<p>"+evento.data+"</p>"; 
            }, false);
            
            vistaWeb.addEventListener("mostrarAlumno", function (evento) {
                document.getElementById("alumno-seleccionado").innerHTML = evento.data;
            }, false); 
            
            vistaWeb.addEventListener("redireccion", function (evento) {
                window.location.replace(evento.data);
            }, false);
            
            vistaWeb.addEventListener("Inasistencias", function (evento) {
                contenido(evento.data);
            }, false);
            
            vistaWeb.addEventListener("Evaluaciones", function (evento) {
                contenido(evento.data);
            }, false);
            
            vistaWeb.addEventListener("mostrarPromediosVacio", function (evento) {
                contenido("<tr><td>"+evento.data+"</td></tr>");
            }, false); 
            
            vistaWeb.addEventListener("mostrarPromedios", function (evento) {
                contenido(evento.data);
            }, false);
            
            vistaWeb.addEventListener("mostrarCalendario", function (evento) {
                contenido(evento.data);
                addeventatc.refresh();
            }, false);
            
            vistaWeb.addEventListener("mostrarNotificaciones", function (evento) {
                notificaciones(evento.data);
            }, false);
            
            vistaWeb.addEventListener("mostrarNotificacion", function (evento) {
                mostrarNotificacion(evento.data);
            }, false);
            
            vistaWeb.addEventListener("hayNuevasNotificaciones", function (evento) {
                hayNuevasNotificaciones();
            }, false);
            
            vistaWeb.addEventListener("noHayNuevasNotificaciones", function (evento) {
                noHayNuevasNotificaciones();
            }, false);
        </script>
        <script type="text/javascript">
            // FUNCIONES - FUNCTIONS    
            function tutorEstudiante(e) {
                e.preventDefault();
                $("#panel-central").show();
                $("#panel-secundario").hide(); 
            }
            
            function panelCentral(e) {
                e.preventDefault();
                $("#panel-secundario").slideUp( "slow", function() {
                    $("#panel-central").slideDown( "slow" );
                    $(".pan-sec").hide();
                });
            }
            
            function panel(e,accion) {
                e.preventDefault();
                $.get("inialumno?accion="+accion , function (data){});
            }
            
            function contenido(datos) {
                limpiarmensajes();
                $(".pan-sec").hide();
                $("#tabla-panel-secundario").html(datos);
                $("#tabla-panel-secundario").show();                
                $("#panel-central").slideUp( "slow", function() {
                    $("#panel-secundario").slideDown( "slow" );
                });
            }
            
            function limpiarmensajes(){
                $("#mensajeExito").html("");
                $("#mensajeError").html("");
            }
            
            function notificaciones(datos) {
                $(".pan-sec").hide();
                $("#tabla-panel-secundario").show();
                $("#tabla-panel-secundario").html(datos);
                $("#panel-central").slideUp( "slow", function() {
                    $("#panel-secundario").slideDown( "slow" );
                });
            }
            
            function hayNuevasNotificaciones() {
                $("#img-notificaciones").attr("src", "resources/images/notificaciones-nueva.png");
            }
            
            function noHayNuevasNotificaciones() {
                $("#img-notificaciones").attr("src", "resources/images/notificaciones.png");
            }
            
            function verNotificacion(e,idN) {
                e.preventDefault();
                var params = "idN="+idN;
                $.get("inialumno?accion=verNotificacion&"+params , function (data){});
            }
            
            function mostrarNotificacion(datos) {
                $("#ver-notificacion").html(datos);
                $("#tabla-panel-secundario").slideUp( "slow", function() {
                    $("#ver-notificacion").slideDown( "slow" );
                });
            }
            
            function cerrarNotificacion(e) {
                e.preventDefault();
                $("#ver-notificacion").slideUp( "slow", function() {
                    $("#panel-secundario").hide();
                    panel(e,'notificaciones');                  
                });
            }
        </script>
        <!-- Calendar button -->
        <script type="text/javascript" src="resources/js/atc.min.js" async defer></script>
        <!-- //Calendar button -->
    </head>
    <body class="alumno dashboard">
        <%@ include file="parts/header.jsp" %>
        
        <div class="container-fluid">
            <!-- Mensajes -->
            <div id="mensajeExito"></div>
            <div id="mensajeError"></div>
            
            <div id="panel-central">
                <a href="#" onclick="panel(event,'asistencias')" id="doc-noti" class="item-dash">
                    <img src="resources/images/presente.svg" alt="Asistencias/Inasistencias" />
                    <span>Asistencias/Inasistencias</span>
                </a>
                <a href="#" onclick="panel(event,'calendario')" id="doc-noti" class="item-dash">
                    <img src="resources/images/calendario-evaluacion.png" alt="Calendario de evaluaciones" />
                    <span>Calendario de evaluaciones</span>
                </a>
                <a href="#" onclick="panel(event,'promedios')" id="doc-noti" class="item-dash">
                    <img src="resources/images/grados.png" alt="Promedios de materias" />
                    <span>Promedios de materias</span>
                </a>
                <a href="#" onclick="panel(event,'notificaciones')" class="item-dash">
                    <img src="resources/images/notificaciones.png" id="img-notificaciones" alt="Notificaciones" />
                    <span>Notificaciones</span>
                </a>
            </div>
            <div class="col-md-10" id="panel-secundario" style="display:none">
                <a href="#" onclick="panelCentral(event)" style="font-size: 20px;">&larr; Volver </a>
                <table class="pan-sec table_style_1" id="tabla-panel-secundario">

                </table>
                <div id="ver-notificacion" class="panel-terciario" style="display:none">

                </div>
            </div>
        </div>
        
        <div id="wp-loader" style="display: none;">
            <div class="loader"></div>
        </div>   
    </body>
</html>
