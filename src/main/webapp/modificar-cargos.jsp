<%-- 
    Document   : modificar-cargos
    Created on : 25-sep-2019, 22:44:53
    Author     : Polachek
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    <%@ include file="parts/head.jsp" %>
        
        <script type="text/javascript">
            // VISTA - VIEW
            var vistaWeb = new EventSource("modcargos");
            
            vistaWeb.addEventListener("mostrarCargos", function (evento){
                document.getElementById("tabla_cargos").innerHTML=evento.data;                
            },false);
            
            vistaWeb.addEventListener("mostrarCargo", function (evento){
                mostrarCargo(evento.data);
            },false);
            
            vistaWeb.addEventListener("Error", function (evento) {
                document.getElementById("mensajeError").innerHTML = "<p>"+evento.data+"</p>";                
            }, false);
            
            vistaWeb.addEventListener("Exito", function (evento) {
                document.getElementById("mensajeExito").innerHTML = "<p>"+evento.data+"</p>"; 
            }, false);                        
        </script>
        
        <script type="text/javascript">          
            function mostrar(e,idC) {
                e.preventDefault();
                var params = "idC="+idC;
                $.get("modcargos?accion=mostrar&"+params , function (data){});
            }
            
            function mostrarCargo(datos) {
                $("#mod-cargo").html(datos);
                $("#wrap-mod-cargo").slideDown( "slow" );
            }
            
            function cerrar(e) {
                e.preventDefault();
                $("#wrap-mod-cargo").slideUp( "slow" );
                $("#mensajeExito").html("");
                $("#mensajeError").html("");
            }
            
            function modificar(miForm) {
                $("#mensajeExito").html("");
                $("#mensajeError").html("");
                var params = "";
                if(miForm.tipoCargo.value){
                    var tipo = miForm.tipoCargo.value;
                    params += "tipo="+tipo;
                }
                if(miForm.descripcion.value){
                    var desc = miForm.descripcion.value;
                    params += "&desc="+desc;
                }
                $.get("modcargos?accion=guardar&"+params , function (data){});
            }
            
        </script>
    </head>
    <body class="alta_docente">
        <%@ include file="parts/header.jsp" %>
        
        <div class="container-fluid">            
            <!-- Mensajes -->
            <div id="mensajeExito"></div>
            <div id="mensajeError"></div>
            
            <div id="wrap-mod-cargo" style="display: none;">
                
                <div id="mod-cargo">
                
                </div>    
                <a href="#" onclick="cerrar(event)" style="font-size: 15px; margin-left: 8px;">Cerrar</a>
            </div>
            <div id="wp-cargos">
                <table id='tabla_cargos' class='table_style_1'>
                </table>
            </div>
            
        </div>
</body>
</html>