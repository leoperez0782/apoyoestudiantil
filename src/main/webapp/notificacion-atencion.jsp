<%-- 
    Document   : notificacion-atencion
    Created on : 5 sept. 2019, 21:29:20
    Author     : Leonardo Pérez
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <c:import url="/parts/head.jsp">

        </c:import>
        <script type="text/javascript">
            $(".notif").click(function(event){
                event.preventDefault();
                var nomTexArea = $(this).attr("id").substring(3);
                alert(nomTexArea);
                $("#txtSeleccionado").val(nomTexArea);
            });
            function mensajeSeleccionado(event, element){
                event.preventDefault();
                var nomTexArea = element.getAttribute("id").substring(3);
                //var valorAguardar = $("#txt" + nomTexArea).val();
                //alert(valorAguardar);
                $("#txtSeleccionado").val(nomTexArea);//guardo el indice
                $("#formLista").submit();
            }
        </script>
    </head>
    <body>
        <div class="container-fluid">
            <c:import url="/parts/header-docente.jsp"></c:import>

                <div class="row">
                    <div class="col-md-12">
                        <label id="lblAviso"></label>
                    </div>
                </div>
                <form method="post" action="notificacionatencion" id="formLista">
                    <div class="form-row">
                        <fieldset class="form-group">
                        <c:choose>
                            <c:when test="${param.evento eq 'Exito'}">
                                <div class="bg-success">
                                    <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                                </div>

                            </c:when>
                            <c:when test="${param.evento eq 'Error'}">
                                <div class="bg-danger">
                                    <label id="lblAviso" > <c:out value="${param.mensaje}"/></label>
                                </div> 
                            </c:when>
                            <c:otherwise>
                                <label id="lblAviso"></label>
                            </c:otherwise>
                        </c:choose>
                    </fieldset>
                </div>

                <div class="table-responsivetable">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <td>Matricula</td>
                        <td>Primer nombre</td>
                        <td>Segundo nombre</td>
                        <td>Primer apellido</td>
                        <td>Segundo apellido</td>
                        <td>Mensaje</td>
                        <td>Enviar</td>

                        </thead>
                        <tbody id="cuerpoTabla">
                            <c:forEach var="alumno" items="${alumnosGrupo}" varStatus="loop">
                                <tr id="tr${loop.index}">
                                    <td>${alumno.matricula}</td>
                                    <td>${alumno.primerNombre}</td>  
                                    <td>${alumno.segundoNombre}</td>  
                                    <td>${alumno.primerApellido}</td>  
                                    <td>${alumno.segundoApellido}</td>
                                    <td><textarea  name="txt${loop.index}" id ="txt${loop.index}"></textarea></td>
                                    <td>
                                        <button class="btn btn-primary notif" onclick="mensajeSeleccionado(event, this)" id="btn${loop.index}" type="button">
                                            <span class="glyphicon glyphicon-envelope"></span> Enviar  
                                        </button>
                                    </td>  

                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <input type="hidden" name="txtSeleccionado" id="txtSeleccionado">
                   
                </div>
                <div class="form-row">
                    <fieldset class="form-group">
                        <div class="col-md-8">
                            
                            <textarea class="form-control" id="txtNotifGrupal" placeholder="Enviar este mensaje a todo el grupo..." name="txtNotifGrupal"></textarea>
                        </div>
                        <div class="col-md-4">

                            <button class="btn btn-primary" id="btnGuardar" type="submit">
                                <span class="glyphicon glyphicon-envelope"></span> Enviar a todos  
                            </button>
                        </div>

                    </fieldset>
                </div>
            </form>
        </div>
    </body>
</html>
