<%-- 
    Document   : estudiante-agregar
    Created on : 18-jun-2019, 15:48:48
    Author     : Polachek
--%>
<%
    String mensaje = request.getParameter("mensaje");
%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Modificar Estudiante</title>
         <%@ include file="parts/head.jsp" %>
        
        <!-- <script src="http://code.jquery.com/jquery-latest.min.js"></script> -->
        <script type="text/javascript">
            // VISTA - VIEW
            var vistaWeb = new EventSource("estudiantemodificar");
           
            //Mostrar SaludoUsuario
            vistaWeb.addEventListener("mostrarUsuario", function (evento){
                document.getElementById("usuario").innerHTML=evento.data;                
            },false);
            
            vistaWeb.addEventListener("mostrarError", function (evento){
                alert(evento.data);                
            },false);
            
           
            //Mostrar Niveles
            vistaWeb.addEventListener("mostrarNiveles", function (evento){
                document.getElementById("listaNiveles").innerHTML=evento.data;                
            },false);
            
            //Mostrar Grupos
            vistaWeb.addEventListener("mostrarGrupos", function (evento){
                document.getElementById("listaGrupos").innerHTML=evento.data;                
            },false);
            
            //Mostrar Alumnos
            vistaWeb.addEventListener("mostrarAlumnos", function (evento){
                document.getElementById("listaAlumnos").innerHTML=evento.data;                
            },false);
            
            //Mostrar Form
            vistaWeb.addEventListener("mostrarFormAlumno", function (evento){
                window.location.replace("estudiante-modificar-form.jsp");                
            },false);
            
            vistaWeb.addEventListener("redireccion", function (evento) {
                window.location.replace(evento.data);
            }, false);
            
        </script>
        <script type="text/javascript">
            // FUNCIONES - FUNCTIONS                       
            function seleccionarNivel(e,element,datanivelid) {
                e.preventDefault();
                $( "#sel-niveles table tr" ).removeClass("selected");
                $(element).closest("tr").addClass('selected');
                $.get("estudiantemodificar?accion=nivels&nivelseleccionado="+datanivelid , function (data){});
            }
            
            function grupoSeleccionado(e,element,grupoid) {
                e.preventDefault();
                $( "#sel-grupo table tr" ).removeClass("selected");
                $(element).closest("tr").addClass('selected');
                $.get("estudiantemodificar?accion=grupo&gruposeleccionado="+grupoid , function (data){});
            }
            
            function editarAlumno(e,element) {
                e.preventDefault();
                var miID = $(element).attr("data-alumno-id");
                $.get("estudiantemodificar?accion=alumnos&alumnoseleccionado=" + miID , function (data){});
            }
            
            function mostrarNivel(e) {
                e.preventDefault();
                var sh = $( "#mostrar_ocultar_nivel" ).attr("data-show");
                if(sh === "hide"){
                    $("#sel-niveles #listaNiveles").show();
                    $("#sel-grupo").show();
                    $( "#mostrar_ocultar_nivel" ).attr("data-show", "show");
                    $( "#mostrar_ocultar_nivel" ).text("Ocultar lista");
                }else{
                    $("#sel-niveles #listaNiveles").hide();
                    $("#sel-grupo").hide();
                    $( "#mostrar_ocultar_nivel" ).attr("data-show", "hide");
                    $( "#mostrar_ocultar_nivel" ).text("Mostrar lista");
                }
                
            }
            
            function limpiarsession(e,element) {
                e.preventDefault();
                $.get("estudiantemodform?accion=limpiarsession",function(data){});
            }
        </script>
    </head>
    <body class="estudiante-agregar estudiante-modificar">
        <%@ include file="parts/header.jsp" %>
        
        <div class="container-fluid">
            <h1 id="usuario"></h1>
            <%
                if(mensaje!=null){
                    out.println("Error: " + mensaje);
                }
            %>
            
            <section id="sel-niveles">
                <h4>Selecciona por nivel - <a data-show="hide" id="mostrar_ocultar_nivel" href='#' onclick='mostrarNivel(event)' style="color: lightblue;">Mostrar lista</a></h4>
                <table id="listaNiveles" class="table_style_1" style="display:none">
                
                </table>
            </section>
            
            <section id="sel-grupo" style="display:none">
                <h4>Seleccione el Grupo</h4>
                <table id="listaGrupos" class="table_style_1">
                
                </table>
            </section>
            
            <section id="sel-estudiante">
                <h4>Seleccione el Alumno</h4>
                <table id="listaAlumnos" class="table_style_1">
                    
                </table>
            </section>
        </div>
    </body>
</html>
