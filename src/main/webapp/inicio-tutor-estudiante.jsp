<%-- 
    Document   : tutor-estudiante
    Created on : 04-ago-2019, 19:50:54
    Author     : Polachek
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Tutor - Estudiante</title>
        <%@ include file="parts/head.jsp" %>
        <script type="text/javascript">
            var vistaWeb = new EventSource("initutest");
          
            vistaWeb.addEventListener("Error", function (evento) {
                document.getElementById('wp-loader').style.display = "none";
                document.getElementById("mensajeError").innerHTML = "<p>"+evento.data+"</p>";                
            }, false);
            
            vistaWeb.addEventListener("Exito", function (evento) {
                document.getElementById('wp-loader').style.display = "none";
                document.getElementById("mensajeExito").innerHTML = "<p>"+evento.data+"</p>"; 
            }, false);
            
            vistaWeb.addEventListener("mostrarAlumno", function (evento) {
                document.getElementById("alumno-seleccionado").innerHTML = evento.data;
            }, false); 
            
            vistaWeb.addEventListener("redireccion", function (evento) {
                window.location.replace(evento.data);
            }, false);
            
            vistaWeb.addEventListener("Inasistencias", function (evento) {
                contenido(evento.data);
            }, false);
            
            vistaWeb.addEventListener("Evaluaciones", function (evento) {
                contenido(evento.data);
            }, false);
            
            vistaWeb.addEventListener("mostrarPromediosVacio", function (evento) {
                contenido("<tr><td>"+evento.data+"</td></tr>");
            }, false); 
            
            vistaWeb.addEventListener("mostrarPromedios", function (evento) {
                contenido(evento.data);
            }, false);
            
            vistaWeb.addEventListener("mostrarCalendario", function (evento) {
                contenido(evento.data);
                addeventatc.refresh();
            }, false);
        </script>
        <script type="text/javascript">
            // FUNCIONES - FUNCTIONS    
            function tutorEstudiante(e) {
                e.preventDefault();
                $("#panel-central").show();
                $("#panel-secundario").hide(); 
            }
            
            function panelCentral(e) {
                e.preventDefault();
                $("#panel-secundario").slideUp( "slow", function() {
                    $("#panel-central").slideDown( "slow" );
                    $(".pan-sec").hide();
                });
            }
            
            function panel(e,accion) {
                e.preventDefault();
                $.get("initutest?accion="+accion , function (data){});
            }
            
            function contenido(datos) {
                limpiarmensajes();
                $(".pan-sec").hide();
                $("#tabla-panel-secundario").html(datos);
                $("#tabla-panel-secundario").show();                
                $("#panel-central").slideUp( "slow", function() {
                    $("#panel-secundario").slideDown( "slow" );
                });
            }
            
            function limpiarmensajes(){
                $("#mensajeExito").html("");
                $("#mensajeError").html("");
            }
        </script>
        
        <!-- Calendar button -->
        <script type="text/javascript" src="resources/js/atc.min.js" async defer></script>
        <!-- //Calendar button -->
    </head>
    <body class="tutor-dash tutor-alumno-dash dashboard">
        <%@ include file="parts/header.jsp" %>
        <div class="container-fluid">
            <!-- Mensajes -->
            <div id="mensajeExito"></div>
            <div id="mensajeError"></div>
            
            <div class="row">
                <!-- Alumno -->
                <div class="col-md-2 alumno-seleccionado" id="alumno-seleccionado">

                </div>
                <!-- /Alumno -->
			
                <!-- Dash -->
                <div class="col-md-10" id="panel-central">
                    <a href="inicio-tutor.jsp" id="doc-noti" class="item-dash">
                        <img src="resources/images/back-icon.png" alt="Volver" />
                        <span>Volver</span>
                    </a>
                    <a href="#" onclick="panel(event,'asistencias')" id="doc-noti" class="item-dash">
                        <img src="resources/images/presente.svg" alt="Inasistencias" />
                        <span>Inasistencias</span>
                    </a>
                    <a href="#" onclick="panel(event,'evaluaciones')" id="doc-noti" class="item-dash">
                        <img src="resources/images/evaluacion.png" alt="Resultados de evaluaciones" />
                        <span>Resultados de evaluaciones</span>
                    </a>
                    <a href="#" onclick="panel(event,'calendario')" id="doc-noti" class="item-dash">
                        <img src="resources/images/calendario-evaluacion.png" alt="Calendario de evaluaciones" />
                        <span>Calendario de evaluaciones</span>
                    </a>
                    <a href="#" onclick="panel(event,'promedios')" id="doc-noti" class="item-dash">
                        <img src="resources/images/grados.png" alt="Promedios de materias" />
                        <span>Promedios de materias</span>
                    </a>
                </div>
                <!-- /Dash -->
                
                <!-- Dash -->
                <div class="col-md-10" id="panel-secundario" style="display:none">
                    <a href="#" onclick="panelCentral(event)" style="font-size: 20px;">&larr; Volver </a>
                    <table class="pan-sec table_style_1" id="tabla-panel-secundario">
                        
                    </table>
                </div>
	    </div>
        </div>
        
        <div id="wp-loader" style="display: none;">
            <div class="loader"></div>
        </div>
    </body>
</html>
