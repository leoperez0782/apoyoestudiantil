<%-- 
    Document   : modificar-tutor
    Author     : Polachek
--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Modificar Administrador</title>
        <%@ include file="parts/head.jsp" %>
        <script type="text/javascript">     
            var vistaWeb = new EventSource("modificardocentes?");           
            
            vistaWeb.addEventListener("Error", function (evento) {
                document.getElementById('wp-loader').style.display = "none";
                document.getElementById("mensajeError").innerHTML = "<p>"+evento.data+"</p>";                
            }, false);
            
            vistaWeb.addEventListener("Exito", function (evento) {
                document.getElementById('wp-loader').style.display = "none";
                document.getElementById("mensajeExito").innerHTML = "<p>"+evento.data+"</p>"; 
            }, false);
            
            vistaWeb.addEventListener("mostrarCargos", function (evento){
                document.getElementById("cargo").innerHTML=evento.data;                
            },false);     
                                 
        </script>
        <script type="text/javascript">
            // FUNCIONES - FUNCTIONS            
            function pasoSeleccionado(e,element) {
                e.preventDefault();
                $(".paso_content").hide();
                $( "#pasos .paso" ).removeClass("selected");
                $(element).closest("div").addClass('selected');
                var selectedID = $("#pasos .selected").attr('id');
                switch (selectedID) {
                    case 'pasos_paso_1':
                      $(".paso_1").show();
                      break;
                    case 'pasos_paso_2':
                      $(".paso_2").show();
                      break;
                    case 'pasos_paso_3':
                      $(".paso_3").show();
                      break;
                }
            }
            
            function agregarTelefono(e) {
                e.preventDefault();
                $.get("resources/partials/telefono.html", function (data) {
                    $("#telefonos_wrap").append(data);
                });                
            }
            
            function borrarTelefono(e,element) {
                e.preventDefault();
                $(element).closest("div.telefono_item").remove();
            }
            
            function siguientePaso(e,paso_id) {
                e.preventDefault();
                $( paso_id ).click();
            }
            
            function agregarDireccion(e) {
                e.preventDefault();
                $.get("resources/partials/direccion.html", function (data) {
                    $("#direcciones_wrap").append(data);
                });                
            }
            
            function borrarDireccion(e,element) {
                e.preventDefault();
                $(element).closest("div.direccion_item").remove();
            }
            
            function formModificar(miForm) {
                $("#mensajeExito").html("");
                $("#mensajeError").html("");
                $( "#wp-loader" ).show();
                formLimpiaErrores();
                
                var valida = validarForm(miForm);
                if(!valida){
                    $('#pasos_paso_1').addClass('error');
                    valida = false;
                }
                if($('#form-telefono .telefono_item:first .tel_num').val()===""){
                   $('#pasos_paso_3').addClass('error');
                   valida = false;
                }
                
                if(!valida){
                    $("#mensajeError").html("Error: Por favor revise los campos reslatados en rojo");
                    $( "#wp-loader" ).show(0).delay(300).hide(0);
                }else{
                    var nombre = miForm.nombre.value;
                    var nombre_segundo = miForm.nombre_segundo.value;
                    var apellido = miForm.apellido.value;
                    var apellido_segundo = miForm.apellido_segundo.value;
                    var fechan = miForm.fechan.value;
                    var email = miForm.email.value;
                    var clave = null;
                    var user_estado = miForm.user_estado.options[miForm.user_estado.selectedIndex].value;
                    var cargo = miForm.cargo.options[miForm.cargo.selectedIndex].value;
                    if(miForm.clave.value){
                        clave = miForm.clave.value;   
                    }                                    
 
                    var params = "";
                    params += "nombre="+nombre;
                    params += "&nombre_segundo="+nombre_segundo+"&apellido="+apellido+"&apellido_segundo="+apellido_segundo;
                    params += "&fechan="+fechan+"&email="+email+"&user_estado="+user_estado+"&cargo="+cargo;
                    if(clave !== null){
                        params += "&clave="+clave;
                    }
                    
                    $( "#form-telefono div.telefono_item" ).each(function( index ) {                        
                        var mi_tel_tipo = $(this).find('.tel_tipo option:selected').text();
                        if($(this).find(".tel_num").val()){
                            var mi_tel_num = $(this).find(".tel_num").val();
                        }                      
                        var mi_tel_obs = "";
                        if($(this).find(".tel_obs").val()){
                            mi_tel_obs = $(this).find(".tel_obs").val();
                        }
                        var tel_params = "tipotel="+mi_tel_tipo;
                        tel_params += "&numtel="+mi_tel_num;
                        tel_params += "&obstel="+mi_tel_obs;
                        $.get("modificardocentes?accion=telefono&"+tel_params , function (data){});    
                    });
                    
                    $( "#direcciones_wrap div.direccion_item" ).each(function( index ) {
                        var calle ="";
                        if($(this).find(".calle").val()){
                            calle = $(this).find(".calle").val();
                        } 
                        var esquina ="";
                        if($(this).find(".esquina").val()){
                            esquina = $(this).find(".esquina").val();
                        }
                        var barrio ="";
                        if($(this).find(".barrio").val()){
                            barrio = $(this).find(".barrio").val();
                        }
                        var numero ="";
                        if($(this).find(".numero").val()){
                            numero = $(this).find(".numero").val();
                        }
                        var apto ="";
                        if($(this).find(".apto").val()){
                            apto = $(this).find(".apto").val();
                        }
                        var block ="";
                        if($(this).find(".block").val()){
                            block = $(this).find(".block").val();
                        }
                        var departamento = $(this).find('.departamento option:selected').text();
                        var localidad ="";
                        if($(this).find(".localidad").val()){
                            localidad = $(this).find(".localidad").val();
                        }
                        var manzanasolar ="";
                        if($(this).find(".manzanasolar").val()){
                            manzanasolar = $(this).find(".manzanasolar").val();
                        }
                        var edificio ="";
                        if($(this).find(".edificio").val()){
                            edificio = $(this).find(".edificio").val();
                        }
                        var isTorre ="";
                        if($(this).find(".isTorre").val()){
                            isTorre = $(this).find(".isTorre").val();
                        }
                        
                        var dir_params = "&calle="+calle+"&esquina="+esquina;
                        dir_params += "&barrio="+barrio+"&numero="+numero+"&apto="+apto;
                        dir_params += "&block="+block+"&departamento="+departamento+"&localidad="+localidad;
                        dir_params += "&manzanasolar="+manzanasolar+"&edificio="+edificio+"&isTorre="+isTorre;
                        $.get("modificardocentes?accion=direccion&"+dir_params , function (data){});
                    }); 
                    
                    $.get("modificardocentes?accion=modificar&"+params , function (data){});                    
                }        
            }            
        </script>
    </head>
    <body class="modificar-admin-form">
        <%@ include file="parts/header.jsp" %>
        
        <div class="container-fluid">
            <div id="pasos">
                <div class="paso selected" id="pasos_paso_1">
                    <a href="#" onclick="pasoSeleccionado(event,this)">
                        Paso 1
                        <span class="subtit">Datos personales</span>
                    </a>
                </div>
                <div class="paso" id="pasos_paso_2">
                    <a href="#" onclick="pasoSeleccionado(event,this)">
                        Paso 2
                        <span class="subtit">Direccion</span>
                    </a>
                </div>
                <div class="paso" id="pasos_paso_3">
                    <a href="#" onclick="pasoSeleccionado(event,this)">
                        Paso 3
                        <span class="subtit">Telefono</span>
                    </a>
                </div>
            </div>
            
            <!-- Mensajes -->
            <div id="mensajeExito"></div>
            <div id="mensajeError"></div>
            
            <section id="formulario-alta">
                <form onsubmit="event.preventDefault(); formModificar(this);" class="form-layout-1">
                    <div class="paso_1 paso_content">
                        <fieldset>
                            <label for="user_estado" style="text-align: center;margin-bottom: 20px;">Estado:
                                <select class="input" id="user_estado" style="font-weight:normal;" name="user_estado">
                                <c:if test="${usermod.activo}">
                                    <option value="true" selected>Activo</option>
                                    <option value="false" style="color:red">No Activo</option>
                                </c:if>
                                <c:if test="${!usermod.activo}">
                                    <option value="false" selected style="color:red">No Activo</option>
                                    <option value="true">Activo</option>
                                </c:if>
                            </select>
                            </label>
                                
                            <legend>Datos personales</legend>
                            <div class="half-screen">
                                <label for="cargo">Seleccione el cargo:</label>
                                <select class="form-control" id="cargo" name="cargo"></select>
                                
                                <label for="documento">Documento</label>
                                <input class="input-half" type="text" id="documento" name="documento" readonly disabled value="${usermod.documento.numero}">

                                <label for="ci_emisor">País emisor</label>
                                <input class="input-half" type="text" id="ci_emisor" name="ci_emisor" readonly disabled value="${usermod.documento.paisEmisor}">

                                <label for="ci_tipo">Tipo de documento</label>
                                <select readonly disabled class="input" id="ci_tipo" name="ci_tipo">
                                    <option value="${usermod.documento.tipo}" selected>${usermod.documento.tipo}</option>
                                    <option value="CEDULA">CEDULA</option>
                                    <option value="PASAPORTE">PASAPORTE</option>
                                    <option value="OTRO">OTRO</option>
                                </select>

                                <label for="email">E-mail</label> 
                                <input class="input-half" type="text" id="email" name="email" value="${usermod.email}">

                                <label for="clave">Nueva clave / Dejar en blanco para conservar la anterior</label> 
                                <input class="input-half" type="text" id="clave" name="clave" value="">
                            </div>
                            <div class="half-screen">
                                <label for="nombre">Primer nombre</label>
                                <input class="input-half" type="text" id="nombre" name="nombre" value="${usermod.primerNombre}">

                                <label for="nombre_segundo">Segundo nombre</label>
                                <input class="input-half" type="text" id="nombre_segundo" name="nombre_segundo" value="${usermod.segundoNombre}">

                                <label for="apellido">Primer apellido</label>
                                    <input class="input-half" type="text" id="apellido" name="apellido" value="${usermod.primerApellido}">

                                <label for="apellido_segundo">Segundo apellido</label> 
                                <input class="input-half" type="text" id="apellido_segundo" name="apellido_segundo" value="${usermod.segundoApellido}">

                                <label for="fechan">Fecha de nacimiento</label> 
                                <input class="input-half" type="date" id="fechan" name="fechan" value="${usermod.fechaNacimiento}">
                            </div>
                            <div class="clear"></div>
                        </fieldset>
                        <div class="paso_siguiente_cont">
                            <a href="#" class="paso_siguiente" onclick="siguientePaso(event,'#pasos #pasos_paso_2 a')">Siguiente</a>
                        </div>
                    </div>
                    <div class="paso_2 paso_content" style="display: none;">
                        <fieldset id="direcciones_wrap">
                            <legend>Direcciones  - <a href="#" class="agregar_item" onclick="agregarDireccion(event)">Agregar nueva dirección</a></legend>
                            <c:forEach items="${usermod.domicilios}" var="dir" varStatus="loop">
                                <div class="direccion_item">
                                    <h1 class="direccion-title">Direccion <c:out value="${loop.count}"/></h1>
                                    <div class="half-screen">
                                        <label>Calle
                                            <input class="input-half calle" type="text" value="<c:out value="${dir.calle}"/>">
                                        </label>

                                        <label>Esquina
                                            <input class="input-half esquina" type="text" value="<c:out value="${dir.esquina}"/>">
                                        </label> 

                                        <label>Barrio
                                            <input class="input-half barrio" type="text" value="<c:out value="${dir.barrio}"/>">
                                        </label>

                                        <label>Numero de puerta
                                            <input class="input-half numero" type="text" value="<c:out value="${dir.numero}"/>">
                                        </label>

                                        <label>Nº de Apartamento si corresponde
                                            <input class="input-half apto" type="text" value="<c:out value="${dir.apartamento}"/>">
                                        </label>

                                        <label>Torre
                                            <input class="input-half isTorre" type="text" value="<c:out value="${dir.torre}"/>">
                                        </label>
                                    </div>
                                    <div class="half-screen">
                                        <label>Block
                                            <input class="input-half block" type="text" value="<c:out value="${dir.block}"/>">
                                        </label>

                                        <label>Departamento</label>
                                        <select class="input departamento">
                                            <option value="<c:out value="${dir.departamento}"/>" selected="selected"><c:out value="${dir.departamento}"/></option>
                                            <option value="MONTEVIDEO">MONTEVIDEO</option>
                                            <option value="ARTIGAS">ARTIGAS</option>
                                            <option value="CANELONES">CANELONES</option>
                                            <option value="CERRO_LARGO">CERRO_LARGO</option>
                                            <option value="COLONIA">COLONIA</option>
                                            <option value="DURAZNO">DURAZNO</option>
                                            <option value="FLORES">FLORES</option>
                                            <option value="FLORIDA">FLORIDA</option>
                                            <option value="LAVALLEJA">LAVALLEJA</option>
                                            <option value="MALDONADO">MALDONADO</option>
                                            <option value="PAYSANDU">PAYSANDU</option>
                                            <option value="RIO_NEGRO">RIO_NEGRO</option>
                                            <option value="RIVERA">RIVERA</option>
                                            <option value="ROCHA">ROCHA</option>
                                            <option value="SALTO">SALTO</option>
                                            <option value="SAN_JOSE">SAN_JOSE</option>
                                            <option value="SORIANO">SORIANO</option>
                                            <option value="TACUAREMBO">TACUAREMBO</option>
                                            <option value="TREINTA_Y_TRES">TREINTA_Y_TRES</option>
                                        </select>

                                        <label>Edificio
                                            <input class="input-half edificio" type="text" value="<c:out value="${dir.edificio}"/>">
                                        </label>

                                        <label>Localidad
                                            <input class="input-half localidad" type="text" value="<c:out value="${dir.localidad}"/>">
                                        </label>

                                        <label>Manzana Solar
                                            <input class="input-half manzanasolar" type="text" value="<c:out value="${dir.manzanaSolar}"/>">
                                        </label>
                                    </div>
                                    <div class="clear"></div>
                                    <c:if test="${loop.count != 1}">  
                                        <a href="#" class="borrar_direccion borrar_item_partial clear" onclick="borrarDireccion(event,this)">Borrar Dirección</a>
                                    </c:if> 
                                </div>                               
                            </c:forEach>
                        </fieldset>
                        <div class="paso_siguiente_cont">
                            <a href="#" class="paso_siguiente" onclick="siguientePaso(event,'#pasos #pasos_paso_3 a')">Siguiente</a>
                        </div>
                    </div>
                    <div id="form-telefono" class="paso_3 paso_content" style="display: none;">
                        <fieldset id="telefonos_wrap">
                            <legend>Telefonos - <a href="#" class="agregar_item" onclick="agregarTelefono(event)">Agregar nuevo telefono</a></legend>
                            <c:forEach items="${usermod.telefonos}" var="tel" varStatus="loop">
                                <div class="telefono_item">
                                    <div class="innner_content">
                                        <label>Tipo de telefono</label>
                                        <select class="input tel_tipo">
                                            <option value="<c:out value="${tel.tipo}"/>" selected><c:out value="${tel.tipo}"/></option>
                                            <option value="MOVIL">MOVIL</option>
                                            <option value="FIJO">FIJO</option>
                                        </select>
                                        <label>Nº de telefono</label>
                                        <input class="input-half tel_num" type="text" value="<c:out value="${tel.numero}"/>"/>
                                        <label>Observaciones</label>
                                        <input class="input-half tel_obs" type="text" value="<c:out value="${tel.observaciones}"/>"/>
                                        <c:if test="${loop.count != 1}">  
                                            <a href="#" class="borrar_telefono" onclick="borrarTelefono(event,this)">Borrar telefono</a>
                                        </c:if>                                        
                                    </div>
                                </div>
                            </c:forEach>
                        </fieldset>
                        <div class="clear"></div>
                        <div class="submit_form">
                            <input type="submit" id="submit" value="Modificar">
                        </div>
                    </div>
                    <p class="login-error">
                    </p>
                </form>
            </section>
            
        </div>
        <div id="wp-loader" style="display: none;">
            <div class="loader"></div>
        </div>
    </body>
</html>
